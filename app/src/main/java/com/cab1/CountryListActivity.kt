package com.cab1

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cab1.adapter.CountrylistAdapter
import com.cab1.pojo.Data
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.activity_country_list.*
import kotlinx.android.synthetic.main.toolbar.*


class CountryListActivity : AppCompatActivity() {
    private lateinit var countrylistAdapter: CountrylistAdapter
    private var countrylist = ArrayList<Data>()

    var countryname: String = ""
    var countryID: String = ""
    var countryCode: String = ""
    var countryFlagImage: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_list)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        tvVerifcationTitle.text = "Country"
//        img_Logout.visibility = View.GONE
//        img_notification.visibility = View.GONE
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        if (intent != null) {
            if (intent.hasExtra("countrylist")) {
                countrylist = intent.getSerializableExtra("countrylist") as ArrayList<Data>
            }
        }



        country_RecycleView.layoutManager = LinearLayoutManager(this@CountryListActivity, RecyclerView.VERTICAL, false)

        countrylistAdapter = CountrylistAdapter(
            this@CountryListActivity,
            countrylist,
            object : CountrylistAdapter.OnItemClick {

                override fun onClicklisneter(pos: Int, countrylist: Data) {
                    countryname = countrylist.countryName
                    countryID = countrylist.countryID
                    countryCode = countrylist.countryDialCode
                    countryFlagImage = countrylist.countryFlagImage
                    var intent = Intent()
                    intent.putExtra("countryname", countryname)
                    intent.putExtra("countryID", countryID)
                    intent.putExtra("countryCode", countryCode)
                    intent.putExtra("countryFlagImage", countryFlagImage)
                    setResult(Activity.RESULT_OK, intent)
                   // onBackPressed()

                }

            })
        country_RecycleView.adapter = countrylistAdapter
        countrylistAdapter.notifyDataSetChanged()

    }

    override fun onBackPressed() {

        MyUtils.finishActivity(this@CountryListActivity, true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.header_menu, menu)

        val notificationTollbarMenu = menu!!.findItem(R.id.notificationTollbarMenu)
        notificationTollbarMenu.setIcon(R.drawable.search_icon_location)

        /*val searchView = notificationTollbarMenu.actionView as android.widget.SearchView

        *//*searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {

                return true
            }


            override fun onQueryTextChange(newText: String?): Boolean {

                return true
            }
        })*/
        return true
    }

}
