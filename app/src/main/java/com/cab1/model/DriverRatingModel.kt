package com.cab1.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.DriverRatingPojo
import retrofit2.Call
import retrofit2.Response

class DriverRatingModel : ViewModel() {
    lateinit var mContext: Context

    var json: String = ""

    fun driverRating(
        context: Context,
        json: String
    ): LiveData<List<DriverRatingPojo>>
    {

        this.mContext = context


        this.json = json


        val data = MutableLiveData<List<DriverRatingPojo>>()



        var call = RestClient.get()!!.driverRating(json)
        call.enqueue(object : retrofit2.Callback<List<DriverRatingPojo>> {
            override fun onFailure(call: Call<List<DriverRatingPojo>>, t: Throwable) {

                data.value = null
            }

            override fun onResponse(call: Call<List<DriverRatingPojo>>, response: Response<List<DriverRatingPojo>>) {

                data.value = response.body()


            }
        })



        return data
    }




}