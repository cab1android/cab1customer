package com.cab1.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.GetMyBookingsPojo
import retrofit2.Response

class GetMyBookingsModel : ViewModel(){

    lateinit var languageresponse: LiveData<List<GetMyBookingsPojo>?>
    lateinit var mContext: Context
    lateinit var pbDialog: Dialog
    var json: String = ""

    fun getMyBookings(
        context: Context,
        json: String
    ): LiveData<List<GetMyBookingsPojo>?> {

        this.mContext = context

        this.json = json
        languageresponse = getMyBookingsApi()

        return languageresponse;
    }

    private fun getMyBookingsApi(): LiveData<List<GetMyBookingsPojo>?> {
        var data = MutableLiveData<List<GetMyBookingsPojo>?>()


        var call = RestClient.get()!!.getMyBookingData(json.toString())
        call.enqueue(object : RestCallback<List<GetMyBookingsPojo>>(mContext) {
            override fun Success(response: Response<List<GetMyBookingsPojo>>) {
                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }
        })


        return data
    }


}