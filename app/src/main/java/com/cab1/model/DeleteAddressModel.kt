package com.cab1.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.CommonPojo
import retrofit2.Call
import retrofit2.Response

class DeleteAddressModel : ViewModel() {


    lateinit var languageresponse: LiveData<List<CommonPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    lateinit var pbDialog: Dialog
    var searchkeyword: String = ""
    var json: String = ""

    fun deleteAddress(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<CommonPojo>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = deleteAddressApi()

        return languageresponse;
    }

    private fun deleteAddressApi(): LiveData<List<CommonPojo>> {
        val data = MutableLiveData<List<CommonPojo>>()


        var call = RestClient.get()!!.deleteAddress(json.toString())
        call.enqueue(object : retrofit2.Callback<List<CommonPojo>> {
            override fun onFailure(call: Call<List<CommonPojo>>, t: Throwable) {

                data.value = null
            }

            override fun onResponse(call: Call<List<CommonPojo>>, response: Response<List<CommonPojo>>) {

                data.value = response.body()

            }
        })


        return data
    }


}