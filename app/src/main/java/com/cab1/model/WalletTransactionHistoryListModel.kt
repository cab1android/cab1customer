package com.cab1.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.WalletTransactionPojo
import retrofit2.Response

class WalletTransactionHistoryListModel : ViewModel()
{

    lateinit var walletTransactionPojo: LiveData<List<WalletTransactionPojo>?>
    lateinit var mContext: Context

    var isShowing:Boolean = false

    var json: String = ""

    var searchkeyword: String = ""

    fun  getWalletHistory(
        context: Context,

        json: String): LiveData<List<WalletTransactionPojo>?> {
        this.json = json

        this.mContext = context


        walletTransactionPojo = getApiResponse()

        return walletTransactionPojo
    }

    private fun getApiResponse(): LiveData<List<WalletTransactionPojo>?> {
        val data = MutableLiveData<List<WalletTransactionPojo>>()





        var call = RestClient.get()!!.getWalletTransactionHistory(json)
        call.enqueue(object : RestCallback<List<WalletTransactionPojo>?>(mContext) {
            override fun Success(response: Response<List<WalletTransactionPojo>?>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }




}


