package com.cab1.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.GetDriverLocation
import retrofit2.Call
import retrofit2.Response

class GetDriverLocationModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<GetDriverLocation>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    lateinit var pbDialog: Dialog
    var searchkeyword: String = ""
    var json: String = ""

    fun getDriverLocation(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<GetDriverLocation>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = getDriverLocationApi()

        return languageresponse;
    }

    private fun getDriverLocationApi(): LiveData<List<GetDriverLocation>> {
        val data = MutableLiveData<List<GetDriverLocation>>()


        var call = RestClient.get()!!.getDriverLocation(json.toString())
        call.enqueue(object : retrofit2.Callback<List<GetDriverLocation>> {
            override fun onFailure(call: Call<List<GetDriverLocation>>, t: Throwable) {

                data.value = null
            }

            override fun onResponse(call: Call<List<GetDriverLocation>>, response: Response<List<GetDriverLocation>>) {

                data.value = response.body()

            }
        })


        return data
    }


}