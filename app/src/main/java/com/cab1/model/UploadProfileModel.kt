package com.cab1.model


import android.app.Activity
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.LoginPojo
import com.cab1.util.MyUtils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File


class UploadProfileModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<LoginPojo>>
    lateinit var mContext: Activity
    var isShowing: Boolean = false


    var folderName: String = ""
    var json: String = ""
    var file: File? = null

    fun uploadFile(
        context: Activity,
        isShowing: Boolean,
        json: String, folderName: String, file: File
    ): LiveData<List<LoginPojo>> {

        this.mContext = context
        this.isShowing = isShowing
        this.folderName = folderName
        this.file = file

        this.json = json
        languageresponse = UploadProfile()

        return languageresponse
    }

    private fun UploadProfile(): LiveData<List<LoginPojo>> {
        if (isShowing)
            MyUtils.showProgressDialog(mContext, "Uploading")

        val filePart = MultipartBody.Part.createFormData(
            "FileField",
            file?.name,
            RequestBody.create(okhttp3.MediaType.parse("image*//*"), file)
        )

        val data = MutableLiveData<List<LoginPojo>>()


        val call = RestClient.get()!!.uploadAttachment(
            filePart,
            RequestBody.create(MediaType.parse("text/plain"), folderName),
            RequestBody.create(MediaType.parse("text/plain"), json)
        )
        call.enqueue(object : retrofit2.Callback<List<LoginPojo>> {
            override fun onFailure(call: Call<List<LoginPojo>>, t: Throwable) {
                if (isShowing)
                    MyUtils.dismissProgressDialog()

                data.value = null
            }

            override fun onResponse(call: Call<List<LoginPojo>>, response: Response<List<LoginPojo>>) {
                if (isShowing)
                    MyUtils.dismissProgressDialog()

                data.value = response.body()


            }
        })



        return data
    }


}


