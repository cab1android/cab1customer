package com.cab1.model

import android.app.Dialog
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.AddAddressPojo
import retrofit2.Call
import retrofit2.Response

class AddAddressModel : ViewModel() {


    lateinit var languageresponse: LiveData<List<AddAddressPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    lateinit var pbDialog: Dialog
    var searchkeyword: String = ""
    var json: String = ""

    fun addAddress(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<AddAddressPojo>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = addAddressApi()

        return languageresponse;
    }

    private fun addAddressApi(): LiveData<List<AddAddressPojo>> {
        val data = MutableLiveData<List<AddAddressPojo>>()
        var call = RestClient.get()!!.addAddress(json.toString())
        call.enqueue(object : retrofit2.Callback<List<AddAddressPojo>> {
            override fun onFailure(call: Call<List<AddAddressPojo>>, t: Throwable) {
                data.value = null
            }

            override fun onResponse(call: Call<List<AddAddressPojo>>, response: Response<List<AddAddressPojo>>) {
                data.value = response.body()
            }
        })
        return data
    }
}