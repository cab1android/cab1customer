package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.CarsCategoryPojo
import retrofit2.Call
import retrofit2.Response

class CarscategoryModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var json: String = ""


    fun getCars(context: Context,json: String): LiveData<List<CarsCategoryPojo>>
    {

        val data = MutableLiveData<List<CarsCategoryPojo>>()
        var call: Call<List<CarsCategoryPojo>>?=null
        call = RestClient.get()!!.getCarList(json)

        call!!.enqueue(object : RestCallback<List<CarsCategoryPojo>> (context){
            override fun Success(response: Response<List<CarsCategoryPojo>>) {

                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }


        })



        return data
    }




}


