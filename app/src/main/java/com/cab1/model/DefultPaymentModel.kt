package com.cab1.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.CommonPojo
import retrofit2.Response

class DefultPaymentModel : ViewModel() {

    lateinit var commonPojo: LiveData<List<CommonPojo>?>
    lateinit var mContext: Context

    var isShowing:Boolean = false
    var json: String = ""



    fun  getDefaultPayment(
        context: Context,  json: String): LiveData<List<CommonPojo>?> {
        this.json = json

        this.mContext = context


        commonPojo = getApiResponse()

        return commonPojo
    }

    private fun getApiResponse(): LiveData<List<CommonPojo>?> {
        val data = MutableLiveData<List<CommonPojo>>()
        var call = RestClient.get()!!.defaultPayment(json.toString())
        call.enqueue(object : RestCallback<List<CommonPojo>?>(mContext) {
            override fun Success(response: Response<List<CommonPojo>?>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })


        return data
    }


}