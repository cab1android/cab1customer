package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.CommonPojo
import retrofit2.Call
import retrofit2.Response

class CancelTiripModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""
    var type: String = ""

    fun cancelTrip(
        context: Context,
        json: String
      ): LiveData<List<CommonPojo>>
    {



        val data = MutableLiveData<List<CommonPojo>>()

        var call: Call<List<CommonPojo>>?=null

         call = RestClient.get()!!.getCancelTrip(json)


        call!!.enqueue(object : RestCallback<List<CommonPojo>> (context){
            override fun Success(response: Response<List<CommonPojo>>) {

                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }


        })



        return data
    }




}


