package com.cab1.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.LoginPojo
import retrofit2.Call
import retrofit2.Response

class OtpVerifyModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<LoginPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    lateinit var pbDialog: Dialog
    var searchkeyword: String = ""
    var json: String = ""

    fun otpVerify(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<LoginPojo>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = otpVerifyApi()

        return languageresponse;
    }

    private fun otpVerifyApi(): LiveData<List<LoginPojo>> {
        val data = MutableLiveData<List<LoginPojo>>()


        var call = RestClient.get()!!.otpVerifyCustomer(json.toString())
        call.enqueue(object : retrofit2.Callback<List<LoginPojo>> {
            override fun onFailure(call: Call<List<LoginPojo>>, t: Throwable) {

                data.value = null
            }

            override fun onResponse(call: Call<List<LoginPojo>>, response: Response<List<LoginPojo>>) {

                data.value = response.body()


            }
        })


        return data
    }

}