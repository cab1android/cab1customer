package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.CouponofferPojo
import retrofit2.Call
import retrofit2.Response

class CouponofferModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var json: String = ""


    fun getcouponoffer(context: Context,json: String): LiveData<List<CouponofferPojo>>
    {

        val data = MutableLiveData<List<CouponofferPojo>>()
        var call: Call<List<CouponofferPojo>>?=null
        call = RestClient.get()!!.couponoffer(json)

        call!!.enqueue(object : RestCallback<List<CouponofferPojo>> (context){
            override fun Success(response: Response<List<CouponofferPojo>>) {

                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }


        })



        return data
    }




}


