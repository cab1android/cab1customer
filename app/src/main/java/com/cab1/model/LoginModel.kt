package com.cab1.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.LoginPojo
import retrofit2.Call
import retrofit2.Response

class LoginModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<LoginPojo>>
    lateinit var mContext: Context
    var isLoginApi: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""


    fun getLogin(
        context: Context,
        isCustomerDetails: Boolean,
        json: String
    ): LiveData<List<LoginPojo>> {

        this.mContext = context
        this.isLoginApi = isCustomerDetails
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = getLoginApi()

        return languageresponse
    }

    private fun getLoginApi(): LiveData<List<LoginPojo>> {
        val data = MutableLiveData<List<LoginPojo>>()
        var call = RestClient.get()!!.getLoginCustomer(json)
          if(!isLoginApi)
         call = RestClient.get()!!.getCustomerDetails(json)

        call.enqueue(object : retrofit2.Callback<List<LoginPojo>> {
            override fun onFailure(call: Call<List<LoginPojo>>, t: Throwable) {

                data.value = null
            }

            override fun onResponse(call: Call<List<LoginPojo>>, response: Response<List<LoginPojo>>) {

                data.value = response.body()


            }
        })


        return data
    }

}