package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.CitylistPojo
import retrofit2.Call
import retrofit2.Response

class CityListModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var json: String = ""


    fun getCity(context: Context,json: String): LiveData<List<CitylistPojo>>
    {

        val data = MutableLiveData<List<CitylistPojo>>()
        var call: Call<List<CitylistPojo>>?=null
        call = RestClient.get()!!.getCityList(json)

        call!!.enqueue(object : RestCallback<List<CitylistPojo>> (context){
            override fun Success(response: Response<List<CitylistPojo>>) {

                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }


        })



        return data
    }




}


