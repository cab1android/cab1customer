package com.cab1.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.CmsPagePojo
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response

class CmsPageModel : ViewModel()
{

    lateinit var walletTransactionPojo: LiveData<List<CmsPagePojo>?>
    lateinit var mContext: Context

    var isShowing:Boolean = false

    var userID: String = ""
    var cmspageConstantCode: String = ""

    var searchkeyword: String = ""

    fun  getCmsPage(
        context: Context,userID: String,cmspageConstantCode: String): LiveData<List<CmsPagePojo>?> {
        this.userID = userID
        this.cmspageConstantCode = cmspageConstantCode

        this.mContext = context


        walletTransactionPojo = getApiResponse()

        return walletTransactionPojo
    }

    private fun getApiResponse(): LiveData<List<CmsPagePojo>?> {
        val data = MutableLiveData<List<CmsPagePojo>>()



        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID",userID)
            jsonObject.put("logindriverID", "0")
            jsonObject.put("cmspageConstantCode", cmspageConstantCode)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        var call = RestClient.get()!!.cmsPage(jsonArray.toString())
        call.enqueue(object : RestCallback<List<CmsPagePojo>?>(mContext) {
            override fun Success(response: Response<List<CmsPagePojo>?>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }




}