package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.FareCardPojo
import retrofit2.Call
import retrofit2.Response

class FarcardModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var json: String = ""


    fun getFarelist(context: Context,json: String): LiveData<List<FareCardPojo>>
    {

        val data = MutableLiveData<List<FareCardPojo>>()
        var call: Call<List<FareCardPojo>>?=null
        call = RestClient.get()!!.getFareCard(json)

        call!!.enqueue(object : RestCallback<List<FareCardPojo>> (context){
            override fun Success(response: Response<List<FareCardPojo>>) {

                data.value = response.body()
            }

            override fun failure()
            {
                data.value = null
            }
       })

        return data
    }




}


