package com.cab1.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.GetPricePojo
import retrofit2.Call
import retrofit2.Response

class GetPriceModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<GetPricePojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""

    fun getPrice(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<GetPricePojo>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = getPriceApi()

        return languageresponse;
    }

    private fun getPriceApi(): LiveData<List<GetPricePojo>> {
        val data = MutableLiveData<List<GetPricePojo>>()

        var call = RestClient.get()!!.getPriceApi(json)
        call.enqueue(object : retrofit2.Callback<List<GetPricePojo>> {
            override fun onFailure(call: Call<List<GetPricePojo>>, t: Throwable) {

                data.value = null
            }

            override fun onResponse(call: Call<List<GetPricePojo>>, response: Response<List<GetPricePojo>>) {

                data.value = response.body()

            }
        })


        return data
    }

}