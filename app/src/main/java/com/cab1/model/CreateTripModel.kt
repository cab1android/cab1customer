package com.cab1.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.CreateTripPojo
import retrofit2.Response

class CreateTripModel : ViewModel() {
    lateinit var languageresponse: LiveData<List<CreateTripPojo>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    lateinit var pbDialog: Dialog
    var searchkeyword: String = ""
    var json: String = ""

    fun createTrip(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<CreateTripPojo>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = createTripApi()

        return languageresponse;
    }

    private fun createTripApi(): LiveData<List<CreateTripPojo>> {
        val data = MutableLiveData<List<CreateTripPojo>>()


        var call = RestClient.get()!!.createTrip(json.toString())
        call.enqueue(object : RestCallback<List<CreateTripPojo>>(mContext) {
            override fun Success(response: Response<List<CreateTripPojo>>) {
                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }
        })


        return data
    }

}