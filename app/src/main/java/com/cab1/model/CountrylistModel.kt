package com.cab1.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestClient
import com.cab1.pojo.Countrylist
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class CountrylistModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<Countrylist>>
    lateinit var mContext: Context

    var isShowing: Boolean = false
    lateinit var pbDialog: Dialog

    var searchkeyword: String = ""

    fun getCountry(
        context: Context,
        isShowing: Boolean
    ): LiveData<List<Countrylist>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        languageresponse = getCountryApi()

        return languageresponse;
    }

    private fun getCountryApi(): LiveData<List<Countrylist>> {
        val data = MutableLiveData<List<Countrylist>>()

        if (isShowing) {
            showPb()
        }
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)


        var call = RestClient.get()!!.getCountryList(jsonArray.toString())
        call.enqueue(object : retrofit2.Callback<List<Countrylist>> {
            override fun onFailure(call: Call<List<Countrylist>>, t: Throwable) {
                // closePb()
                data.value = null
            }

            override fun onResponse(call: Call<List<Countrylist>>, response: Response<List<Countrylist>>) {
                // closePb()
                data.value = response.body()


            }
        })



        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext)
        pbDialog.show()

    }


}


