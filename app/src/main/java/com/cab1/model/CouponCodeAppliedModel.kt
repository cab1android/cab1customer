package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.pojo.CouponCodeAppliedPojo
import retrofit2.Call
import retrofit2.Response

class CouponCodeAppliedModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""
    var type: String = ""

    fun getCouponCodeApplied(
        context: Context,
        json: String
      ): LiveData<List<CouponCodeAppliedPojo>>
    {



        val data = MutableLiveData<List<CouponCodeAppliedPojo>>()

        var call: Call<List<CouponCodeAppliedPojo>>?=null

         call = RestClient.get()!!.getCouponCardApplied(json)


        call!!.enqueue(object : RestCallback<List<CouponCodeAppliedPojo>> (context){
            override fun Success(response: Response<List<CouponCodeAppliedPojo>>) {

                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }


        })



        return data
    }




}


