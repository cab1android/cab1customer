package com.cab1

import android.animation.AnimatorSet
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.cab1.api.RestClient
import com.cab1.fragment.*
import com.cab1.iterfaces.NavigationHost
import com.cab1.notification.Push
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.navigation_header.view.*


class MainActivity : AppCompatActivity(), NavigationHost {


    lateinit var sessionManager: SessionManager

    private var doubleBackToExitPressedOnce = false
    private var snackBarParent: View? = null
    private val animatorSet = AnimatorSet()
    var push: Push? = null
    var fromBookingStatus: String=""
    var userLong: Double? = null
    var userLat: Double? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sessionManager = SessionManager(this@MainActivity)
        if (intent != null) {
            if (intent.hasExtra("push")) {
                push = intent.getSerializableExtra("push") as Push
                fromBookingStatus = intent.getStringExtra("fromBookingStatus")

            }
        }
        addViewSnackBar()



        if (savedInstanceState == null) {

            var mainFragment = MainFragment()
            if (push != null) {
                var bundle = Bundle()

                bundle.putSerializable("push", push)
                bundle.putSerializable("fromBookingStatus", fromBookingStatus)

                mainFragment.arguments = bundle
            }



            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, mainFragment,MainFragment::class.java.name)
                .addToBackStack(null)
                .commit()
        }
        val navigationViewHeaderView = navigation.getHeaderView(0)
        navigationViewHeaderView.ll_profile_navigation.setOnClickListener {
            mDrawerLayout.closeDrawers()
            if (getCurrentFragment() !is MyProfileFragment)
                navigateTo(MyProfileFragment(), true)
        }


        if (sessionManager.isLoggedIn()) {

            if (sessionManager.get_Authenticate_User().userFullName.length == 0) {

                navigationViewHeaderView.nameHeader_Tv.text = "View Profile"
            } else {
                navigationViewHeaderView.nameHeader_Tv.text = sessionManager.get_Authenticate_User().userFullName

            }

            navigationViewHeaderView.phoneHeader_Tv.text =
                sessionManager.get_Authenticate_User().userCountryCode + " " + sessionManager.get_Authenticate_User().userMobile

            val imageUri =
                Uri.parse(RestClient.image_customer_url + sessionManager.get_Authenticate_User().userProfilePicture)

            navigationViewHeaderView.profilePicture.setImageURI(imageUri)

        }
        navigation.setNavigationItemSelectedListener {
            // set item as selected to persist highlight
            it.isChecked = false
            // close drawer when item is tapped
            mDrawerLayout.closeDrawers()
            when (it.itemId) {
                R.id.bookMyTrip_Menu -> {
                    if (getCurrentFragment() !is MainFragment)
                        navigateTo(MainFragment(), true)
                }
                R.id.myBooking_Menu -> {
                    if (getCurrentFragment() !is MyBookingFragment)
                        navigateTo(MyBookingFragment(), true)
                }
                R.id.fareCard_Menu -> {
                    var fareCardFragment=FareCardFragment()
                    var bundle=Bundle()

                    if(userLong!=null)
                        bundle.putDouble("cityLong",userLong!!)
                    if(userLat!=null)
                        bundle.putDouble("cityLat",userLat!!)
                    if (getCurrentFragment() !is FareCardFragment)
                        fareCardFragment.arguments=bundle
                       navigateTo(fareCardFragment, true)
                }
                R.id.wallet_Menu -> {

                    if (getCurrentFragment() !is WalletFragment)
                        navigateTo(WalletFragment(), true)

                }
                R.id.payment_Menu -> {
                    if (getCurrentFragment() !is PaymentFragment)
                        navigateTo(PaymentFragment(), true)
                }
                R.id.notification_Menu -> {
                    if (getCurrentFragment() !is NotificationFragment)
                        navigateTo(NotificationFragment(), true)
                }
                R.id.support_Menu -> {
                    if (getCurrentFragment() !is SupportFragment)
                        navigateTo(SupportFragment(), true)
                }
                R.id.about_Menu -> {
                    if (getCurrentFragment() !is AboutUsFragment)
                        navigateTo(AboutUsFragment(), true)
                }
                R.id.logout_Menu -> {
                    MyUtils.showMessageOKCancel(this@MainActivity, "Are you sure you want to Logout?", "Logout",
                        DialogInterface.OnClickListener { dialog, which ->
                            dialog.dismiss()
                            logOut()
                        })
                }
                R.id.ll_profile_navigation -> {
                    if (getCurrentFragment() !is MyProfileFragment)
                        navigateTo(MyProfileFragment(), true)
                }
                R.id.referandearn_Menu -> {
                    if (getCurrentFragment() !is ReferandearnFragment)
                        navigateTo(ReferandearnFragment(), true)
                }


            }
            false
        }
    }


    private fun logOut() {

        sessionManager.clear_login_session()


        MyUtils.startActivity(
            this@MainActivity,
            SplashActivity::class.java,
            true
        )
    }


    override fun navigateTo(fragment: Fragment, addToBackstack: Boolean) {


        val transaction = supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_left,
                R.anim.slide_out_right,
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
            .replace(R.id.container, fragment,fragment::class.java.name)

        if (addToBackstack) {
            transaction.addToBackStack(null)
        }

        transaction.commitAllowingStateLoss()

    }

    private fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.container)
    }

    fun openDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START)
    }

    fun showSnackBar(message: String) {
        if ((snackBarParent != null) and !isFinishing)
            Snackbar.make(this.snackBarParent!!, message, Snackbar.LENGTH_LONG).show()

    }

    private fun addViewSnackBar() {
        snackBarParent = View(this)
        val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 200)
        layoutParams.gravity = Gravity.BOTTOM
        snackBarParent!!.layoutParams = layoutParams
        container.addView(snackBarParent)
    }


    fun drawerSwipe(swipe: Boolean) {

        if (swipe) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }

    }

    private fun showexit() {
        //Store Cart Data before exit app

        if (doubleBackToExitPressedOnce) {

            finishAffinity()
            return
        }

        doubleBackToExitPressedOnce = true


        showSnackBar("To exit, press back again.")

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 3000)

    }

    override fun onBackPressed() {

        if (getCurrentFragment() == null) {
            super.onBackPressed()

        } else if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers()
        } else if (getCurrentFragment() is MainFragment) {

           if((getCurrentFragment() as MainFragment).isBackValid())
           showexit()


        } else if (getCurrentFragment() is PaymentPopupFragment) {

            (getCurrentFragment() as PaymentPopupFragment).openRatingActivity()

        } else {
            if (supportFragmentManager.backStackEntryCount >= 1) {

                val f = supportFragmentManager.findFragmentById(R.id.container)
                if (supportFragmentManager.backStackEntryCount == 1)
                {
                    if (f != null && f is MainFragment) {
                        showexit()
                    } else {
                        super.onBackPressed()
                }

                } else
                    supportFragmentManager.popBackStack()
            } else {
                showexit()
            }
        }
    }

    public fun onUpdate() {

        val navigationViewHeaderView = navigation.getHeaderView(0)

        if (sessionManager.isLoggedIn()) {

            if (sessionManager.get_Authenticate_User().userFullName.length == 0) {

                navigationViewHeaderView.nameHeader_Tv.text = "View Profile"
            } else {
                navigationViewHeaderView.nameHeader_Tv.text = sessionManager.get_Authenticate_User().userFullName
            }

            navigationViewHeaderView.phoneHeader_Tv.text =
                sessionManager.get_Authenticate_User().userCountryCode + " " + sessionManager.get_Authenticate_User().userMobile

            val imageUri =
                Uri.parse(RestClient.image_customer_url + sessionManager.get_Authenticate_User().userProfilePicture)

            navigationViewHeaderView.profilePicture.setImageURI(imageUri)

        }
    }

    fun animation(view: View) {

        view.animation = AnimationUtils.loadAnimation(
            applicationContext,
            R.anim.abc_slide_in_bottom
        )

    }

    /* override fun onCreateOptionsMenu(menu: Menu?): Boolean {
         menuInflater.inflate(R.menu.header_menu, menu)

         val notificationTollbarMenu=menu?.findItem(R.id.notificationTollbarMenu)
         notificationTollbarMenu!!.setOnMenuItemClickListener {
             if (getCurrentFragment() !is AboutUsFragment)
                 navigateTo(AboutUsFragment(), true)
 true
         }
         return super.onCreateOptionsMenu(menu)
     }*/


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (getCurrentFragment() is MainFragment) {
            getCurrentFragment()!!.onActivityResult(requestCode, resultCode, data)

        }
        if(getCurrentFragment() is MyBookingFragment) {
            getCurrentFragment()!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun errorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(this@MainActivity)) {
                showSnackBar(resources.getString(R.string.error_common_network))
            } else {
                showSnackBar(resources.getString(R.string.error_crash_error_message))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}
