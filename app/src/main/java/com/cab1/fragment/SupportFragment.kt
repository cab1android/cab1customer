package com.cab1.fragment


import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.model.CmsPageModel
import com.cab1.pojo.CmsPageData
import com.cab1.pojo.CmsPagePojo
import com.cab1.pojo.LoginData
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.fragment_support.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import java.io.Serializable

class SupportFragment : Fragment() , View.OnClickListener{

    private var v:View?=null
    lateinit var cmsPageModel: CmsPageModel
    lateinit var sessionManager: SessionManager
    lateinit var userData: LoginData
    private val MY_PERMISSIONS_REQUEST_CALL_PHONE = 501
    var phoneNumber = ""
    private var cmsPageData = ArrayList<CmsPageData>()


    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if(v == null){
            v = inflater.inflate(R.layout.fragment_support, container, false)
        }

        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationIcon(R.drawable.menu_icon)
        tvVerifcationTitle.text = "Support"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }
        sessionManager= SessionManager(activity!!)
        userData=sessionManager.get_Authenticate_User()

        cmsPageModel  = ViewModelProviders.of(this).get(CmsPageModel::class.java)


        getCmsPage()
        btnRetry.setOnClickListener {  getCmsPage() }
        cardViewFaq!!.setOnClickListener (this)
        emailLinearLayout!!.setOnClickListener (this)
        CallLinearLayout!!.setOnClickListener (this)

    }


    fun getCmsPage(){
        nointernetMainRelativelayout.visibility=View.GONE
        relativeprogressBar.visibility=View.VISIBLE
        ll_support_main.visibility=View.GONE

        cmsPageModel.getCmsPage(activity as MainActivity,userData.userID,"000002")


            .observe(this,
                Observer<List<CmsPagePojo>?> { cmspagePojo: List<CmsPagePojo>? ->


                    if (cmspagePojo != null && cmspagePojo.size > 0) {
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility=View.GONE
                        ll_support_main.visibility=View.VISIBLE
                        cmsPageData.clear()
                        cmsPageData.addAll(cmspagePojo[0].data)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            tvsupport.text = Html.fromHtml(cmspagePojo!![0].data[0].cmspageContents, Html.FROM_HTML_MODE_LEGACY)
                        }
                        else
                        {
                            tvsupport.text = Html.fromHtml(cmspagePojo!![0].data[0].cmspageContents)
                        }

                        tv_support_emailId.text=userData.setting[0].settingsSupportEmail
                        textViewHelpLineNumberSupport.text=userData.setting[0].settingsSupportPhone
                        phoneNumber = userData.setting[0].settingsSupportPhone

                    } else {

                        if (activity != null) {

                            try {
                                relativeprogressBar.visibility=View.GONE
                                ll_support_main.visibility=View.GONE
                                nointernetMainRelativelayout.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                                    nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                                    nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    }

                })

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.cardViewFaq -> {

              val faqFragment=FaqFragment()
                if (cmsPageData!=null) {
                    val bundle = Bundle()
                    bundle.putSerializable("cmsPageData",cmsPageData[0].faq as Serializable)
                    faqFragment.arguments=bundle
                }
                (activity as MainActivity).navigateTo(faqFragment,true)

            }
            R.id.emailLinearLayout -> {

               val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type="plain/text"
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,userData.setting[0].settingsSupportEmail)
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject")
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text")
                startActivity(Intent.createChooser(emailIntent, "Send mail..."))

            }
            R.id.CallLinearLayout -> {
            call(phoneNumber)
 }
        }
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.header_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
        val notificationTollbarMenu = menu.findItem(R.id.notificationTollbarMenu)

        notificationTollbarMenu.setOnMenuItemClickListener {

            (activity as MainActivity).navigateTo(NotificationFragment(), true)
            true

        }

    }

    private fun call(phoneNumber: String) {

        try {
            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                this@SupportFragment.requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    MY_PERMISSIONS_REQUEST_CALL_PHONE
                )

            } else if (phoneNumber.isNotEmpty()) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + phoneNumber)

                activity!!.startActivity(callIntent)

            }

        } catch (e: ActivityNotFoundException) {
            Log.e("call", "Call failed", e)
        }

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_CALL_PHONE && grantResults.isNotEmpty()) {
            call(phoneNumber)
        }
    }
}
