package com.cab1.fragment


import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.model.CmsPageModel
import com.cab1.pojo.CmsPagePojo
import com.cab1.pojo.LoginData
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.terms_condition.*
import kotlinx.android.synthetic.main.toolbar.*

/**
 * A simple [Fragment] subclass.
 *
 */
class TermConditionFragment : Fragment() {
    private var v:View?=null
    var  code:String=""
    lateinit var cmsPageModel: CmsPageModel
    lateinit var sessionManager: SessionManager
    lateinit var userData: LoginData
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v == null){
            v = inflater.inflate(R.layout.terms_condition, container, false)
        }
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        if(arguments!=null)
        {
            code= arguments!!.getString("code")
        }

        sessionManager= SessionManager(activity!!)
        userData=sessionManager.get_Authenticate_User()

        cmsPageModel  = ViewModelProviders.of(this).get(CmsPageModel::class.java)

        getCmsPage(code)
        btnRetry.setOnClickListener {  getCmsPage(code) }


        when(code){
            "000003"->{
                tvVerifcationTitle.text = resources.getString(R.string.terms_condition)
            }
            "000004"->{
                tvVerifcationTitle.text = resources.getString(R.string.privacy_policy)
            }
            "000005"->  {
                tvVerifcationTitle.text = resources.getString(R.string.Cancellation_ruels)
            }

        }


    }



    fun getCmsPage(code:String){
        nointernetMainRelativelayout.visibility=View.GONE
        relativeprogressBar.visibility=View.VISIBLE


        cmsPageModel.getCmsPage(activity as MainActivity,userData.userID,code)


            .observe(this,
                Observer<List<CmsPagePojo>?> { cmspagePojo: List<CmsPagePojo>? ->


                    if (cmspagePojo != null && cmspagePojo.size > 0) {
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility=View.GONE
                        val settings = tvwebviewcms.settings
                        settings.domStorageEnabled=true

                        tvwebviewcms.settings.javaScriptEnabled=true
                        tvwebviewcms.settings.loadsImagesAutomatically=true
                        tvwebviewcms.setBackgroundColor(0x00000000)
                        if (Build.VERSION.SDK_INT >= 11) {
                            tvwebviewcms.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
                        }

                        tvwebviewcms.isHorizontalScrollBarEnabled=true
                        tvwebviewcms.settings.layoutAlgorithm=WebSettings.LayoutAlgorithm.SINGLE_COLUMN
                        tvwebviewcms.scrollBarStyle=View.SCROLLBARS_INSIDE_OVERLAY

                        tvwebviewcms.loadDataWithBaseURL(
                            null,
                            cmspagePojo[0].data[0].cmspageContents,
                            "text/html",
                            "utf-8",
                            null
                        )


                    } else {
                        if (activity != null) {
                            relativeprogressBar.visibility=View.GONE

                            try {
                                nointernetMainRelativelayout.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                                    nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                                    nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    }

                })

    }



}
