package com.cab1.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.adapter.FaqAdapter
import com.cab1.pojo.Faq
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.activity_country_list.*
import kotlinx.android.synthetic.main.toolbar.*

/**
 * A simple [Fragment] subclass.
 *
 */
class FaqFragment : Fragment() {
    private var v:View?=null
    private lateinit var faqAdapter: FaqAdapter
    private var faqlist = ArrayList<Faq>()

    lateinit var sessionManager: SessionManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v == null){
            v = inflater.inflate(R.layout.activity_country_list, container, false)
        }
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        tvVerifcationTitle.text = "FAQ"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        if(arguments!=null)
        {
            faqlist= arguments!!.getSerializable("cmsPageData") as ArrayList<Faq>
        }

        country_RecycleView.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        faqAdapter = FaqAdapter(
            activity!!,
            faqlist,
            object : FaqAdapter.OnItemClick {
                override fun onClicklisneter(pos: Int, faq: Faq) {


                }

            })
        country_RecycleView.adapter = faqAdapter
        faqAdapter.notifyDataSetChanged()

    }







}
