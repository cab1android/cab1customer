package com.cab1.fragment


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.cab1.MainActivity
import com.cab1.R
import kotlinx.android.synthetic.main.fragment_my_booking.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 *
 */

class MyBookingFragment : Fragment() {


    private var v: View? = null
    var adapter: ViewPagerAdapter? = null
    var isAddedView: Boolean = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater!!.inflate(R.layout.fragment_my_booking, container, false)

        }
        // Inflate the layout for this fragment
        return v

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationIcon(R.drawable.menu_icon)
        tvVerifcationTitle.text = "My Booking"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }

        setupViewPager(myBookingViewPager!!)
        myBookingTablayout!!.setupWithViewPager(myBookingViewPager)
    }


    private fun setupViewPager(viewPager: ViewPager) {
        adapter = ViewPagerAdapter(childFragmentManager)
        adapter?.addFragment(BookingFragment(), "Upcoming")
        adapter?.addFragment(BookingFragment(), "Completed")
        adapter?.addFragment(BookingFragment(), "Cancel")
        viewPager.adapter = adapter
    }

    inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
//            val bundle = Bundle()
//            bundle.putInt("position", position)
//            val fragment = mFragmentList[position]
//            fragment.arguments = bundle

            val bundle = Bundle()
            bundle.putInt("position", position)

            if (position == 0)
                bundle.putString("type", "Upcoming")
            else if (position == 1)
                bundle.putString("type", "Completed")
            else if (position == 2)
                bundle.putString("type", "Canceled")

            val fragment = mFragmentList[position]
            fragment.arguments = bundle

            return fragment
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        override fun saveState(): Parcelable? {
            return null
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }

    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.header_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
        val notificationTollbarMenu = menu.findItem(R.id.notificationTollbarMenu)
        notificationTollbarMenu.setOnMenuItemClickListener {

            (activity as MainActivity).navigateTo(NotificationFragment(), true)
            true
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


      /*  when (requestCode) {
            301 -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {

                        var from: String = ""
                        if (data!!.hasExtra("from")) {
                            from = data.getStringExtra("from")
                        }
                        if (data!!.hasExtra("bookingData") ||from.equals("trackTrip", true)||from.equals("cancelTrip", true)) {

                            if (childFragmentManager != null && childFragmentManager.fragments.size > 0)
                                childFragmentManager.fragments[0].onActivityResult(requestCode, resultCode, data)
                        }

                       else if (from.equals("supportTrip", true)) {
                            (activity as MainActivity).navigateTo(SupportFragment(), false)

                        }


                    }
                }
            }
        }*/


    }




}
