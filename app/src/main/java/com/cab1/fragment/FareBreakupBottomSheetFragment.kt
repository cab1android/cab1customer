package com.cab1.fragment

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cab1.R
import com.cab1.util.MyUtils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.fragment_bottomsheet_dialog.*

class FareBreakupBottomSheetFragment : BottomSheetDialogFragment() {


    var baseFare: String = ""
    var from: String = ""
    var bookingPaymentPercentage: String = ""
    var extra: String = ""
    var ratePerKM: String = ""
    var timeCharge: String = ""
    var waitingFare: String = ""
    var totalPayableValue: String = ""
    var isDiscount = false
    var totalAmountDiscount: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_bottomsheet_dialog, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        headerText.text = resources.getString(R.string.fare_breakup)
        headerText.gravity = Gravity.START

        if (arguments != null) {
            baseFare = arguments!!.getString("baseFare")
            extra = arguments!!.getString("payextra")
            ratePerKM = arguments!!.getString("ratePerKM")
            timeCharge = arguments!!.getString("timeCharge")
            waitingFare = arguments!!.getString("waitingFare")
            from = arguments!!.getString("from", "")
            bookingPaymentPercentage = arguments!!.getString("bookingPaymentPercentage", "0.00")
            totalPayableValue = arguments!!.getString("totalPayableValue")
            isDiscount = arguments!!.getBoolean("isDiscount")
            totalAmountDiscount = arguments!!.getString("totalAmountDiscount", "0.00")

            if (isDiscount) {
                couponCodeDiscountLL.visibility = View.VISIBLE
                discountedAmountTv.text = "- Rs. " + MyUtils.discountFormat(totalAmountDiscount.toDouble())
            } else {
                couponCodeDiscountLL.visibility = View.GONE
            }

            if (bookingPaymentPercentage.equals("0.00")) {
                payFifteenPerLayout.visibility = View.GONE
            } else if (bookingPaymentPercentage.equals("15.00")) {
                payFifteenPerLayout.visibility = View.VISIBLE
                payPercentageTitleTv.text = "Pay 15%"
            } else {
                payFifteenPerLayout.visibility = View.VISIBLE
                payPercentageTitleTv.text = "Pay 100%"
            }


            if (from.equals("main") || bookingPaymentPercentage.equals("0.00")) {
                payFifteenPerLayout.visibility = View.GONE
            } else {
                payFifteenPerLayout.visibility = View.VISIBLE
            }

//            var total = baseFare.toDouble() - extra.toDouble()
            baseFareValue.text = "Rs. " + MyUtils.priceFormat(baseFare)
            payExtraValue.text = "Rs. " + MyUtils.priceFormat(extra.toDouble())
            totalPayableValuetv.text = "Rs. " + MyUtils.priceFormat(totalPayableValue.toDouble())
            ratePerKmTv.text = "Rs. " + MyUtils.priceFormat(ratePerKM.toDouble())
            timeChargeTv.text = "Rs. " + MyUtils.priceFormat(timeCharge.toDouble())
            waitingChargeTv.text = "Rs. " + MyUtils.priceFormat(waitingFare.toDouble())

        }

        backIconImageView.visibility = View.VISIBLE

        backIconImageView.setOnClickListener {
            dismiss()
        }

        dialogCloseIcon.setOnClickListener {

            dismiss()

        }

        okButton.setOnClickListener {
            dismiss()
//            (activity as MainActivity).navigateTo(PaymentFragment(), true)

        }


    }
}
