package com.cab1.fragment


import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.Html
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.api.RestClient
import com.cab1.driver.model.CarscategoryModel
import com.cab1.driver.model.CityListModel
import com.cab1.driver.model.FarcardModel
import com.cab1.pojo.*
import com.cab1.util.LocationProvider
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.fragment_fare_card.*
import kotlinx.android.synthetic.main.nodafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.Serializable
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class FareCardFragment : Fragment(),CitylistBottomSheetFragment.CityListener,CategorylistBottomSheetFragment.CategoryListener {



    private var v:View?=null
    val citylistData:ArrayList<CitylistData?>?= ArrayList()
    val carscategoryData:ArrayList<CarsCategoryData?>?= ArrayList()
    var selectedCityId: String="1"
    var selectedCategoryId: String="3"
    var cityName: String=""
    var cityLat:Double=0.0
    var cityLong:Double=0.0
    private var locationProvider: LocationProvider? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v == null)
        {
            v = inflater.inflate(R.layout.fragment_fare_card, container, false)
        }
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationIcon(R.drawable.menu_icon)
        tvVerifcationTitle.text = "Fare Card"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }
        relativeprogressBar.visibility-View.GONE
        nointernetMainRelativelayout.visibility-View.GONE
        noDatafoundRelativelayout.visibility-View.GONE
        if(arguments!=null)
        {
            cityLat=arguments!!.getDouble("cityLat")
            cityLong=arguments!!.getDouble("cityLong")
        }

        if(cityLat!=null && cityLong!=null)
        {
            CurrentCityName(cityLat,cityLong)
        }
        getCitylistApi("wihoutClick")

        ll_cars_category.setOnClickListener{
            getCarlistApi()
        }

        ll_city.setOnClickListener {
            getCitylistApi("onClick")

        }

         btnRetry.setOnClickListener {
             getFarcardApi(selectedCategoryId,selectedCityId)
         }



    }

    override fun onSelectCity(cityID: String, cityname: String) {
        selectedCityId=cityID
        tvSelectCityValue.text=cityname
        getFarcardApi(selectedCategoryId,selectedCityId)

    }
    override fun onSelectCarsCategory(categoryId: String, categoryName: String) {
        selectedCategoryId=categoryId
        tvSelectVehicleValue.text=categoryName

        getFarcardApi(selectedCategoryId,selectedCityId)

    }

    private fun getCitylistApi(from:String) {
        if(from.equals("wihoutClick",true)) {
            relativeprogressBar.visibility-View.VISIBLE
            nointernetMainRelativelayout.visibility-View.GONE
            noDatafoundRelativelayout.visibility-View.GONE
            ll_fare_card.visibility=View.GONE

        }else  if(from.equals("onClick",true)){
            MyUtils.showProgressDialog(activity!!, "Please wait...")

        }
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val cityListModel=ViewModelProviders.of(this@FareCardFragment).get(CityListModel::class.java)
        cityListModel.getCity(activity!!,jsonArray.toString()).observe(this@FareCardFragment, Observer<List<CitylistPojo>>  {citylistPojo ->
            if (citylistPojo != null && citylistPojo.isNotEmpty()) {
                if (citylistPojo[0].status.equals("true", true)) {

                     citylistData!!.clear()
                     citylistData.addAll(citylistPojo[0].data)
                  if(from.equals("wihoutClick",true)) {
                      noDatafoundRelativelayout.visibility = View.GONE
                      nointernetMainRelativelayout.visibility = View.GONE
                      relativeprogressBar.visibility = View.GONE
                      ll_fare_card.visibility=View.VISIBLE
                       var cityId:String=""
                       for (i in 0 until citylistPojo[0].data.size) {
                           if (cityName.equals(citylistPojo[0].data[i].cityName, true)) {
                               cityId = citylistPojo[0].data[i].cityID
                               tvSelectCityValue.text=citylistPojo[0].data[i].cityName

                           }
                       }
                      if(cityId.equals("",true))
                      {
                          tvSelectCityValue.text=citylistPojo[0].data[0].cityName
                      }
                      else
                      {
                          selectedCityId=cityId
                      }
                      getFarcardApi(selectedCategoryId,selectedCityId)
                   }else   if(from.equals("onClick",true)){
                      MyUtils.dismissProgressDialog()

                       var citylistBottomSheetFragment = CitylistBottomSheetFragment()
                       var bundle = Bundle()
                       bundle.putSerializable("citylistData", citylistPojo[0].data as Serializable)
                       citylistBottomSheetFragment.arguments = bundle
                       citylistBottomSheetFragment.show(childFragmentManager, "CancelTrip")

                }



                }
                else
                {
                    if(from.equals("wihoutClick",true)) {
                        getFarcardApi(selectedCategoryId,selectedCityId)

                    }else  if(from.equals("onClick",true)){
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).showSnackBar(citylistPojo[0].message)
                    }


                }
            }else
            {
                MyUtils.dismissProgressDialog()

                (activity as MainActivity).errorMethod()
            }
        })
    }

    private fun getCarlistApi() {
        MyUtils.showProgressDialog(activity!!, "Please wait...")

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val carscategoryModel=ViewModelProviders.of(this@FareCardFragment).get(CarscategoryModel::class.java)
        carscategoryModel.getCars(activity!!,jsonArray.toString()).observe(this@FareCardFragment, Observer<List<CarsCategoryPojo>>  {carsCategoryPojo ->
            if (carsCategoryPojo != null && carsCategoryPojo.isNotEmpty()) {
                if (carsCategoryPojo[0].status.equals("true", true)) {
                    MyUtils.dismissProgressDialog()

                    carscategoryData!!.clear()
                    carscategoryData.addAll(carsCategoryPojo[0].data)

                    var categorylistBottomSheetFragment =CategorylistBottomSheetFragment()
                    var bundle = Bundle()
                    bundle.putSerializable("categoryData",carsCategoryPojo[0].data as Serializable)
                    categorylistBottomSheetFragment.arguments = bundle
                    categorylistBottomSheetFragment.show(childFragmentManager, "CancelTrip")
                }
                else
                {
                    MyUtils.dismissProgressDialog()

                    (activity as MainActivity).showSnackBar(carsCategoryPojo[0].message)
                }
            }else
            {
                MyUtils.dismissProgressDialog()

                (activity as MainActivity).errorMethod()
            }
        })
    }

    private fun getFarcardApi(categoryId:String,cityId:String) {
        relativeprogressBar.visibility-View.VISIBLE
        nointernetMainRelativelayout.visibility-View.GONE
        noDatafoundRelativelayout.visibility-View.GONE
        ll_fare_card.visibility=View.GONE

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("cityID", cityId)
            jsonObject.put("categoryID", categoryId)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val farcardModel=ViewModelProviders.of(this@FareCardFragment).get(FarcardModel::class.java)
        farcardModel.getFarelist(activity!!,jsonArray.toString()).observe(this@FareCardFragment, Observer<List<FareCardPojo>>  {fareCardPojo ->
            if (fareCardPojo != null && fareCardPojo.isNotEmpty()) {
                if (fareCardPojo[0].status.equals("true", true)) {

                    noDatafoundRelativelayout.visibility = View.GONE
                    nointernetMainRelativelayout.visibility = View.GONE
                    relativeprogressBar.visibility = View.GONE
                    ll_fare_card.visibility=View.VISIBLE

                    tvTotalFareValue.text=Html.fromHtml(fareCardPojo[0].data[0].farecardTotalFare)
                    tvCancellationFeeValue.text=Html.fromHtml(fareCardPojo[0].data[0].farecardCancellationFee)
                }
                else
                {
                    relativeprogressBar.visibility-View.GONE
                    if (fareCardPojo[0].data!!.size == 0) {
                        noDatafoundRelativelayout.visibility = View.VISIBLE
                        ll_fare_card.visibility=View.GONE
                    } else {
                        noDatafoundRelativelayout.visibility = View.GONE
                        ll_fare_card.visibility=View.VISIBLE
                    }
                }
            }else
            {

                if (activity != null) {
                    relativeprogressBar.visibility = View.GONE
                    ll_fare_card.visibility=View.GONE


                    try {
                        nointernetMainRelativelayout.visibility = View.VISIBLE
                        if (MyUtils.isInternetAvailable(activity!!)) {
                            nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                            nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                        } else {
                            nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))
                            nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.header_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
        val notificationTollbarMenu = menu.findItem(R.id.notificationTollbarMenu)
        notificationTollbarMenu.setOnMenuItemClickListener {

            (activity as MainActivity).navigateTo(NotificationFragment(), true)
            true
        }
    }



    private fun CurrentCityName(lattitude: Double, longitude: Double) {
        val geocoder: Geocoder
        var addresses: List<Address>? = null
        geocoder = Geocoder(activity!!, Locale.getDefault())

        try {

            addresses = geocoder.getFromLocation(
                lattitude,
                longitude,
                1
            )

            if (addresses != null) {


                cityName = addresses[0].locality



            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

}
