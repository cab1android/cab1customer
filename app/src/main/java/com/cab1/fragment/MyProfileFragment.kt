package com.cab1.fragment


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.api.RestClient
import com.cab1.model.UpdateProfileDataModel
import com.cab1.model.UploadProfileModel
import com.cab1.model.UploadProfilePictureModel
import com.cab1.pojo.LoginData
import com.cab1.pojo.LoginPojo
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.facebook.common.util.UriUtil
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_otp_verification.*
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File


/**
 * A simple [Fragment] subclass.
 *
 */
class MyProfileFragment : Fragment(), View.OnClickListener {


    private var v: View? = null
    private var isEdit: Boolean? = false
    var mfUser_Profile_Image: File? = null
    lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_my_profile, container, false)
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(context as MainActivity)

        setValues()

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationIcon(R.drawable.back_icon)
        tvVerifcationTitle.text = "Profile"

//        toolbar.setNavigationOnClickListener {
//            (activity as MainActivity).openDrawer()
//        }

        toolbar.setNavigationOnClickListener {
            (activity as AppCompatActivity).onBackPressed()
        }

        btnMyProfile!!.setOnClickListener(this)
        Img_UserProfileIcon.setOnClickListener {

            getWriteStoragePermissionOther()
        }
        setEnableDisable(false)

        fullname_edit_text.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
        fullname_edit_text.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (fullname_edit_text!!.text.toString().trim().length > 0) {

                    var x: Char
                    val t = IntArray(fullname_edit_text!!.text.toString().trim().length)

                    for (i in 0 until fullname_edit_text!!.text.toString().trim().length) {
                        x = fullname_edit_text!!.text.toString().trim().toCharArray()[i]
                        val z = x.toInt()
                        t[i] = z

                        if (z > 47 && z < 58 || z > 64 && z < 91
                            || z > 96 && z < 123 || z == 32
                        ) {

                        } else {

                            Toast.makeText(activity, "" + "Special Character not allowed", Toast.LENGTH_SHORT).show()
                            var ss = fullname_edit_text!!.text.toString()
                                .substring(0, fullname_edit_text!!.text.toString().length - 1)
                            fullname_edit_text.setText(ss)
                            fullname_edit_text.setSelection(fullname_edit_text!!.text.toString().length)
                        }

                    }
                }
            }
        })

    }

    fun getWriteStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(activity as MainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getReadStoragePermissionOther()
        } else {
            this.requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1001)
        }
    }

    fun getReadStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(activity as MainActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            openGallery()
        } else {
            this.requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1002)
        }
    }

    fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1211)
    }


    private fun setValues() {

        fullname_edit_text.text =
            Editable.Factory.getInstance().newEditable(sessionManager.get_Authenticate_User().userFullName)

        emailid_edit_text.text =
            Editable.Factory.getInstance().newEditable(sessionManager.get_Authenticate_User().userEmail)

        mobileno_edit_text.text =
            Editable.Factory.getInstance().newEditable(sessionManager.get_Authenticate_User().userMobile)


        val imageUri =
            Uri.parse(RestClient.image_customer_url + sessionManager.get_Authenticate_User().userProfilePicture)

        Img_UserProfileIcon.setImageURI(imageUri);

    }

    private fun updateProfilePictureApi(path: String) {

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {

            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("userProfilePicture", path)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        MyUtils.showProgressDialog(context as MainActivity, "Uploading")

        val loginModel = ViewModelProviders.of(context as MainActivity).get(UploadProfilePictureModel::class.java)
        loginModel.updateProfilePicture(context as MainActivity, false, jsonArray.toString())
            .observe(context as MainActivity,
                Observer<List<LoginPojo>> { loginPojo ->

                    MyUtils.dismissProgressDialog()


                    if (loginPojo != null && loginPojo.isNotEmpty()) {
                        if (loginPojo[0].status.equals("true", true)) {

                            sessionManager.clear_login_session()
                            StoreSessionManager(loginPojo[0].data)

                            (context as MainActivity).onUpdate()

                        } else {
                            showSnackBar(loginPojo[0].message)
                        }


                    } else {
                        btnSubmit.revertAnimation()
                        ErrorMethod()
                    }
                })
    }

    private fun setEnableDisable(EnableDisable: Boolean) {

        fullname_edit_text!!.isEnabled = EnableDisable
        mobileno_edit_text!!.isEnabled = false
        mobileno_edit_text!!.setTextColor(Color.parseColor("#1E1E1E"))
        emailid_text_input!!.isEnabled = EnableDisable

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1001 -> {
                if (grantResults.isNotEmpty()) {
                    getReadStoragePermissionOther()
                } else {
                    (activity as MainActivity).showSnackBar("Permission denied")
                }
            }
            1002 -> {
                if (grantResults.isNotEmpty()) {
                    openGallery()
                } else {
                    (activity as MainActivity).showSnackBar("Permission denied")
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var picturePath: String? = ""
        when (requestCode) {

            1211 ->

                if (data != null && resultCode == Activity.RESULT_OK && activity != null) {
                    val imageUri = data.data

                    picturePath = MyUtils.getPath1(imageUri, activity!!)

                    if (picturePath != null) {
                        if (picturePath.contains("https:")) {
                            (activity as MainActivity).showSnackBar("Please, select another profile pic.")
                        } else {
                            mfUser_Profile_Image = File(picturePath)

                            Img_UserProfileIcon.setImageURI(UriUtil.getUriForFile(mfUser_Profile_Image))

                            if (MyUtils.isInternetAvailable(activity!!)) {
                                uploadProfilePic()
                            } else {
                                (activity as MainActivity).showSnackBar(activity?.getString(R.string.error_common_network)!!)
                            }

                        }
                    } else {

                        (activity as MainActivity).showSnackBar("Please, select another profile pic.")

                    }
                }
        }
    }

    private fun uploadProfilePic() {

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)

            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val uploadProfileModel = ViewModelProviders.of(this@MyProfileFragment).get(UploadProfileModel::class.java)

        uploadProfileModel.uploadFile(
            activity!!, true, jsonArray.toString(), "user",
            mfUser_Profile_Image!!
        ).observe(this@MyProfileFragment,
            Observer<List<LoginPojo>> { loginPojo ->
                if (loginPojo != null && loginPojo.isNotEmpty()) {
                    if (loginPojo[0].status.equals("true", true)) {

                        updateProfilePictureApi(mfUser_Profile_Image!!.name)


                    } else {
                        (activity as MainActivity).showSnackBar(loginPojo[0].message)
                    }


                } else {

                    errorMethod()
                }
            })
    }

    fun errorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(activity as MainActivity)) {
                (activity as MainActivity).showSnackBar(resources.getString(R.string.error_common_network))
            } else {
                (activity as MainActivity).showSnackBar(resources.getString(R.string.error_crash_error_message))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnMyProfile -> {

                if (isEdit == true) {
                    fullname_text_input.error = null // Clear the error
                    mobileno_text_input.error = null // Clear the error
                    emailid_text_input.error = null // Clear the error
                    var valid: Boolean
                    when {

                        fullname_edit_text!!.text.toString().isEmpty() -> {
                            fullname_text_input.error =
                                    activity!!.resources.getString(R.string.textfield_reg_eror_fullname)
                            valid = false
                        }

                        fullname_edit_text!!.text.toString().length < 5 -> {
                            fullname_text_input.error =
                                    activity!!.resources.getString(R.string.textfield_reg_eror_valid_fullname)
                            valid = false
                        }
//                        mobileno_edit_text!!.text.toString().isEmpty() -> mobileno_text_input.error = activity!!.resources.getString(R.string.textfield_reg_eror_mobileno)
//                        mobileno_edit_text!!.text.toString().length < 10 -> emailid_text_input.error = activity!!.resources.getString(R.string.textfield_reg_eror_validmobileno)

                        emailid_edit_text!!.text.toString().isEmpty() -> {
                            emailid_text_input.error =
                                    activity!!.resources.getString(R.string.textfield_reg_eror_emailid)
                            valid = false
                        }
                        !MyUtils.isEmailValid(emailid_edit_text!!.text.toString().trim()) -> {
                            emailid_text_input.error =
                                    activity!!.resources.getString(R.string.signup_enter_valid_emailid)
                            valid = false
                        }

                        else -> {
                            valid = true
                            fullname_text_input.error = null // Clear the error
                            mobileno_text_input.error = null // Clear the error
                            emailid_text_input.error = null // Clear the error
                        }
                    }
                    if (valid) {
                        FirebaseInstanceId.getInstance().instanceId
                            .addOnCompleteListener {
                                if (it.isSuccessful) {
                                    updateProfileDetails(it.result!!.token)
                                } else {
                                    ErrorMethod()
                                }
                            }
                    }
                } else {
                    isEdit = true
                    btnMyProfile.text = context!!.resources.getString(R.string.update)
                    setEnableDisable(true)
                }


            }

        }
    }


    private fun updateProfileDetails(fcmToken: String) {

        btnMyProfile.startAnimation()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {

            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("userFullName", fullname_edit_text.text.toString())
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("userDeviceID", fcmToken)
            jsonObject.put("userEmail", emailid_edit_text.text.toString().trim())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val loginModel = ViewModelProviders.of(context as MainActivity).get(UpdateProfileDataModel::class.java)
        loginModel.updateProfileData(context as MainActivity, false, jsonArray.toString())
            .observe(context as MainActivity,
                Observer<List<LoginPojo>> { loginPojo ->

                    btnMyProfile.revertAnimation()

                    if (loginPojo != null && loginPojo.isNotEmpty()) {
                        if (loginPojo[0].status.equals("true", true)) {

                            sessionManager.clear_login_session()


                            StoreSessionManager(loginPojo[0].data)

                            showSnackBar("Profile has been updated")


                            Handler().postDelayed({
                                (activity as MainActivity).onUpdate()
                                (activity as MainActivity).onBackPressed()
                            }, 2000)


                        } else {

                            showSnackBar(loginPojo[0].message)
                        }


                    } else {

                        ErrorMethod()
                    }
                })
    }

    private fun StoreSessionManager(driverdata: List<LoginData>) {

        val gson = Gson()

        val json = gson.toJson(driverdata[0])

        sessionManager.create_login_session(
            json,
            driverdata[0].userMobile,
            "",
            true,
            true,
            sessionManager.isEmailLogin()
        )

    }


    fun showSnackBar(message: String) {

        Snackbar.make(mainLayoutMyProfile, message, Snackbar.LENGTH_LONG).show()

    }

    fun ErrorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(context as MainActivity)) {
                showSnackBar(resources.getString(R.string.error_common_network))
            } else {
                showSnackBar(resources.getString(R.string.error_crash_error_message))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}
