package com.cab1.fragment


import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.adapter.WalletTransactionsAdapter
import com.cab1.api.RestClient
import com.cab1.model.WalletTransactionHistoryListModel
import com.cab1.pojo.WalletTransactionData
import com.cab1.pojo.WalletTransactionPojo
import com.cab1.util.MyUtils
import com.cab1.util.PopupMenu
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.fragment_wallet.*
import kotlinx.android.synthetic.main.nodafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException


class WalletFragment : Fragment() {

    private var v: View? = null
    lateinit var sessionManager: SessionManager
    private lateinit var walletTransactionsAdapter : WalletTransactionsAdapter
    private var walletHistoryPojo:ArrayList<WalletTransactionPojo?>? = ArrayList()
    var y: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    var pageNo = 0
    var visibleItemCount: Int = 0
    var totalItemCount:Int = 0
    var firstVisibleItemPosition:Int = 0
    private var isLoading = false
    private var isLastpage = false
    var filter:String=""
    lateinit var walletTransactionHistoryListModel:WalletTransactionHistoryListModel
    override fun onCreate(savedInstanceState: Bundle?) {

        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        v = inflater.inflate(R.layout.fragment_wallet, container, false)


        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationIcon(R.drawable.menu_icon)
        tvVerifcationTitle.text = "Wallet"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }
        sessionManager = SessionManager(activity!!)
         walletTransactionHistoryListModel = ViewModelProviders.of(this).get(WalletTransactionHistoryListModel::class.java)

        wallet_TextView.text = "Wallet (Rs. ${sessionManager.get_Authenticate_User().userWallet})"


        // Set up the RecyclerView
        walletTransactionsAdapter = WalletTransactionsAdapter(this.context!!,walletHistoryPojo)
        linearLayoutManager = LinearLayoutManager(activity)
        walletTransaction_RecycleView.layoutManager = linearLayoutManager
        walletTransaction_RecycleView.adapter = walletTransactionsAdapter

        getWalletHistory()

         filterImageView.setOnClickListener {
            var optionList: ArrayList<String> = ArrayList()
            optionList.add("All")
            optionList.add("Received")
            optionList.add("Paid")

            PopupMenu(activity!!, it, optionList).showPopUp(resources.getDimensionPixelSize(R.dimen._110sdp),
                object : PopupMenu.OnMenuSelectItemClickListener {
                    override fun onItemClick(item: String, pos: Int) {
                        filter=item
                        pageNo=0
                        walletHistoryPojo!!.clear()
                        getWalletHistory()
                    }
                })
        }

        walletTransaction_RecycleView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.getChildCount()
                totalItemCount = linearLayoutManager.getItemCount()
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0 && walletHistoryPojo != null && walletHistoryPojo!!.size > 0) {

                        isLoading = true
                        getWalletHistory()
                    }
                }
            }
        })
        btnRetry.setOnClickListener {
            getWalletHistory()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.header_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)

        val notificationTollbarMenu = menu.findItem(R.id.notificationTollbarMenu)
        notificationTollbarMenu.setOnMenuItemClickListener {

            (activity as MainActivity).navigateTo(NotificationFragment(), true)
            true

        }
    }

    fun getWalletHistory(){


        noDatafoundRelativelayout.visibility=View.GONE
        nointernetMainRelativelayout.visibility=View.GONE

        if (pageNo == 0) {
            relativeprogressBar.visibility=View.VISIBLE
            walletHistoryPojo!!.clear()
            walletTransactionsAdapter.notifyDataSetChanged()
        } else {
            relativeprogressBar.visibility=View.GONE
            walletTransaction_RecycleView.visibility=(View.VISIBLE)
            walletHistoryPojo!!.add(null)
            walletTransactionsAdapter.notifyItemInserted(walletHistoryPojo!!.size - 1)
        }
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("logindriverID", "0")
            jsonObject.put("filter", filter)
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        walletTransactionHistoryListModel.getWalletHistory(activity as MainActivity, jsonArray.toString())
            .observe(this,
                Observer<List<WalletTransactionPojo>?> { walletTransactionPojo: List<WalletTransactionPojo>? ->


                    if (walletTransactionPojo != null && walletTransactionPojo.size > 0) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility=View.GONE
                      //  wallet_TextView.text = "Wallet (Rs. ${walletTransactionPojo[0].walletamount})"


                        if (pageNo > 0) {
                            walletHistoryPojo!!.removeAt(walletHistoryPojo!!.size - 1)
                            walletTransactionsAdapter.notifyItemRemoved(walletHistoryPojo!!.size)
                        }
                        if (walletTransactionPojo[0].status.equals("true"))
                        {
                            walletTransaction_RecycleView.visibility=View.VISIBLE
                            if (pageNo == 0) {
                                walletHistoryPojo!!.clear()
                            }
                            for (datum in walletTransactionPojo[0].data!!) {
                                var transactionDate = ""
                                try {
                                    transactionDate = MyUtils.formatDate(datum!!.wallettransactonDate!!, "yyyy-MM-dd HH:mm:ss", "MMM yyyy")

                                } catch (e: ParseException) {
                                    e.printStackTrace()
                                }
                                if (walletHistoryPojo!!.size == 0) {
                                    val myTransactionhistoryPojo = WalletTransactionPojo()
                                    myTransactionhistoryPojo.transactiondate=transactionDate

                                    val transactionArrayList = ArrayList<WalletTransactionData?>()
                                    transactionArrayList.add(datum!!)
                                    myTransactionhistoryPojo.data=transactionArrayList
                                    walletHistoryPojo!!.add(myTransactionhistoryPojo)

                                } else if (walletHistoryPojo!!.size > 0) {
                                    if (walletHistoryPojo!![walletHistoryPojo!!.size - 1]!!.transactiondate.equals(transactionDate,true)) {

                                        val myTransactionhistoryPojo = walletHistoryPojo!![walletHistoryPojo!!.size - 1]
                                        myTransactionhistoryPojo!!.transactiondate=transactionDate
                                        myTransactionhistoryPojo!!.data.add(datum)

                                    } else {
                                        val myTransactionhistoryPojo = WalletTransactionPojo()
                                        myTransactionhistoryPojo.transactiondate=transactionDate

                                        val myTransactionArrayList = ArrayList<WalletTransactionData?>()
                                        myTransactionArrayList.add(datum!!)
                                        myTransactionhistoryPojo.data=myTransactionArrayList
                                        walletHistoryPojo!!.add(myTransactionhistoryPojo)
                                    }
                                }
                            }
                            walletTransactionsAdapter.notifyDataSetChanged()
                            pageNo =pageNo+ 1
                            if (walletTransactionPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }
                        } else {
                            relativeprogressBar.visibility=View.GONE

                            if (walletHistoryPojo!!.size == 0) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                walletTransaction_RecycleView.visibility=View.GONE

                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                walletTransaction_RecycleView.visibility=View.VISIBLE

                            }

                        }

                    } else {
                        if (activity != null) {
                            relativeprogressBar.visibility=View.GONE

                            try {
                                nointernetMainRelativelayout.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                                    nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                                    nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    }

                })

    }






}
