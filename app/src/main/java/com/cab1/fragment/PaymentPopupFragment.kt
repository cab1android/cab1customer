package com.cab1.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.cab1.DriverRatingActivity
import com.cab1.R
import com.cab1.api.RestClient
import com.cab1.notification.RefData
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.payment_pop_up.*

class PaymentPopupFragment : Fragment() {

    var referData: RefData? = null

    lateinit var sessionManager: SessionManager
    var v: View? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        v = inflater.inflate(R.layout.payment_pop_up, container, false)

        return v
    }



    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(activity!!)
        headerText.visibility=View.VISIBLE
        backIconImageView.visibility=View.INVISIBLE
        headerText.text = resources.getString(R.string.trip_details)
        headerText.gravity = Gravity.CENTER
        if(arguments!=null)
        {
            referData=arguments!!.getSerializable("referData") as RefData

        }


       if(referData!=null)
       {
           tv_payment_popup_BookingId.text="Booking Id: "+referData!!.bookingNo
           val bookDate = MyUtils.formatDate(referData!!.bookingDate, "yyyy-MM-dd HH:mm:ss", "EEE, MMM d, HH:mm a")
           tv_payment_popup_BookingDate.text=bookDate
           img_payment_popup_CallDriverIcon.setImageURI(RestClient.image_driver_url + referData!!.driverProfilePic)
           tv_payment_popup_DriverName.text=referData!!.driverName
           tv_payment_popup_PaymentOptionValue.text=referData!!.bookingPaymentMode

           tv_payment_popup_FromLocationAddress.text=referData!!.bookingPickupAddress
           tv_payment_popup_destinationLocationAddress.text=referData!!.bookingDropAddress

           tvDriverValue.text = "" +
                   referData!!.bookingDistanceActual + " Kms"

           tvestimatedamountValue.text = "Rs. " + MyUtils.priceFormat(referData!!.bookingEstimatedAmount)
           tvActualAmountValue.text = "Rs. " + MyUtils.priceFormat(referData!!.bookingActualAmount)

           if (referData!!.bookingPaymentMode.equals("Cash", true)) {
               tvwalletAmountText.visibility = View.GONE
               tvCreaditAmountText.text = "Due"
               tvCreaditWalletAmountValue.text = "Rs. " + MyUtils.priceFormat(referData!!.bookingActualAmount)

           } else {

               var duePament =
                   (referData!!.bookingEstimatedAmount.toDouble()).minus(referData!!.bookingActualAmount.toDouble())
               if (duePament >= 0) {
                   tvwalletAmountText.visibility = View.VISIBLE
                   tvCreaditAmountText.text = "Credit"
                   tvCreaditWalletAmountValue.text = "Rs. " + MyUtils.priceFormat(duePament)
               } else {
                   tvwalletAmountText.visibility = View.VISIBLE
                   tvCreaditAmountText.text = "Debit"
                   tvCreaditWalletAmountValue.text = "Rs. " + MyUtils.priceFormat(Math.abs(duePament))
               }
           }



          fromDateDisplyTv_completedTrip.text =  MyUtils.formatDate(
               referData!!.bookingTripStartTime,
               "yyyy-MM-dd HH:mm:ss",
               "EEE,dd MMM"
           )

          fromTimeDisplayTv_completedTrip.text = MyUtils.formatDate(referData!!.bookingTripStartTime, "yyyy-MM-dd HH:mm:ss", "hh:mm a")

          toDateDisplayTv_completedTrip.text =  MyUtils.formatDate(
              referData!!.bookingTripEndTime,
              "yyyy-MM-dd HH:mm:ss",
              "EEE,dd MMM"
          )
          toTimeDisplayTv_completedTrip.text =  MyUtils.formatDate(referData!!.bookingTripEndTime, "yyyy-MM-dd HH:mm:ss", "hh:mm a")


           choosePackageCarImageView_completedTrip.setImageURI(RestClient.image_category_url+referData!!.categoryImage)

           carTypeTextView_completedTrip.text=referData!!.drvcarModel

           tvBaseFareValue.text="Rs. "+MyUtils.priceFormat(referData!!.optionBasefare.toString())
           tvTimeValue.text=""+referData!!.Duration.toString()
           tvSubtotalValue.text="Rs. "+MyUtils.priceFormat(referData!!.bookingGST)
           tvTotalValue.text="Rs. "+MyUtils.priceFormat(referData!!.bookingActualAmount)
           tv_booking_type.text="Booking type:"+referData!!.bookingType

           if (referData!!.bookingSubType!!.contains("street", true)) {
               tvBookingType.text = resources.getString(R.string.easy_ride)
           }else {
               tvBookingType.text="Booking type:"+referData!!.bookingType

           }

           carTypeTextView_completedTrip.text =referData!!.categoryName
           carNamesTextView_completedTrip.text = referData!!.drvcarModel
           catTypeTv_completedTrip.text =referData!!.categoryType
           catSeatsTv_completedTrip.text = referData!!.categorySeats + " Seats"
           luggagesTv_completedTrip.text =referData!!.categoryNoOfBags + " Luggage Bag"

           tvPerkmValue.text ="Rs. "+MyUtils.priceFormat(referData!!.optionAdditionalKmCharge)
           tvPerTimeValue.text ="Rs. "+MyUtils.priceFormat(referData!!.optionPerMinCharge)


       }


        btn_payment_popup_Done.setOnClickListener {
            openRatingActivity()
        }
        dialogCloseIcon.setOnClickListener {
            btn_payment_popup_Done.performClick()
        }

    }

     fun openRatingActivity()
    {
        var intent = Intent(activity!!, DriverRatingActivity::class.java)
        intent.putExtra("driverName", referData!!.driverName)
        intent.putExtra("driverID", referData!!.driverID)
        intent.putExtra("driverProfilePic", referData!!.driverProfilePic)
        intent.putExtra("bookingID", referData!!.bookingID)
        startActivity(intent)
        (activity!!).overridePendingTransition(
            R.anim.slide_in_right,
            R.anim.slide_out_left
        )
    }
}