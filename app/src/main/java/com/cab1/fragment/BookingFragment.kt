package com.cab1.fragment


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.adapter.BookingAdapter
import com.cab1.api.RestClient
import com.cab1.model.GetMyBookingsModel
import com.cab1.pojo.GetMyBookingData
import com.cab1.pojo.GetMyBookingsPojo
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.fragment_booking.*
import kotlinx.android.synthetic.main.nodafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 *
 */
class BookingFragment : Fragment() {


    private var v: View? = null
    private var tabposition: Int = 0
    var mContext: Context? = null
    lateinit var sessionManager: SessionManager
    var bookingType = ""
    var myBookingTimer = Timer()
    private lateinit var bookingAdapter: BookingAdapter
    private var bookingData: ArrayList<GetMyBookingData?> = ArrayList()
    var y: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    var pageNo = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var firstVisibleItemPosition: Int = 0
    private var isLoading = false
    private var isLastpage = false
    lateinit var loginModel: GetMyBookingsModel
    var from: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        v = inflater.inflate(R.layout.fragment_booking, container, false)


        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(context as MainActivity)
        if (arguments != null) {
            tabposition = arguments!!.getInt("position", 0)
            bookingType = arguments!!.getString("type")
        }


        if (tabposition == 0 || tabposition == 2) {

            LocalBroadcastManager.getInstance(activity!!)
                .registerReceiver(broadcast, IntentFilter("Upcomming"))
        }



        loginModel = ViewModelProviders.of(this@BookingFragment).get(GetMyBookingsModel::class.java)


        bookingAdapter = BookingAdapter(
            this!!.activity!!,
            tabposition,
            bookingData!!,
            object : BookingAdapter.OnItemClickListener {
                override fun onItemClick(position: Int, typeofOperation: String) {
                    var bundle = Bundle()
                    bundle.putInt("position", tabposition)
                    bundle.putSerializable("bookingData", bookingData!![position] as Serializable)

                    var fragment = BookingDetailsFragment()
                    fragment.arguments = bundle
                    (activity as MainActivity).navigateTo(fragment, true)

                }
            })
        linearLayoutManager = LinearLayoutManager(activity)
        bookingRecyclerView.layoutManager = linearLayoutManager
        bookingRecyclerView.hasFixedSize()
        bookingRecyclerView.adapter = bookingAdapter
        getMyBookingData()

        bookingRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.getChildCount()
                totalItemCount = linearLayoutManager.getItemCount()
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {

                        isLoading = true
                        getMyBookingData()
                    }
                }
            }
        })

        btnRetry.setOnClickListener {
            pageNo = 0
            getMyBookingData()
        }
    }

    private val broadcast = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if (intent.action.equals("Upcomming", true)) {
                cancelTrip()
            }

        }
    }


    private fun getMyBookingData() {

        if (mContext == null) {
            return
        }


        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE

        if (pageNo == 0) {
            relativeprogressBar.visibility = View.VISIBLE
            bookingData!!.clear()
            bookingAdapter.notifyDataSetChanged()
        } else {
            relativeprogressBar.visibility = View.GONE
            bookingRecyclerView.visibility = View.VISIBLE
            bookingData!!.add(null)
            bookingAdapter.notifyItemInserted(bookingData!!.size - 1)
        }
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("Type", bookingType)
            jsonObject.put("bookingID", "0")
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        Log.d("System out", "json : " + jsonArray)

        loginModel.getMyBookings(activity as MainActivity, jsonArray.toString())
            .observe(this@BookingFragment,
                Observer<List<GetMyBookingsPojo>?> { bookinglistpojo: List<GetMyBookingsPojo>? ->


                    if (bookinglistpojo != null && bookinglistpojo.size > 0) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility = View.GONE


                        if (pageNo > 0) {
                            bookingData!!.removeAt(bookingData!!.size - 1)
                            bookingAdapter.notifyItemRemoved(bookingData!!.size)
                        }
                        if (bookinglistpojo[0].status.equals("true")) {

                            if (pageNo == 0) {
                                bookingData!!.clear()
                            }
                            bookingData!!.addAll(bookinglistpojo[0].data)
                            bookingAdapter.notifyDataSetChanged()
                            pageNo += 1
                            if (bookinglistpojo[0].data!!.size < 10) {
                                isLastpage = true
                            }
                        } else {
                            relativeprogressBar.visibility = View.GONE

                            if (bookingData!!.size == 0) {
                                noDatafoundRelativelayout.visibility = View.VISIBLE
                                bookingRecyclerView.visibility = View.GONE

                            } else {
                                noDatafoundRelativelayout.visibility = View.GONE
                                bookingRecyclerView.visibility = View.VISIBLE

                            }

                        }

                    } else {
                        if (activity != null) {
                            relativeprogressBar.visibility = View.GONE


                            try {
                                nointernetMainRelativelayout.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                                    nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                                    nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    }

                })
    }


//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        when (requestCode) {
//            301 -> {
//                if (resultCode == Activity.RESULT_OK) {
//                    if (data != null) {
//                        if (data!!.hasExtra("from"))
//                        {
//                            from = data.getStringExtra("from")
//                        }
//                        if (data!!.hasExtra("bookingData")) {
//                            getMyBookingData = data.getSerializableExtra("bookingData") as GetMyBookingData
//                        }
//
//                        if(from.equals("trackTrip",true))
//                        {
//                            if(getMyBookingData!=null) {
//                               trackTrip(getMyBookingData)
//                            }
//                        }else if(from.equals("cancelTrip",true))
//                        {
//                            if(getMyBookingData!=null) {
//
//                                cancelTrip(getMyBookingData)
//                            }
//                        }
//                        else if(from.equals("supportTrip",true))
//                        {
//                            (activity as MainActivity).navigateTo(SupportFragment(),false)
//
//                        }
//
//
//                    }
//                }
//            }
//            else -> super.onActivityResult(requestCode, resultCode, data)
//        }
//
//    }

    /*  private fun trackTrip(getMyBookingData:GetMyBookingData?)
      {
          if (getMyBookingData!!.statusName.equals(
                  "Accepted",
                  true
              ) || getMyBookingData!!.statusName.equals(
                  "Started",
                  true
              ) || getMyBookingData!!.statusName.equals("Reached", true)
          ) {
              var push: Push = Push()
              when (getMyBookingData!!.statusName)
              {
                  "Accepted" -> {
                      push.type = Push.bokingRequestAccepted

                  }
                  "Started" -> {
                      push.type = Push.tripStarted

                  }
                  "Reached" -> {
                      push.type = Push.driverReachedAtPickupLocation

                  }
              }
              push.RefData = Gson().toJson(getMyBookingData)


             var fragment =            (parentFragment?.activity!! as MainActivity).supportFragmentManager.findFragmentByTag(MainFragment::class.java.name)

              var bundle = Bundle()


              when (getMyBookingData!!.statusName) {
                  "Accepted" -> {
                      push.type = Push.bokingRequestAccepted

                  }
                  "Started" -> {
                      push.type = Push.tripStarted

                  }
                  "Reached" -> {
                      push.type = Push.driverReachedAtPickupLocation

                  }
              }
              push.RefData = Gson().toJson(getMyBookingData)
              bundle.putString("fromBookingStatus",getMyBookingData!!.statusName)
              bundle.putSerializable("push", push)
              fragment?.arguments = bundle
              (parentFragment?.activity!! as MainActivity) .navigateTo(fragment!!, true)





          }
      }*/
    private fun cancelTrip() {

        pageNo = 0
        getMyBookingData()

    }


}
