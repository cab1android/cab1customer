package com.cab1.fragment


import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.*
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.model.CmsPageModel
import com.cab1.pojo.CmsPagePojo
import com.cab1.pojo.LoginData
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.fragment_about_us.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.webview_layout.*

/**
 * A simple [Fragment] subclass.
 *
 */
class AboutUsFragment : Fragment() {
    private var v:View?=null

    lateinit var cmsPageModel: CmsPageModel
    lateinit var sessionManager: SessionManager
    lateinit var userData: LoginData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(v == null){
            v = inflater.inflate(R.layout.fragment_about_us, container, false)
        }
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationIcon(R.drawable.menu_icon)
        tvVerifcationTitle.text = "About"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }
        customTextView(tv_term_condition,resources.getString(R.string.terms_condition))
        customTextView(tv_privacy_policy,resources.getString(R.string.privacy_policy))
        customTextView(tv_cancellation_rules,resources.getString(R.string.Cancellation_ruels))

        sessionManager= SessionManager(activity!!)
        userData=sessionManager.get_Authenticate_User()

        cmsPageModel  = ViewModelProviders.of(this).get(CmsPageModel::class.java)

        getCmsPage("000001")
        btnRetry.setOnClickListener {  getCmsPage("000001") }

        tv_term_condition.setOnClickListener {
            val fragment=TermConditionFragment()

                val bundle = Bundle()
                bundle.putString("code","000003")
                fragment.arguments=bundle
                (activity as MainActivity).navigateTo(fragment,true)

        }
        tv_privacy_policy.setOnClickListener {
            val fragment=TermConditionFragment()

            val bundle = Bundle()
            bundle.putString("code","000004")
            fragment.arguments=bundle

            (activity as MainActivity).navigateTo(fragment,true)
        }
        tv_cancellation_rules.setOnClickListener {
            val fragment=TermConditionFragment()

            val bundle = Bundle()
            bundle.putString("code","000005")
            fragment.arguments=bundle

            (activity as MainActivity).navigateTo(fragment,true)
        }

    }




    fun getCmsPage(code:String){
        nointernetMainRelativelayout.visibility=View.GONE
        relativeprogressBar.visibility=View.VISIBLE


        cmsPageModel.getCmsPage(activity as MainActivity,userData.userID,code)


            .observe(this,
                Observer<List<CmsPagePojo>?> { cmspagePojo: List<CmsPagePojo>? ->


                    if (cmspagePojo != null && cmspagePojo.size > 0) {
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility=View.GONE
                        val settings = webviewcms.settings
                        settings.domStorageEnabled=true

                        webviewcms.settings.javaScriptEnabled=true
                        webviewcms.settings.loadsImagesAutomatically=true
                        webviewcms.setBackgroundColor(0x00000000)
                        if (Build.VERSION.SDK_INT >= 11) {
                            webviewcms.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
                        }

                        webviewcms.isHorizontalScrollBarEnabled=true
                        webviewcms.settings.layoutAlgorithm=WebSettings.LayoutAlgorithm.SINGLE_COLUMN
                        webviewcms.scrollBarStyle=View.SCROLLBARS_INSIDE_OVERLAY

                        webviewcms.loadDataWithBaseURL(
                            null,
                            cmspagePojo[0].data[0].cmspageContents,
                            "text/html",
                            "utf-8",
                            null
                        )


                    } else {
                        if (activity != null) {
                            relativeprogressBar.visibility=View.GONE

                            try {
                                nointernetMainRelativelayout.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                                    nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                                    nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    }

                })

    }

    private fun customTextView(tv: TextView?,text:String)
    {

        val spanTxt: SpannableStringBuilder
        spanTxt = SpannableStringBuilder(text)
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {

            }
        }, spanTxt.length - text.length, spanTxt.length, 0)
        spanTxt.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorPrimary)),
            spanTxt.length -text.length,
            spanTxt.length,
            0
        )


        tv?.setMovementMethod(LinkMovementMethod.getInstance())
        tv?.setText(spanTxt, TextView.BufferType.SPANNABLE)

    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.header_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
        val notificationTollbarMenu = menu.findItem(R.id.notificationTollbarMenu)
        notificationTollbarMenu.isVisible = false
        notificationTollbarMenu.setOnMenuItemClickListener {

            (activity as MainActivity).navigateTo(NotificationFragment(), true)
            true
        }
    }
}
