package com.cab1.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.cab1.R
import com.cab1.adapter.CarscategoryListAdapter
import com.cab1.pojo.CarsCategoryData
import com.example.admin.myapplication.SessionManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.fragment_cancel_trip_bottomsheet.*

class CategorylistBottomSheetFragment : BottomSheetDialogFragment(), View.OnClickListener  {

    private var mListener:  CategoryListener? = null

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var carsCategoryAdapter: CarscategoryListAdapter
    lateinit var sessionManager: SessionManager

    var categoryData:ArrayList<CarsCategoryData?>?= ArrayList()

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.btnCancelTrip -> {


            }

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

        return super.onCreateDialog(savedInstanceState)

    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_cancel_trip_bottomsheet, container, false)

        return view
    }



    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        reasonTextView.visibility=View.GONE
        sessionManager = SessionManager(activity!!)


        headerText.text ="Select Category"
        headerText.gravity = Gravity.START
        backIconImageView.visibility = View.VISIBLE

        btnCancelTrip.visibility=View.GONE
        if (arguments != null) {
            categoryData = arguments?.getSerializable("categoryData") as ArrayList<CarsCategoryData?>?
        }
        layoutManager = LinearLayoutManager(this!!.context!!)


        /*val list = ArrayList<String>()

        list.add("Please tell us why you want to cancel")
        list.add("Driver denied to go to destination")
        list.add("Driver denied to come to pickup")
        list.add("Expected a shorter wait time")
        list.add("Unable to contact driver")
        list.add("My reason is not listed")*/



        cancelTripRecyclerView.layoutManager = layoutManager
        carsCategoryAdapter = CarscategoryListAdapter(
           activity!!,
            categoryData,
            object :CarscategoryListAdapter.OnItemClickListener{
                override fun itemClick(pos: Int, categoryName: String, categoryId: String) {
                    dismiss()

                    if(mListener!=null)
                         mListener!!.onSelectCarsCategory(categoryId,categoryName)
                }

            })
        cancelTripRecyclerView.adapter = carsCategoryAdapter

        backIconImageView.setOnClickListener {
            dismiss()
        }

        dialogCloseIcon.setOnClickListener {
            dismiss()
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener = if (parent != null) {
            parent as CategoryListener
        } else {
            context as CategoryListener
        }
    }

    interface CategoryListener {

        fun onSelectCarsCategory(categoryId: String, categoryName: String)

    }


}