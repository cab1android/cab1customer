package com.cab1.fragment


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Location
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.cab1.DestinationLocationActivity
import com.cab1.MainActivity
import com.cab1.PaymentActivity
import com.cab1.R
import com.cab1.adapter.CarsLocalTripAdapter
import com.cab1.adapter.CarsRentalAdapter
import com.cab1.adapter.OutstationoffersAdapter
import com.cab1.api.RestCallback
import com.cab1.api.RestClient
import com.cab1.cab1.driver.util.Googleutil
import com.cab1.driver.model.CancelTripMasterModel
import com.cab1.driver.model.CouponofferModel
import com.cab1.model.AddAddressModel
import com.cab1.model.GetDriverLocationModel
import com.cab1.model.GetPriceModel
import com.cab1.model.GoogleDirectionModel
import com.cab1.notification.Push
import com.cab1.notification.RefData
import com.cab1.pojo.*
import com.cab1.util.LocationProvider
import com.cab1.util.LocationProvider.Companion.CONNECTION_FAILURE_RESOLUTION_REQUEST
import com.cab1.util.MarkerAnimation
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.Polyline
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.tabs.TabLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.google.maps.android.SphericalUtil
import kotlinx.android.synthetic.main.bottom_trip_view.*
import kotlinx.android.synthetic.main.confirm_trip_bottomview.*
import kotlinx.android.synthetic.main.confirming_book_car.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.outstation_trip.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.fixedRateTimer


class MainFragment : Fragment(), OnMapReadyCallback, View.OnClickListener, GoogleMap.OnCameraMoveStartedListener,
    GoogleMap.OnCameraMoveListener,
    GoogleMap.OnCameraMoveCanceledListener,
    GoogleMap.OnCameraIdleListener,
    CancelBottomSheetFragment.CancelTripListener, SelectDateTimeBottomSheetFragment.ConfirmBookNowListener,
    SelectCouponCodeBottomSheetFragment.CouponcodeListener {


    var bookingPickupTime = ""
    private lateinit var carsTripAdapter: CarsLocalTripAdapter
    private lateinit var carsRentalAdapter: CarsRentalAdapter
    private lateinit var outstationoffersAdapter: OutstationoffersAdapter
    private lateinit var mMap: GoogleMap
    var isfromDisabled = false
    var disscountAmount: Double = 0.00
    var totalAmountDiscount: Double = 0.00
    var disscountType = ""
    var finalAmount = ""
    var disscontPercentage: Double = 0.00
    private var mDestinationMarker: Marker? = null
    private var mfromMarker: Marker? = null
    private var blackPolyLine: Polyline? = null
    lateinit var mBottomSheetDialog: BottomSheetDialog
    var mCarMarker: Marker? = null
    var pickupLat = ""
    var pickupLongi = ""
    var dropLat = ""
    var dropLongi = ""
    var pushType = ""
    var outstationPickupLat = ""
    var outstationPickupLongi = ""
    var outstationDropLat = ""
    var outstationDropLongi = ""
    var couponCode = ""
    var radioSelection = ""
    var bookingType = "Local"
    var bookingSubType = "Book Now"
    var from = true
    var call: Call<List<GetDriverLocation>>? = null
    var isFirstTime = false
    lateinit var sessionManager: SessionManager
    var destination = false
    var isDriverReached = false
    private var priceData = ArrayList<Data1>()
    private var localTripData = ArrayList<LocalPrice>()
    private var rentalTripData = ArrayList<RentalPrice>()
    private var addAddressData = ArrayList<Useraddres>()
    var displayOutStationData = ArrayList<OutStationPrice>()
    var directionRouteResponse: GoogleDirection? = null
    private lateinit var locationProvider: LocationProvider
    private lateinit var dialogText: TextView
    private lateinit var localLayoutManager: LinearLayoutManager
    private lateinit var rentalLayoutManager: LinearLayoutManager
    var destinationEdited = false
    var totalAmount: Double = 0.0
    var extra: Double = 0.0
    var googleDirectionModel: GoogleDirectionModel? = null
    var currentLatLong: LatLng? = null
    var destinationLatLong: LatLng? = null
    var driversLatLng: LatLng? = null
    var points = ArrayList<LatLng>()
    var referData: RefData? = null
    lateinit var getDriverLocationCall: GetDriverLocationModel
    var currentTime: Calendar? = null
    var updateTime: Calendar? = null
    var differenceTimemins: Long = 0
    var cabchecked = false
    var isCouponApplied = false
    var primaryAddressFrom = ""
    var secondaryAddressFrom = ""
    var v: View? = null
    var myTimer = Timer()
    var isTrackingOn = false
    var mContext: Context? = null
    var count = 0
    var myBookingTimer = Timer()
    lateinit var mapFragment: MapView
    private val MY_PERMISSIONS_REQUEST_CALL_PHONE = 501
    var phoneNumber = ""
    var carMarkerList: ArrayList<Marker> = ArrayList()
    var defaultZoom = 15f
    var push: Push? = null
    var fromBookingStatus: String = ""
    var bookingId = "-1"
    var outstationType = "One Way"
    var localSelectedPos = -1
    var rentalSelectedPos = -1
    var isMapLoaded = false
    var pickupAddress: java.util.ArrayList<Address> = ArrayList()
    var dropAddress: java.util.ArrayList<Address> = ArrayList()
    private var cancelTripMaster = ArrayList<CancelTripData>()
    private var couponofferData = ArrayList<CouponofferData>()
    val currentLocation: LatLng? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_main, container, false)

            mapFragment = v!!.findViewById(R.id.map) as MapView
            mapFragment.onCreate(savedInstanceState)

            mapFragment.onResume()
            MapsInitializer.initialize(activity!!.applicationContext)

            mapFragment.getMapAsync(this)
            driversLatLng = null

        }
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(this.mContext!!)
        mainMenu_ImageView.setImageResource(R.drawable.menu_icon)


        if (arguments != null) {
            if (arguments!!.containsKey("push"))
                push = arguments!!.getSerializable("push") as Push
            fromBookingStatus = arguments!!.getString("fromBookingStatus", "")
            if (fromBookingStatus.isNotEmpty() && push != null) {
                pushHandle(push!!)
            }

        }

        isFirstTime = true

        returnDateTextView.setTextColor(mContext!!.resources.getColor(R.color.text_secondary))
        returnTimeTextView.setTextColor(mContext!!.resources.getColor(R.color.text_secondary))
        ll_ReturnDate.isClickable = false

        val c = Calendar.getInstance()
        val df = SimpleDateFormat("EEE,dd MMM")
        val tdf = SimpleDateFormat("hh:mm a")
        outStationPickupDateTv.text = df.format(c.time)
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 30)
        outStationPickupTimeTv.text = tdf.format(c.time)

        localLayoutManager = LinearLayoutManager(activity as MainActivity, LinearLayout.HORIZONTAL, false)
        recyclertCarItem.layoutManager = localLayoutManager

        carsTripAdapter = CarsLocalTripAdapter(activity as MainActivity,
            directionRouteResponse, localTripData, object : CarsLocalTripAdapter.OnItemClick {
                override fun onFareBreakupClick(pos: Int) {

                    var basefare = ""
                    val args = Bundle()

                    if (localTripData[pos].priceType.equals("specialprice") && !localTripData[pos].specialpriceoptions.isNullOrEmpty()) {
                        basefare = localTripData[pos].specialpriceoptions[0].optionBasefare
                        args.putString(
                            "ratePerKM",
                            "" + localTripData[pos].specialpriceoptions[0].optionAdditionalKmCharge
                        )
                        args.putString("timeCharge", "" + localTripData[pos].specialpriceoptions[0].optionPerMinCharge)
                    } else {
                        if (localTripData[pos].optionMinBaseFare.toDouble() > calculateCharge(pos, withGST = "No")) {
                            basefare = localTripData[pos].optionMinBaseFare
                        } else {
                            basefare = localTripData[pos].optionBasefare
                        }

                        args.putString("ratePerKM", "" + localTripData[pos].optionAdditionalKmCharge)
                        args.putString("timeCharge", "" + localTripData[pos].optionPerMinCharge)
                    }

                    args.putString("baseFare", "" + basefare)
                    args.putBoolean("isDiscount", isCouponApplied)
                    args.putString("from", "main")
                    args.putString("totalAmountDiscount", "" + MyUtils.discountFormat(totalAmountDiscount))
                    args.putString(
                        "waitingFare",
                        "" + localTripData[pos].priceWaitingPerMinuteCharge
                    )

                    if (isCouponApplied) {

                        if (localTripData[pos].optionMinBaseFare.toDouble() > disscountAmount) {

                            var payableWithGst = 0.00

                            payableWithGst =
                                localTripData[pos].optionMinBaseFare.toDouble() + localTripData[pos].optionMinBaseFare.toDouble() * localTripData[pos].categoryGST.toDouble() / 100

                            args.putString(
                                "totalPayableValue",
                                "" + payableWithGst
                            )
                        } else {

                            args.putString(
                                "totalPayableValue",
                                "" + disscountAmount
                            )
                        }


                    } else {
                        args.putString(
                            "totalPayableValue",
                            "" + calculateCharge(pos)
                        )
                    }
//                            args.putString("totalPayableValue", "" + MyUtils.priceFormat(calculateCharge()))
                    args.putString(
                        "payextra",
                        "" + extra
                    )

                    val bottomSheetFragment = FareBreakupBottomSheetFragment()
                    bottomSheetFragment.arguments = args
                    bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
                }

                override fun onClicklisneter(pos: Int, carsName: String) {

                    localSelectedPos = pos

                    checkTimeDifference()

                    setCarsMarker(priceData[0].localPrice[pos].categoryID)
                    if (priceData[0].localPrice[pos].drivers.isNotEmpty()) {
                        setButtonsEnable()
                    } else {
//                        setButtonsDisable()
                        btnbooknow.isEnabled = false
                        btnbooknow.alpha = .5f
                    }

                    for (i in 0 until localTripData.size) {
                        localTripData[i].selected = false
                        if (i == pos)
                            localTripData[i].selected = true
                    }
                    carsTripAdapter.notifyDataSetChanged()

                }

            })
        recyclertCarItem.adapter = carsTripAdapter

        rentalLayoutManager = LinearLayoutManager(activity as MainActivity, LinearLayout.HORIZONTAL, false)
        recyclerRentalCarItem.layoutManager = rentalLayoutManager
        carsRentalAdapter = CarsRentalAdapter(activity as MainActivity,
            rentalTripData, object :
                CarsRentalAdapter.OnItemClick {

                override fun onClicklisneter(pos: Int, carsName: String) {


                    for (i in 0 until rentalTripData[0].rentalpackages.size) {
                        rentalTripData[0].rentalpackages[i].selected = false
                    }

                    rentalTripData[0].rentalpackages[pos].selected = true
                    rentalSelectedPos = pos
                    carsRentalAdapter.notifyDataSetChanged()
                    checkTimeDifference()

                }

            })

        recyclerRentalCarItem.adapter = carsRentalAdapter
        rg_oneway_round_trip.setOnCheckedChangeListener { radioGroup, i ->

            if (rb_oneway_.isChecked) {
                returnDateTextView.setTextColor(mContext!!.resources.getColor(R.color.text_secondary))
                returnTimeTextView.setTextColor(mContext!!.resources.getColor(R.color.text_secondary))
                ll_ReturnDate.isClickable = false
                outstationType = "One Way"
            } else {
                returnDateTextView.setTextColor(mContext!!.resources.getColor(R.color.text_primary))
                returnTimeTextView.setTextColor(mContext!!.resources.getColor(R.color.text_primary))
                ll_ReturnDate.isClickable = true
                outstationType = "Round Trip"
            }

        }



        btn_outstation_booknow.setOnClickListener {

            displayOutStationData.clear()
            displayOutStationData.addAll(priceData[0].outstationPrice)

            var positions = java.util.ArrayList<Int>()

            if (displayOutStationData != null && displayOutStationData.size > 0) {

//
//            priceData[0].outstationPrice.removeAll(priceData[0].outstationPrice,outstationType)

                for (i in 0 until displayOutStationData.size) {
                    if (!displayOutStationData[i].outstationType.equals(outstationType)) {
                        positions.add(i)
                        Log.d("System out", "" + i)
                    }
                }

                val itr = displayOutStationData.iterator()
                var pos = 0
                var index = 0
                while (itr.hasNext()) {
                    itr.next()
                    if (pos >= positions.size) {
                        break
                    }
                    if (positions[pos] === index) {
                        itr.remove()
                        pos++
                    }

                    index++
                }
            }



            if (outstationType.equals("One Way", true)) {
                if (TextUtils.isEmpty(edittext_from_outstation.text.toString())) {
                    (activity as MainActivity).showSnackBar("Please enter pickup location")
                } else if (TextUtils.isEmpty(edit_outstation_destnation.text.toString())) {
                    (activity as MainActivity).showSnackBar("Please enter destination location")

                } else if (displayOutStationData == null || displayOutStationData.size == 0) {

                    (activity as MainActivity).showSnackBar("No cars available, try with different drop location")

                } else {

                    val args = Bundle()
                    args.putSerializable("priceData", priceData as Serializable)
                    args.putString("pickupLat", pickupLat)
                    args.putString("pickupLongi", pickupLongi)
                    args.putString("dropLat", dropLat)
                    args.putString("dropLongi", dropLongi)
                    args.putString("bookingType", bookingType)
                    args.putString("destinationLocation", edit_outstation_destnation.text.toString())
                    args.putString("pickupLocation", edittext_from_outstation.text.toString())
                    Log.d("System out", "main frag : " + edittext_from_outstation.text.toString())
                    args.putString("outstationType", outstationType)
                    args.putString("bookingSubType", bookingSubType)
                    args.putString("bookingPickupTime", bookingPickupTime)
                    args.putString("outStationPickupDate", outStationPickupDateTv.text.toString())
                    args.putString("outStationPickupTime", outStationPickupTimeTv.text.toString())

                    if (returnDateTextView.text.toString().equals("Select Date")) {
                        args.putString("returnDate", "")
                    } else {
                        args.putString("returnDate", returnDateTextView.text.toString())
                    }

                    if (returnTimeTextView.text.toString().equals("Time")) {
                        args.putString("returnTime", "")
                    } else {
                        args.putString("returnTime", returnTimeTextView.text.toString())
                    }
// args.putSerializable("directionRouteResponse", directionRouteResponse as Serializable)

                    val bottomSheetFragment = ChoosePackageListingBottomsheetFragment()
                    bottomSheetFragment.arguments = args
                    bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
                }

            } else {
                if (TextUtils.isEmpty(edittext_from_outstation.text.toString())) {
                    (activity as MainActivity).showSnackBar("Please enter pickup location")
                } else if (TextUtils.isEmpty(edit_outstation_destnation.text.toString())) {
                    (activity as MainActivity).showSnackBar("Please enter destination location")

                } else if (TextUtils.isEmpty(bookingPickupTime)) {
                    (activity as MainActivity).showSnackBar("Please select date and time")

                } else if (TextUtils.isEmpty(bookingPickupTime)) {
                    (activity as MainActivity).showSnackBar("Please select date and time")

                } else if (priceData[0].outstationPrice == null || priceData[0].outstationPrice.size == 0) {

                    (activity as MainActivity).showSnackBar("No cars available, try with different drop location")

                } else {

                    val args = Bundle()
                    args.putSerializable("priceData", priceData as Serializable)
                    args.putString("pickupLat", pickupLat)
                    args.putString("pickupLongi", pickupLongi)
                    args.putString("dropLat", dropLat)
                    args.putString("dropLongi", dropLongi)
                    args.putString("bookingType", bookingType)
                    args.putString("destinationLocation", edit_outstation_destnation.text.toString())
                    args.putString("pickupLocation", edittext_from_outstation.text.toString())
                    args.putString("outstationType", outstationType)
                    args.putString("bookingSubType", bookingSubType)
                    args.putString("bookingPickupTime", bookingPickupTime)
                    args.putString("outStationPickupDate", outStationPickupDateTv.text.toString())
                    args.putString("outStationPickupTime", outStationPickupTimeTv.text.toString())
                    args.putString("returnDate", returnDateTextView.text.toString())
                    args.putString("returnTime", returnTimeTextView.text.toString())
                    // args.putSerializable("directionRouteResponse", directionRouteResponse as Serializable)

                    val bottomSheetFragment = ChoosePackageListingBottomsheetFragment()
                    bottomSheetFragment.arguments = args
                    bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
                }
            }

        }

        ll_edit_from.setOnClickListener {
            if (mMap != null && mMap.isMyLocationEnabled && mMap.myLocation != null) {
                openDestinationActivity("Pickup", 2)
            }
        }
        edittext_from_outstation.setOnClickListener {
            ll_edit_from.performClick()
        }
        edit_outstation_destnation.setOnClickListener {
            ll_edit_destnation.performClick()

        }
        textinput_to.setOnClickListener {
            ll_edit_destnation.performClick()
        }
        textinput_from.setOnClickListener {
            ll_edit_from.performClick()
        }

        ll_edit_destnation.setOnClickListener {


            openDestinationActivity("Destination", 1)
        }

        swapImageView.setOnClickListener {

            directionRouteResponse = null

            val fromTv = edittext_from_outstation.text.toString()
            val destinationTv = edit_outstation_destnation.text.toString()

            val swapPickupLat = pickupLat
            val swapPickupLongi = pickupLongi
            val swapDropLat = dropLat
            val swapDropLongi = dropLongi

            pickupLat = swapDropLat
            pickupLongi = swapDropLongi
            dropLat = swapPickupLat
            dropLongi = swapPickupLongi

            edittext_from_outstation.text = destinationTv
            edit_outstation_destnation.text = fromTv

            (context as MainActivity).runOnUiThread {
                getPricingList()
            }

        }


        mainMenu_ImageView.setOnClickListener {
            if (mainMenu_ImageView.tag == 1) {
                (activity as MainActivity).onBackPressed()
            } else {

                (activity as MainActivity).openDrawer()
            }

        }

        googleDirectionModel = ViewModelProviders.of(this).get(GoogleDirectionModel::class.java)


        btnconfirmingbook.setOnClickListener(this)
        lldestination.setOnClickListener(this)
        llfrom.setOnClickListener(this)
        fromFavIv.setOnClickListener(this)
        destinationFavIv.setOnClickListener(this)
        img_farebreakupdetails.setOnClickListener(this)
        ll_couponcode.setOnClickListener(this)
        img_notification.setOnClickListener(this)
        img_outstation_toolbar_notification.setOnClickListener(this)
        btnboollater.setOnClickListener(this)
        btn_cancelTrip.setOnClickListener(this)
        ll_selectDate.setOnClickListener(this)
        ll_ReturnDate.setOnClickListener(this)
        centerMapFab.setOnClickListener(this)

        findingDriverDialog()


        getDriverLocationCall = ViewModelProviders.of(this@MainFragment).get(GetDriverLocationModel::class.java)


        (activity as AppCompatActivity).setSupportActionBar(toolbarTop)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbarTop.setNavigationOnClickListener {
            (activity as MainActivity).openDrawer()
        }

        btnbooknow.setOnClickListener {

            bookingSubType = "Book Now"

//            if (directionRouteResponse != null && blackPolyLine != null) {
            if (bookingType.equals("Local") && directionRouteResponse != null && blackPolyLine != null) {

                if (!localTripData.isNullOrEmpty()) {
                    if (destinationTextView.text.toString().trim().length > 0) {

                        var pickupCity = ""
                        var dropCity = ""

                        for (i in 0 until pickupAddress.size) {
                            if (pickupAddress[i].locality != null && pickupAddress[i].locality.length > 0) {
                                pickupCity = pickupAddress[i].locality
                                break
                            }
                        }

                        for (i in 0 until dropAddress.size) {
                            if (dropAddress[i].locality != null && dropAddress[i].locality.length > 0) {
                                dropCity = dropAddress[i].locality
                                break
                            }
                        }


                        if (!pickupCity.equals(dropCity)) {
                            showMessageOKCancel(
                                mContext!!,
                                "Drop location is outside city limits.Continue booking Outstation instead?",
                                "Book Outstation",
                                DialogInterface.OnClickListener { dialog, which ->
                                    // continue button event
                                    setDataTab3()
                                    tabs.getTabAt(2)!!.select()
                                })
                        } else {

                            if (directionRouteResponse == null) {

                                try {
                                    if (btnbooknow != null && bookingSubType == "Book Now") {
                                        btnbooknow.startAnimation()
                                    } else if (btnboollater != null && bookingSubType == "Book Later") {
                                        btnboollater.startAnimation()
                                    }
                                } catch (e: Exception) {
                                    Log.d("System out", "Upper")
                                }


                                drawDirectionRoute(
                                    LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                                    this.destinationLatLong!!
                                )
                            } else {
                                openBookedcar()
                            }

                        }


                    } else {

                        MyUtils.expand(confirmDestinationTextView)

                        Handler().postDelayed(
                            {
                                MyUtils.collapse(confirmDestinationTextView)
                            }, 3000
                        )
                    }
                }


            } else if (bookingType.equals("Rental")) {

                if (!rentalTripData.isNullOrEmpty()) {
                    if (fromLocationNameTextView.text.toString().length > 0) {

                        if (priceData[0].rentalPrice.size > 0) {
                            priceData[0].rentalPrice.clear()
                        }
                        priceData[0].rentalPrice.addAll(rentalTripData)

                        val args = Bundle()
                        args.putSerializable("priceData", priceData as Serializable)
                        args.putString("pickupLat", pickupLat)
                        args.putString("pickupLongi", pickupLongi)
                        args.putString("pickupLocation", fromLocationNameTextView.text.toString())
                        args.putString("bookingType", bookingType)
                        args.putString("bookingSubType", bookingSubType)
                        args.putString("pickupCityName", pickupAddress[0].locality)

//                args.putSerializable("directionRouteResponse", directionRouteResponse as Serializable)

                        val bottomSheetFragment = ChoosePackageListingBottomsheetFragment()
                        bottomSheetFragment.arguments = args
                        bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)

                    } else {
                        MyUtils.expand(confirmDestinationTextView)
                        confirmDestinationTextView.text = "Confirm your Pickup Location"
                        Handler().postDelayed(
                            {
                                MyUtils.collapse(confirmDestinationTextView)
                            }, 3000
                        )
                    }

                }

            } else {
//
                if (destinationTextView.text.toString().length > 0) {
                    drawDirectionRoute(
                        LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                        LatLng(dropLat.toDouble(), dropLongi.toDouble())
                    )
                } else {

                    if (confirmDestinationTextView != null) {
                        MyUtils.expand(confirmDestinationTextView)

                        Handler().postDelayed(
                            {
                                MyUtils.collapse(confirmDestinationTextView)
                            }, 3000
                        )
                    }

                }
            }


        }

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                when (tab?.position) {
                    0 -> {
                        setDataTab1(true)
                    }
                    1 -> {
                        setDataTab2(true)
                    }
                    2 -> {
                        setDataTab3(true)
                    }
                }

            }
        })


        LocalBroadcastManager.getInstance(activity!!)
            .registerReceiver(pushReceiver, IntentFilter(Push.action))
        googleDirectionModel = ViewModelProviders.of(this).get(GoogleDirectionModel::class.java)


    }

    private fun openDestinationActivity(s: String, requestCode: Int) {
        var i = Intent(mContext, DestinationLocationActivity::class.java)
        i.putExtra("lats", "" + currentLatLong!!.latitude)
        i.putExtra("from", s)
        i.putExtra("longs", "" + currentLatLong!!.longitude)
        startActivityForResult(i, requestCode)
        activity!!.overridePendingTransition(
            R.anim.slide_in_right,
            R.anim.slide_out_left
        )
    }

    override fun onConfirmation(bookingPickupTime: String, isPickup: Boolean) {

        this.bookingPickupTime = bookingPickupTime

        if (bookingType.equals("Outstation")) {

            if (isPickup) {
                outStationPickupDateTv.text = MyUtils.formatDate(
                    bookingPickupTime,
                    "yyyy-MM-dd HH:mm:ss",
                    "EEE,dd MMM"
                )
                outStationPickupTimeTv.text = MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "hh:mm a")
                returnDateTextView.text = "Select Date"
                returnTimeTextView.text = "Time"
            } else {

                returnDateTextView.text = MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "EEE,dd MMM")
                returnTimeTextView.text = MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "hh:mm a")
            }


        } else {
            openBookedcar()
        }

    }

    fun findingDriverDialog() {
        mBottomSheetDialog = BottomSheetDialog(this.mContext!!)
        var sheetView = layoutInflater.inflate(R.layout.finding_driver_layout, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.setCancelable(false)
        dialogText = sheetView.findViewById<TextView>(R.id.dialogText)


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    fun setDataTab1(isAnim: Boolean = false) {
        val params =
            FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT)
        params.gravity = Gravity.BOTTOM
        ll_bottomView.layoutParams = params
        ll_boockcar.visibility = View.GONE
        lldestination.visibility = View.VISIBLE
        recyclertCarItem.visibility = View.VISIBLE
        recyclerRentalCarItem.visibility = View.GONE
        if (blackPolyLine == null) {
            central_marker.visibility = View.VISIBLE
        }
        ll_booktrip.visibility = View.VISIBLE
        ll_couponcode.visibility = View.GONE
        couponCodeUpperBaseLine.visibility = View.GONE
        ll_outstation.visibility = View.GONE
        centerMapFab.visibility = View.VISIBLE
        isfromDisabled = false

        if (directionRouteResponse != null && destinationTextView.text.toString().length > 0) {
            drawRoutePolyLine(
                directionRouteResponse!!,
                LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                LatLng(dropLat.toDouble(), dropLongi.toDouble())
            )
        } else {

            if (!isFirstTime && destinationTextView.text.toString().trim().length > 0) {
                drawRouteForPrice(
                    LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                    LatLng(dropLat.toDouble(), dropLongi.toDouble())
                )
            }

        }

        if (localTripData != null && localTripData.size > 0) {

            if (localTripData[0].drivers != null && localTripData[0].drivers.size > 0) {

                for (i in 0 until localTripData.size) {
                    if (localTripData[i].selected && localTripData[i].drivers.size > 0) {
                        setButtonsEnable()
                        break
                    } else {
                        setButtonsDisable()
                    }
                }

            }

        }

        bookingType = "Local"

        ll_booktrip.visibility = View.VISIBLE
        btnconfirmingbook.visibility = View.GONE
        setCarsLocalTripAdapter()

        if (isAnim) {

            lldestination.animation = AnimationUtils.loadAnimation(
                activity,
                R.anim.abc_fade_in
            )

            if (bottomLayout.tag != null && bottomLayout.tag == 0) {
                bottomLayout.animation = AnimationUtils.loadAnimation(
                    activity,
                    R.anim.abc_slide_in_top
                )

                toolbarTop.animation = AnimationUtils.loadAnimation(
                    activity,
                    R.anim.abc_slide_out_top
                )
                Handler().postDelayed(
                    {
                        toolbarTop.visibility = View.GONE
                        topLayout.animation = AnimationUtils.loadAnimation(
                            activity,
                            R.anim.abc_slide_in_top
                        )

                        topLayout.visibility = View.VISIBLE


                    }
                    , 200)

            }
            bottomLayout.tag = 1
        }

    }

    fun setButtonsEnable() {
        btnbooknow.isEnabled = true
        btnbooknow.alpha = 1f

    }

    fun setButtonsDisable() {
        btnbooknow.isEnabled = false
        btnbooknow.alpha = .5f

    }

    fun backConfirmBooking() {


        if (ll_boockcar != null && ll_boockcar.visibility == View.VISIBLE) {

            ll_boockcar.visibility = View.GONE
            recyclertCarItem.visibility = View.VISIBLE
            ll_booktrip.visibility = View.VISIBLE
            btnconfirmingbook.visibility = View.GONE
            mainMenu_ImageView.tag = 0
            disscountAmount = 0.00
            isfromDisabled = false
            isCouponApplied = false
            mainMenu_ImageView.setImageResource(R.drawable.menu_icon)
            ll_couponcode.visibility = View.GONE

            if (confirmBottomLayout.visibility == View.VISIBLE) {
                confirmBottomLayout.visibility = View.GONE
                bottomLayout.visibility = View.VISIBLE
            }
            if (blackPolyLine != null) {
                central_marker.visibility == View.VISIBLE
            }

//            if (mfromMarker != null) {
//                mfromMarker!!.remove()
//            }
//
//            if (mDestinationMarker != null) {
//                mDestinationMarker!!.remove()
//            }

            if (blackPolyLine != null) {
                blackPolyLine!!.remove()
            }

        }
    }

    private val pushReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action!!.equals(Push.action, ignoreCase = true) && intent.hasExtra("PushData")) {
                var push = intent.getSerializableExtra("PushData") as Push
                pushHandle(push)
            } else if (intent.hasExtra("from")) {
                (mContext as Activity).runOnUiThread {

                    //                    mBottomSheetDialog.show()
//
//                    val bookingId = intent.extras.getString("bookingId")
//
//                    myBookingTimer = fixedRateTimer("timer", false, 0, 30000) {
//
//                        getMyBookingData(bookingId)
//
//                    }

                }
            }
        }
    }


    private fun setDriverData(
        driverName: String,
        bookingOTP: String,
        driverRatting: String,
        drvcarModel: String,
        drvcarNo: String,
        driverMobile: String,
        drvcarFrontImage: String,
        driverProfilePic: String
    ) {

        tv_confirmTrip_driverName.text = driverName
        tv_confirmTrip_otp.text = "OTP:" + bookingOTP
        ratingBar_confirmTrip.rating = driverRatting.toFloat()
        img_confirmTrip_car_name.text = drvcarModel
        img_confirmTrip_car_number.text = drvcarNo
        phoneNumber = driverMobile
        btnCallDriver_confirmTrip.setOnClickListener {
            call(phoneNumber)
        }
        img_confirmTrip_car.setImageURI(RestClient.image_category_url + drvcarFrontImage)
        img_confirmTrip_Driver.setImageURI(RestClient.image_driver_url + driverProfilePic)
        img_confirmTrip_car.background = activity!!.getDrawable(R.drawable.ic_car_background_unselected_24dp)

    }

    private fun setDataTab2(isAnim: Boolean = false) {

        val params =
            FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT)
        params.gravity = Gravity.BOTTOM
        ll_bottomView.layoutParams = params
        ll_boockcar.visibility = View.GONE
        recyclertCarItem.visibility = View.GONE
        recyclerRentalCarItem.visibility = View.VISIBLE
        ll_booktrip.visibility = View.VISIBLE
        ll_couponcode.visibility = View.GONE
        couponCodeUpperBaseLine.visibility = View.GONE
        ll_outstation.visibility = View.GONE
        centerMapFab.visibility = View.VISIBLE
        mainMenu_ImageView.tag = 0
        setCarsRentalAdapter()
        setButtonsEnable()
        from = true
        bookingType = "Rental"
        ll_booktrip.visibility = View.VISIBLE
        btnconfirmingbook.visibility = View.GONE
        mainMenu_ImageView.setImageResource(R.drawable.menu_icon)

        if (mMap != null) {
            mMap.clear()
            blackPolyLine = null
            central_marker.visibility = View.VISIBLE
            central_marker.setImageResource(R.drawable.map_pin_green)
        }

        if (isAnim) {
            lldestination.animation = AnimationUtils.loadAnimation(
                activity,
                R.anim.abc_fade_out
            )



            if (bottomLayout.tag != null && bottomLayout.tag == 0) {
                bottomLayout.animation = AnimationUtils.loadAnimation(
                    activity,
                    R.anim.abc_slide_in_top
                )

                toolbarTop.animation = AnimationUtils.loadAnimation(
                    activity,
                    R.anim.abc_slide_out_top
                )
                Handler().postDelayed(
                    {
                        toolbarTop.visibility = View.GONE
                        topLayout.animation = AnimationUtils.loadAnimation(
                            activity,
                            R.anim.abc_slide_in_top
                        )

                        topLayout.visibility = View.VISIBLE


                    }
                    , 200)

                bottomLayout.tag = 1
            }
        }
        lldestination.visibility = View.GONE
//        setFromLocationSelected()


    }

    private fun setDataTab3(isAnim: Boolean = false) {

        bottomLayout.tag = 0
        mainMenu_ImageView.tag = 0

        ll_ReturnDate.isClickable = false

        outstationPickupLat = pickupLat
        outstationPickupLongi = pickupLongi
        outstationDropLat = dropLat
        outstationDropLongi = dropLongi

        no_car_found.visibility = View.GONE
        rv_no_cars.visibility = View.GONE
        ll_bottomView.gravity = Gravity.TOP
        ll_boockcar.visibility = View.GONE
        ll_booktrip.visibility = View.GONE
        ll_couponcode.visibility = View.GONE
        central_marker.visibility = View.GONE
        lldestination.visibility = View.VISIBLE
        recyclerRentalCarItem.visibility = View.GONE
        recyclertCarItem.visibility = View.GONE
        bookingType = "Outstation"
        ll_outstation.visibility = View.VISIBLE
        btnconfirmingbook.visibility = View.GONE
        centerMapFab.visibility = View.GONE
        btnboollater.isEnabled = true
        btnbooknow.isEnabled = true
        mainMenu_ImageView.setImageResource(R.drawable.menu_icon)
        val params = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.MATCH_PARENT
        )
        params.gravity = Gravity.TOP
        ll_bottomView.layoutParams = params
        getCouponofferApi("", pickupAddress[0].locality, "", "")
        if (couponofferData != null) {
            setOutstationOffersAdapter()
        }

//        edittext_from_outstation.text = fromLocationNameTextView.text.toString().trim()
//        edit_outstation_destnation.text = destinationTextView.text.toString().trim()


        if (isAnim) {

            rb_oneway_.isChecked = true
            bottomLayout.animation = AnimationUtils.loadAnimation(
                activity,
                R.anim.abc_slide_in_bottom
            )
            topLayout.animation = AnimationUtils.loadAnimation(
                activity,
                R.anim.abc_slide_out_top
            )
            Handler().postDelayed(
                {
                    topLayout.visibility = View.GONE
                    toolbarTop.animation = AnimationUtils.loadAnimation(
                        activity,
                        R.anim.abc_slide_in_top
                    )


                    toolbarTop.visibility = View.VISIBLE


                }
                , 200)
        }


    }

    private fun setCarsRentalAdapter() {
        carsRentalAdapter.notifyDataSetChanged()

        if (priceData.size > 0 && priceData[0].rentalPrice != null && priceData[0].rentalPrice.size > 0) {


            recyclerRentalCarItem.visibility = View.VISIBLE
            no_car_found.visibility = View.GONE
            rv_no_cars.visibility = View.GONE

        } else {
            recyclerRentalCarItem.visibility = View.GONE
            no_car_found.visibility = View.VISIBLE
            rv_no_cars.visibility = View.VISIBLE

        }
    }

    private fun checkTimeDifference() {
        updateTime = Calendar.getInstance()
        try {
            differenceTimemins = MyUtils.timeDifference(currentTime!!.timeInMillis, updateTime!!.timeInMillis)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (differenceTimemins > 5) {
            getPricingList()

        }
    }

    private fun setCarsLocalTripAdapter() {

        if (priceData != null && priceData.size > 0 && priceData[0].localPrice != null && priceData[0].localPrice.isNotEmpty()) {
            for (i in 0 until priceData[0].localPrice.size) {
                if (priceData[0].localPrice[i].selected) {

                    cabchecked = true
                }
            }
            if (!cabchecked)
                if (priceData[0].localPrice != null && priceData[0].localPrice.size > 0)
                    priceData[0].localPrice[0].selected = true

            carsTripAdapter.notifyDataSetChanged()
            recyclertCarItem.visibility = View.VISIBLE
            no_car_found.visibility = View.GONE
            rv_no_cars.visibility = View.GONE

        } else {
            recyclertCarItem.visibility = View.GONE
            no_car_found.visibility = View.VISIBLE
            rv_no_cars.visibility = View.VISIBLE
        }
    }


    private fun setOutstationOffersAdapter() {
        recycleroutstationoffersItem.layoutManager =
            LinearLayoutManager(activity as MainActivity, LinearLayout.HORIZONTAL, false)
        outstationoffersAdapter =
            OutstationoffersAdapter(
                activity as MainActivity,
                couponofferData,
                object : OutstationoffersAdapter.OnItemClickListener {
                    override fun itemClick(pos: Int, code: String) {
                        // getCouponofferApi("", "", "", "local_outstation")
                    }
                })
        recycleroutstationoffersItem.adapter = outstationoffersAdapter
        outstationoffersAdapter.notifyDataSetChanged()
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        mMap?.uiSettings?.isScrollGesturesEnabled = false
        topLayout.visibility = View.VISIBLE
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isCompassEnabled = false
        mMap.uiSettings.isMyLocationButtonEnabled = false
        mMap.isMyLocationEnabled = true
        mMap.setOnCameraIdleListener(this)
        mMap.setOnCameraMoveStartedListener(this)
        mMap.setOnCameraMoveListener(this)
        mMap.setOnCameraMoveCanceledListener(this)
        mMap.setMinZoomPreference(12f)
        mMap.setMaxZoomPreference(18f)
        centerMapFab.visibility = View.VISIBLE

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    activity, R.raw.map_style
                )
            )


        } catch (e: Exception) {
            Log.e("Map Style", "Can't find style. Error: ", e)
        }

        if (push != null) {
            pushHandle(push!!)
        }
        connectLocation()
    }

    private fun openBookedcar() {

        central_marker.visibility = View.GONE
        ll_boockcar.visibility = View.VISIBLE
        recyclertCarItem.visibility = View.GONE
        recyclerRentalCarItem.visibility = View.GONE
        ll_booktrip.visibility = View.GONE
        btnconfirmingbook.visibility = View.VISIBLE
        ll_couponcode.visibility = View.VISIBLE
        ll_coupon_code_text.visibility = View.GONE
        couponCodeUpperBaseLine.visibility = View.VISIBLE
        isfromDisabled = true
        mainMenu_ImageView.tag = 1
        mainMenu_ImageView.setImageResource(R.drawable.back_icon)
        tv_boockedcar_price.text = "" + MyUtils.priceFormat(calculateCharge())
        disscountAmount = calculateCharge()
        lldestination.setBackgroundColor(mContext!!.resources.getColor(R.color.whiteselector))
        llfrom.setBackgroundColor(mContext!!.resources.getColor(R.color.white))
        setPickupMarker(pickupLat.toDouble(), pickupLongi.toDouble())
        setDropMarker(dropLat.toDouble(), dropLongi.toDouble())

//        bounceMarker(mMap, this.mDestinationMarker as Marker)

        destinationEdited = true

        for (i in 0 until localTripData.size) {

            if (localTripData[i].selected) {
                tv_boock_carsname.text = localTripData[i].categoryName
//                tvdriver_boockcar_eta_time.text = localTripData[i].categoryName
                if (localTripData[i].drivers.size > 0) {
                    tvdriver_boockcar_eta_time.text = localTripData[i].drivers[0].etaMin + "min"
                }

                val imageUri =
                    Uri.parse(RestClient.image_category_url + localTripData[i].categoryImage)

                img_boocked_cars.setImageURI(imageUri)

                tv_boock_ridersize.text = "1 - " + localTripData[i].categorySeats

                break

            }

        }

        btnconfirmingbook.setOnClickListener {

            //            if (!isCouponApplied) {
//                finalAmount = calculateCharge().toString()
//            }

            if (bookingSubType.equals("Book Now")) {

                priceData[0].localPrice.clear()
                priceData[0].localPrice.addAll(localTripData)

                var i = Intent(mContext, PaymentActivity::class.java)
                i.putExtra("priceData", priceData as Serializable)
                i.putExtra("directionRouteResponse", directionRouteResponse as Serializable)
                i.putExtra("longs", pickupLongi)
                i.putExtra("dropLat", dropLat)
                i.putExtra("dropLongi", dropLongi)
                i.putExtra("pickupLat", pickupLat)
                i.putExtra("pickupLongi", pickupLongi)
                i.putExtra("bookingType", bookingType)
                i.putExtra("disscountAmount", "" + disscontPercentage)
                i.putExtra("disscountType", "" + disscountType)
                i.putExtra("finalAmount", "" + finalAmount)
                i.putExtra("couponCode", couponCode)
                for (i1 in 0 until localTripData.size) {

                    if (localTripData[i1].selected) {
                        i.putExtra("localCarsposition", i1)
                    }
                }
                i.putExtra("bookingSubType", bookingSubType)
                i.putExtra("fromLocation", fromLocationNameTextView.text.toString())
                i.putExtra("destinationLocation", destinationTextView.text.toString())
                startActivityForResult(i, 3)
                activity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
                    activity!!.overridePendingTransition(
                        R.anim.slide_in_bottom,
                        R.anim.stay
                    )
                }
            } else {
                val i = Intent(activity!!, PaymentActivity::class.java)
                i.putExtra("priceData", priceData as Serializable)
                i.putExtra("directionRouteResponse", directionRouteResponse as Serializable)
                i.putExtra("longs", pickupLongi)
                i.putExtra("dropLat", dropLat)
                i.putExtra("dropLongi", dropLongi)
                i.putExtra("pickupLat", pickupLat)
                i.putExtra("couponCode", couponCode)
                i.putExtra("pickupLongi", pickupLongi)
                i.putExtra("bookingType", bookingType)
                i.putExtra("disscountAmount", "" + disscontPercentage)
                i.putExtra("disscountType", "" + disscountType)
                i.putExtra("finalAmount", "" + finalAmount)
                for (i1 in 0 until localTripData.size) {

                    if (localTripData[i1].selected) {
                        i.putExtra("localCarsposition", i1)
                    }
                }
                i.putExtra("bookingSubType", bookingSubType)
                i.putExtra("fromLocation", fromLocationNameTextView.text.toString())
                i.putExtra("destinationLocation", destinationTextView.text.toString())
                i.putExtra("bookingPickupTime", bookingPickupTime)

                activity!!.startActivityForResult(i, 3)
                activity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
        }

    }


    @SuppressLint("NewApi")
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnconfirmingbook -> {
                (activity as MainActivity).navigateTo(PaymentFragment(), true)
            }
            R.id.btnboollater -> {

                bookingSubType = "Book Later"

                btnboollater.setTag(1)

                if (bookingType.equals("Local")) {

                    if (destinationTextView.text.toString().trim().length > 0) {

                        var pickupCity = ""
                        var dropCity = ""

                        for (i in 0 until pickupAddress.size) {
                            if (pickupAddress[i].locality != null && pickupAddress[i].locality.length > 0) {
                                pickupCity = pickupAddress[i].locality
                                break
                            }
                        }

                        for (i in 0 until dropAddress.size) {
                            if (dropAddress[i].locality != null && dropAddress[i].locality.length > 0) {
                                dropCity = dropAddress[i].locality
                                break
                            }
                        }


                        if (!pickupCity.equals(dropCity)) {
                            showMessageOKCancel(
                                mContext!!,
                                "Drop location is outside city limits.Continue booking Outstation instead?",
                                "Book Outstation",
                                DialogInterface.OnClickListener { dialog, which ->
                                    // continue button event
                                    setDataTab3()
                                    tabs.getTabAt(2)!!.select()
                                })
                        } else {

                            try {
                                if (btnbooknow != null && bookingSubType == "Book Now") {
                                    btnbooknow.startAnimation()
                                } else if (btnboollater != null && bookingSubType == "Book Later") {
                                    btnboollater.startAnimation()
                                }
                            } catch (e: Exception) {
                            }


                            drawDirectionRoute(
                                LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                                this.destinationLatLong!!
                            )
                        }


                    } else {
                        MyUtils.expand(confirmDestinationTextView)
                        confirmDestinationTextView.text = "Confirm your Destination"
                        Handler().postDelayed(
                            {
                                MyUtils.collapse(confirmDestinationTextView)
                            }, 3000
                        )
                    }
                } else {
                    if (bookingType.equals("Local")) {
                        priceData[0].localPrice.clear()
                        priceData[0].localPrice.addAll(localTripData)
                    } else {
                        priceData[0].rentalPrice.clear()
                        priceData[0].rentalPrice.addAll(rentalTripData)
                    }

                    val bundle = Bundle()
                    bundle.putString("from", "Book Later")
                    bundle.putSerializable("priceData", priceData as Serializable)
//                    bundle.putSerializable("directionRouteResponse", directionRouteResponse as Serializable)
                    bundle.putString("longs", pickupLongi)
                    bundle.putString("dropLat", dropLat)
                    bundle.putString("dropLongi", dropLongi)
                    bundle.putString("pickupLat", pickupLat)
                    bundle.putString("pickupLongi", pickupLongi)
                    bundle.putString("bookingType", bookingType)
                    bundle.putString("bookingSubType", bookingSubType)
                    bundle.putString("pickupLocation", fromLocationNameTextView.text.toString())
                    bundle.putString("destinationLocation", destinationTextView.text.toString())

                    val bottomSheetFragment = SelectDateTimeBottomSheetFragment()
                    bottomSheetFragment.arguments = bundle
                    bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
                    bottomSheetFragment.setConfirmBookNowListener(this)
                }

            }
            R.id.lldestination -> {
                if (confirmBottomLayout.visibility != View.VISIBLE && ll_boockcar.visibility != View.VISIBLE) {
                    setDestinationSelected()
                }
            }

            R.id.llfrom -> {


                if (confirmBottomLayout.visibility != View.VISIBLE) {
                    setFromLocationSelected()
                }
            }

            R.id.fromFavIv -> {

                var isAlreadyFav = false
                for (i in 0 until sessionManager.get_Authenticate_User().useraddress.size) {


                    if (sessionManager.get_Authenticate_User().useraddress[i].addressLatitude.equals(pickupLat) && sessionManager.get_Authenticate_User().useraddress[i].addressLongitude.equals(
                            pickupLongi
                        )
                    ) {
                        isAlreadyFav = true
                        break
                    }

                }

                if (isAlreadyFav) {
                    MyUtils.showSnackbar(
                        this.mContext!!,
                        "It is already added in favourites",
                        mainFragmentFrameLayout
                    )
                } else {
                    showDialog()
                }

            }

            R.id.destinationFavIv -> {

                var isAlreadyFav = false
                for (i in 0 until sessionManager.get_Authenticate_User().useraddress.size) {


                    if (sessionManager.get_Authenticate_User().useraddress[i].addressLatitude.equals(dropLat) && sessionManager.get_Authenticate_User().useraddress[i].addressLongitude.equals(
                            dropLongi
                        )
                    ) {
                        isAlreadyFav = true
                        break
                    }

                }

                if (isAlreadyFav) {
                    MyUtils.showSnackbar(
                        this.mContext!!,
                        "It is already added in favourite.",
                        mainFragmentFrameLayout
                    )
                } else {
                    showDialog()
                }

            }

            R.id.img_farebreakupdetails -> {

                if (priceData.size > 0 && localTripData.size > 0) {
                    for (i in 0 until localTripData.size) {
                        if (localTripData[i].selected) {

                            var basefare = ""
                            val args = Bundle()


                            if (localTripData[i].priceType.equals("specialprice") && !localTripData[i].specialpriceoptions.isNullOrEmpty()) {
                                basefare = localTripData[i].specialpriceoptions[0].optionBasefare
                                args.putString(
                                    "ratePerKM",
                                    "" + localTripData[i].specialpriceoptions[0].optionAdditionalKmCharge
                                )
                                args.putString(
                                    "timeCharge",
                                    "" + localTripData[i].specialpriceoptions[0].optionPerMinCharge
                                )
                            } else {

                                if (localTripData[i].optionMinBaseFare.toDouble() > calculateCharge(withGST = "No")) {
                                    basefare = localTripData[i].optionMinBaseFare
                                } else {
                                    basefare = localTripData[i].optionBasefare
                                }
                                args.putString("ratePerKM", "" + localTripData[i].optionAdditionalKmCharge)
                                args.putString("timeCharge", "" + localTripData[i].optionPerMinCharge)
                            }

                            args.putString("baseFare", "" + basefare)
                            args.putString("from", "main")
                            args.putBoolean("isDiscount", isCouponApplied)
                            args.putString("totalAmountDiscount", "" + MyUtils.discountFormat(totalAmountDiscount))
                            args.putString(
                                "waitingFare",
                                "" + localTripData[i].priceFreeWaitingMinute
                            )

                            if (localTripData[i].optionMinBaseFare.toDouble() > calculateCharge(withGST = "No")) {

                                var payableWithGst = 0.00

                                payableWithGst =
                                    localTripData[i].optionMinBaseFare.toDouble() + localTripData[i].optionMinBaseFare.toDouble() * localTripData[i].categoryGST.toDouble() / 100

                                args.putString(
                                    "totalPayableValue",
                                    "" + payableWithGst
                                )
                            } else {

                                args.putString(
                                    "totalPayableValue",
                                    "" + disscountAmount
                                )
                            }
//                            args.putString("totalPayableValue", "" + MyUtils.priceFormat(calculateCharge()))
                            args.putString(
                                "payextra",
                                "" + extra
                            )

                            val bottomSheetFragment = FareBreakupBottomSheetFragment()
                            bottomSheetFragment.arguments = args
                            bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)

                            break
                        }
                    }
                }

            }
            R.id.ll_selectDate -> {

                bookingType = "Outstation"
                val bundle = Bundle()
                bundle.putString("from", "selectDate")
                bundle.putString("bookingType", bookingType)
                bundle.putString(
                    "pickupDate",
                    outStationPickupDateTv.text.toString() + " " + Calendar.getInstance().get(Calendar.YEAR) + " " + outStationPickupTimeTv.text.toString()
                )

                if (outstationType.equals("One Way")) {
                    bundle.putString(
                        "returnDate",
                        returnDateTextView.text.toString() + " " + Calendar.getInstance().get(Calendar.YEAR) + " " + returnTimeTextView.text.toString()
                    )


                } else {
                    val c = Calendar.getInstance()
                    val df = SimpleDateFormat("EEE,dd MMM yyyy")
                    val tdf = SimpleDateFormat("hh:mm a")
                    //     c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) +60)

                    bundle.putString("returnDate", df.format(c.time) + " " + tdf.format(c.time).toString())

                }

                bundle.putString("outstationType", outstationType)

                val bottomSheetFragment = SelectDateTimeBottomSheetFragment()
                bottomSheetFragment.arguments = bundle

                bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
                bottomSheetFragment.setConfirmBookNowListener(this)
            }
            R.id.ll_ReturnDate -> {

                bookingType = "Outstation"
                val bundle = Bundle()
                bundle.putString("from", "returnDate")
                bundle.putString("bookingType", bookingType)
                bundle.putString("outstationType", outstationType)
                bundle.putString(
                    "pickupDate",
                    outStationPickupDateTv.text.toString() + " " + Calendar.getInstance().get(Calendar.YEAR) + " " + outStationPickupTimeTv.text.toString()
                )
                if (outstationType.equals("One way")) {
                    bundle.putString(
                        "returnDate",
                        returnDateTextView.text.toString() + " " + Calendar.getInstance().get(Calendar.YEAR) + " " + returnTimeTextView.text.toString()
                    )
                } else {
                    if (returnDateTextView.text.toString().contains("Select")) {
                        val c = Calendar.getInstance()
                        val df = SimpleDateFormat("EEE,dd MMM yyyy")
                        val tdf = SimpleDateFormat("hh:mm a")
                        //     c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) +60)
                        bundle.putString("returnDate", df.format(c.time) + " " + tdf.format(c.time).toString())
                    } else {
                        bundle.putString(
                            "returnDate",
                            returnDateTextView.text.toString() + " " + Calendar.getInstance().get(Calendar.YEAR) + " " + returnTimeTextView.text.toString()
                        )
                    }
                }
                val bottomSheetFragment = SelectDateTimeBottomSheetFragment()
                bottomSheetFragment.arguments = bundle
                bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
                bottomSheetFragment.setConfirmBookNowListener(this)

            }
            R.id.ll_couponcode -> {


                var selectedcategoryID: String = ""
                if (tabs.getTabAt(0)!!.isSelected) {
                    localTripData.forEach {
                        if (it.selected) {
                            selectedcategoryID = it.categoryID
                        }
                    }


                } else
                    if (tabs.getTabAt(1)!!.isSelected) {
                        rentalTripData.forEachIndexed { index, rentalPrice ->

                            rentalPrice.rentalpackages.forEach { rentalPackage ->
                                if (rentalPackage.selected) {
                                    selectedcategoryID = rentalPackage.categoryID

                                }
                            }
                        }


                    }
                if (selectedcategoryID != null && selectedcategoryID.isEmpty()) {
                    Toast.makeText(mContext, "Please select car", Toast.LENGTH_LONG).show()

                } else {

                    getCouponofferApi(selectedcategoryID, pickupAddress[0].locality, "", "local_outstation")

                }


            }
            R.id.img_notification -> {
                (activity as MainActivity).navigateTo(NotificationFragment(), true)


            }
            R.id.img_outstation_toolbar_notification -> {
                img_notification.performClick()

            }
            R.id.btn_cancelTrip -> {
                cancelTripReasonMaster()

            }
            R.id.centerMapFab -> {

                var currentLatLong1: LatLng? = null
                if (mMap != null && mMap.isMyLocationEnabled == true && mMap.myLocation != null)
                    currentLatLong1 = LatLng(mMap.myLocation.latitude, mMap.myLocation.longitude)
                else
                    currentLatLong1 = currentLatLong

                if (!from && !destination) {
                    from = true
                }


                directionRouteResponse = null


                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            currentLatLong1?.latitude!!,
                            currentLatLong1?.longitude!!

                        ), defaultZoom
                    )
                )


                if (blackPolyLine == null) {
                    for (i in 0 until sessionManager.get_Authenticate_User().useraddress.size) {

                        if (MyUtils.coordinatesFormat(sessionManager.get_Authenticate_User().useraddress[i].addressLatitude.toDouble()).equals(
                                pickupLat

                            ) && MyUtils.coordinatesFormat(sessionManager.get_Authenticate_User().useraddress[i].addressLongitude.toDouble()).equals(
                                pickupLongi
                            )
                        ) {

                            if (from) {
                                fromFavIv.setImageResource(R.drawable.frovt_icon_selected)
                                destinationFavIv.setImageResource(R.drawable.frovt_icon)
                            } else {
                                destinationFavIv.setImageResource(R.drawable.frovt_icon_selected)
                                fromFavIv.setImageResource(R.drawable.frovt_icon)
                            }


                            break
                        }
                    }

                    if (ll_boockcar.visibility == View.GONE) {
                        pickupLat = "" + currentLatLong1?.latitude!!
                        pickupLongi = "" +
                                currentLatLong1?.longitude!!

                        fromLocationNameTextView.text = Googleutil.getMyLocationAddress(
                            mContext!!,
                            currentLatLong1?.latitude!!,
                            currentLatLong1?.longitude!!

                        )

                    }



                    if (pickupLat.length > 0 && pickupLongi.length > 0 && dropLat.length > 0 && dropLongi.length > 0) {

                        if (confirmBottomLayout.visibility != View.VISIBLE)
                            getPricingList()

                    } else {
                        MyUtils.showSnackbar(this!!.mContext!!, "Getting Location...", mainFragmentFrameLayout)
                    }



                    if (bookingType == "Local" && blackPolyLine == null && destinationTextView.text.toString().isNotEmpty() && confirmBottomLayout.visibility != View.VISIBLE) {
                        drawRouteForPrice(
                            LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                            LatLng(dropLat.toDouble(), dropLongi.toDouble())
                        )
                    }
                }


            }

        }


    }

    private fun setDestinationSelected() {

        centerMapFab.visibility = View.GONE
        from = false
        lldestination.setBackgroundColor(activity!!.resources.getColor(R.color.whiteselector))
        llfrom.setBackgroundColor(activity!!.resources.getColor(R.color.white))

        if (destinationTextView.text.toString().isNotEmpty()) {

            fromFavIv.visibility = View.GONE
            destinationFavIv.visibility = View.VISIBLE
            central_marker.visibility = View.GONE
            central_marker.setImageResource(R.drawable.map_pin_red)
            if (!destination) {
                destination = true
                var destinationlatlng = LatLng(dropLat.toDouble(), dropLongi.toDouble())
                mMap.animateCamera(CameraUpdateFactory.newLatLng(destinationlatlng))
            } else {
                if (pickupLat.length > 0 && pickupLongi.length > 0) {
                    openDestinationActivity("Destination", 1)
                }
            }


        } else {

            if (pickupLat.length > 0 && pickupLongi.length > 0) {
                openDestinationActivity("Destination", 1)
            }

        }

    }

    private fun setFromLocationSelected() {
        if (!isfromDisabled) {

            destination = false
            centerMapFab.visibility = View.VISIBLE
            if (bookingType.equals("Local"))
                central_marker.visibility = View.GONE
            central_marker.setImageResource(R.drawable.map_pin_green)
            fromFavIv.visibility = View.VISIBLE
            destinationFavIv.visibility = View.GONE
            llfrom.setBackgroundColor(activity!!.resources.getColor(R.color.whiteselector))
            lldestination.setBackgroundColor(activity!!.resources.getColor(R.color.white))

            if (!from) {
                from = true
                var fromlatlng = LatLng(pickupLat.toDouble(), pickupLongi.toDouble())
                mMap.animateCamera(CameraUpdateFactory.newLatLng(fromlatlng), 400, null)
            } else {

                if (pickupLat.isNotEmpty() && pickupLongi.isNotEmpty() && !tabs.getTabAt(1)!!.isSelected || !tabs.getTabAt(
                        2
                    )!!.isSelected
                ) {
                    openDestinationActivity("Pickup", 2)

                }
            }
        }
    }

    private fun getDriverLocation(driverID: String) {


        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("driverID", driverID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)


        call = RestClient.get()!!.getDriverLocation(jsonArray.toString())

        call!!.enqueue(object : RestCallback<List<GetDriverLocation>>(mContext) {
            override fun Success(response: Response<List<GetDriverLocation>>) {

                if (mContext == null || !isTrackingOn) {
                    Log.d("System out", "in return")
                    return
                }

                if (response.body() != null) {

                    val it = response.body()

                    if (it!!.isNotEmpty() && it!![0].status == "true" && it!![0].data.isNotEmpty()) {

                        if (driversLatLng == null) {

                            val pickupLatLong = referData!!.bookingPickuplatlong!!.split(",").toTypedArray()
                            val dropLatLong = referData!!.bookingDroplatlong!!.split(",").toTypedArray()


                            driversLatLng =
                                LatLng(it[0].data[0].driverLat.toDouble(), it[0].data[0].driverLongi.toDouble())


                            if (pickupLatLong.size == 2) {

                                pickupLat = pickupLatLong[0]
                                pickupLongi = pickupLatLong[1]

                                if (!isDriverReached) {

                                    drawDirectionRoute(
                                        driversLatLng!!,
                                        LatLng(pickupLatLong[0].toDouble(), pickupLatLong[1].toDouble())
                                    )

                                } else {

                                    if (dropLatLong.size == 2) {

                                        dropLat = dropLatLong[0]
                                        dropLongi = dropLatLong[1]

                                        drawDirectionRoute(
                                            LatLng(pickupLatLong[0].toDouble(), pickupLatLong[1].toDouble()),
                                            LatLng(dropLatLong[0].toDouble(), dropLatLong[1].toDouble())
                                        )
                                    }
                                }

                            }
                        } else {

                            driversLatLng =
                                LatLng(it[0].data[0].driverLat.toDouble(), it[0].data[0].driverLongi.toDouble())

                        }

                        /* if (!points.isNullOrEmpty())
                         {
                             var idx = PolyUtil.locationIndexOnEdgeOrPath(
                                 LatLng(
                                     driversLatLng!!.latitude,
                                     driversLatLng!!.longitude
                                 ), points, true, false, 20.0
                             )
                             if (idx > 0) {
                                 // do your stuff here
                                 driversLatLng = LatLng(points[idx].latitude, points[idx].longitude)

                             }
                         }*/


                        try {
                            if (bookingId == "-1" && myTimer != null) {
//                                myTimer.cancel()
                                Log.d("System out", "timer canceled")
                            } else {
                                Log.d("System out", "timer running.." + bookingId)
                                if (mCarMarker != null && bookingId != "-1") {


                                    if (SphericalUtil.computeDistanceBetween(
                                            mCarMarker!!.position,
                                            driversLatLng
                                        ) > 15
                                    ) {

                                        /*   mCarMarker!!.rotation =
                                               it[0].data[0].driverSerializeLocation.mBearing*/
                                        Googleutil.rotateMarker(
                                            mCarMarker!!,
                                            it[0].data[0].driverSerializeLocation.mBearing
                                        )

//                                        mCarMarker!!.position =
//                                            LatLng(driversLatLng!!.latitude, driversLatLng!!.longitude)

                                        Log.d(
                                            "System out",
                                            "Bearing : " + SphericalUtil.computeHeading(
                                                mCarMarker!!.position,
                                                driversLatLng
                                            ).toFloat() + ",b2" + it[0].data[0].driverSerializeLocation.mBearing
                                        )


                                        MarkerAnimation.animateMarker(
                                            mCarMarker!!, LatLng(driversLatLng!!.latitude, driversLatLng!!.longitude)
                                        )


                                        var currentPlace = CameraPosition.Builder()
                                            .target(LatLng(driversLatLng!!.latitude, driversLatLng!!.longitude))
                                            .zoom(mMap.cameraPosition.zoom)
                                            .bearing(it[0].data[0].driverSerializeLocation.mBearing)
                                            .build()
                                        mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(currentPlace))
//                                        mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(currentPlace),500,null)

                                    }

                                } else {
                                    mCarMarker = mMap.addMarker(
                                        MarkerOptions().position(driversLatLng!!)
                                            .title("")
                                    )

                                    mCarMarker!!.isFlat = true
                                    mCarMarker?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_pin))
                                    mMap.animateCamera(
                                        CameraUpdateFactory.newLatLngZoom(
                                            LatLng(
                                                driversLatLng!!.latitude!!,
                                                driversLatLng!!.longitude!!

                                            ), 17f
                                        )
                                    )

                                }

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()

                        }


                    } else {
                        Log.d("System out", "on false status")
                    }

                } else {
                    Log.d("System out", "on Null response")
                }

                Handler().postDelayed({
                    call!!.clone().enqueue(this)
                }, 1000)

            }

            override fun failure() {
                if (isTrackingOn) {
                    call!!.clone().enqueue(this)
                }
            }
        })
    }

    private fun showDialog() {


        radioSelection = "Home"

        var dialogs = Dialog(activity)

        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.custom_dialog_layout)

        val v = activity?.window?.decorView
        v?.setBackgroundResource(android.R.color.transparent)

        var lp = WindowManager.LayoutParams()
        lp.copyFrom(dialogs.window.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.y = 130
        lp.gravity = Gravity.TOP;Gravity.CENTER_HORIZONTAL
        dialogs.window.attributes = lp


        val dialogCloseIcon = dialogs.findViewById<AppCompatImageView>(R.id.dialogCloseIcon)
        val headerText = dialogs.findViewById<TextView>(R.id.headerText)
        val homeRadioButton = dialogs.findViewById<RadioButton>(R.id.homeRadioButton)
        val addressFavDialogTextView = dialogs.findViewById<TextView>(R.id.addressFavDialogTextView)
        headerText!!.text = resources.getString(R.string.favourites)
        val dialogRadioGroupPreferences = dialogs.findViewById<RadioGroup>(R.id.dialogRadioGroupPreferences)
        val dialogDoneButton = dialogs.findViewById<MaterialButton>(R.id.dialogDoneButton)
        val dialogEnterFavouriteNameTIEditText =
            dialogs.findViewById<TextInputEditText>(R.id.dialogEnterFavouriteNameTIEditText)
        dialogCloseIcon?.setOnClickListener {
            MyUtils().dismissDialog(activity!!, dialogs)
        }

        homeRadioButton.isChecked = true
        dialogRadioGroupPreferences.setOnCheckedChangeListener { radioGroup, i ->

            when (i) {
                R.id.homeRadioButton -> {
                    radioSelection = "Home"
                    dialogEnterFavouriteNameTIEditText.visibility = View.GONE
                }
                R.id.workRadioButton -> {
                    dialogEnterFavouriteNameTIEditText.visibility = View.GONE
                    radioSelection = "Work"
                }
                R.id.otherRadioButton -> {
                    dialogEnterFavouriteNameTIEditText.visibility = View.VISIBLE
                    radioSelection = "Other"
                }
            }

        }

        if (from) {

            if (fromLocationNameTextView.text.toString().trim().isNotEmpty()) {
                addressFavDialogTextView.text = fromLocationNameTextView.text

            } else {
                addressFavDialogTextView.text = getString(R.string.getting_address)
            }
        } else if (destination) {
            if (destinationTextView.text.toString().trim().isNotEmpty()) {
                addressFavDialogTextView.text = destinationTextView.text

            } else {
                addressFavDialogTextView.text = getString(R.string.getting_address)
            }
        }


        dialogDoneButton?.setOnClickListener {

            MyUtils.hideKeyboard1(this.mContext!!)


            if (radioSelection.isNotEmpty()) {

                if (radioSelection.equals("Other") && dialogEnterFavouriteNameTIEditText.text.toString().trim().length == 0) {
                    Toast.makeText(mContext, "Enter name of favourite", Toast.LENGTH_LONG).show()
                } else if (radioSelection.equals("Other") && !TextUtils.isEmpty(dialogEnterFavouriteNameTIEditText.text) && dialogEnterFavouriteNameTIEditText.text!!.length < 2) {
                    Toast.makeText(mContext, "Location should be minimum of 2 characters", Toast.LENGTH_LONG).show()

                } else {

                    if (radioSelection.equals("Other")) {
                        radioSelection = dialogEnterFavouriteNameTIEditText.text.toString()
                    }

                    addAddressFavourite(radioSelection, dialogs)
                }

            } else {
                Toast.makeText(mContext, "Please select type", Toast.LENGTH_LONG).show()
            }

        }

        dialogs.show()

    }


    private fun connectLocation() {
        locationProvider = LocationProvider(
            activity as MainActivity,
            LocationProvider.HIGH_ACCURACY,
            object : LocationProvider.CurrentLocationCallback {
                override fun handleNewLocation(location: Location) {
                    if (mMap != null && location != null) {

//                        fromLocationNameTextView.text =
//                            getMyLocationAddress(mContext, location.latitude, location.longitude)
//
//                        try {
//                            if (pickupLat.length > 0 && pickupLongi.length > 0 && driversLatLng != null) {
////                                updatePickupUI()
//                            }
//                        } catch (e: Exception) {
//                        }

                        destinationCardView.visibility = View.VISIBLE

                        setMyCurrentMarker(location)

                    }
                }

            })

        locationProvider.connect()
    }

    fun updatePickupUI() {
        if (SphericalUtil.computeDistanceBetween(
                LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                driversLatLng
            ) > 50.0
        ) {

            navigateRoute(
                LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                this.driversLatLng!!
            )

        } else {
            myTimer.cancel()
        }
    }

    private fun setMyCurrentMarker(location: Location) {

        var currentLocation = LatLng(location.latitude, location.longitude)
        (activity as MainActivity).userLat = location.latitude
        (activity as MainActivity).userLong = location.longitude

        currentLatLong = currentLocation

        if (!isMapLoaded && mMap != null && mMap.isMyLocationEnabled && mMap.myLocation != null) {
            isMapLoaded = true

            /*  mCurrLocationMarker = mMap.addMarker(
                  MarkerOptions().position(currentLocation)
                      .title("You are here")
              )

              bounceMarker(mMap, this.mCurrLocationMarker as Marker)*/

            val dropLocation = mMap.cameraPosition.target

            pickupLat = location.latitude.toString()
            pickupLongi = location.longitude.toString()
            dropLat = location.latitude.toString()
            dropLongi = location.longitude.toString()

            fromLocationNameTextView.text = Googleutil.getMyLocationAddress(
                activity as MainActivity,
                mMap.myLocation.latitude, mMap.myLocation.longitude
            )

            edittext_from_outstation.text = Googleutil.getMyLocationAddress(
                activity as MainActivity,
                mMap.myLocation.latitude, mMap.myLocation.longitude
            )


            if (mContext != null) {

                ((mContext as MainActivity)).runOnUiThread {

                    getFromToLocations()
                }

            }


            //  mCurrLocationMarker?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.my_location_pin))
            //destinationLatLong = LatLng(dropLat.toDouble(), dropLongi.toDouble())
            mMap.animateCamera(CameraUpdateFactory.newLatLng(currentLocation))

            getPricingList()


        }
    }

    private fun getFromToLocations() {

        try {
            pickupAddress =
                Googleutil.getMyLocationAddressList(
                    this!!.mContext!!,
                    pickupLat.toDouble(),
                    pickupLongi.toDouble()
                ) as java.util.ArrayList<Address>

            dropAddress =
                Googleutil.getMyLocationAddressList(
                    this!!.mContext!!,
                    dropLat.toDouble(),
                    dropLongi.toDouble()
                ) as java.util.ArrayList<Address>
        } catch (e: Exception) {
        }
    }

    override fun onCameraMoveStarted(reason: Int) {
        when (reason) {
            GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE -> {

                if (tabs.selectedTabPosition < 2 && toolbarTop.visibility != View.VISIBLE && confirmBottomLayout.visibility != View.VISIBLE) {
                    topLayout.animation = AnimationUtils.loadAnimation(
                        activity,
                        R.anim.abc_slide_out_top
                    )

                    bottomLayout.animation = AnimationUtils.loadAnimation(
                        activity,
                        R.anim.abc_slide_out_bottom
                    )
                    Thread.sleep(200)
                    topLayout.visibility = View.GONE
                    bottomLayout.visibility = View.GONE
                }

            }
            GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION -> {
                // "The user tapped something on the map."

            }
            GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION -> {
                //  "The app moved the camera."

            }
        }
    }

    override fun onCameraMove() {
    }

    override fun onCameraMoveCanceled() {
    }

    override fun onCameraIdle() {

        try {
            if (ll_boockcar.visibility == View.VISIBLE) {
                central_marker.visibility = View.GONE
            } else if (bookingType.equals("Outstation")) {
                central_marker.visibility = View.GONE
            } else {
                central_marker.visibility = View.VISIBLE
            }

            if (blackPolyLine != null) {
                central_marker.visibility = View.GONE
            }


            if (priceData != null && priceData.size > 0 && confirmBottomLayout.visibility != View.VISIBLE && bottomLayout.visibility != View.VISIBLE) {
                bottomLayout.visibility = View.VISIBLE
                bottomLayout.animation = AnimationUtils.loadAnimation(
                    activity,
                    R.anim.abc_slide_in_bottom
                )
            }

            val makeMarker = mMap.cameraPosition.target
            if (topLayout.visibility != View.VISIBLE) {
                topLayout.visibility = View.VISIBLE
                topLayout.animation = AnimationUtils.loadAnimation(
                    activity,
                    R.anim.abc_slide_in_top
                )
//            bottomLayout.visibility = View.VISIBLE
//            bottomLayout.animation = AnimationUtils.loadAnimation(
//                activity,
//                R.anim.abc_slide_in_bottom
//            )

                getPricingList()

                if (central_marker.visibility == View.VISIBLE) {
                    if (from) {
                        fromLocationNameTextView.text = Googleutil.getMyLocationAddress(
                            activity as MainActivity,
                            makeMarker.latitude,
                            makeMarker.longitude
                        )
                        fromFavIv.setImageResource(R.drawable.frovt_icon)

                        pickupLat = "" + makeMarker.latitude
                        pickupLongi = "" + makeMarker.longitude

                        directionRouteResponse = null

//            } else if (!isDestinationDisabled) {
                    } else {

                        destinationTextView.text = Googleutil.getMyLocationAddress(
                            activity as MainActivity,
                            makeMarker.latitude,
                            makeMarker.longitude
                        )

                        dropLat = "" + makeMarker.latitude
                        dropLongi = "" + makeMarker.longitude

                        destinationFavIv.setImageResource(R.drawable.frovt_icon)

                    }
                }

            }


            for (i in 0 until sessionManager.get_Authenticate_User().useraddress.size) {

                if (makeMarker.latitude.equals(sessionManager.get_Authenticate_User().useraddress[i].addressLatitude.toDouble()) && makeMarker.longitude.equals(
                        sessionManager.get_Authenticate_User().useraddress[i].addressLongitude.toDouble()
                    )
                ) {

                    if (from) {
                        fromFavIv.setImageResource(R.drawable.frovt_icon_selected)
                        destinationFavIv.setImageResource(R.drawable.frovt_icon)
                    } else {
                        fromFavIv.setImageResource(R.drawable.frovt_icon)
                        destinationFavIv.setImageResource(R.drawable.frovt_icon_selected)
                    }

                    break
                }

            }
        } catch (e: Exception) {

        }

//        val makeMarker = mMap.cameraPosition.target
//
//        destinationTextView.text =
//            Googleutil.getMyLocationAddress(mContext, makeMarker.latitude, makeMarker.longitude)
    }

    private fun getDisplayHeight(): Int {
        return this.resources.displayMetrics.heightPixels
    }


    override fun onDestroyView() {
        super.onDestroyView()

        try {

            if (myTimer != null) {
                myTimer.cancel()
            }

            if (myBookingTimer != null) {
                myBookingTimer.cancel()
            }
        } catch (e: java.lang.Exception) {

            e.printStackTrace()
        }

        if (locationProvider != null) {
            locationProvider.disconnect()
        }

        if (call != null) {
            call!!.cancel()
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (activity == null)
            return
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data != null) {

                var lats: String = data.getStringExtra("lattitude")
                var longs: String = data.getStringExtra("longitude")
                pushType = ""
                primaryAddressFrom = data.getStringExtra("primaryAddress")
                secondaryAddressFrom = data.getStringExtra("secondaryAddress")

                directionRouteResponse = null
                isFirstTime = false

                btnboollater.setTag(0)

                for (i in 0 until sessionManager.get_Authenticate_User().useraddress.size) {

                    if (MyUtils.coordinatesFormat(sessionManager.get_Authenticate_User().useraddress[i].addressLatitude.toDouble()).equals(
                            lats
                        ) && MyUtils.coordinatesFormat(sessionManager.get_Authenticate_User().useraddress[i].addressLongitude.toDouble()).equals(
                            longs
                        )
                    ) {
                        destinationFavIv.setImageResource(R.drawable.frovt_icon_selected)
                        break
                    } else {
                        destinationFavIv.setImageResource(R.drawable.frovt_icon)
                    }

                }

//                if (bookingType.equals("Local")) {
                dropLat = lats.toString()
                dropLongi = longs.toString()
//                } else {
//                    outstationDropLat = MyUtils.coordinatesFormat(lats.toDouble())
//                    outstationDropLongi = MyUtils.coordinatesFormat(longs.toDouble())
//                }


                if (ll_boockcar.visibility == View.VISIBLE) {
                    if (destinationTextView.text.toString().trim().isNotEmpty()) {
                        ll_coupon_code_text.visibility = View.GONE
                        drawDirectionRoute(
                            LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                            this.destinationLatLong!!
                        )

                    }
                } else {
                    drawRouteForPrice(
                        LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                        LatLng(dropLat.toDouble(), dropLongi.toDouble())
                    )
                }

                if (SphericalUtil.computeDistanceBetween(
                        LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                        LatLng(lats.toDouble(), longs.toDouble())
                    ) > 10.0
                ) {


                    central_marker.setImageResource(R.drawable.map_pin_red)
                    if (dropAddress != null && dropAddress.size > 0) {
                        dropAddress.clear()
                    }

                    if (mContext != null) {

                        try {

                            dropAddress = Googleutil.getMyLocationAddressList(
                                this!!.mContext!!,
                                dropLat.toDouble(),
                                dropLongi.toDouble()
                            ) as java.util.ArrayList<Address>
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }


                    }

                    destinationLatLong = LatLng(dropLat.toDouble(), dropLongi.toDouble())
                    destinationTextView.text = primaryAddressFrom + ", " + secondaryAddressFrom
                    edit_outstation_destnation.text = primaryAddressFrom + ", " + secondaryAddressFrom
                    getPricingList()
                    MyUtils.hideKeyboard1(this!!.mContext!!)
                    if (destinationTextView.text.toString().trim().isNotEmpty() && ll_boockcar.visibility == View.VISIBLE) {
                        drawDirectionRoute(
                            LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                            LatLng(dropLat.toDouble(), dropLongi.toDouble())
                        )

                    }

                    from = false
                    destination = true
                    fromFavIv.visibility = View.GONE
                    destinationFavIv.visibility = View.VISIBLE


                    if (bookingType.equals("Local")) {
                        var pickupCity = ""
                        var dropCity = ""

                        for (i in 0 until pickupAddress.size) {
                            if (pickupAddress[i].locality != null && pickupAddress[i].locality.length > 0) {
                                pickupCity = pickupAddress[i].locality
                                break
                            }
                        }

                        for (i in 0 until dropAddress.size) {
                            if (dropAddress[i].locality != null && dropAddress[i].locality.length > 0) {
                                dropCity = dropAddress[i].locality
                                break
                            }
                        }



                        if (!pickupCity.equals(dropCity)) {
                            showMessageOKCancel(
                                mContext!!,
                                "Drop location is outside city limits.Continue booking Outstation instead?",
                                "Book Outstation",
                                DialogInterface.OnClickListener { dialog, which ->
                                    // continue button event
                                    setDataTab3()
                                    tabs.getTabAt(2)!!.select()
                                })
                        }

                    }


                    var destination = LatLng(dropLat.toDouble(), dropLongi.toDouble())
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(destination))

                } else {
                    MyUtils.showSnackbar(
                        mContext as MainActivity,
                        "Choose another location",
                        mainFragmentFrameLayout
                    )
                }
            }
        }
        if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            if (data != null) {

                var lats: String = data.getStringExtra("lattitude")
                var longs: String = data.getStringExtra("longitude")
                primaryAddressFrom = data.getStringExtra("primaryAddress")
                secondaryAddressFrom = data.getStringExtra("secondaryAddress")
                pushType = ""
                fromLocationNameTextView.text = primaryAddressFrom + ", " + secondaryAddressFrom
                edittext_from_outstation.text = primaryAddressFrom + ", " + secondaryAddressFrom

//                if (bookingType.equals("Local") || bookingType.equals("Rental")) {

                pickupLat = lats
                pickupLongi = longs
//                } else {
//                    outstationPickupLat = MyUtils.coordinatesFormat(lats.toDouble())
//                    outstationPickupLongi = MyUtils.coordinatesFormat(longs.toDouble())
//                }

                directionRouteResponse = null
                if (pickupAddress.size > 0) {
                    pickupAddress.clear()
                }
                isFirstTime = false
                if (mContext != null) {

                    (mContext as MainActivity).runOnUiThread {

                        pickupAddress = Googleutil.getMyLocationAddressList(
                            this!!.mContext!!,
                            pickupLat.toDouble(),
                            pickupLongi.toDouble()
                        ) as java.util.ArrayList<Address>
                    }
                }

                central_marker.setImageResource(R.drawable.map_pin_green)
                currentLatLong = LatLng(pickupLat.toDouble(), pickupLongi.toDouble())

                getPricingList()

                MyUtils.hideKeyboard1(this!!.mContext!!)

                for (i in 0 until sessionManager.get_Authenticate_User().useraddress.size) {

                    if (MyUtils.coordinatesFormat(sessionManager.get_Authenticate_User().useraddress[i].addressLatitude.toDouble()).equals(
                            lats
                        ) && MyUtils.coordinatesFormat(sessionManager.get_Authenticate_User().useraddress[i].addressLongitude.toDouble()).equals(
                            longs
                        )
                    ) {
                        Log.d("System out", "result code from")
                        fromFavIv.setImageResource(R.drawable.frovt_icon_selected)
                        break
                    } else {
                        fromFavIv.setImageResource(R.drawable.frovt_icon)
                    }

                }

                var fromlatlng = LatLng(pickupLat.toDouble(), pickupLongi.toDouble())
                mMap.animateCamera(CameraUpdateFactory.newLatLng(fromlatlng))
                pushType = ""
                destination = false
                from = true
                fromFavIv.visibility = View.VISIBLE
                destinationFavIv.visibility = View.GONE


                if (bookingType.equals("Local")) {
                    var pickupCity = ""
                    var dropCity = ""

                    for (i in 0 until pickupAddress.size) {
                        if (pickupAddress[i].locality != null && pickupAddress[i].locality.length > 0) {
                            pickupCity = pickupAddress[i].locality
                            break
                        }
                    }

                    for (i in 0 until dropAddress.size) {
                        if (dropAddress[i].locality != null && dropAddress[i].locality.length > 0) {
                            dropCity = dropAddress[i].locality
                            break
                        }
                    }


                    if (!pickupCity.equals(dropCity)) {
                        showMessageOKCancel(
                            mContext!!,
                            "Drop location is outside city limits.Continue booking Outstation instead?",
                            "Book Outstation",
                            DialogInterface.OnClickListener { dialog, which ->
                                // continue button event
                                setDataTab3()
                                tabs.getTabAt(2)!!.select()
                            })
                    }
                }



                if (bookingType.equals("Local") && destinationTextView.text.toString().trim().length > 0) {
                    drawRouteForPrice(
                        LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                        LatLng(dropLat.toDouble(), dropLongi.toDouble())
                    )
                }


            }
        }


        if (requestCode == 3 && resultCode == Activity.RESULT_OK) {


            if (mContext != null) {


                if (bookingSubType.equals("Book Later")) {

                    MyUtils.showSnackbar(mContext!!, "Your Trip Booked Successfully", mainFragmentFrameLayout)

                    backConfirmBooking()
                    getPricingList()
//                    setDataTab1()

                } else {

                    mBottomSheetDialog.show()
                    dialogText.text = "Finding your ride"
                    count = 0

                    val bookingIds = data!!.extras.getString("bookingId")

                    bookingId = bookingIds

                    backConfirmBooking()

                    myBookingTimer = fixedRateTimer("timer", false, 0, 30000) {
                        getMyBookingData(bookingIds)
                    }
                }
            }
        }


        if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
            connectLocation()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }

    }

    fun drawDirectionRoute(pickuplatlong: LatLng, dropDestination: LatLng) {

        googleDirectionModel?.getRoute(activity!!, pickuplatlong, dropDestination)
            ?.observe(this@MainFragment,
                Observer<GoogleDirection> { googleDirectionResponse: GoogleDirection? ->

                    try {
                        if (bookingSubType == "Book Now") {
                            btnbooknow.revertAnimation {
                                btnbooknow.text = "Book Now"
                            }
                        } else if (bookingSubType == "Book Later") {
                            btnboollater.revertAnimation {
                                btnboollater.text = "Book Later"
                            }
                        }
                    } catch (e: Exception) {

                    }



                    if (googleDirectionResponse != null && googleDirectionResponse.status.equals("OK")) {


                        directionRouteResponse = googleDirectionResponse

                        carsTripAdapter.fillDirectionResponse(directionRouteResponse)
                        drawRoutePolyLine(googleDirectionResponse, pickuplatlong, dropDestination)
                        if (bookingSubType == "Book Later" && ll_boockcar.visibility == View.VISIBLE) {
                            openBookedcar()
                        }

                        if (bookingSubType == "Book Now") {

                            if (confirmBottomLayout.visibility != View.VISIBLE) {
                                openBookedcar()
                            }


                        } else if (bookingSubType == "Book Later" && btnboollater.getTag() == 1) {

                            priceData[0].localPrice.clear()
                            priceData[0].localPrice.addAll(localTripData)

                            priceData[0].rentalPrice.clear()
                            priceData[0].rentalPrice.addAll(rentalTripData)


                            val bundle = Bundle()
                            bundle.putString("from", "Book Later")
                            bundle.putSerializable("priceData", priceData as Serializable)
                            bundle.putSerializable(
                                "directionRouteResponse",
                                directionRouteResponse as Serializable
                            )
                            bundle.putString("longs", pickupLongi)
                            bundle.putString("dropLat", dropLat)
                            bundle.putString("dropLongi", dropLongi)
                            bundle.putString("pickupLat", pickupLat)
                            bundle.putString("pickupLongi", pickupLongi)
                            bundle.putString("bookingType", bookingType)
                            bundle.putString("bookingSubType", bookingSubType)
                            bundle.putString("fromLocation", fromLocationNameTextView.text.toString())
                            bundle.putString("destinationLocation", destinationTextView.text.toString())

                            val bottomSheetFragment = SelectDateTimeBottomSheetFragment()
                            bottomSheetFragment.arguments = bundle
                            bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
                            bottomSheetFragment.setConfirmBookNowListener(this)

                        }

                        //                            polylinePickUp()


                    } else {

                        btnbooknow.revertAnimation()
                        if (googleDirectionResponse != null)
                            ((activity) as MainActivity).showSnackBar(googleDirectionResponse.error_message!!)
                    }

                })


    }


    fun drawRouteForPrice(pickuplatlong: LatLng, dropDestination: LatLng) {

        googleDirectionModel?.getRoute(activity!!, pickuplatlong, dropDestination)
            ?.observe(this@MainFragment,
                Observer<GoogleDirection> { googleDirectionResponse: GoogleDirection? ->
                    run {


                        if (googleDirectionResponse != null && googleDirectionResponse.status.equals("OK")) {

                            directionRouteResponse = googleDirectionResponse

                            carsTripAdapter.fillDirectionResponse(directionRouteResponse)



                            drawRoutePolyLine(
                                directionRouteResponse!!, LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                                LatLng(dropLat.toDouble(), dropLongi.toDouble())
                            )


                        }
                    }
                })
    }

    private fun navigateRoute(customerLatLongs: LatLng, driverLatLongs: LatLng) {
        if (mMap != null) {
            points = ArrayList<LatLng>()
            points.add(customerLatLongs)
            points.add(driverLatLongs)

            if (blackPolyLine != null) {
                blackPolyLine!!.remove()
            }



            blackPolyLine = mMap.addPolyline(Googleutil.getPolyLineOption(Color.BLACK, points))

            if (mCarMarker != null) {
                MarkerAnimation.animateMarker(mCarMarker!!, driverLatLongs)
            } else {
                mCarMarker = mMap.addMarker(
                    MarkerOptions().position(driverLatLongs)
                        .title("Drop Location")
                )
            }
            mCarMarker?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_pin))

            moveToBounds(blackPolyLine!!)
        }
    }


    @SuppressLint("MissingPermission")
    private fun drawRoutePolyLine(
        directionRouteResponse: GoogleDirection,
        pickuplatlong: LatLng,
        dropDestination: LatLng
    ) {
        if (mMap != null) {

            mMap.clear()
            mCarMarker = null

            points = ArrayList<LatLng>()
            for (it in directionRouteResponse.routes!![0]?.legs!!) {
                it?.steps!!.forEach { step ->

                    points.addAll(
                        Googleutil.decodePoly(step?.polyline!!.points.toString())
                    )
                }
            }

            central_marker.visibility = View.GONE

            if (pushType.equals(Push.tripStarted, true)) {

                val pickupLatLong = referData!!.bookingPickuplatlong!!.split(",").toTypedArray()
                val dropLatLong = referData!!.bookingDroplatlong!!.split(",").toTypedArray()

                mMap.isMyLocationEnabled = false

                setPickupMarker(pickupLatLong[0].toDouble(), pickupLatLong[1].toDouble())
                setDropMarker(dropLatLong[0].toDouble(), dropLatLong[1].toDouble())


            } else if (driversLatLng != null && referData != null) {
                val pickupLatLong = referData!!.bookingPickuplatlong!!.split(",").toTypedArray()
                setPickupMarker(pickupLatLong[0].toDouble(), pickupLatLong[1].toDouble())
            } else if (pickupLat.isNotEmpty() && pickupLongi.isNotEmpty() && dropLat.isNotEmpty() && dropLongi.isNotEmpty()) {
                setPickupMarker(pickupLat.toDouble(), pickupLongi.toDouble())
                setDropMarker(dropLat.toDouble(), dropLongi.toDouble())
            }

            blackPolyLine = mMap.addPolyline(Googleutil.getPolyLineOption(Color.BLACK, points))

//            if (mDestinationMarker != null)
//                mDestinationMarker?.remove()

//            mDestinationMarker = mMap.addMarker(
//                MarkerOptions().position(dropDestination)
//                    .title("Drop Location")
//            )
//
//            mDestinationMarker?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_green))

            moveToBounds(blackPolyLine!!)
//            mCarMarker = null
        }

    }


    private fun getMyBookingData(bookingIds: String) {

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("Type", bookingType)
            jsonObject.put("bookingID", bookingIds)
            jsonObject.put("page", "0")
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        Log.d("System out", "json : " + jsonArray)


        var call = RestClient.get()!!.getMyBookingData(jsonArray.toString())
        call.enqueue(object : RestCallback<List<GetMyBookingsPojo>>(mContext) {
            @SuppressLint("NewApi")
            override fun Success(response: Response<List<GetMyBookingsPojo>>) {

                if (activity == null)
                    return

                val bookingData = response.body()

                if (bookingData!![0].status.equals("true", true)) {

                    try {
                        if (!bookingData[0].data[0].driverID.equals("0")) {


                            if (confirmBottomLayout.visibility != View.VISIBLE) {
                                confirmBottomLayout.visibility = View.VISIBLE
                            }
                            bottomLayout.visibility = View.GONE

                            setDriverData(
                                response.body()!![0].data[0].driverName,
                                response.body()!![0].data[0].bookingOTP,
                                response.body()!![0].data[0].driverRatting,
                                response.body()!![0].data[0].drvcarModel,
                                response.body()!![0].data[0].drvcarNo,
                                response.body()!![0].data[0].driverMobile,
                                response.body()!![0].data[0].categoryImage,
                                response.body()!![0].data[0].driverProfilePic
                            )
                            myBookingTimer.cancel()
                            mBottomSheetDialog.dismiss()


                        } else if (count >= 4) {
                            myBookingTimer.cancel()
                            mBottomSheetDialog.dismiss()
                            bookingId = "-1"
                            MyUtils.showSnackbar(
                                this!!.mContext!!,
                                "No drivers found nearby !",
                                mainFragmentFrameLayout
                            )
                            backConfirmBooking()

                        }

                        count++
                    } catch (e: java.lang.Exception) {

                    }


                } else {
                    count++
                }
            }

            override fun failure() {

                try {
                    count++
                    if (count >= 4) {
                        myBookingTimer.cancel()
                        mBottomSheetDialog.dismiss()

                    }
                    if (!MyUtils.isInternetAvailable(this.mContext!!)) {

                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.error_common_network),
                            Toast.LENGTH_LONG
                        )
                            .show()


                    } else {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.error_crash_error_message),
                            Toast.LENGTH_LONG
                        ).show()

                    }
                } catch (e: Exception) {
                }
            }
        })

    }


    fun setPickupMarker(lats: Double, longis: Double) {
        if (mfromMarker != null) {
            mfromMarker!!.remove()
        }

        mfromMarker = mMap.addMarker(
            MarkerOptions().position(LatLng(lats, longis))
                .title("Pickup Location")
        )
        mfromMarker?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_green))
    }

    fun setDropMarker(lats: Double, longis: Double) {
        if (mDestinationMarker != null) {
            mDestinationMarker?.remove()
        }

        mDestinationMarker = mMap.addMarker(
            MarkerOptions().position(LatLng(lats, longis))
                .title("Drop Location")
        )

        mDestinationMarker?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_red))
    }

    fun getPricingList() {

        if (mContext == null) {
            return
        }

        if (!(fromBookingStatus.equals("Accepted", true) || fromBookingStatus.equals(
                "Started",
                true
            ) || fromBookingStatus.equals("Reached", true))
        ) {

            var dropCityName = ""

            if (bookingType.equals("Outstation", true)) {

                (mContext as MainActivity).runOnUiThread {
                    getFromToLocations()
                }
            }


            try {

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

            if (dropAddress != null && dropAddress.size > 0 && dropAddress[0].locality != null) {
                dropCityName = dropAddress[0].locality
            } else {
                dropCityName = pickupAddress[0].locality
            }

            if (dropLat.trim().length == 0 && dropLongi.trim().length == 0) {
                dropLat = pickupLat
                dropLongi = pickupLongi
            }

            val jsonArray = JSONArray()
            val jsonObject = JSONObject()
            try {
                jsonObject.put("pickupLat", pickupLat)
                jsonObject.put("pickupLongi", pickupLongi)
                jsonObject.put("dropLat", dropLat)
                jsonObject.put("dropLongi", dropLongi)
                jsonObject.put("pickupCityID", "")
                jsonObject.put("pickupCityName", pickupAddress[0].locality)
                jsonObject.put("dropCityID", "")
                jsonObject.put("dropCityName", dropCityName)
                jsonObject.put("radius", "10")
                jsonObject.put("pagesize", "20")
                jsonObject.put("page", "0")
                jsonObject.put("apiType", RestClient.apiType)
                jsonObject.put("apiVersion", RestClient.apiVersion)

            } catch (e: JSONException) {
                e.printStackTrace()
            }
            jsonArray.put(jsonObject)
            Log.d("System out", "price Data Req : " + jsonArray)

            val getPricingsModel = ViewModelProviders.of(context as MainActivity).get(GetPriceModel::class.java)
            getPricingsModel.getPrice(context as MainActivity, false, jsonArray.toString())
                .observe(this,
                    Observer<List<GetPricePojo>> { getPriceList ->
                        if (getPriceList != null && getPriceList.isNotEmpty()) {
                            if (getPriceList[0].status.equals("true", true)) {
                                currentTime = Calendar.getInstance()
                                priceData.clear()
                                priceData.addAll(getPriceList[0].data)

                                mMap?.uiSettings?.isScrollGesturesEnabled = true

                                if (bottomLayout.visibility != View.VISIBLE) {
                                    bottomLayout.visibility = View.VISIBLE
                                    bottomLayout.animation = AnimationUtils.loadAnimation(
                                        activity,
                                        R.anim.abc_slide_in_bottom
                                    )
                                }

                                localTripData.clear()

                                if (priceData.size > 0 && priceData[0].localPrice != null && priceData[0].localPrice.size > 0) {
                                    localTripData.addAll(priceData[0].localPrice)
                                    carsTripAdapter.notifyDataSetChanged()
                                    if (localSelectedPos >= 0 && localSelectedPos < localTripData.size) {
                                        localTripData[localSelectedPos].selected = true
                                    } else {
                                        localTripData[0].selected = true
                                        if (localTripData[0].drivers.size == 0) {
                                            localSelectedPos = 0
                                            setButtonsDisable()
                                        } else {
                                            setButtonsEnable()
                                        }
                                    }

                                } else {

                                    setButtonsDisable()
                                    btnboollater.isEnabled = false
                                    btnboollater.alpha = .5f
                                }


                                rentalTripData.clear()

                                if (priceData.size > 0 && priceData[0].rentalPrice != null && priceData[0].rentalPrice.size > 0 && priceData[0].rentalPrice[0].rentalpackages.size > 0) {

                                    rentalTripData.addAll(priceData[0].rentalPrice)
                                    carsRentalAdapter.notifyDataSetChanged()
                                    if (rentalSelectedPos >= 0 && rentalSelectedPos < rentalTripData.size) {
                                        rentalTripData[rentalSelectedPos].selected = true
                                    } else {
                                        rentalTripData[0].rentalpackages[0].selected = true
                                        rentalSelectedPos = 0
                                    }

                                }

                                when {
                                    ll_boockcar.visibility == View.VISIBLE -> {

                                    }
                                    confirmBottomLayout.visibility == View.VISIBLE -> {

                                    }
                                    else -> {
                                        bottomLayout.visibility = View.VISIBLE
                                        when {
                                            tabs.getTabAt(0)!!.isSelected -> setDataTab1()
                                            tabs.getTabAt(1)!!.isSelected -> setDataTab2()
                                            else -> setDataTab3()
                                        }

                                    }
                                }

                            } else {

                                MyUtils.showSnackbar(
                                    this.mContext!!,
                                    getPriceList[0].message,
                                    mainFragmentFrameLayout
                                )

                            }

                        } else {

                            try {
                                if (!MyUtils.isInternetAvailable(this.mContext!!)) {
                                    MyUtils.showSnackbar(
                                        this.mContext!!,
                                        resources.getString(R.string.error_common_network),
                                        mainFragmentFrameLayout
                                    )
                                } else {

                                    MyUtils.showSnackbar(
                                        this.mContext!!,
                                        resources.getString(R.string.error_crash_error_message),
                                        mainFragmentFrameLayout
                                    )

                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    })

        }

    }

    private fun addAddressFavourite(title: String, dialog: Dialog) {


        if (!from && !destination) {
            from = true
        }

        MyUtils.showProgressDialog(context as MainActivity, "Please Wait")

//        val addressDetails =
//            Googleutil.getMyLocationAddressList(
//                this.mContext!!,
//                pickupLat.toDouble(),
//                pickupLongi.toDouble()
//            )

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("addressTitle", title)
            jsonObject.put("addressAddressLine1", fromLocationNameTextView.text.toString())
            jsonObject.put("addressAddressLine2", "")
            jsonObject.put("countryName", pickupAddress[0].countryName)
            jsonObject.put("stateName", pickupAddress[0].adminArea)
            jsonObject.put("cityName", pickupAddress[0].locality)
            jsonObject.put("addressPincode", pickupAddress[0].postalCode)

            if (from) {
                jsonObject.put("addressLatitude", pickupLat.toDouble())
                jsonObject.put("addressLongitude", pickupLongi.toDouble())

            } else {
                jsonObject.put("addressLatitude", dropLat.toDouble())
                jsonObject.put("addressLongitude", dropLongi.toDouble())
            }

            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val addAddressCall = ViewModelProviders.of(context as MainActivity).get(AddAddressModel::class.java)
        addAddressCall.addAddress(this.mContext!!, false, jsonArray.toString())
            .observe(this@MainFragment,
                Observer<List<AddAddressPojo>> { addAddressList ->

                    MyUtils.dismissProgressDialog()
                    if (addAddressList != null && addAddressList.size > 0) {
                        if (addAddressList[0].status.equals("true")) {

                            dialog.dismiss()

                            addAddressData.clear()
                            addAddressData.addAll(addAddressList[0].data)

                            val updateSessionManager = sessionManager.get_Authenticate_User()

                            updateSessionManager.useraddress.addAll(addAddressData)

                            sessionManager.clear_login_session()

                            val gson = Gson()
                            val json = gson.toJson(updateSessionManager)

                            sessionManager.create_login_session(
                                json,
                                updateSessionManager.userMobile,
                                "",
                                true, true,
                                sessionManager.isEmailLogin()
                            )

                            if (from) {
                                fromFavIv.setImageResource(R.drawable.frovt_icon_selected)
                                destinationFavIv.setImageResource(R.drawable.frovt_icon)
                            } else {
                                fromFavIv.setImageResource(R.drawable.frovt_icon)
                                destinationFavIv.setImageResource(R.drawable.frovt_icon_selected)
                            }

                        }

                    } else {

                        MyUtils.dismissProgressDialog()
                        try {
                            if (!MyUtils.isInternetAvailable(this.mContext!!)) {
                                MyUtils.showSnackbar(
                                    this.mContext!!,
                                    resources.getString(R.string.error_crash_error_message),
                                    mainFragmentFrameLayout
                                )

                            } else {

                                MyUtils.showSnackbar(
                                    this.mContext!!,
                                    resources.getString(R.string.error_common_network),
                                    mainFragmentFrameLayout
                                )
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                })
    }


    fun calculateCharge(position: Int = localSelectedPos, withGST: String = "Yes"): Double {

        val distance = directionRouteResponse!!.routes!![0]!!.legs!![0]!!.distance!!.value!!.toDouble() / 1000
        val duration = directionRouteResponse!!.routes!![0]!!.legs!![0]!!.duration!!.value!!.toDouble() / 60

        var baseCharge = 0.00
        var baseKms = 0.00
        var baseMin = 0.00
        var diffDistance: Double = 0.0
        var diffDuration: Double = 0.0
        totalAmount = 0.0

//        for (i in 0 until localTripData.size) {
//            if (localTripData[i].selected) {


        if (localTripData[position].priceType.equals("specialprice") && !localTripData[position].specialpriceoptions.isNullOrEmpty()) {

            var mDistnaceKm = distance
            var subTotal = 0.0

            for (i in 0 until localTripData[position].specialpriceoptions.size) {

                if (localTripData[position].specialpriceoptions[i].slotEndKM.toInt() <= mDistnaceKm) {

                    baseCharge = localTripData[position].specialpriceoptions[i].optionBasefare.toDouble()
                    baseKms = localTripData[position].specialpriceoptions[i].optionBaseKM.toDouble()

                    if (localTripData[position].specialpriceoptions[i].slotEndKM.toInt() > baseKms) {
                        diffDistance =
                            (localTripData[position].specialpriceoptions[i].slotEndKM.toInt() - baseKms) * localTripData[position].specialpriceoptions[i].optionAdditionalKmCharge.toDouble()
                    }

                    mDistnaceKm -= localTripData[position].specialpriceoptions[i].slotEndKM.toInt()
                    subTotal += baseCharge + diffDistance

                }

                if (mDistnaceKm <= 0)
                    break
            }


            if (mDistnaceKm > 0 && !localTripData[position].specialpriceoptions.isNullOrEmpty()) {
                baseCharge =
                    localTripData[position].specialpriceoptions[0].optionBasefare.toDouble()
                baseKms =
                    localTripData[position].specialpriceoptions[0].optionBaseKM.toDouble()

                if (baseKms >= mDistnaceKm) {
                    subTotal += baseCharge
                } else {
                    diffDistance =
                        (mDistnaceKm - baseKms) * localTripData[position].specialpriceoptions[0].optionAdditionalKmCharge.toDouble()

                    subTotal += baseCharge + diffDistance
                }

            }
            // duration
            if (!localTripData[position].specialpriceoptions.isNullOrEmpty()) {
                baseMin = duration.minus(localTripData[position].specialpriceoptions[0].optionBaseMinute.toInt())
                if (baseMin > 0) {
                    subTotal += baseMin * localTripData[position].specialpriceoptions[0].optionPerMinCharge.toDouble()
                }
            }

            totalAmount = subTotal

        } else {


//        if (localTripData[position].optionBasefare.toDouble() > localTripData[position].optionMinBaseFare.toDouble()) {
            baseCharge = localTripData[position].optionBasefare.toDouble()
//        } else {
//            baseCharge = localTripData[position].optionMinBaseFare.toDouble()
//        }


            baseKms = localTripData[position].optionBaseKM.toDouble()
            baseMin = localTripData[position].optionBaseMinute.toDouble()
            if (distance > baseKms) {
                diffDistance = (distance - baseKms) * localTripData[position].optionAdditionalKmCharge.toDouble()
            }

            if (duration > baseMin) {
                diffDuration = (duration - baseMin) * localTripData[position].optionPerMinCharge.toDouble()
            }
//                break
//            }
//        }


            totalAmount = baseCharge + diffDistance + diffDuration
            if (localTripData[position].optionMinBaseFare.toDouble() > totalAmount) {
                totalAmount = localTripData[position].optionMinBaseFare.toDouble()
            }
        }

        if (withGST.equals("Yes")) {
            totalAmount += totalAmount * this.localTripData[position].categoryGST.toDouble() / 100
        }

        return totalAmount
    }


    fun moveToBounds(p: Polyline) {

        var builder = LatLngBounds.Builder()

        for (point in p.points) {
            builder.include(point)
        }


        var bounds = builder.build()
        var padding = 150 // offset from edges of the map in pixels

        var cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
        mMap.animateCamera(cu)
    }


    private fun call(phoneNumber: String) {

        try {
            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                this@MainFragment.requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    MY_PERMISSIONS_REQUEST_CALL_PHONE
                )

            } else if (phoneNumber.isNotEmpty()) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + phoneNumber)

                activity!!.startActivity(callIntent)

            }

        } catch (e: ActivityNotFoundException) {
            Log.e("call", "Call failed", e)
        }

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_CALL_PHONE && grantResults.isNotEmpty()) {
            call(phoneNumber)
        }
    }


    private fun setCarsMarker(categoryID: String) {
        for (marker in carMarkerList) {
            marker.remove()
        }

        for (localPrice in priceData[0].localPrice) {
            if (localPrice.categoryID.equals(categoryID, true)) {
                for (driver in localPrice.drivers) {
                    var driverlatLng = LatLng(driver.driverLat.toDouble(), driver.driverLongi.toDouble())

                    var marker = mMap.addMarker(
                        MarkerOptions().position(driverlatLng)
                    )
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_pin))
                    carMarkerList.add(marker!!)
                }
            }
        }
    }


    companion object {

        // TODO: Customize parameters
        fun newInstance(push: Push): MainFragment =
            MainFragment().apply {
                arguments = Bundle().apply {

                    putSerializable("push", push)


                }
            }

    }

    @SuppressLint("NewApi", "MissingPermission")
    fun pushHandle(push: Push) {
        try {
            if (push != null) {
                referData = Gson().fromJson(push.RefData!!, RefData::class.java)
                bookingId = referData!!.bookingID

                if (push.type.equals(Push.bokingRequestAccepted, true)) {


                    pushType = push.type!!

                    mMap.isMyLocationEnabled = false

                    if (mBottomSheetDialog != null && mBottomSheetDialog.isShowing) {
                        mBottomSheetDialog.dismiss()
                    }

                    if (myBookingTimer != null) {
                        myBookingTimer.cancel()
                    }
                    if (myTimer != null)
                        myTimer.cancel()


                    if (blackPolyLine != null) {
                        blackPolyLine!!.remove()
                    }

                    try {
                        Log.d("System out", "pnAccepted")
                        confirmBottomLayout.visibility = View.VISIBLE
                        bottomLayout.visibility = View.GONE
                        tv_confirmTrip_otp.visibility = View.VISIBLE
                        btn_cancelTrip.visibility = View.VISIBLE
                        mainMenu_ImageView.tag = 1
                        mainMenu_ImageView.setImageResource(R.drawable.back_icon)
                        central_marker.visibility = View.GONE
                        centerMapFab.visibility = View.GONE
                    } catch (e: Exception) {

                    }


                    confirmBottomLayout.setTag(0)
                    driversLatLng = null
                    isDriverReached = false
                    mCarMarker = null
                    isTrackingOn = true

                    doAsync {

                        getDriverLocation(referData!!.driverID)
                    }

                    bookingId = referData!!.bookingID

                    setDriverData(
                        referData!!.driverName,
                        referData!!.bookingOTP,
                        referData!!.driverRatting,
                        referData!!.drvcarModel,
                        referData!!.drvcarNo,
                        referData!!.driverMobile,
                        referData!!.categoryImage,
                        referData!!.driverProfilePic
                    )

                } else if (push.type.equals(Push.tripStarted, true)) {
                    confirmBottomLayout.visibility = View.VISIBLE
                    mainMenu_ImageView.tag = 1
                    mainMenu_ImageView.setImageResource(R.drawable.back_icon)
                    bottomLayout.visibility = View.GONE
                    tv_confirmTrip_otp.visibility = View.VISIBLE
                    btn_cancelTrip.visibility = View.VISIBLE
                    Log.d("System out", "trip started :  " + push.type)
                    btn_cancelTrip.alpha = .5f
                    tv_confirmTrip_otp.alpha = .5f
                    btn_cancelTrip.isEnabled = false
                    tv_confirmTrip_otp.isEnabled = false
                    pushType = push.type!!

                    if (mBottomSheetDialog != null && mBottomSheetDialog.isShowing) {

                        mBottomSheetDialog.dismiss()
                    }

                    if (myBookingTimer != null) {
                        myBookingTimer.cancel()

                    }

                    mMap.uiSettings.isZoomGesturesEnabled = true
                    mMap.uiSettings.isScrollGesturesEnabled = true

                    if (blackPolyLine != null) {
                        blackPolyLine!!.remove()
                    }

                    val pickupLatLong = referData!!.bookingPickuplatlong!!.split(",").toTypedArray()
                    val dropLatLong = referData!!.bookingDroplatlong!!.split(",").toTypedArray()


                    if (dropLatLong != null) {
                        driversLatLng = null
                        isDriverReached = true
                        mCarMarker = null
                        isTrackingOn = true

                        doAsync {

                            getDriverLocation(referData!!.driverID)
                        }

                    } else {
                        if (pickupLatLong.size == 2 && dropLatLong.size == 2) {

                            drawRouteForPrice(
                                LatLng(pickupLatLong[0].toDouble(), pickupLatLong[1].toDouble()),
                                LatLng(dropLatLong[0].toDouble(), dropLatLong[1].toDouble())
                            )
                        }
                    }


                    setDriverData(
                        referData!!.driverName,
                        referData!!.bookingOTP,
                        referData!!.driverRatting,
                        referData!!.drvcarModel,
                        referData!!.drvcarNo,
                        referData!!.driverMobile,
                        referData!!.categoryImage,
                        referData!!.driverProfilePic
                    )


                } else if (push.type.equals(Push.driverReachedAtPickupLocation, true)) {
                    try {


                        if (activity != null) {


                            if (mBottomSheetDialog != null && mBottomSheetDialog.isShowing) {
                                mBottomSheetDialog.dismiss()
                            }

                            if (myBookingTimer != null) {
                                myBookingTimer.cancel()
                            }

                            pushType = push.type!!
                            isDriverReached = true
                            driversLatLng = null
                            MyUtils.expand(confirmDestinationTextView)
                            confirmDestinationTextView.text = "Driver has arrived at your location"
                            Handler().postDelayed(
                                {
                                    MyUtils.collapse(confirmDestinationTextView)
                                }, 3000
                            )

                            confirmBottomLayout.visibility = View.VISIBLE
                            bottomLayout.visibility = View.GONE

                            setDriverData(
                                referData!!.driverName,
                                referData!!.bookingOTP,
                                referData!!.driverRatting,
                                referData!!.drvcarModel,
                                referData!!.drvcarNo,
                                referData!!.driverMobile,
                                referData!!.categoryImage,
                                referData!!.driverProfilePic
                            )

                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (push.type.equals(Push.tripCompleted, true)) {

                    tv_confirmTrip_otp.visibility = View.GONE
                    btn_cancelTrip.visibility = View.GONE
                    confirmBottomLayout.visibility = View.GONE
                    centerMapFab.visibility = View.VISIBLE
                    topLayout.visibility = View.VISIBLE
                    mainMenu_ImageView.tag = 0
                    mainMenu_ImageView.setImageResource(R.drawable.menu_icon)
                    isTrackingOn = false

                    pushType = push.type!!
                    bookingId = "-1"
                    if (myTimer != null) {
                        myTimer.cancel()
                    }
                    mMap.clear()

                    mMap.uiSettings.isZoomGesturesEnabled = true
                    mMap.uiSettings.isScrollGesturesEnabled = true

                    getPricingList()

                    if (referData != null) {
                        val args = Bundle()
                        args.putSerializable("referData", referData as Serializable)
                        val bottomSheetFragment = PaymentPopupFragment()
                        bottomSheetFragment.arguments = args
                        (activity as MainActivity).navigateTo(bottomSheetFragment, true)
                    }

                    mMap.isMyLocationEnabled = true

                } else if (push.type.equals(Push.tripCancelledByDriver, true)) {
                    isTrackingOn = false
                    (activity as MainActivity).showSnackBar("Trip cancelled by driver")

                    setCancelTripData()
                }

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }
    }

    private fun cancelTripReasonMaster() {
        MyUtils.showProgressDialog(activity as MainActivity, "Please wait...")


        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("reasonFor", "Customer")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val cancelTripModel = ViewModelProviders.of(this@MainFragment).get(CancelTripMasterModel::class.java)
        cancelTripModel.getCancelTripMaster(activity as MainActivity, false, jsonArray.toString())
            .observe(this@MainFragment,
                Observer<List<CancelTrip>> { canceltripPojo ->
                    if (canceltripPojo != null && canceltripPojo.size > 0) {
                        if (canceltripPojo[0].status.equals("true")) {

                            MyUtils.dismissProgressDialog()

                            cancelTripMaster.clear()
                            cancelTripMaster.addAll(canceltripPojo[0].data)
                            var canceltripBottomSheetFragment = CancelBottomSheetFragment()
                            var bundle = Bundle()
                            bundle.putSerializable("CancelTripData", canceltripPojo[0].data as Serializable)
                            bundle.putString("bookingId", bookingId)
                            canceltripBottomSheetFragment.arguments = bundle
                            canceltripBottomSheetFragment.show(childFragmentManager, "CancelTrip")

                        } else {
                            MyUtils.dismissProgressDialog()
                            (activity as MainActivity).showSnackBar(canceltripPojo[0].message)
                        }

                    } else {
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).errorMethod()
                    }
                })

    }


    override fun onSucessfullyCancelTrip() {
        setCancelTripData()

        if (mMap != null) {
            mMap.clear()
        }

        if (call != null) {
            call!!.cancel()
        }

        isTrackingOn = false

        setDataTab1()

    }


    private fun setCancelTripData() {
        driversLatLng = null
        if (myTimer != null) {
            myTimer.cancel()
        }
        if (myBookingTimer != null) {
            myBookingTimer.cancel()
        }
        mMap.clear()

        mMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    currentLatLong!!.latitude, currentLatLong!!.longitude
                ), 15f
            )
        )

        destination = false
        llfrom.setBackgroundColor(activity!!.resources.getColor(R.color.whiteselector))
        lldestination.setBackgroundColor(activity!!.resources.getColor(R.color.white))

        destinationTextView.text = ""
        centerMapFab.visibility = View.VISIBLE
        mainMenu_ImageView.tag = 0
        isfromDisabled = false
        bookingType = "Local"
        bookingSubType = ""
        central_marker.setImageResource(R.drawable.map_pin_green)
        getPricingList()
        confirmBottomLayout.visibility = View.GONE
        ll_boockcar.visibility = View.GONE
        recyclertCarItem.visibility = View.VISIBLE
        ll_booktrip.visibility = View.VISIBLE
        btnconfirmingbook.visibility = View.GONE

    }

    fun setTabPosition() {
        ll_outstation.visibility = View.GONE
        tabs.getTabAt(0)!!.select()
    }

    fun showMessageOKCancel(
        context: Context,
        message: String,
        title: String = "",
        okListener: DialogInterface.OnClickListener
    ): MaterialAlertDialogBuilder {
        val builder = MaterialAlertDialogBuilder(context)
        builder.setMessage(message)
        builder.setTitle(title)
        builder.setCancelable(false)
        builder.setPositiveButton("CONTINUE", okListener)
        builder.setNegativeButton("CHANGE DROP") { dialog, which ->
            dialog.dismiss()
            openDestinationActivity("Destination", 1)
        }
        builder.show()

        return builder
    }

    public fun isBackValid(): Boolean {
        when {
            ll_boockcar.visibility == View.VISIBLE -> {

                backConfirmBooking()
                mainMenu_ImageView.setImageResource(R.drawable.menu_icon)
                mainMenu_ImageView.tag = 0
                return false
            }

            confirmBottomLayout.visibility == View.VISIBLE -> {
                mMap.clear()
                backConfirmBooking()
                centerMapFab.performClick()
                ll_boockcar.visibility == View.GONE
                bottomLayout.visibility = View.VISIBLE
                confirmBottomLayout.visibility = View.GONE
                getPricingList()
                mainMenu_ImageView.setImageResource(R.drawable.menu_icon)
                mainMenu_ImageView.tag = 0
                return false


            }


            ll_outstation.visibility == View.VISIBLE -> {

                setDataTab1()
                setTabPosition()
                return false
            }
            else -> {
                return true
            }
        }
    }


    private fun getCouponofferApi(
        categoryID: String,
        pickupCityName: String,
        searchkeyword: String,
        from: String
    ) {
        if (from.equals("local_outstation", true)) {
            MyUtils.showProgressDialog(activity!!, "Please Wait..")

        }
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("categoryID", categoryID)
            jsonObject.put("pickupCityName", pickupCityName)
            jsonObject.put("pickupStateName", pickupAddress[0].adminArea)
            jsonObject.put("searchkeyword", searchkeyword)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val couponofferModel = ViewModelProviders.of(this@MainFragment).get(CouponofferModel::class.java)
        couponofferModel.getcouponoffer(activity!!, jsonArray.toString())
            .observe(this, Observer<List<CouponofferPojo>> { couponofferPojo ->
                if (couponofferPojo != null && couponofferPojo.isNotEmpty()) {
                    if (couponofferPojo[0].status.equals("true", true)) {
                        if (from.equals("local_outstation", true)) {
                            MyUtils.dismissProgressDialog()
                        }
                        couponofferData.clear()
                        couponofferData.addAll(couponofferPojo[0].data)

                        if (from.equals("local_outstation", true)) {
                            val bottomSheetFragment = SelectCouponCodeBottomSheetFragment()
                            var bundle = Bundle()
                            bundle.putSerializable("couponofferData", couponofferPojo[0].data as Serializable)
                            bundle.putSerializable("categoryID", categoryID)
                            bundle.putSerializable("pickupCityName", pickupCityName)
                            bundle.putSerializable("amount", "" + calculateCharge(withGST = "No"))
                            bottomSheetFragment.arguments = bundle
                            bottomSheetFragment.show(childFragmentManager!!, bottomSheetFragment.tag)
                        } else {
                            outstationoffersAdapter.notifyDataSetChanged()
                        }

                    } else {
                        if (from.equals("local_outstation", true)) {
                            MyUtils.dismissProgressDialog()
                            (activity as MainActivity).showSnackBar(couponofferPojo[0].message)

                        }


                    }

                } else {
                    if (from.equals("local_outstation", true)) {
                        MyUtils.dismissProgressDialog()
                    }

                    try {
                        if (!MyUtils.isInternetAvailable(activity!!)) {
                            (activity as MainActivity).showSnackBar(resources.getString(R.string.error_common_network))

                        } else {

                            (activity as MainActivity).showSnackBar(resources.getString(R.string.error_crash_error_message))

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }


    override fun onSucessfullyApplyCouponcode(
        code: String,
        amount: String,
        couponDiscount: String,
        discountType: String
    ) {
        if (code.isNotEmpty()) {
            ll_coupon_code_text.visibility = View.VISIBLE
            tv_couponcode.text = code
            img_right_arrow.visibility = View.GONE
        } else {
            ll_coupon_code_text.visibility = View.GONE
            tv_couponcode.text = "Coupon code"
            img_right_arrow.visibility = View.VISIBLE
        }
//        disscountAmount = couponDiscount.toDouble()

        isCouponApplied = true
        var discountedValue = 0.00
        this.disscountType = discountType

        var totalCharge = calculateCharge(withGST = "No")

        if (code.isNotEmpty()) {

            if (discountType.equals("Percentage")) {
                discountedValue = totalCharge - totalCharge * couponDiscount.toDouble() / 100
                totalAmountDiscount = totalCharge * couponDiscount.toDouble() / 100
            } else {
                discountedValue = totalCharge.minus(couponDiscount.toDouble())
                totalAmountDiscount = couponDiscount.toDouble()
            }
        } else {
            discountedValue = totalCharge
        }

        discountedValue =
            discountedValue + discountedValue * localTripData[localSelectedPos].categoryGST.toDouble() / 100

        couponCode = code
        disscontPercentage = couponDiscount.toDouble()
        disscountAmount = MyUtils.discountFormat(discountedValue)
        finalAmount = disscountAmount.toString()
        if (discountedValue < 0.00) {
            tv_boockedcar_price.text = "0.00"

        } else {
            tv_boockedcar_price.text = "" + MyUtils.discountFormat(discountedValue)

        }
    }

}




