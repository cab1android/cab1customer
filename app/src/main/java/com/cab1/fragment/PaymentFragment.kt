package com.cab1.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.adapter.PaymentStoredcarddapter
import com.cab1.api.RestClient
import com.cab1.model.DefultPaymentModel
import com.cab1.pojo.CommonPojo
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 *
 */
class PaymentFragment : Fragment() {

    private lateinit var paymentStoredcarddapter: PaymentStoredcarddapter
    lateinit var sessionManager: SessionManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(activity!!)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)


        tvVerifcationTitle.text = "Payment"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        if (sessionManager.get_Authenticate_User().userDefaultCash.equals("Credit Card", true)) {
            radioButton_creditCard.isChecked = true
        } else if (sessionManager.get_Authenticate_User().userDefaultCash.equals("Debit Card", true)) {
            radioButton_debitCard.isChecked = true
        } else if (sessionManager.get_Authenticate_User().userDefaultCash.equals("Paytm", true)) {
            radioButton_paytm.isChecked = true

        } else if (sessionManager.get_Authenticate_User().userDefaultCash.equals("Cash", true)) {
            radioButton_cash.isChecked = true
        }
        rgGroup.setOnCheckedChangeListener { group, checkedId ->
            if (radioButton_creditCard.isChecked) {
                getDefultPayment(radioButton_creditCard.text.toString())
            } else if (radioButton_debitCard.isChecked) {
                getDefultPayment(radioButton_debitCard.text.toString())
            } else if (radioButton_paytm.isChecked) {
                getDefultPayment(radioButton_paytm.text.toString())
            } else if (radioButton_cash.isChecked) {
                getDefultPayment(radioButton_cash.text.toString())
            }
        }
    }

    private fun getDefultPayment(defaultCash: String) {

        MyUtils.showProgressDialog(activity!!, "Please Wait..")

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("userDefaultCash", defaultCash)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val defaultPaymentModel = ViewModelProviders.of(this@PaymentFragment).get(DefultPaymentModel::class.java)
        defaultPaymentModel.getDefaultPayment(activity!!, jsonArray.toString())
            .observe(this@PaymentFragment,
                Observer<List<CommonPojo>?> { commonPojo ->
                    if (commonPojo != null && commonPojo.isNotEmpty()) {
                        if (commonPojo[0].status.equals("true", true)) {
                            MyUtils.dismissProgressDialog()
                            (activity as MainActivity).showSnackBar(commonPojo[0].message)
                            if (defaultCash.equals("Credit Card", true)) {
                                radioButton_creditCard.isChecked = true
                            } else if (defaultCash.equals("Debit Card", true)) {
                                radioButton_debitCard.isChecked = true
                            } else if (defaultCash.equals("Paytm", true)) {
                                radioButton_paytm.isChecked = true

                            } else if (defaultCash.equals("Cash", true)) {
                                radioButton_cash.isChecked = true
                            }

                           var paymentMode=sessionManager.get_Authenticate_User()

                            paymentMode.userDefaultCash=defaultCash

                            val gson = Gson()
                            val json = gson.toJson(paymentMode)

                            sessionManager.create_login_session(
                                json,
                               paymentMode.userMobile,
                                "",
                                true, true,
                                sessionManager.isEmailLogin(),true
                            )



                        } else {
                            MyUtils.dismissProgressDialog()
                            (activity as MainActivity).showSnackBar(commonPojo[0].message)
                        }


                    } else {
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).errorMethod()
                    }
                })

    }

}
