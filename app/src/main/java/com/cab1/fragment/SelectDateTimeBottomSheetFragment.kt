package com.cab1.fragment


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.format.DateUtils
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.PagerAdapter
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.adapter.DaysAdapter
import com.cab1.adapter.PaymentStoredcarddapter
import com.cab1.pojo.Data1
import com.cab1.pojo.DaysMonthsPojo
import com.cab1.pojo.GoogleDirection
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.fragment_select_date_time_bottom_sheet.*
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class SelectDateTimeBottomSheetFragment : BottomSheetDialogFragment() {

    var totalAmount: Double = 0.0
    var extra: Double = 0.0
    var dropLat = ""
    var dropLongi = ""
    var pickupLat = ""
    var pickupLongi = ""
    var fromLocation = ""
    var destinationLocation = ""
    var bookingType = ""
    private var mListener: ConfirmBookNowListener? = null
    var bookingSubType = ""
    private lateinit var paymentStoredcarddapter: PaymentStoredcarddapter
    var directionRouteResponse: GoogleDirection? = null
    lateinit var sessionManager: SessionManager
    private var priceData = java.util.ArrayList<Data1>()
    private var year: Int = 0
    private var tmpMonth: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private var hour: Int = 0
    private var minute: Int = 0
    private var seconds: Int = 0
    private var bookingPickupTime: String = ""
    private lateinit var layoutManagerRv: LinearLayoutManager
    private lateinit var daysAdapter: DaysAdapter
    var i = 0
    var list = ArrayList<String>()
    var monthList = ArrayList<DaysMonthsPojo>()
    var dateList = ArrayList<DaysMonthsPojo>()
    private var from: String = ""
    private var pickupDate: String = ""
    private var returnDate: String = ""
    lateinit var startDate: Date
    lateinit var endDate: Date
    private var sdf = SimpleDateFormat("yyyy-MM-dd")
    lateinit var snakview: View
    var isPickupLayout = true
    var outstationType: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }


    interface ConfirmBookNowListener {

        fun onConfirmation(bookingPickupTime: String, isPickup: Boolean)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        snakview = inflater.inflate(R.layout.fragment_select_date_time_bottom_sheet, container, false)

        return snakview
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        snakview.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                snakview.viewTreeObserver.removeGlobalOnLayoutListener(this)
                val height = snakview.measuredHeight
                val mBottomSheetBehavior = BottomSheetBehavior.from(snakview.parent as View)
                if (mBottomSheetBehavior != null) {
                    mBottomSheetBehavior.peekHeight = height
                    snakview.requestLayout()
                }
            }
        })
    }

    /* override fun onAttach(activity: Context) {
         super.onAttach(activity)

         val parent = parentFragment
         mListener = if (parent != null) {
             parent as ConfirmBookNowListener
         } else {
             activity as ConfirmBookNowListener
         }

     }*/
    fun setConfirmBookNowListener(listener: ConfirmBookNowListener) {
        this.mListener = listener
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        if (arguments != null) {
            bookingType = arguments!!.getString("bookingType")
            from = arguments!!.getString("from")

            if (bookingType.equals("Local")) {
                directionRouteResponse = arguments!!.getSerializable("directionRouteResponse") as GoogleDirection?
                dropLat = arguments!!.getString("dropLat")
                dropLongi = arguments!!.getString("dropLongi")
                pickupLat = arguments!!.getString("pickupLat")
                pickupLongi = arguments!!.getString("pickupLongi")
                fromLocation = arguments!!.getString("fromLocation")
                destinationLocation = arguments!!.getString("destinationLocation")
                bookingSubType = arguments!!.getString("bookingSubType")
                priceData = arguments!!.getSerializable("priceData") as java.util.ArrayList<Data1>
                Log.d("System out", "pickup : " + pickupLat + "\n" + pickupLongi)
            } else if (bookingType.equals("Rental")) {
                directionRouteResponse = arguments!!.getSerializable("directionRouteResponse") as GoogleDirection?
                dropLat = arguments!!.getString("dropLat")
                dropLongi = arguments!!.getString("dropLongi")
                pickupLat = arguments!!.getString("pickupLat")
                pickupLongi = arguments!!.getString("pickupLongi")
                fromLocation = arguments!!.getString("pickupLocation")
                destinationLocation = arguments!!.getString("destinationLocation")
                bookingSubType = arguments!!.getString("bookingSubType")
                priceData = arguments!!.getSerializable("priceData") as java.util.ArrayList<Data1>
            } else if (bookingType.equals("Outstation")) {

                pickupDate = arguments!!.getString("pickupDate", "")
                returnDate = arguments!!.getString("returnDate", "")
                outstationType = arguments!!.getString("outstationType", "")

            }

        }

        val c = Calendar.getInstance()
        val df = SimpleDateFormat("EEE,dd MMM")
        val tdf = SimpleDateFormat("hh:mm a")


        returnDateDisplayMainLL.isClickable = !outstationType.equals("One Way", true)
        when (from) {
            "Book Later" -> {
                baseLineCommonLayout.visibility = View.GONE
                ll_outstation_date.visibility = View.GONE
            }
            "selectDate" -> {
                pickupDateDisplayMainLL.background = resources.getDrawable(R.color.bg)
                returnDateDisplayMainLL.background = resources.getDrawable(R.color.white)
                isPickupLayout = true
                selectPickup()
            }
            "returnDate" -> {
                isPickupLayout = false
                pickupDateDisplayMainLL.background = resources.getDrawable(R.color.white)
                returnDateDisplayMainLL.background = resources.getDrawable(R.color.bg)
                selectReturn()

            }
        }
        headerText.text = resources.getString(R.string.select_date_time)
        headerText.gravity = Gravity.START
        baseLineCommonLayout.visibility = View.GONE
        backIconImageView.visibility = View.VISIBLE


        val currCalendar = Calendar.getInstance()
        val returnDateCalendar = Calendar.getInstance()

        if (bookingType.equals("Outstation")) {

            currCalendar.time = (SimpleDateFormat("EEE,dd MMM yyyy hh:mm a").parse(pickupDate))

            if (outstationType.equals("One Way", true)) {
                returnDateDisplayTextView.text = "Select Date and Time"
                returnDateDisplayTextView.setTextColor(resources.getColor(R.color.text_secondary))
                returnTimeDisplayTextView.visibility = View.INVISIBLE
            } else {
                returnDateCalendar.time = (SimpleDateFormat("EEE,dd MMM yyyy hh:mm a").parse(returnDate))
            }

        }


        continueButton.setOnClickListener {
            try {
                bookingPickupTime = dateList.get(daysAdapter.selectedDatePostion).date + " " +
                        MyUtils.formatDate(selectedTimeValueTextView.text.toString(), "hh:mm a", "HH:mm:ss")
                var simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

                if (simpleDateFormat.parse(bookingPickupTime).before(
                        simpleDateFormat.parse(
                            simpleDateFormat.format(
                                startDate
                            )
                        )
                    )
                ) {
                    Toast.makeText(activity!!, "Please select valid booking date and time", Toast.LENGTH_LONG).show()
                } else if (bookingType.equals("Local")) {


                    if (mListener != null)
                        mListener!!.onConfirmation(bookingPickupTime, false)

                    dismiss()
    //            }


                } else if (bookingType.equals("Rental")) {
                    val args = Bundle()

                    args.putSerializable("priceData", priceData as Serializable)
                    args.putString("pickupLat", pickupLat)
                    args.putString("pickupLongi", pickupLongi)
                    args.putString("bookingType", bookingType)
                    args.putString("bookingPickupTime", bookingPickupTime)
                    args.putString("bookingSubType", bookingSubType)
                    args.putString("pickupLocation", fromLocation)
    //                args.putSerializable("directionRouteResponse", directionRouteResponse as Serializable)

                    val bottomSheetFragment = ChoosePackageListingBottomsheetFragment()
                    bottomSheetFragment.arguments = args
                    bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
                    dismiss()
                } else {
                    if (mListener != null)
                        mListener!!.onConfirmation(bookingPickupTime, isPickupLayout)

                    dismiss()
                }
            } catch (e: Exception) {
            }
        }

        year = currCalendar.get(Calendar.YEAR)
        month = currCalendar.get(Calendar.MONTH)
        tmpMonth = currCalendar.get(Calendar.MONTH)
        day = currCalendar.get(Calendar.DAY_OF_MONTH)
        hour = currCalendar.get(Calendar.HOUR_OF_DAY)

        if (bookingType.equals("Outstation")) {

            minute = currCalendar.get(Calendar.MINUTE)


        } else {

            minute = currCalendar.get(Calendar.MINUTE) + 5
        }

        currCalendar.set(Calendar.MINUTE, minute)
        seconds = currCalendar.get(Calendar.SECOND)
        selectedTimeValueTextView.text = SimpleDateFormat("hh:mm a").format(currCalendar.time)

        pickupDateDisplyTextView.text = df.format(currCalendar.time)
        pickupTimeDisplayTextView.text = tdf.format(currCalendar.time)
        if (bookingType == "Outstation" && !outstationType.equals("One Way", true)) {
            minute = currCalendar.get(Calendar.MINUTE) + 60
            currCalendar.set(Calendar.MINUTE, minute)


            returnDateDisplayTextView.text = df.format(currCalendar.time)
            returnDateDisplayTextView.visibility = View.VISIBLE
            returnTimeDisplayTextView.visibility = View.VISIBLE

            returnTimeDisplayTextView.text = tdf.format(currCalendar.time)
            selectedTimeValueTextView.text = SimpleDateFormat("hh:mm a").format(currCalendar.time)


        }


        startDate = currCalendar.time
        currCalendar.add(Calendar.DAY_OF_MONTH, 7)
        endDate = currCalendar.time



        layoutManagerRv = LinearLayoutManager(activity!!, LinearLayout.HORIZONTAL, false)

        daysAdapter = DaysAdapter(activity!!, dateList, object : DaysAdapter.onClickedListener {
            override fun clicked(pos: Int) {

                bookingPickupTime = dateList.get(pos).date + " " +
                        MyUtils.formatDate(selectedTimeValueTextView.text.toString(), "hh:mm a", "HH:mm:ss")


                if (isPickupLayout) {
                    pickupDateDisplyTextView.text =
                        MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "EEE,dd MMM")
                    pickupTimeDisplayTextView.text =
                        MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "hh:mm: a")
                } else {
                    returnDateDisplayTextView.text =
                        MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "EEE,dd MMM")
                    returnTimeDisplayTextView.text =
                        MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "hh:mm: a")
                }

            }
        })

        daysRecyclerView.layoutManager = layoutManagerRv
        daysRecyclerView.adapter = daysAdapter
        timePickerView.currentHour = hour
        timePickerView.currentMinute = minute
        setDate()


        timePickerView.setOnTimeChangedListener { _, hour1, minute1 ->


            val datetime = Calendar.getInstance()
            datetime.set(Calendar.HOUR_OF_DAY, hour1)
            datetime.set(Calendar.MINUTE, minute1)


            selectedTimeValueTextView.text = SimpleDateFormat("hh:mm a").format(datetime.time)

        }
        monthYearViewPager.adapter = CustomPagerAdapter(activity as MainActivity)


        if (monthList.size == 0) {
            nextArrow.visibility = View.INVISIBLE
            previousArrow.visibility = View.INVISIBLE
        }

        previousArrow.setOnClickListener {

            if (monthList.size >= 2) {

                i = monthYearViewPager.currentItem
                i--
                monthYearViewPager.currentItem = i

                if (i < monthYearViewPager?.adapter!!.count) {
                    nextArrow.visibility = View.VISIBLE
                }

                if (i > 0) {
                    previousArrow.visibility = View.VISIBLE
                } else {
                    previousArrow.visibility = View.INVISIBLE
                }

                for (i in 0 until dateList.size) {
                    if (dateList[i].monthName.equals(monthList[monthYearViewPager.currentItem].monthName)) {
                        daysRecyclerView.smoothScrollToPosition(i)
                        break
                    }
                }

            }

        }

        nextArrow.setOnClickListener {
            if (monthList.size >= 2) {
                i = monthYearViewPager.currentItem
                i++
                monthYearViewPager.currentItem = i

                if (i > 0) {
                    previousArrow.visibility = View.VISIBLE
                }

                if (i < monthYearViewPager?.adapter!!.count) {
                    nextArrow.visibility = View.VISIBLE
                } else {
                    nextArrow.visibility = View.INVISIBLE
                }


                for (i in 0 until dateList.size) {
                    if (dateList[i].monthName.equals(monthList[monthYearViewPager.currentItem].monthName)) {
                        daysRecyclerView.smoothScrollToPosition(i)
                        break
                    }
                }


            }


        }

        returnDateDisplayMainLL.setOnClickListener {

            if (outstationType.equals("Round Trip"))
                selectReturn()

        }

        pickupDateDisplayMainLL.setOnClickListener {
            selectPickup()
        }

        dialogCloseIcon.setOnClickListener {
            dismiss()
        }

        backIconImageView.setOnClickListener {
            dismiss()
        }

    }


    fun selectPickup() {
        isPickupLayout = true
        pickupBaseLine.visibility = View.VISIBLE
        returnBaseLine.visibility = View.INVISIBLE
        pickupDateDisplayMainLL.setBackgroundColor(resources.getColor(R.color.bg))
        returnDateDisplayMainLL.setBackgroundColor(resources.getColor(R.color.white))

    }

    fun selectReturn() {
        isPickupLayout = false
        pickupBaseLine.visibility = View.INVISIBLE
        returnBaseLine.visibility = View.VISIBLE
        pickupDateDisplayMainLL.setBackgroundColor(resources.getColor(R.color.white))
        returnDateDisplayMainLL.setBackgroundColor(resources.getColor(R.color.bg))

    }


    internal inner class CustomPagerAdapter(var mContext: Context) : PagerAdapter() {

        var mLayoutInflater: LayoutInflater

        init {
            mLayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        override fun getCount(): Int {
            return monthList.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as LinearLayoutCompat
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = mLayoutInflater.inflate(R.layout.view_pager_layout, container, false)

            val text = itemView.findViewById<TextView>(R.id.textVal)

            text.text = monthList.get(position).monthName

            container.addView(itemView)

            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as LinearLayoutCompat)
        }
    }

    fun setDate() {
        dateList.clear()
        monthList.clear()
        var c = Calendar.getInstance()
        c.time = startDate

        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)


        var endCalendar = Calendar.getInstance()
        endCalendar.time = endDate
        endCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        endCalendar.add(Calendar.DAY_OF_WEEK, 6)
        var endWeekDate: Date = endCalendar.time


        while (c.time <= endWeekDate)
        {

            val data = DaysMonthsPojo()

            var dt = Date()

            dt = c.time

            data.month = c.get(Calendar.MONTH)
            data.monthName = SimpleDateFormat("MMMM yyyy").format(c.time)
            data.year = c.get(Calendar.YEAR)
            data.day = c.get(Calendar.DAY_OF_MONTH)
            data.date = sdf.format(dt)
            data.dayName= SimpleDateFormat("EEE").format(c.time)


            if (DateUtils.isToday(c.timeInMillis)) {
                data.isEnable = true
                data.isSelect = true

            }
            else {
                data.isEnable = sdf.parse(sdf.format(startDate)).before(sdf.parse(sdf.format(c.time))) && sdf.parse(
                    sdf.format(
                        endDate
                    )
                ) >= (sdf.parse(sdf.format(c.time)))

            }




            c.add(Calendar.DATE, 1)


            dateList.add(data)


//            daylistNumbers.add("" + day++)

        }
        for(i in 0 until dateList.size ){

            if(dateList[i].isSelect)
                daysAdapter.selectedDatePostion=i

            if (monthList.isEmpty())
                monthList.add(dateList[i])
            else {
                for(j in 0 until monthList.size){
                    if (monthList[j].month != dateList[i].month)

                        monthList.add(dateList[i])
                }


            }
        }
        daysAdapter.notifyDataSetChanged()
    }



}
