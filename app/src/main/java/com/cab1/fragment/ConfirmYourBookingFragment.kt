package com.cab1.fragment

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Address
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cab1.MainActivity
import com.cab1.PaymentActivity
import com.cab1.R
import com.cab1.SuccessTripActivity
import com.cab1.api.RestClient
import com.cab1.cab1.driver.util.Googleutil
import com.cab1.driver.model.CouponofferModel
import com.cab1.model.CreateTripModel
import com.cab1.notification.Push
import com.cab1.pojo.CouponofferData
import com.cab1.pojo.CouponofferPojo
import com.cab1.pojo.Data1
import com.cab1.pojo.GoogleDirection
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.fragment_confirm_your_booking.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ConfirmYourBookingFragment : Fragment(),
    SelectCouponCodeBottomSheetFragment.CouponcodeListener {

    var discountedValue = 0.00
    private var priceData = java.util.ArrayList<Data1>()
    var pickupLat = ""
    var pickupLongi = ""
    var dropLat = ""
    var dropLongi = ""
    lateinit var mContext: Activity
    lateinit var sessionManager: SessionManager
    var bookingType = ""
    var bookingSubType = ""
    var bookingPaymentPercentage = ""
    var bookingPaymentAmount = ""
    var bookingPickupTime = ""
    var rentalPricePos = 0
    var rentalPackagePos = 0
    var outstationID = ""
    var outstationType = ""
    var destinationLocation = ""
    var pickupLocation = ""
    var pickupCityName = ""
    var couponCode = ""
    var isCouponApplied = false
    var totalAmountDiscount: Double = 0.00
    var disscountAmount: Double = 0.00
    var directionRouteResponse: GoogleDirection? = null
    var outStationPickupDate = ""
    var outStationPickupTime = ""
    var returnDate = ""
    var returnTime = ""
    var disscountType = ""
    var amoutResult = ""
    var mainPrice = ""
    var disscontPercentage: Double = 0.00
    private var couponofferData = ArrayList<CouponofferData>()
    var pickupAddress: List<Address> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_confirm_your_booking, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        mContext = context as Activity

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        sessionManager = SessionManager(mContext)

        tvVerifcationTitle.text = "Confirm your booking"

        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        (activity as MainActivity).drawerSwipe(false)

        getIntentValues()

        payLaterRadioButton.isClickable = false
        payfifteenRaduiButton.isClickable = false
        payHundredRaduiButton.isClickable = false

        LocalBroadcastManager.getInstance(activity!!)
            .registerReceiver(pushReceiver, IntentFilter(Push.action))


        payFifteenLL.setOnClickListener {

            var result: Double
            if (bookingType == "Outstation") {
                result =
                    priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble() - priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble() * 15 / 100

            } else {
                result =
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble() - priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble() * 15 / 100

            }

            bookingPaymentAmount = "" + result
            bookingPaymentPercentage = "15.00"
            payfifteenRaduiButton.isChecked = true
            payHundredRaduiButton.isChecked = false
            payLaterRadioButton.isChecked = false

        }

        payHundredPercentLL.setOnClickListener {

            bookingPaymentPercentage = "100.00"
            payfifteenRaduiButton.isChecked = false
            payHundredRaduiButton.isChecked = true
            payLaterRadioButton.isChecked = false
            bookingPaymentAmount = if (bookingType == "Outstation") {
                priceData[0].outstationPrice[rentalPricePos].outstationPrice
            } else {

                priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice

            }


        }


        payLaterLL.setOnClickListener {

            bookingPaymentPercentage = "0.00"
            bookingPaymentAmount = "0.00"
            payfifteenRaduiButton.isChecked = false
            payHundredRaduiButton.isChecked = false
            payLaterRadioButton.isChecked = true

        }

        editProfileButton.setOnClickListener {

            (context as MainActivity).navigateTo(MyProfileFragment(), true)

        }


        btn_paynow.setOnClickListener {


            //            if (payLaterRadioButton.isChecked) {
//                createTrip("", "")
//            } else {
//                if (disscountAmount == 0.00) {
//                    disscountAmount =
//                        priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice!!.toDouble()
//                }

            var i = Intent(mContext, PaymentActivity::class.java)
            i.putExtra("priceData", priceData as Serializable)
//            i.putExtra("directionRouteResponse", directionRouteResponse as Serializable)
            i.putExtra("longs", pickupLongi)
            i.putExtra("pickupLat", pickupLat)
            i.putExtra("pickupLongi", pickupLongi)
            i.putExtra("dropLat", dropLat)
            i.putExtra("dropLongi", dropLongi)
            i.putExtra("bookingType", bookingType)
            i.putExtra("disscountAmount", "" + disscountAmount)
            i.putExtra("bookingSubType", bookingSubType)
            i.putExtra("bookingPaymentAmount", bookingPaymentAmount)
            i.putExtra("bookingPaymentPercentage", bookingPaymentPercentage)
            i.putExtra("disscountAmount", "" + disscontPercentage)
            i.putExtra("disscountType", "" + disscountType)
            i.putExtra("couponCode", "" + couponCode)
//            i.putExtra("dropLat", "0.00")
//            i.putExtra("dropLongi", "0.00")
            i.putExtra("bookingPickupTime", bookingPickupTime)
            i.putExtra("rentalPricePos", rentalPricePos)
            i.putExtra("destinationLocation", destinationLocation)
            i.putExtra("rentalPackagePos", rentalPackagePos)
            i.putExtra("outstationID", outstationID)
            i.putExtra("outstationType", outstationType)
            i.putExtra("fromLocation", pickupLocation)
            i.putExtra("bookingReturnTime", returnDate + " " + returnTime)

//            i.putExtra("destinationLocation", destinationTextView.text.toString())
            startActivityForResult(i, 5)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
                activity!!.overridePendingTransition(
                    R.anim.slide_in_bottom,
                    R.anim.stay
                )
            }
//            }

        }

        ll_confirmbooking_couponcode.setOnClickListener {
            if (bookingType.equals("Outstation")) {
                getCouponofferApi(
                    priceData[0].outstationPrice[rentalPricePos].categoryID,
                    pickupLocation,
                    "", priceData[0].outstationPrice[rentalPricePos].outstationPrice
                )
            } else {
                getCouponofferApi(
                    priceData[0].rentalPrice[0].rentalpackages[rentalPackagePos].categoryID,
                    pickupLocation,
                    "", priceData[0].rentalPrice[0].rentalpackages[rentalPackagePos].rentalPrice
                )
            }

        }

        choosePackageSelectionImageView.setOnClickListener {
            openFareBreakup()
        }

        tv_fare.setOnClickListener {
            openFareBreakup()
        }
    }

    private val pushReceiver = object : BroadcastReceiver() {
        override fun onReceive(context1: Context, intent: Intent) {
            if (intent.hasExtra("from")) {

                if (intent.extras.getString("from").equals("rental", true))
                    (context as MainActivity).navigateTo(MainFragment(), false)

            }
        }
    }

    private fun openFareBreakup() {

        if (bookingType.equals("Rental")) {
            if (disscountAmount == 0.00) {

                var amount = 0.00

                if (priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice!!.toDouble() > priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice!!.toDouble()) {
                    amount =
                        priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice!!.toDouble()
                } else {
                    amount =
                        priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice!!.toDouble()
                }

                disscountAmount =
                    amount + amount * priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].categoryGST!!.toDouble() / 100
            }

        } else {

            if (totalAmountDiscount == 0.00) {

                var amount = 0.00

                if (priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble() > priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()) {
                    amount = priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble()
                } else {
                    amount = priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()
                }


                disscountAmount =
                    amount + amount * priceData[0].outstationPrice[rentalPricePos].categoryGST.toDouble() / 100

            }
        }


        if (bookingType.equals("Rental")) {

            var payextra = 0.00

            if (priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble() > priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice.toDouble()) {


                var GstIncVal = 0.00
                var takingPrice = 0.00

                takingPrice =
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble()

                GstIncVal =
                    takingPrice + takingPrice * priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].categoryGST.toDouble() / 100


                payextra = GstIncVal * 15 / 100
            } else {
                var GstIncVal = 0.00
                var takingPrice = 0.00

                takingPrice =
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice.toDouble()

                GstIncVal =
                    takingPrice + takingPrice * priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].categoryGST.toDouble() / 100


                payextra = GstIncVal * 15 / 100
            }

            val args = Bundle()

            var baseCharge = "0.00"

            if (priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble() > priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble()) {
                baseCharge = priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice
            } else {
                baseCharge = priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice
            }

            args.putString(
                "baseFare",
                "" + baseCharge
            )
            args.putString(
                "ratePerKM",
                "" + priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalAdditionalKM
            )
            args.putString(
                "timeCharge",
                "" + priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalAdditionalMinute
            )

            if (bookingType.equals("Rental")) {

                args.putString(
                    "waitingFare",
                    "" + priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalWaitingPerMinuteCharge
                )
            } else {
                args.putString(
                    "waitingFare",
                    "" + priceData[0].outstationPrice[rentalPricePos].outstationWaitingPerMinuteCharge
                )
            }


            args.putString("bookingPaymentPercentage", bookingPaymentPercentage)
            args.putBoolean("isDiscount", isCouponApplied)
            args.putString("totalAmountDiscount", "" + MyUtils.discountFormat(totalAmountDiscount))

            args.putString(
                "totalPayableValue",
                "" + disscountAmount
            )

            if (payfifteenRaduiButton.isChecked) {
                args.putString("payextra", "" + payextra)
            } else if (payHundredRaduiButton.isChecked) {
                args.putString("payextra", "" + disscountAmount)

            } else {
                args.putString("payextra", "" + 0.00)
            }


            val bottomSheetFragment = FareBreakupBottomSheetFragment()
            bottomSheetFragment.arguments = args
            bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
        } else if (bookingType.equals("Outstation")) {

            var payextra = 0.00
            var GstIncVal = 0.00
            var takingPrice = 0.00

            if (priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble() > priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()) {

                takingPrice = priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble()

                GstIncVal =
                    takingPrice + takingPrice * priceData[0].outstationPrice[rentalPricePos].categoryGST.toDouble() / 100


                payextra = GstIncVal * 15 / 100
            } else {

                takingPrice = priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()

                GstIncVal =
                    takingPrice + takingPrice * priceData[0].outstationPrice[rentalPricePos].categoryGST.toDouble() / 100
                payextra = GstIncVal * 15 / 100
            }


            val args = Bundle()

            var baseCharge = "0.00"

            if (priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble() > priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()) {
                baseCharge = priceData[0].outstationPrice[rentalPricePos].outstationPrice
            } else {
                baseCharge = priceData[0].outstationPrice[rentalPricePos].outstationMinPrice
            }
            args.putString(
                "baseFare",
                "" + baseCharge
            )
            args.putString(
                "ratePerKM",
                "" + priceData[0].outstationPrice[rentalPricePos].outstationAdditionalKM
            )
            args.putString(
                "timeCharge",
                "" + priceData[0].outstationPrice[rentalPricePos].outstationAdditionalMinute
            )

            args.putString(
                "waitingFare",
                "" + priceData[0].outstationPrice[rentalPricePos].outstationWaitingPerMinuteCharge
            )
            args.putString("bookingPaymentPercentage", bookingPaymentPercentage)
            args.putString(
                "totalPayableValue",
                "" + disscountAmount
            )

            if (payfifteenRaduiButton.isChecked) {
                args.putString("payextra", "" + payextra)
            } else if (payHundredRaduiButton.isChecked) {
                args.putString("payextra", "" + disscountAmount)
            } else {
                args.putString("payextra", "" + 0.00)
            }
            args.putBoolean("isDiscount", isCouponApplied)
            args.putString("totalAmountDiscount", "" + MyUtils.discountFormat(totalAmountDiscount))

            val bottomSheetFragment = FareBreakupBottomSheetFragment()
            bottomSheetFragment.arguments = args
            bottomSheetFragment.show(fragmentManager!!, bottomSheetFragment.tag)
        }
    }

    private fun getIntentValues() {

        if (arguments != null) {
            priceData = arguments!!.getSerializable("priceData") as java.util.ArrayList<Data1>
            pickupLat = arguments!!.getString("pickupLat")
            pickupLongi = arguments!!.getString("pickupLongi")
            dropLongi = arguments!!.getString("dropLongi", "0.00")
            dropLat = arguments!!.getString("dropLat", "0.00")
            bookingType = arguments!!.getString("bookingType")
            bookingSubType = arguments!!.getString("bookingSubType")

            outstationType = arguments!!.getString("outstationType", "")
            bookingPickupTime = arguments!!.getString("bookingPickupTime", "")
            rentalPackagePos = arguments!!.getInt("rentalPackagePos", 0)
            rentalPricePos = arguments!!.getInt("rentalPricePos", 0)
            outstationID = arguments!!.getString("outstationID", "")
            destinationLocation = arguments!!.getString("destinationLocation", "")
            pickupLocation = arguments!!.getString("pickupLocation", "")
            pickupCityName = arguments!!.getString("pickupCityName", "")
            outStationPickupDate = arguments!!.getString("outStationPickupDate", "")
            outStationPickupTime = arguments!!.getString("outStationPickupTime", "")
            returnDate = arguments!!.getString("returnDate", "")
            returnTime = arguments!!.getString("returnTime", "")
            directionRouteResponse = arguments!!.getSerializable("directionRouteResponse") as GoogleDirection?

            if (pickupLat.length > 0 && pickupLongi.length > 0) {
                pickupLocationTv.text =
                    Googleutil.getMyLocationAddress(mContext, pickupLat.toDouble(), pickupLongi.toDouble())
            }

            pickupAddress =
                Googleutil.getMyLocationAddressList(mContext, pickupLat.toDouble(), pickupLongi.toDouble())

            bookingPaymentPercentage = "100.00"
            val result: Double

            usernameTv.text = sessionManager.get_Authenticate_User().userFullName
            userEmailTv.text = sessionManager.get_Authenticate_User().userEmail
            userMobileTv.text =
                sessionManager.get_Authenticate_User().userCountryCode + " " + sessionManager.get_Authenticate_User().userMobile

            if (bookingType.equals("Outstation")) {
                ll_pickup_address.visibility = View.GONE
                outStationFromToLL.visibility = View.VISIBLE
                ll_duration.visibility = View.GONE
                outStationFromAddressTv.text = pickupLocation
                outStationDestinationAddressTv.text = destinationLocation
                fromDateDisplyTv.text = outStationPickupDate
                fromTimeDisplayTv.text = outStationPickupTime

                val imageUri =
                    RestClient.image_category_url + priceData[0].outstationPrice[rentalPricePos].categoryImage
                choosePackageCarImageView.setImageURI(imageUri)

                if (returnDate != null && returnTime != null && returnDate.isNotEmpty() && returnTime.isNotEmpty()) {
                    toDateDisplayTv.text = returnDate
                    toTimeDisplayTv.text = returnTime
                } else {
                    toDateDisplayTv.visibility = View.INVISIBLE
                    toTimeDisplayTv.visibility = View.INVISIBLE
                }


                carTypeTextView.text = priceData[0].outstationPrice[rentalPricePos].categoryName
                carNamesTextView.text = priceData[0].outstationPrice[rentalPricePos].categoryCarModel
                catTypeTv.text = priceData[0].outstationPrice[rentalPricePos].categoryType
                catSeatsTv.text = priceData[0].outstationPrice[rentalPricePos].categorySeats + " Seats"
                luggagesTv.text = priceData[0].outstationPrice[rentalPricePos].categoryNoOfBags + " Luggage Bag"


                var GstIncludedPrice = 0.00

                if (priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble() > priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()) {
                    GstIncludedPrice = priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble()
                } else {
                    GstIncludedPrice = priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()
                }

                val gstval = priceData[0].outstationPrice[rentalPricePos].categoryGST.toDouble()

                GstIncludedPrice = GstIncludedPrice + GstIncludedPrice * gstval / 100

                packagePriceTextView.text =
                    "Rs. " + MyUtils.discountFormat(GstIncludedPrice)
                payWholeAmountTv.text =
                    "Rs. " + MyUtils.discountFormat(GstIncludedPrice)
                totalPriceTv.text =
                    "Rs. " + MyUtils.discountFormat(GstIncludedPrice)
                result =
                    GstIncludedPrice * 15 / 100
                fifteenPercentResultTv.text = "Rs. " + result


            } else {
                ll_pickup_address.visibility = View.VISIBLE
                outStationFromToLL.visibility = View.GONE
                ll_duration.visibility = View.VISIBLE


                var rentalpackage = priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos]

                bookingPaymentAmount =
                    MyUtils.formatingPriceDisplay(rentalpackage.rentalPrice)!!

                carTypeTextView.text = rentalpackage.categoryName
                carNamesTextView.text = rentalpackage.categoryCarModel
                catTypeTv.text = rentalpackage.categoryType
                catSeatsTv.text = rentalpackage.categorySeats + " Seats"
                luggagesTv.text = rentalpackage.categoryNoOfBags + " Luggage Bag"
                totalDurationTv.text =
                    rentalpackage.rentalHours + "hr " + rentalpackage.rentalKM + " kms"

                var GstIncludedPrice = 0.00

                if (rentalpackage.rentalPrice.toDouble() > rentalpackage.rentalMinPrice.toDouble()) {
                    GstIncludedPrice = rentalpackage.rentalPrice.toDouble()
                } else {
                    GstIncludedPrice = rentalpackage.rentalMinPrice.toDouble()
                }

                val gstval = rentalpackage.categoryGST.toDouble()

                GstIncludedPrice = GstIncludedPrice + GstIncludedPrice * gstval / 100

                packagePriceTextView.text =
                    "Rs. " + MyUtils.discountFormat(GstIncludedPrice)

                payWholeAmountTv.text =
                    "Rs. " + MyUtils.discountFormat(GstIncludedPrice)
                totalPriceTv.text =
                    "Rs. " + MyUtils.discountFormat(GstIncludedPrice)
                result =
                    GstIncludedPrice * 15 / 100
                val imageUri =
                    RestClient.image_category_url + rentalpackage.categoryImage
                choosePackageCarImageView.setImageURI(imageUri)
                fifteenPercentResultTv.text = "Rs. " + result


            }

            if (bookingType.equals("Rental")) {

                val rentalAmount = priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice

                if (disscountType.equals("Percentage")) {
                    amoutResult =
                        "" + MyUtils.discountFormat(
                            rentalAmount.toDouble().minus(
                                rentalAmount.toDouble() * disscountAmount / 100
                            )
                        )
                } else {
                    amoutResult = "" + MyUtils.discountFormat(
                        rentalAmount.toDouble().minus(
                            disscountAmount
                        )
                    )
                }


            } else if (bookingType.equals("Outstation")) {

                val outstationAmount = priceData[0].outstationPrice[rentalPricePos].outstationPrice

                if (disscountType.equals("Percentage")) {
                    amoutResult =
                        "" + MyUtils.discountFormat(
                            outstationAmount.toDouble().minus(
                                outstationAmount.toDouble() * disscountAmount / 100
                            )
                        )
                } else {
                    amoutResult = "" + MyUtils.discountFormat(
                        outstationAmount.toDouble().minus(
                            disscountAmount
                        )
                    )
                }
            }

            if (bookingPaymentPercentage.equals("15.00")) {
                amoutResult = "" + amoutResult.toDouble() * 15 / 100
            }



            if (bookingPickupTime != null && bookingPickupTime.length > 0) {

                dateDisplayTv.text = MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "EEE,dd MMM")
                timeDisplayTv.text = MyUtils.formatDate(bookingPickupTime, "yyyy-MM-dd HH:mm:ss", "hh:mm a")

            } else {
                //show Current time
                val c = Calendar.getInstance()
                val df = SimpleDateFormat("EEE,dd MMM")
                val tdf = SimpleDateFormat("hh:mm: a")
                dateDisplayTv.text = df.format(c.time)
                timeDisplayTv.text = tdf.format(c.time)

            }

        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 5 && resultCode == Activity.RESULT_OK) {
            (context as MainActivity).navigateTo(MainFragment(), false)
        }

    }


    private fun createTrip(transactionID: String, bookingPaymentMode: String) {

        btn_paynow.startAnimation()

        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val sdfpickup = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val currentDate = sdf.format(Date())

        var pickupTime: String
        if (bookingSubType.equals("Book Later")) {
            pickupTime = bookingPickupTime
        } else {
            pickupTime = sdfpickup.format(Date())
        }

        val jsonArray = JSONArray()
        val dropaddress =
            Googleutil.getMyLocationAddressList(mContext, dropLat.toDouble(), dropLongi.toDouble())


//        val bookingTravelRoute = JSONArray( as Js)
        val jsonObject = JSONObject()

        try {

//            if (bookingType.equals("Local")) {
//                val gson = GsonBuilder().disableHtmlEscaping().create()
//                val type = object : TypeToken<List<Route>>() {}.type
//                val json1 = gson.toJson(directionRouteResponse!!.routes, type)
//
//                jsonObject.put(
//                    "bookingDurationEst",
//                    "" + (directionRouteResponse!!.routes!![0]!!.legs!![0]!!.duration!!.value!!).toDouble() / 60
//                )
//                val jsonObject1 = JSONArray(json1)
//
//                jsonObject.put(
//                    "bookingDistanceEst",
//                    "" + (directionRouteResponse!!.routes!![0]!!.legs!![0]!!.distance!!.value!!).toDouble() / 1000
//                )
//
//                jsonObject.put("bookingSubType", bookingSubType)
//                jsonObject.put("couponCode", couponCode)
//                jsonObject.put("bookingDiscount", disscountAmount)
//                jsonObject.put("categoryID", priceData[0].localPrice[localCarsposition].categoryID)
//                jsonObject.put("dropCityName", dropaddress[0].locality)
//                jsonObject.put("optionPerMinCharge", priceData[0].localPrice[localCarsposition].optionPerMinCharge)
//
//                jsonObject.put(
//                    "bookingEstimatedAmount",
//                    "" + amoutResult
//                )
//                jsonObject.put(
//                    "optionAdditionalKmCharge",
//                    priceData[0].localPrice[localCarsposition].optionAdditionalKmCharge
//                )
//                jsonObject.put("bookingTravelRoute", jsonObject1)
//                jsonObject.put(
//                    "bookingDroplatlong",
//                    dropLat + "," + dropLongi
//                )
//                jsonObject.put("optionBaseMinute", priceData[0].localPrice[localCarsposition].optionBaseMinute)
//                jsonObject.put("optionBasefare", priceData[0].localPrice[localCarsposition].optionBasefare)
//                jsonObject.put("optionBaseKM", priceData[0].localPrice[localCarsposition].optionBaseKM)
//                jsonObject.put("optionID", priceData[0].localPrice[localCarsposition].optionID)
            if (bookingType.equals("Rental")) {

                jsonObject.put("bookingSubType", bookingSubType)
                jsonObject.put("couponCode", couponCode)
                jsonObject.put("bookingDiscount", disscountAmount)
                jsonObject.put(
                    "categoryID",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].categoryID
                )
                if (bookingSubType.equals("Book Now")) {
                    bookingPaymentPercentage = bookingPaymentPercentage
                    bookingPaymentAmount = bookingPaymentAmount
                }
                jsonObject.put(
                    "optionBaseMinute",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalHours.toDouble() * 60
                )
                jsonObject.put(
                    "optionBasefare",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice
                )
                jsonObject.put(
                    "optionBaseKM",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalKM
                )
                jsonObject.put("dropCityName", "")
                jsonObject.put(
                    "optionAdditionalKmCharge",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalAdditionalKM
                )
                jsonObject.put("bookingPaymentPercentage", bookingPaymentPercentage)
                jsonObject.put("bookingPaymentAmount", bookingPaymentAmount)
                jsonObject.put(
                    "optionPerMinCharge",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalAdditionalMinute
                )
                jsonObject.put(
                    "rentalID",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalID
                )

                jsonObject.put(
                    "bookingEstimatedAmount",
                    "" + amoutResult
                )
                jsonObject.put(
                    "bookingDistanceEst",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalKM
                )
                jsonObject.put(
                    "bookingDurationEst",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalHours.toDouble() * 60
                )

            } else if (bookingType.equals("Outstation")) {
                if (returnTime != null && returnTime.isNotEmpty() && returnDate != null && !returnDate.equals(
                        "Select Date Time",
                        true
                    ) && !returnTime.equals(
                        "Select Date Time",
                        true
                    )
                ) {
                    jsonObject.put(
                        "bookingReturnTime",
                        MyUtils.formatDate(returnDate + " " + returnTime, "EEE,dd MMM hh:mm a", "yyyy-MM-dd HH:mm:ss")
                    )
                } else {
                    jsonObject.put(
                        "bookingReturnTime", "Select Date Time"
                    )
                }
                jsonObject.put("categoryID", priceData[0].outstationPrice[rentalPricePos].categoryID)
                jsonObject.put("outstationID", outstationID)
                jsonObject.put("couponCode", couponCode)
                jsonObject.put("bookingDiscount", disscountAmount)
                jsonObject.put("bookingSubType", outstationType)
                jsonObject.put("bookingPaymentPercentage", bookingPaymentPercentage)
                jsonObject.put(
                    "bookingDistanceEst",
                    priceData[0].outstationPrice[rentalPricePos].outstationKM
                )
                jsonObject.put(
                    "bookingDurationEst",
                    priceData[0].outstationPrice[rentalPricePos].outstationHours.toDouble() * 60
                )
                jsonObject.put(
                    "optionBaseMinute",
                    priceData[0].outstationPrice[rentalPricePos].outstationAdditionalMinute.toDouble() * 60
                )
                jsonObject.put(
                    "bookingEstimatedAmount",
                    "" + amoutResult
                )
                jsonObject.put("bookingPaymentPercentage", bookingPaymentPercentage)
                jsonObject.put(
                    "optionAdditionalKmCharge",
                    priceData[0].outstationPrice[rentalPricePos].outstationAdditionalKM
                )
                jsonObject.put("bookingPaymentAmount", bookingPaymentAmount)
                jsonObject.put(
                    "bookingDroplatlong",
                    dropLat + "," + dropLongi
                )
                jsonObject.put(
                    "optionPerMinCharge",
                    priceData[0].outstationPrice[rentalPricePos].outstationAdditionalMinute
                )
                jsonObject.put("optionBaseKM", priceData[0].outstationPrice[rentalPricePos].outstationKM)
                jsonObject.put("optionBasefare", priceData[0].outstationPrice[rentalPricePos].outstationPrice)
            }

            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("bookingDate", "" + currentDate)
            jsonObject.put("transactionID", transactionID)
            jsonObject.put("radius", "5")
//            jsonObject.put("categoryID", "1")
            jsonObject.put("dropCityID", "")
            jsonObject.put("bookingDropAddress", destinationLocation)
            jsonObject.put("bookingPickupAddress", pickupLocation)
            jsonObject.put("bookingPickuplatlong", pickupLat + "," + pickupLongi)
            jsonObject.put("bookingPickupTime", pickupTime)
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("pickupCityID", "")
            jsonObject.put("pickupCityName", pickupAddress[0].locality)
            jsonObject.put("bookingPaymentMode", bookingPaymentMode)
            jsonObject.put("bookingType", bookingType)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        Log.d("System out", "Data : " + jsonArray)

        val createTripCall = ViewModelProviders.of(this).get(CreateTripModel::class.java)
        createTripCall.createTrip(mContext, false, jsonArray.toString()).observe(this,
            androidx.lifecycle.Observer { addAddressList ->

                btn_paynow.revertAnimation()
                btn_paynow.setText(mContext.resources.getString(R.string.pay_now))

                if (addAddressList != null && addAddressList.size > 0) {
                    if (addAddressList[0].status.equals("true")) {

                        if (bookingType.equals("Rental") && bookingSubType.equals("Book Now")) {
                            val intentBroadcast = Intent(Push.action)
                            intentBroadcast.putExtra("from", "rental")
                            intentBroadcast.putExtra("bookingId", addAddressList[0].data[0].bookingID)
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentBroadcast)
                            (activity as MainActivity).onBackPressed()

                        } else {
                            val i = Intent(mContext, SuccessTripActivity::class.java)
                            i.putExtra("bookingId", "" + addAddressList[0].data[0].bookingNo)
                            startActivityForResult(i, 5)
                        }

                    } else {

                        MyUtils.showSnackbar(
                            mContext,
                            addAddressList[0].message,
                            confirmBookingMainLL
                        )

                    }

                } else {

                    MyUtils.dismissProgressDialog()
                    try {
                        if (!MyUtils.isInternetAvailable(mContext)) {
                            MyUtils.showSnackbar(
                                mContext,
                                resources.getString(R.string.error_common_network),
                                confirmBookingMainLL
                            )


                        } else {

                            MyUtils.showSnackbar(
                                mContext,
                                resources.getString(R.string.error_crash_error_message),
                                confirmBookingMainLL
                            )

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            })

    }


    private fun getCouponofferApi(
        categoryID: String,
        pickupCityName: String,
        searchkeyword: String,
        amount: String

    ) {
        MyUtils.showProgressDialog(activity!!, "Please Wait..")

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("categoryID", categoryID)
            jsonObject.put("pickupCityName", pickupCityName)
            jsonObject.put("searchkeyword", searchkeyword)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val couponofferModel = ViewModelProviders.of(this@ConfirmYourBookingFragment).get(CouponofferModel::class.java)
        couponofferModel.getcouponoffer(activity!!, jsonArray.toString())
            .observe(this, Observer<List<CouponofferPojo>> { couponofferPojo ->
                if (couponofferPojo != null && couponofferPojo.isNotEmpty()) {
                    if (couponofferPojo[0].status.equals("true", true)) {
                        MyUtils.dismissProgressDialog()
                        couponofferData.clear()
                        couponofferData.addAll(couponofferPojo[0].data)

                        val bottomSheetFragment = SelectCouponCodeBottomSheetFragment()
                        var bundle = Bundle()
                        bundle.putSerializable("couponofferData", couponofferPojo[0].data as Serializable)
                        bundle.putSerializable("categoryID", categoryID)
                        bundle.putSerializable("pickupCityName", pickupCityName)
                        bundle.putSerializable(
                            "amount", amount

                        )

                        bottomSheetFragment.arguments = bundle
                        bottomSheetFragment.show(childFragmentManager!!, bottomSheetFragment.tag)

                    } else {
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).showSnackBar(couponofferPojo[0].message)


                    }

                } else {
                    MyUtils.dismissProgressDialog()

                    try {
                        if (!MyUtils.isInternetAvailable(activity!!)) {
                            (activity as MainActivity).showSnackBar(resources.getString(R.string.error_common_network))

                        } else {

                            (activity as MainActivity).showSnackBar(resources.getString(R.string.error_crash_error_message))

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }

    override fun onSucessfullyApplyCouponcode(
        code: String,
        amount: String,
        couponDiscount: String,
        discountType: String
    ) {

        if (code.isNotEmpty()) {
            tv_confirmbooking_couponcode.text = code
            img_applied.setImageResource(R.drawable.coupone_applied_icon)
            tv_confirmbooking_couponcode_Applied.visibility = View.VISIBLE

            isCouponApplied = true
            this.disscountType = discountType
            disscontPercentage = couponDiscount.toDouble()

            var actualPrice = 0.00

            if (bookingType.equals("Rental")) {

                if (priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble() > priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice.toDouble()) {

                    actualPrice =
                        priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble()
                } else {
                    actualPrice =
                        priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice.toDouble()
                }

            } else {

                if (priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble() > priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()) {
                    actualPrice = priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble()
                } else {
                    actualPrice = priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()
                }
            }


            if (discountType.equals("Percentage")) {
                discountedValue = actualPrice - actualPrice * couponDiscount.toDouble() / 100
                totalAmountDiscount = actualPrice * couponDiscount.toDouble() / 100
            } else {
                discountedValue = actualPrice - couponDiscount.toDouble()
                totalAmountDiscount = couponDiscount.toDouble()
            }


            if (bookingType.equals("Rental")) {
                discountedValue =
                    discountedValue + discountedValue * priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].categoryGST.toDouble() / 100
            } else {
                discountedValue =
                    discountedValue + discountedValue * priceData[0].outstationPrice[rentalPricePos].categoryGST.toDouble() / 100
            }

            couponCode = code

            disscountAmount = discountedValue
            fifteenPercentResultTv.text = "Rs. " + MyUtils.discountFormat(disscountAmount * 15 / 100)

            if (discountedValue < 0.00) {
                packagePriceTextView.text = "Rs. " + "0.00"
                payWholeAmountTv.text = "Rs. " + "0.00"
                totalPriceTv.text = "Rs. " + "0.00"

            } else {
                packagePriceTextView.text = "Rs. " + MyUtils.discountFormat(discountedValue)
                payWholeAmountTv.text = "Rs. " + MyUtils.discountFormat(discountedValue)
                totalPriceTv.text = "Rs. " + MyUtils.discountFormat(discountedValue)
            }
        }

    }
}
