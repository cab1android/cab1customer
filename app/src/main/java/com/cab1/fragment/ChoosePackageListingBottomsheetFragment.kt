package com.cab1.fragment

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.adapter.ChoosePackageAdapter
import com.cab1.pojo.Data1
import com.cab1.pojo.GoogleDirection
import com.cab1.pojo.OutStationPrice
import com.cab1.pojo.Rentalpackage
import com.cab1.util.PopupMenu
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.fragment_choose_package_list_bottomsheet_dialog.*
import java.io.Serializable
import java.util.*


class ChoosePackageListingBottomsheetFragment : BottomSheetDialogFragment() {

    private lateinit var choosePackageAdapter: ChoosePackageAdapter
    private lateinit var layoutManager: LinearLayoutManager
    var pickupLat = ""
    var pickupLongi = ""
    var dropLat = ""
    var dropLongi = ""
    var bookingType = ""
    var bookingSubType = ""
    var bookingPickupTime = ""
    var rentalPackagePos = 0
    var rentalPricePos = 0
    var outstationType = ""
    var destinationLocation = ""
    var pickupLocation = ""
    var pickupCityName = ""
    var outStationPickupDate = ""
    var outStationPickupTime = ""
    var returnDate = ""
    var returnTime = ""
    var fromLocation = ""
    val optionList = ArrayList<String>()
    var directionRouteResponse: GoogleDirection? = null
    var carCatgoryId = ""
    private var priceData = java.util.ArrayList<Data1>()
    private var rentalpackage = java.util.ArrayList<Rentalpackage>()
    private var outStationPricepackage = java.util.ArrayList<OutStationPrice>()
    private var displayOutStationData = java.util.ArrayList<OutStationPrice>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_choose_package_list_bottomsheet_dialog, container, false)

        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        headerText.text = resources.getString(R.string.choose_package)
        headerText.gravity = Gravity.START
        backIconImageView.visibility = View.VISIBLE

        isCancelable = false

        getIntentValues()

        if (bookingType.equals("Rental", true)) {

            durationSelectedTextView.text = optionList[0]

            continueButton.visibility = View.VISIBLE

        } else if (bookingType.equals("Outstation", true)) {
            continueButton.visibility = View.GONE

        }

        layoutManager =
            LinearLayoutManager(activity!!)

        durationMainLinearLayout.setOnClickListener {


            PopupMenu(activity as MainActivity, durationMainLinearLayout, optionList).showPopUp(object :
                PopupMenu.OnMenuSelectItemClickListener {
                override fun onItemClick(item: String, pos: Int) {

                    durationSelectedTextView.text = item
                    rentalPackagePos = pos
                    rentalpackage.clear()

                    for (i in 0 until priceData[0].rentalPrice.size) {
                        for (j in 0 until priceData[0].rentalPrice[i].rentalpackages.size) {
                            priceData[0].rentalPrice[i].rentalpackages[j].selected = false
                        }
                    }
                    var isSelected = false
                    for (j in 0 until priceData[0].rentalPrice[pos].rentalpackages.size) {
                        if (carCatgoryId == priceData[0].rentalPrice[pos].rentalpackages[j].categoryID) {
                            rentalPricePos = j
                            priceData[0].rentalPrice[pos].rentalpackages[j].selected = true
                            isSelected = true
                        }

                    }
                    if (!isSelected && priceData[0].rentalPrice[pos].rentalpackages.size > 0) {
                        rentalPricePos = 0
                        priceData[0].rentalPrice[pos].rentalpackages[0].selected = true
                    }
                    rentalpackage.addAll(priceData[0].rentalPrice[pos].rentalpackages)
                    choosePackageAdapter.notifyDataSetChanged()

                }

            })

        }

        packageDetailsRecyclerView.layoutManager = layoutManager

        var positions = ArrayList<Int>()

        if (displayOutStationData != null && displayOutStationData.size > 0) {

//
//            priceData[0].outstationPrice.removeAll(priceData[0].outstationPrice,outstationType)

            for (i in 0 until displayOutStationData.size) {
                if (!displayOutStationData[i].outstationType.equals(outstationType)) {
                    positions.add(i)
                    Log.d("System out", "" + i)
                }
            }

            val itr = displayOutStationData.iterator()
            var pos = 0
            var index = 0
            while (itr.hasNext()) {
                itr.next()
                if (pos >= positions.size) {
                    break
                }
                if (positions[pos] === index) {
                    itr.remove()
                    pos++
                }

                index++
            }


//


        }
        rentalpackage.clear()
        rentalpackage.addAll(priceData[0].rentalPrice[0].rentalpackages)

        outStationPricepackage.clear()
        outStationPricepackage.addAll(displayOutStationData)

        choosePackageAdapter =
            ChoosePackageAdapter(
                activity as MainActivity, rentalPackagePos, bookingType, outstationType,
                rentalpackage, outStationPricepackage, bookingType,
                object : ChoosePackageAdapter.OnItemClickListener {
                    override fun onClickCell(position: Int) {

                        priceData[0].outstationPrice.clear()
                        priceData[0].outstationPrice.addAll(displayOutStationData)

                        if (bookingType.equals("Outstation")) {
                            val args = Bundle()
                            args.putSerializable("priceData", priceData as Serializable)
                            args.putString("pickupLat", pickupLat)
                            args.putString("pickupLongi", pickupLongi)
                            args.putString("dropLongi", dropLongi)
                            args.putString("dropLat", dropLat)
                            args.putString("bookingType", bookingType)
                            args.putString("bookingSubType", bookingSubType)
                            args.putString("destinationLocation", destinationLocation)
                            args.putString("pickupLocation", pickupLocation)
                            args.putString("pickupCityName", pickupCityName)
                            args.putString("bookingPickupTime", bookingPickupTime)
                            args.putString("outStationPickupDate", outStationPickupDate)
                            args.putString("outStationPickupTime", outStationPickupTime)
                            args.putString("returnDate", returnDate)
                            args.putString("returnTime", returnTime)
                            args.putString("outstationType", outstationType)
                            args.putInt("rentalPackagePos", rentalPackagePos)
                            args.putString("outstationID", displayOutStationData[position].outstationID)
                            args.putInt("rentalPricePos", position)
                            args.putString("categoryID", bookingType)

                            val confirmBookingFragment = ConfirmYourBookingFragment()
                            confirmBookingFragment.arguments = args
                            (activity as MainActivity).navigateTo(confirmBookingFragment, true)

                            dismiss()
                        } else {


                            for (j in 0 until rentalpackage.size) {

                                rentalpackage[j].selected = false

                            }
                            rentalpackage[position].selected = true
                            choosePackageAdapter.updateData()
                            rentalPricePos = position
                        }

                    }

                    override fun onItemClick(position: Int, typeofOperation: String) {
                        dismiss()
                    }

                })
        choosePackageAdapter.notifyDataSetChanged()

        continueButton.setOnClickListener {


            val args = Bundle()
            args.putSerializable("priceData", priceData as Serializable)
            args.putString("pickupLat", pickupLat)
            args.putString("pickupLongi", pickupLongi)
            args.putString("dropLongi", dropLongi)
            args.putString("dropLat", dropLat)
            args.putString("bookingType", bookingType)
            args.putString("bookingSubType", bookingSubType)
            args.putString("bookingPickupTime", bookingPickupTime)
            args.putString("destinationLocation", destinationLocation)
            args.putString("pickupLocation", pickupLocation)
            args.putInt("rentalPackagePos", rentalPackagePos)
            args.putInt("rentalPricePos", rentalPricePos)
            args.putString("outStationPickupDate", outStationPickupDate)
            args.putString("outStationPickupTime", outStationPickupTime)
            args.putString("returnDate", returnDate)
            args.putString("returnTime", returnTime)
            val confirmBookingFragment = ConfirmYourBookingFragment()
            confirmBookingFragment.arguments = args
            (activity as MainActivity).navigateTo(confirmBookingFragment, true)


            dismiss()
        }
        packageDetailsRecyclerView.adapter = choosePackageAdapter
        val itemDecor = DividerItemDecoration(context, layoutManager.orientation)
        packageDetailsRecyclerView.addItemDecoration(itemDecor)


        backIconImageView.setOnClickListener {
            dismiss()
        }

        dialogCloseIcon.setOnClickListener {
            dismiss()
        }

    }

    private fun getIntentValues() {

        if (arguments != null) {
            priceData = arguments!!.getSerializable("priceData") as java.util.ArrayList<Data1>
            pickupLat = arguments!!.getString("pickupLat")
            pickupLongi = arguments!!.getString("pickupLongi")
            dropLat = arguments!!.getString("dropLat", "0.00")
            dropLongi = arguments!!.getString("dropLongi", "0.00")
            bookingType = arguments!!.getString("bookingType")
            bookingSubType = arguments!!.getString("bookingSubType")
            outstationType = arguments!!.getString("outstationType", "")
            destinationLocation = arguments!!.getString("destinationLocation", "")
            pickupLocation = arguments!!.getString("pickupLocation", "")
            pickupCityName = arguments!!.getString("pickupCityName", "")
            bookingPickupTime = arguments!!.getString("bookingPickupTime", "")
            outStationPickupDate = arguments!!.getString("outStationPickupDate", "")
            outStationPickupTime = arguments!!.getString("outStationPickupTime", "")
            returnDate = arguments!!.getString("returnDate", "")
            returnTime = arguments!!.getString("returnTime", "")


//            directionRouteResponse = arguments!!.getSerializable("directionRouteResponse") as GoogleDirection?

            if (bookingType.equals("Outstation")) {
                durationMainLinearLayout.visibility = View.GONE
                headerText.text = "Select Your Car"
            } else {

                durationMainLinearLayout.visibility = View.VISIBLE
            }

            if (displayOutStationData != null && displayOutStationData.size > 0) {
                displayOutStationData.clear()
            }

            displayOutStationData.addAll(priceData[0].outstationPrice)

            if (optionList.size > 0)
                optionList.clear()
            if (bookingType.equals("Rental", true)) {
                for (i in 0 until priceData[0].rentalPrice.size) {

                    optionList.add(priceData[0].rentalPrice[i].rentalHours + " hr " + priceData[0].rentalPrice[i].rentalKM + " kms")


                }

                for (j in 0 until priceData[0].rentalPrice[0].rentalpackages.size) {
                    if (priceData[0].rentalPrice[0].rentalpackages[j].selected)
                        carCatgoryId = priceData[0].rentalPrice[0].rentalpackages[j].categoryID
                }

                continueButton.visibility = View.VISIBLE

            } else if (bookingType.equals("Outstation", true)) {
                continueButton.visibility = View.GONE

            }

        }

    }

}