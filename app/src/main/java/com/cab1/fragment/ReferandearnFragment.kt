package com.cab1.fragment


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.cab1.MainActivity

import com.cab1.R
import kotlinx.android.synthetic.main.fragment_referandearn.*
import kotlinx.android.synthetic.main.toolbar.*


/**
 * A simple [Fragment] subclass.
 *
 */
class ReferandearnFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_referandearn, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        tvVerifcationTitle.text = "Share and earn"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }

        btn_share_referral_code.setOnClickListener {
            val content =
            "Let me recommend you this application, use my code " + tv_referral_code.text.toString().trim() + "\n\n" + " Download the app now - https://play.google.com/store/apps/details?id=" + activity!!.packageName

            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_SUBJECT, "")
            intent.putExtra(Intent.EXTRA_TEXT,content)
            startActivity(Intent.createChooser(intent, "Invite"))


        }
    }






}
