package com.cab1.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.cab1.R
import com.cab1.adapter.CityListAdapter
import com.cab1.pojo.CitylistData
import com.example.admin.myapplication.SessionManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.fragment_cancel_trip_bottomsheet.*

class CitylistBottomSheetFragment : BottomSheetDialogFragment(), View.OnClickListener  {

    private var mListener:  CityListener? = null

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var cancelTripAdapter: CityListAdapter
    lateinit var sessionManager: SessionManager

    var citylistData:ArrayList<CitylistData?>?= ArrayList()

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.btnCancelTrip -> {


            }

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

        return super.onCreateDialog(savedInstanceState)

    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_cancel_trip_bottomsheet, container, false)

        return view
    }



    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(activity!!)
        reasonTextView.visibility=View.GONE

        headerText.text ="Select city"
        headerText.gravity = Gravity.START
        backIconImageView.visibility = View.VISIBLE

        btnCancelTrip.visibility=View.GONE
        if (arguments != null) {
            citylistData = arguments?.getSerializable("citylistData") as ArrayList<CitylistData?>?
        }
        layoutManager = LinearLayoutManager(this!!.context!!)


        /*val list = ArrayList<String>()

        list.add("Please tell us why you want to cancel")
        list.add("Driver denied to go to destination")
        list.add("Driver denied to come to pickup")
        list.add("Expected a shorter wait time")
        list.add("Unable to contact driver")
        list.add("My reason is not listed")*/



        cancelTripRecyclerView.layoutManager = layoutManager
        cancelTripAdapter = CityListAdapter(
           activity!!,
            citylistData,
            object :CityListAdapter.OnItemClickListener{
                override fun itemClick(pos: Int, cityname: String, cityID: String) {
                    dismiss()
                     if(mListener!=null)
                         mListener!!.onSelectCity(cityID,cityname)
                }

            })
        cancelTripRecyclerView.adapter = cancelTripAdapter

        backIconImageView.setOnClickListener {
            dismiss()
        }

        dialogCloseIcon.setOnClickListener {
            dismiss()
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener = if (parent != null) {
            parent as CityListener
        } else {
            context as CityListener
        }
    }

    interface CityListener {

        fun onSelectCity(cityID: String, cityname: String)

    }


}