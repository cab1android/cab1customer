package com.cab1.fragment

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.cab1.MainActivity
import com.cab1.R
import com.cab1.adapter.CouponCodeDataAdapter
import com.cab1.api.RestClient
import com.cab1.driver.model.CouponCodeAppliedModel
import com.cab1.pojo.CouponCodeAppliedPojo
import com.cab1.pojo.CouponofferData
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.fragment_coupon_code_bottomsheet_dialog.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class SelectCouponCodeBottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var couponsAdapter: CouponCodeDataAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var couponofferData = ArrayList<CouponofferData>()
    private var mListener: CouponcodeListener? = null
    var categoryID: String = ""
    var pickupCityName: String = ""
    var amount: String = ""
    lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val view = inflater.inflate(R.layout.fragment_coupon_code_bottomsheet_dialog, container, false)


        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(activity!!)

        headerText.text = resources.getString(R.string.select_coupon_code)
        headerText.gravity = Gravity.START

        if (arguments != null) {
            couponofferData = arguments!!.getSerializable("couponofferData") as ArrayList<CouponofferData>
            categoryID = arguments!!.getString("categoryID")
            pickupCityName = arguments!!.getString("pickupCityName")
            amount = arguments!!.getString("amount")
        }

        backIconImageView.visibility = View.VISIBLE

        linearLayoutManager = LinearLayoutManager(activity)

        couponsAdapter =
            CouponCodeDataAdapter(
                activity as MainActivity,
                couponofferData,
                object : CouponCodeDataAdapter.OnItemClickListener {
                    override fun itemClick(pos: Int, code: String) {
                        couponCodeEnterTIEditText.setText("")
                        getAppliedCouponCode(code)


                    }

                })
        couponCodeRecyclerView.layoutManager = linearLayoutManager

        couponCodeRecyclerView.adapter = couponsAdapter

        backIconImageView.setOnClickListener {
            dismiss()
        }

        dialogCloseIcon.setOnClickListener {
            dismiss()
        }

        applyCouponCodeTextView.setOnClickListener {

            if (applyCouponCodeTextView.length() > 0) {
                val inputMethodManager =
                    activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(view!!.windowToken, 0)
                if (couponCodeEnterTIEditText.text.toString().trim().length == 0) {
                    (activity as MainActivity).showSnackBar("Enter Couponcode")
                } else {
                    getAppliedCouponCode(couponCodeEnterTIEditText.text.toString().trim())
                }

            }
        }

        couponCodeEnterTIEditText.setOnClickListener {
            couponsAdapter.mSelection = -1
            couponsAdapter.notifyDataSetChanged()
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener = if (parent != null) {
            parent as CouponcodeListener
        } else {
            context as CouponcodeListener
        }
    }

    interface CouponcodeListener {

        fun onSucessfullyApplyCouponcode(code: String, amount: String, couponDiscount: String, discountType: String)


    }


    public fun getAppliedCouponCode(couponCode: String) {
        MyUtils.showProgressDialog(activity!!, "Please wait...")

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("categoryID", categoryID)
            jsonObject.put("pickupStateName", "")
            jsonObject.put("pickupCityName", pickupCityName)
            jsonObject.put("couponCode", couponCode)
            jsonObject.put("amount", amount)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        jsonArray.put(jsonObject)
        val couponCodeArrayList =
            ViewModelProviders.of(this@SelectCouponCodeBottomSheetFragment).get(CouponCodeAppliedModel::class.java)
        couponCodeArrayList.getCouponCodeApplied(activity!!, jsonArray.toString())
            .observe(
                this@SelectCouponCodeBottomSheetFragment,
                Observer<List<CouponCodeAppliedPojo>> { couponCodeAppliedPojo ->
                    if (couponCodeAppliedPojo != null && couponCodeAppliedPojo.isNotEmpty()) {
                        if (couponCodeAppliedPojo[0].status.equals("true", true)) {
                            MyUtils.dismissProgressDialog()

                            dismiss()
                            if (mListener != null)
                                mListener!!.onSucessfullyApplyCouponcode(
                                    couponCode,
                                    amount,
                                    couponCodeAppliedPojo[0].date[0].couponDiscount
                                    , couponCodeAppliedPojo[0].date[0].couponDiscountType
                                )
                        } else {
                            MyUtils.dismissProgressDialog()
                            Toast.makeText(activity!!, couponCodeAppliedPojo[0].message, Toast.LENGTH_LONG).show()
                            mListener!!.onSucessfullyApplyCouponcode(
                                "",
                                "0.00",
                                "0.00"
                                , ""
                            )
                        }
                    } else {

                        MyUtils.dismissProgressDialog()

                        (activity as MainActivity).errorMethod()
                    }
                })

    }
}