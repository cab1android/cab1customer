package com.cab1.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cab1.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class OutstationChoosePackageBottomSheetFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_outstation_choose_package_bottom_sheet, container, false)
    }


}
