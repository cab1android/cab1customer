package com.cab1.api

import com.cab1.pojo.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*


public interface RestApi {

    @FormUrlEncoded
    @POST("users/create-account")
    fun getLoginCustomer(@Field("json") json: String): retrofit2.Call<List<LoginPojo>>

    @FormUrlEncoded
    @POST("users/otp-verification")
    fun otpVerifyCustomer(@Field("json") json: String): retrofit2.Call<List<LoginPojo>>

    @FormUrlEncoded
    @POST("users/get-user-profile")
    fun getCustomerDetails(@Field("json") json: String): retrofit2.Call<List<LoginPojo>>

    @FormUrlEncoded
    @POST("users/user-update-profile")
    fun updateProfileData(@Field("json") json: String): retrofit2.Call<List<LoginPojo>>

    @FormUrlEncoded
    @POST("users/user-update-profile-picture")
    fun updateProfilePicture(@Field("json") json: String): retrofit2.Call<List<LoginPojo>>


    @Multipart
    @POST("users/file-upload")
    fun uploadAttachment(
        @Part filePart: MultipartBody.Part, @Part("FilePath") filePath: okhttp3.RequestBody, @Part(
            "json"
        ) json: okhttp3.RequestBody
    ): Call<List<LoginPojo>>


    @FormUrlEncoded
    @POST("country/get-country-list")
    fun getCountryList(@Field("json") json: String): Call<List<Countrylist>>

    @FormUrlEncoded
    @POST("price/get-prices")
    fun getPriceApi(@Field("json") json: String): Call<List<GetPricePojo>>

    @FormUrlEncoded
    @POST("useraddress/address-list")
    fun getAddressList(@Field("json") json: String): Call<List<AddressListPojo>>

    @FormUrlEncoded
    @POST("useraddress/add-address")
    fun addAddress(@Field("json") json: String): Call<List<AddAddressPojo>>

    @FormUrlEncoded
    @POST("useraddress/delete-address")
    fun deleteAddress(@Field("json") json: String): Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("trips/create-trip")
    fun createTrip(@Field("json") json: String): Call<List<CreateTripPojo>>

   @FormUrlEncoded
    @POST("trips/give-trip-ratting")
    fun driverRating(@Field("json") json: String): Call<List<DriverRatingPojo>>

    @FormUrlEncoded
    @POST("drivers/get-driver-location")
    fun getDriverLocation(@Field("json") json: String): Call<List<GetDriverLocation>>

    @FormUrlEncoded
    @POST("trips/get-my-booking")
    fun getMyBookingData(@Field("json") json: String): Call<List<GetMyBookingsPojo>>

    @GET
    fun GetDirectionRoute(@Url url: String): Call<GoogleDirection>

    @FormUrlEncoded
    @POST("notification/get-notification-list")
    fun getNotification(@Field("json") json: String): Call<List<NotificationlistPojo>>
    @FormUrlEncoded
    @POST("wallettransacton/get-wallet-history")
    fun getWalletTransactionHistory(@Field("json") json: String): Call<List<WalletTransactionPojo>>

    @FormUrlEncoded
    @POST("trips/user-cancelled-trip")
    fun getCancelTrip(@Field("json") json: String): Call<List<CommonPojo>>

    @FormUrlEncoded
    @POST("cancelreason/get-cancel-reason-list")
    fun cancelTripMaster(@Field("json") json: String): Call<List<CancelTrip>>

    @FormUrlEncoded
    @POST("cmspage/get-cms-page")
    fun cmsPage(@Field("json") json: String): Call<List<CmsPagePojo>>

    @FormUrlEncoded
    @POST("users/otp-resend")
    fun resendOtp(@Field("json") json: String): Call<List<CommonPojo>>


    @FormUrlEncoded
    @POST("transactionpaytm/user-payment")
    fun paymentCard(@Field("json") json: String): Call<List<PaymentPojo>>

    @FormUrlEncoded
    @POST("couponoffer/get-coupon-offer")
    fun couponoffer(@Field("json") json: String): Call<List<CouponofferPojo>>


    @FormUrlEncoded
    @POST("category/get-category-list")
    fun getCarList(@Field("json") json: String): Call<List<CarsCategoryPojo>>

    @FormUrlEncoded
    @POST("city/get-city-list")
    fun getCityList(@Field("json") json: String): Call<List<CitylistPojo>>

    @FormUrlEncoded
    @POST("farecard/get-fare-card")
    fun getFareCard(@Field("json") json: String): Call<List<FareCardPojo>>

    @FormUrlEncoded
    @POST("couponoffer/apply-coupon-offer-code")
    fun getCouponCardApplied(@Field("json") json: String): Call<List<CouponCodeAppliedPojo>>

    @FormUrlEncoded
    @POST("users/set-default-payment-method\n")
    fun defaultPayment(@Field("json") json: String): Call<List<CommonPojo>>



}
