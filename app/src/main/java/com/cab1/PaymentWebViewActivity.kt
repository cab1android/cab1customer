package com.cab1

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.payment_webview_activity.*

class PaymentWebViewActivity : AppCompatActivity() {
    lateinit var sBundle: Bundle
    private var TAG = PaymentWebViewActivity::class.java.name
    val webViewClient = WebClientClass()
    var totalAmount: String = ""

    var transactionID: String = ""
    var url: String = ""
    var transactionPAYMENTMODE: String = ""


   /* companion object {
        fun startActivity(sContext: Context, sBundle: Bundle) {
            val sIntent = Intent(sContext, PaymentActivity::class.java)
            sIntent.putExtra("paytm_bundle", sBundle)
            sContext.startActivity(sIntent)
        }

        fun startActivity(sContext: Context) {
            val sIntent = Intent(sContext, PaymentActivity::class.java)
            sContext.startActivity(sIntent)
        }
    }
*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_webview_activity)
       if(intent!=null)
       {
           if(intent.hasExtra("transactionID"))
           {
               transactionID=intent.getStringExtra("transactionID")
           }
           if(intent.hasExtra("url"))
           {
               url=intent.getStringExtra("url")
           }
           if(intent.hasExtra("transactionPAYMENTMODE"))
           {
               transactionPAYMENTMODE=intent.getStringExtra("transactionPAYMENTMODE")
           }
       }
       webViewPayment.settings.javaScriptEnabled = true
       webViewPayment.addJavascriptInterface(WebAppInterface(this@PaymentWebViewActivity), "Android")

       webViewPayment.isVerticalScrollBarEnabled = false
       webViewPayment.isHorizontalScrollBarEnabled = false
       if (Build.VERSION.SDK_INT >= 21) {
           webViewPayment.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
       }
       webViewPayment.settings.pluginState = WebSettings.PluginState.ON
       webViewPayment.webViewClient = WebViewClient()
       webViewPayment.loadUrl(url)

    }


   inner class WebAppInterface(private val mContext: Context) {

        @JavascriptInterface
        fun successPayment(orderId: String) {
            var intent = Intent()
            intent.putExtra("from","successPayment")
            intent.putExtra("transactionID","transactionID")
            intent.putExtra("transactionPAYMENTMODE",transactionPAYMENTMODE)
            setResult(Activity.RESULT_OK, intent)
            MyUtils.finishActivity(this@PaymentWebViewActivity, true)

        }

        @JavascriptInterface
        fun failPayment(message: String) {

            MyUtils.showMessageOK(this@PaymentWebViewActivity,message,object:DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    var intent = Intent()
                    intent.putExtra("from","failPayment")
                    intent.putExtra("failMessage",message)

                    setResult(Activity.RESULT_OK, intent)
                    MyUtils.finishActivity(this@PaymentWebViewActivity, true)                }

            })


        }
    }
    inner class WebClientClass : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {

            return false
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            when {
                url.contains("success.php") -> {

                }
                url.contains("error.php") -> {

                }
            }
        }

        override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
            var message = "SSL Certificate error."

            MyUtils.showMessageOK(this@PaymentWebViewActivity,message,object:DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    MyUtils.finishActivity(this@PaymentWebViewActivity,true)
                }

            })

        }
    }

    override fun onBackPressed() {
        MyUtils.showMessageOK(this@PaymentWebViewActivity,"Are you sure you want to exit?",object:DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                MyUtils.finishActivity(this@PaymentWebViewActivity,true)
            }

        })
    }




}