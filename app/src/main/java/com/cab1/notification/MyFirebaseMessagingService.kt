package com.cab1.notification

import android.annotation.SuppressLint
import android.app.*
import android.app.ActivityManager.RunningAppProcessInfo
import android.app.Notification.BADGE_ICON_SMALL
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cab1.R
import com.cab1.SplashActivity
import com.example.admin.myapplication.SessionManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson




/**
 * Created by Chandresh
 * FIL
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {

    private var push = Push()
    var sessionManager: SessionManager? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        Log.d("System out", "push message" + remoteMessage?.data)

        try {
            if (remoteMessage?.data != null) {
                val gson = Gson()
                val message = gson.toJson(remoteMessage.data)
                val push: Push?
                push = Gson().fromJson(message, Push::class.java)

                if (push != null) {
                    if (ForegroundCheckTask().execute(this).get()) {
                        Log.e("pushData", "" + push)
                        val intentBroadcast = Intent(Push.action)
                        intentBroadcast.putExtra("PushData", push)
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intentBroadcast)
                    }

                        sendNotification(push,this)



                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    @SuppressLint("WrongConstant")
    fun sendNotification(push: Push, context: Context) {

        try {
            sessionManager=SessionManager(context)
            sessionManager!!.NotificationRead=false
            val msg = push.msg

//            PrefDb(context).putInt("unReadNotification", 1)
            var intent: Intent? = null


            intent = Intent(context, SplashActivity::class.java)
            intent.putExtra("Push", push)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)

            val largeIcon = BitmapFactory.decodeResource(context.resources, R.drawable.appicon)

            val notificationCompat = NotificationCompat.Builder(context, context.getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_stat_directions_bike)
                .setLargeIcon(largeIcon)
                .setBadgeIconType(BADGE_ICON_SMALL)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setStyle(NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
                .setContentTitle(context.getString(R.string.app_name))
                .setGroup(context.getString(R.string.app_name))
                .setPriority(Notification.PRIORITY_MAX)

            val notification = notificationCompat.build()
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                val id = context.getString(R.string.app_name)
                val name = context.getString(R.string.app_name)
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                var channel: NotificationChannel? = null
                channel = NotificationChannel(id, name, importance)
                channel.description = msg
                channel.setShowBadge(true)
                notificationManager.createNotificationChannel(channel)
            }
            val requestID = System.currentTimeMillis().toInt()
            notificationManager.notify(requestID, notification)


            val intentBroadcast = Intent("Push")
            intentBroadcast.putExtra("pushdata", push)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intentBroadcast)
        } catch (e: Exception) {
        }


    }

    companion object {
        private val TAG = MyFirebaseMessagingService::class.java.name
        internal fun shouldShowNotification(context: Context): Boolean {
            val myProcess = ActivityManager.RunningAppProcessInfo()
            ActivityManager.getMyMemoryState(myProcess)
            if (myProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND)
                return true
            val km = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            return km.inKeyguardRestrictedInputMode()
        }



    }

    internal inner class ForegroundCheckTask : AsyncTask<Context, Void, Boolean>() {

        override fun doInBackground(vararg params: Context): Boolean? {
            val context = params[0].applicationContext
            return isAppOnForeground(context)
        }

        private fun isAppOnForeground(context: Context): Boolean {
            val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val appProcesses = activityManager.runningAppProcesses ?: return false
            val packageName = context.packageName
            for (appProcess in appProcesses) {
                if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName == packageName) {
                    return true
                }
            }
            return false
        }
    }

}