package com.cab1

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cab1.adapter.PaymentStoredcarddapter
import com.cab1.api.RestClient
import com.cab1.cab1.driver.util.Googleutil
import com.cab1.driver.model.PaymentModel
import com.cab1.model.CreateTripModel
import com.cab1.model.LoginModel
import com.cab1.notification.Push
import com.cab1.pojo.*
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.nodafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class PaymentActivity : AppCompatActivity(), View.OnClickListener {


    var totalAmount: Double = 0.0
    var extra: Double = 0.0
    var dropLat = "0.00"
    var dropLongi = "0.00"
    var pickupLat = "0.00"
    var pickupLongi = "0.00"
    var fromLocation = ""
    var finalAmount = ""
    var destinationLocation = ""
    var bookingPaymentPercentage = "0.00"
    var bookingPaymentAmount = "0.00"
    var bookingType = ""
    var bookingSubType = ""
    var bookingPickupTime = ""
    var outstationID = ""
    var outstationType = ""
    var disscountType = ""
    var bookingReturnTime = ""
    var rentalPricePos = 0
    var localCarsposition = 0
    var rentalPackagePos = 0
    var from: String = ""
    var totalAmountWithoutGST = 0.00
    var gstValue: String = ""
    var disscountAmount: String = "0.00"
    var discountedVal: String = "0.00"
    var appliedGst: String = "0.00"
    var couponCode: String = ""
    var transactionID: String = ""
    var bookingPaymentMode: String = ""
    var bookingWallet: String = "0.00"
    var actualBookingWallet: String = "0.00"
    var defaultPayment: String = ""
    lateinit var pickupAddress: List<Address>
    private lateinit var paymentStoredcarddapter: PaymentStoredcarddapter
    var directionRouteResponse: GoogleDirection? = null
    lateinit var sessionManager: SessionManager
    private var priceData = ArrayList<Data1>()
    var amoutResult = "0.00"
    var netAmoutResult = ""
    var baseFare = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        sessionManager = SessionManager(this)

        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        relativeprogressBar.visibility = View.GONE
        ll_paymentmain_activity.visibility = View.GONE

        ll_paymentWallet_activity.setOnClickListener(this)
        payment_wallet_checkbox_activty.setOnClickListener(this)
        btn_processPay.setOnClickListener(this)

        if (intent != null) {

            directionRouteResponse = intent.extras.getSerializable("directionRouteResponse") as GoogleDirection?

            dropLat = intent.getStringExtra("dropLat")
            dropLongi = intent.getStringExtra("dropLongi")
            pickupLat = intent.getStringExtra("pickupLat")
            pickupLongi = intent.getStringExtra("pickupLongi")
            bookingType = intent.getStringExtra("bookingType")
            fromLocation = intent.getStringExtra("fromLocation")
            destinationLocation = intent.getStringExtra("destinationLocation")
            bookingSubType = intent.getStringExtra("bookingSubType")
            bookingPickupTime = intent.extras.getString("bookingPickupTime", "")
            disscountType = intent.extras.getString("disscountType", "")
            disscountAmount = intent.extras.getString("disscountAmount", "0.00")
            bookingPaymentPercentage = intent.extras.getString("bookingPaymentPercentage", "0.00")
//            amoutResult = intent.extras.getString("finalAmount", "")
            outstationID = intent.extras.getString("outstationID", "")
            outstationType = intent.extras.getString("outstationType", "")
            rentalPackagePos = intent.extras.getInt("rentalPackagePos", 0)
            rentalPricePos = intent.extras.getInt("rentalPricePos", 0)
            localCarsposition = intent.extras.getInt("localCarsposition", 0)
            bookingReturnTime = intent.extras.getString("bookingReturnTime", "")
            couponCode = intent.extras.getString("couponCode", "")
            priceData = intent.extras.getSerializable("priceData") as ArrayList<Data1>

            pickupAddress =
                Googleutil.getMyLocationAddressList(this@PaymentActivity, pickupLat.toDouble(), pickupLongi.toDouble())


            if (bookingType.equals("Local")) {

                gstValue = priceData[0].localPrice[localCarsposition].categoryGST

                totalAmountWithoutGST = calculateCharge(withGST = "No")


            }
            if (bookingType.equals("Rental")) {

                var rentalAmount = "0.00"

                if (priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble() > priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice.toDouble()) {
                    rentalAmount = priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice
                } else {
                    rentalAmount =
                        priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice
                }


                gstValue = priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].categoryGST

                totalAmountWithoutGST = rentalAmount.toDouble()

                amoutResult = "" + totalAmountWithoutGST

            } else if (bookingType.equals("Outstation")) {

                totalAmountWithoutGST = priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble()

                gstValue = priceData[0].outstationPrice[rentalPricePos].categoryGST
                amoutResult = "" + totalAmountWithoutGST

            }


            //coupon code discount
            if (couponCode.isNotEmpty()) {
                if (disscountType.equals("Percentage")) {
                    amoutResult =
                        "" + MyUtils.discountFormat(
                            totalAmountWithoutGST.minus(
                                totalAmountWithoutGST * disscountAmount.toDouble() / 100
                            )
                        )

                    discountedVal = "" + totalAmountWithoutGST * disscountAmount.toDouble() / 100

                } else {
                    amoutResult = "" + MyUtils.discountFormat(
                        totalAmountWithoutGST.minus(
                            disscountAmount.toDouble()
                        )
                    )
                    discountedVal = disscountAmount
                }
            }


            var optionMinBaseFare = 0.00

            if (priceData[0].localPrice[localCarsposition].priceType.equals(
                    "specialprice",
                    true
                ) && !priceData[0].localPrice[localCarsposition].specialpriceoptions.isNullOrEmpty()
            ) {


                amoutResult = "" + calculateCharge(withGST = "No")
                baseFare = priceData[0].localPrice[localCarsposition].specialpriceoptions[0].optionMinBaseFare
                optionMinBaseFare =
                    priceData[0].localPrice[localCarsposition].specialpriceoptions[0].optionMinBaseFare.toDouble()

                if (optionMinBaseFare > amoutResult.toDouble()) {
//                    amoutResult = "" + optionMinBaseFare
                    baseFare = "" + optionMinBaseFare
                }

            } else {
//                optionMinBaseFare = priceData[0].localPrice[localCarsposition].optionMinBaseFare.toDouble()
                baseFare = priceData[0].localPrice[localCarsposition].optionBasefare
            }


            appliedGst = "" + amoutResult.toDouble() * gstValue.toDouble() / 100


            //Gst calculation
            if (amoutResult.toDouble() == 0.00 && bookingType.equals("Local")) {
                appliedGst = "" + totalAmountWithoutGST * gstValue.toDouble() / 100
                amoutResult = "" + calculateCharge(withGST = "Yes")

            } else {
                amoutResult =
                    "" + MyUtils.discountFormat(amoutResult.toDouble() + amoutResult.toDouble() * gstValue.toDouble() / 100)
            }


            if (bookingPaymentPercentage.equals("15.00")) {
                amoutResult = "" + amoutResult.toDouble() * 15 / 100
            }
        }

        payment_wallet_checkbox_activty.isChecked = true
        payment_wallet_checkbox_activty.isEnabled = false
        rgGroup_activity.setOnCheckedChangeListener { group, checkedId ->

        }


        tvVerifcationTitle.text = "Payment"
        toolbar.setNavigationOnClickListener {
            MyUtils.finishActivity(this, true)
        }
        gerUserDetails()

        /* storedcard_RecycleView_activty.layoutManager =
             LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
         paymentStoredcarddapter = PaymentStoredcarddapter(this)
         storedcard_RecycleView_activty.adapter = paymentStoredcarddapter
         paymentStoredcarddapter.notifyDataSetChanged()*/

        btnRetry.setOnClickListener {
            gerUserDetails()
        }


    }

    private fun gerUserDetails() {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE
        relativeprogressBar.visibility = View.VISIBLE
        ll_paymentmain_activity.visibility = View.GONE
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("userMobile", sessionManager.get_Authenticate_User().userMobile)
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("userCountryCode", sessionManager.get_Authenticate_User().userCountryCode)
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("userDeviceID", sessionManager.get_Authenticate_User().userDeviceID)
            jsonObject.put("userLat", "")
            jsonObject.put("userLongi", "")
            jsonObject.put("languageID", "-1")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val loginModel = ViewModelProviders.of(this@PaymentActivity).get(LoginModel::class.java)
        loginModel.getLogin(this@PaymentActivity, false, jsonArray.toString()).observe(this@PaymentActivity,
            Observer<List<LoginPojo>> { loginPojo ->
                if (loginPojo != null && loginPojo.isNotEmpty()) {
                    if (loginPojo[0].status.equals("true", true) && loginPojo[0].data.isNotEmpty()) {
                        ll_paymentmain_activity.visibility = View.VISIBLE
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility = View.GONE
                        bookingWallet = loginPojo[0].data[0].userWallet
                        payment_wallet_TextView_activty.text = "Wallet (Rs. ${loginPojo[0].data[0].userWallet})"
                        defaultPayment = loginPojo[0].data[0].userDefaultCash

                        if (defaultPayment.equals("Credit Card", true)) {
                            radioButton_creditCard_activity.isChecked = true
                        } else if (defaultPayment.equals("Debit Card", true)) {
                            radioButton_debitCard_activity.isChecked = true
                        } else if (defaultPayment.equals("Paytm", true)) {
                            radioButton_paytm_activity.isChecked = true

                        } else if (defaultPayment.equals("Cash", true)) {
                            radioButton_cash_activity.isChecked = true
                        }


                    } else {

                        relativeprogressBar.visibility = View.GONE
                        noDatafoundRelativelayout.visibility = View.VISIBLE
                        ll_paymentWallet_activity.visibility = View.GONE

                    }

                } else {
                    relativeprogressBar.visibility = View.GONE
                    ll_paymentmain_activity.visibility = View.GONE

                    try {
                        nointernetMainRelativelayout.visibility = View.VISIBLE
                        if (MyUtils.isInternetAvailable(this@PaymentActivity)) {
                            nointernetImageview.setImageDrawable(this@PaymentActivity.getDrawable(R.drawable.ic_warning_black_24dp))
                            nointernettextview.text =
                                (this@PaymentActivity.getString(R.string.error_crash_error_message))
                        } else {
                            nointernetImageview.setImageDrawable(this@PaymentActivity.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                            nointernettextview.text = (this@PaymentActivity.getString(R.string.error_common_network))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }


            })
    }


    fun calculateCharge(position: Int = localCarsposition, withGST: String = "Yes"): Double {

        val distance = directionRouteResponse!!.routes!![0]!!.legs!![0]!!.distance!!.value!!.toDouble() / 1000
        val duration = directionRouteResponse!!.routes!![0]!!.legs!![0]!!.duration!!.value!!.toDouble() / 60

        var baseCharge = 0.00
        var baseKms = 0.00
        var baseMin = 0.00
        var diffDistance: Double = 0.0
        var diffDuration: Double = 0.0
        totalAmount = 0.0

//        for (i in 0 until localTripData.size) {
//            if (localTripData[i].selected) {


        if (priceData[0].localPrice[position].priceType.equals("specialprice") && !priceData[0].localPrice[position].specialpriceoptions.isNullOrEmpty()) {

            var mDistnaceKm = distance
            var subTotal = 0.0

            for (i in 0 until priceData[0].localPrice[position].specialpriceoptions.size) {

                if (priceData[0].localPrice[position].specialpriceoptions[i].slotEndKM.toInt() <= mDistnaceKm) {

                    baseCharge = priceData[0].localPrice[position].specialpriceoptions[i].optionBasefare.toDouble()
                    baseKms = priceData[0].localPrice[position].specialpriceoptions[i].optionBaseKM.toDouble()

                    if (priceData[0].localPrice[position].specialpriceoptions[i].slotEndKM.toInt() > baseKms) {
                        diffDistance =
                            (priceData[0].localPrice[position].specialpriceoptions[i].slotEndKM.toInt() - baseKms) * priceData[0].localPrice[position].specialpriceoptions[i].optionAdditionalKmCharge.toDouble()
                    }

                    mDistnaceKm -= priceData[0].localPrice[position].specialpriceoptions[i].slotEndKM.toInt()
                    subTotal += baseCharge + diffDistance

                }

                if (mDistnaceKm <= 0)
                    break
            }


            if (mDistnaceKm > 0 && !priceData[0].localPrice[position].specialpriceoptions.isNullOrEmpty()) {
                baseCharge =
                    priceData[0].localPrice[position].specialpriceoptions[0].optionBasefare.toDouble()
                baseKms =
                    priceData[0].localPrice[position].specialpriceoptions[0].optionBaseKM.toDouble()

                if (baseKms >= mDistnaceKm) {
                    subTotal += baseCharge
                } else {
                    diffDistance =
                        (mDistnaceKm - baseKms) * priceData[0].localPrice[position].specialpriceoptions[0].optionAdditionalKmCharge.toDouble()

                    subTotal += baseCharge + diffDistance
                }

            }
            // duration
            if (!priceData[0].localPrice[position].specialpriceoptions.isNullOrEmpty()) {
                baseMin =
                    duration.minus(priceData[0].localPrice[position].specialpriceoptions[0].optionBaseMinute.toInt())
                if (baseMin > 0) {
                    subTotal += baseMin * priceData[0].localPrice[position].specialpriceoptions[0].optionPerMinCharge.toDouble()
                }
            }

            totalAmount = subTotal

        } else {


//        if (priceData[0].localPrice[position].optionBasefare.toDouble() > priceData[0].localPrice[position].optionMinBaseFare.toDouble()) {
            baseCharge = priceData[0].localPrice[position].optionBasefare.toDouble()
//        } else {
//            baseCharge = priceData[0].localPrice[position].optionMinBaseFare.toDouble()
//        }


            baseKms = priceData[0].localPrice[position].optionBaseKM.toDouble()
            baseMin = priceData[0].localPrice[position].optionBaseMinute.toDouble()
            if (distance > baseKms) {
                diffDistance =
                    (distance - baseKms) * priceData[0].localPrice[position].optionAdditionalKmCharge.toDouble()
            }

            if (duration > baseMin) {
                diffDuration = (duration - baseMin) * priceData[0].localPrice[position].optionPerMinCharge.toDouble()
            }
//                break
//            }
//        }

//        extra = baseCharge * 15 / 100

            totalAmount = baseCharge + diffDistance + diffDuration
            if (priceData[0].localPrice[position].optionMinBaseFare.toDouble() > totalAmount) {
                totalAmount = priceData[0].localPrice[position].optionMinBaseFare.toDouble()
            }
        }

        if (withGST.equals("Yes")) {
            totalAmount += totalAmount * this.priceData[0].localPrice[position].categoryGST.toDouble() / 100
        }

        return totalAmount
    }

//    fun calculateCharge(position: Int = localCarsposition, withGST: String = "Yes"): Double {
//
//        val distance = directionRouteResponse!!.routes!![0]!!.legs!![0]!!.distance!!.value!!.toDouble() / 1000
//        val duration = directionRouteResponse!!.routes!![0]!!.legs!![0]!!.duration!!.value!!.toDouble() / 60
//
//        var baseCharge = 0.00
//        var baseKms = 0.00
//        var baseMin = 0.00
//        var diffDistance: Double = 0.0
//        var diffDuration: Double = 0.0
//        var optionAdditionalKm: Double = 0.0
//        var optionPerMinCharge: Double = 0.0
//        totalAmount = 0.0
////        for (i in 0 until localTripData.size) {
////
////     if (localTripData[i].selected) {
//
//
////        if (priceData[0].localPrice[position].optionBasefare.toDouble() > priceData[0].localPrice[position].optionMinBaseFare.toDouble()) {
//
//        if (priceData[0].localPrice[position].priceType.equals(
//                "specialprice",
//                true
//            ) && !priceData[0].localPrice[position].specialpriceoptions.isNullOrEmpty()
//        ) {
//            baseCharge = priceData[0].localPrice[position].specialpriceoptions[0].optionBasefare.toDouble()
//            baseKms = priceData[0].localPrice[position].specialpriceoptions[0].optionBaseKM.toDouble()
//            baseMin = priceData[0].localPrice[position].specialpriceoptions[0].optionBaseMinute.toDouble()
//            optionAdditionalKm =
//                priceData[0].localPrice[position].specialpriceoptions[0].optionAdditionalKmCharge.toDouble()
//            optionPerMinCharge = priceData[0].localPrice[position].specialpriceoptions[0].optionPerMinCharge.toDouble()
//
//        } else {
//            baseKms = priceData[0].localPrice[position].optionBaseKM.toDouble()
//            baseMin = priceData[0].localPrice[position].optionBaseMinute.toDouble()
//            baseCharge = priceData[0].localPrice[position].optionBasefare.toDouble()
//            optionAdditionalKm = priceData[0].localPrice[position].optionAdditionalKmCharge.toDouble()
//            optionPerMinCharge = priceData[0].localPrice[position].optionPerMinCharge.toDouble()
//        }
//
//
////        } else {
////            baseCharge = priceData[0].localPrice[position].optionMinBaseFare  .toDouble()
////        }
//
//        if (distance > baseKms) {
//            diffDistance = (distance - baseKms) * optionAdditionalKm
//        }
//
//        if (duration > baseMin) {
//            diffDuration = (duration - baseMin) * optionPerMinCharge
//        }
////                break
////            }
////        }
//
////        extra = baseCharge * 15 / 100
//
//        totalAmount = baseCharge + diffDistance + diffDuration
//
//        if (priceData[0].localPrice[position].priceType.equals("specialprice", true)) {
//            if (priceData[0].localPrice[position].specialpriceoptions[0].optionMinBaseFare.toDouble() > totalAmount) {
//                totalAmount = priceData[0].localPrice[position].specialpriceoptions[0].optionMinBaseFare.toDouble()
//            }
//        } else {
//            if (priceData[0].localPrice[position].optionMinBaseFare.toDouble() > totalAmount) {
//                totalAmount = priceData[0].localPrice[position].optionMinBaseFare.toDouble()
//            }
//        }
//
//
//
//        if (withGST.equals("Yes")) {
//            totalAmount = totalAmount + totalAmount * priceData[0].localPrice[position].categoryGST.toDouble() / 100
//        }
//
//        return totalAmount
//    }

    private fun createTrip(transactionID: String, bookingPaymentMode: String) {

        MyUtils.showProgressDialog(this@PaymentActivity, "Please Wait..")
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val sdfpickup = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val currentDate = sdf.format(Date())

        var pickupTime: String
        if (bookingSubType.equals("Book Later")) {
            pickupTime = bookingPickupTime
        } else {
            pickupTime = sdfpickup.format(Date())
        }

        val jsonArray = JSONArray()
        val dropaddress =
            Googleutil.getMyLocationAddressList(this, dropLat.toDouble(), dropLongi.toDouble())


//        val bookingTravelRoute = JSONArray( as Js)
        val jsonObject = JSONObject()

        try {

            if (bookingType.equals("Local")) {
                val gson = GsonBuilder().disableHtmlEscaping().create()
                val type = object : TypeToken<List<Route>>() {}.type
                val json1 = gson.toJson(directionRouteResponse!!.routes, type)

                jsonObject.put(
                    "bookingDurationEst",
                    "" + (directionRouteResponse!!.routes!![0]!!.legs!![0]!!.duration!!.value!!).toDouble() / 60
                )
                val jsonObject1 = JSONArray(json1)

                jsonObject.put(
                    "bookingDistanceEst",
                    "" + (directionRouteResponse!!.routes!![0]!!.legs!![0]!!.distance!!.value!!).toDouble() / 1000
                )

                jsonObject.put("bookingSubType", bookingSubType)
                jsonObject.put("surgeFlage", priceData[0].localPrice[localCarsposition].surgeFlage)
                jsonObject.put("localprice", priceData[0].localPrice[localCarsposition].localprice)
                jsonObject.put("couponCode", couponCode)
                jsonObject.put("bookingDiscount", discountedVal)
                jsonObject.put("categoryID", priceData[0].localPrice[localCarsposition].categoryID)
                jsonObject.put("dropCityName", dropaddress[0].locality)
                jsonObject.put(
                    "bookingEstimatedAmount",
                    "" + netAmoutResult
                )

                jsonObject.put("bookingTravelRoute", jsonObject1)
                jsonObject.put(
                    "bookingDroplatlong",
                    dropLat + "," + dropLongi
                )


                if (priceData[0].localPrice[localCarsposition].priceType.equals(
                        "specialprice",
                        true
                    ) && !priceData[0].localPrice[localCarsposition].specialpriceoptions.isNullOrEmpty()
                ) {

                    jsonObject.put(
                        "optionID",
                        priceData[0].localPrice[localCarsposition].specialpriceoptions[0].optionID
                    )
                    jsonObject.put(
                        "optionBaseMinute",
                        priceData[0].localPrice[localCarsposition].specialpriceoptions[0].optionBaseMinute
                    )
                    jsonObject.put(
                        "optionBasefare",
                        priceData[0].localPrice[localCarsposition].specialpriceoptions[0].optionBasefare
                    )
                    jsonObject.put(
                        "optionBaseKM",
                        priceData[0].localPrice[localCarsposition].specialpriceoptions[0].optionBaseKM
                    )

                    jsonObject.put(
                        "optionAdditionalKmCharge",
                        priceData[0].localPrice[localCarsposition].specialpriceoptions[0].optionAdditionalKmCharge
                    )
                    jsonObject.put(
                        "optionPerMinCharge",
                        priceData[0].localPrice[localCarsposition].specialpriceoptions[0].optionPerMinCharge
                    )
                } else {
                    jsonObject.put("optionID", priceData[0].localPrice[localCarsposition].optionID)
                    jsonObject.put("optionBaseMinute", priceData[0].localPrice[localCarsposition].optionBaseMinute)
                    jsonObject.put("optionBasefare", baseFare)
                    jsonObject.put("optionBaseKM", priceData[0].localPrice[localCarsposition].optionBaseKM)
                    jsonObject.put(
                        "optionAdditionalKmCharge",
                        priceData[0].localPrice[localCarsposition].optionAdditionalKmCharge
                    )
                    jsonObject.put("optionPerMinCharge", priceData[0].localPrice[localCarsposition].optionPerMinCharge)

                }

            } else if (bookingType.equals("Rental")) {

                jsonObject.put("bookingSubType", bookingSubType)
                jsonObject.put("couponCode", couponCode)
                jsonObject.put("bookingDiscount", discountedVal)
                jsonObject.put(
                    "categoryID",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].categoryID
                )
                if (bookingSubType.equals("Book Now")) {
                    bookingPaymentPercentage = intent.getStringExtra("bookingPaymentPercentage")
                    bookingPaymentAmount = intent.getStringExtra("bookingPaymentAmount")
                }
                jsonObject.put(
                    "optionBaseMinute",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalHours.toDouble() * 60
                )

                if (priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice.toDouble() > priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice.toDouble()) {
                    jsonObject.put(
                        "optionBasefare",
                        priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalPrice

                    )
                } else {
                    jsonObject.put(
                        "optionBasefare",
                        priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalMinPrice

                    )
                }

                jsonObject.put(
                    "optionBaseKM",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalKM
                )
                jsonObject.put("dropCityName", "")
                jsonObject.put(
                    "optionAdditionalKmCharge",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalAdditionalKM
                )
                jsonObject.put("bookingPaymentPercentage", bookingPaymentPercentage)
                jsonObject.put("bookingPaymentAmount", bookingPaymentAmount)
                jsonObject.put(
                    "optionPerMinCharge",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalAdditionalMinute
                )
                jsonObject.put(
                    "rentalID",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalID
                )

                jsonObject.put(
                    "bookingEstimatedAmount",
                    "" + netAmoutResult
                )
                jsonObject.put(
                    "bookingDistanceEst",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalKM
                )
                jsonObject.put(
                    "bookingDurationEst",
                    priceData[0].rentalPrice[rentalPackagePos].rentalpackages[rentalPricePos].rentalHours.toDouble() * 60
                )

            } else if (bookingType.equals("Outstation")) {
                if (bookingReturnTime != null && bookingReturnTime.isNotEmpty() && bookingReturnTime.trim().length > 0) {
                    jsonObject.put(
                        "bookingReturnTime",
                        MyUtils.formatDate(bookingReturnTime, "EEE,dd MMM hh:mm a", "yyyy-MM-dd HH:mm:ss")
                    )
                } else {
                    jsonObject.put(
                        "bookingReturnTime", ""
                    )
                }
                jsonObject.put("categoryID", priceData[0].outstationPrice[rentalPricePos].categoryID)
                jsonObject.put("outstationID", outstationID)
                jsonObject.put("couponCode", couponCode)
                jsonObject.put("bookingDiscount", discountedVal)
                jsonObject.put("bookingSubType", outstationType)
                jsonObject.put("bookingPaymentPercentage", bookingPaymentPercentage)
                jsonObject.put(
                    "bookingDistanceEst",
                    priceData[0].outstationPrice[rentalPricePos].outstationKM
                )
                jsonObject.put(
                    "bookingDurationEst",
                    priceData[0].outstationPrice[rentalPricePos].outstationHours.toDouble() * 60
                )
                jsonObject.put(
                    "optionBaseMinute",
                    priceData[0].outstationPrice[rentalPricePos].outstationAdditionalMinute.toDouble() * 60
                )
                jsonObject.put(
                    "bookingEstimatedAmount",
                    "" + netAmoutResult
                )
                jsonObject.put("bookingPaymentPercentage", bookingPaymentPercentage)
                jsonObject.put(
                    "optionAdditionalKmCharge",
                    priceData[0].outstationPrice[rentalPricePos].outstationAdditionalKM
                )
                jsonObject.put("bookingPaymentAmount", bookingPaymentAmount)
                jsonObject.put(
                    "bookingDroplatlong",
                    dropLat + "," + dropLongi
                )
                jsonObject.put(
                    "optionPerMinCharge",
                    priceData[0].outstationPrice[rentalPricePos].outstationAdditionalMinute
                )
                jsonObject.put("optionBaseKM", priceData[0].outstationPrice[rentalPricePos].outstationKM)

                if (priceData[0].outstationPrice[rentalPricePos].outstationPrice.toDouble() > priceData[0].outstationPrice[rentalPricePos].outstationMinPrice.toDouble()) {

                    jsonObject.put("optionBasefare", priceData[0].outstationPrice[rentalPricePos].outstationPrice)
                } else {
                    jsonObject.put("optionBasefare", priceData[0].outstationPrice[rentalPricePos].outstationMinPrice)

                }
            }

            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("bookingDate", "" + currentDate)
            jsonObject.put("transactionID", transactionID)
            jsonObject.put("radius", "5")
//            jsonObject.put("categoryID", "1")
            jsonObject.put("dropCityID", "")

            jsonObject.put("bookingGST", MyUtils.discountFormat(appliedGst.toDouble()))
//            jsonObject.put("bookingGST", MyUtils.discountFormat(amoutResult.toDouble() * gstValue.toDouble() / 100))
            jsonObject.put("bookingDropAddress", destinationLocation)
            jsonObject.put("bookingPickupAddress", fromLocation)
            jsonObject.put("bookingPickuplatlong", pickupLat + "," + pickupLongi)
            jsonObject.put("bookingPickupTime", pickupTime)
            jsonObject.put("bookingWallet", actualBookingWallet)
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("pickupCityID", "")
            jsonObject.put("pickupCityName", pickupAddress[0].locality)
            jsonObject.put("bookingPaymentMode", bookingPaymentMode)
            jsonObject.put("bookingType", bookingType)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        Log.d("System out", "Data : " + jsonArray)

        val createTripCall = ViewModelProviders.of(this).get(CreateTripModel::class.java)
        createTripCall.createTrip(this, false, jsonArray.toString()).observe(this,
            androidx.lifecycle.Observer { addAddressList ->

                MyUtils.dismissProgressDialog()
                if (addAddressList != null && addAddressList.size > 0) {
                    if (addAddressList[0].status.equals("true")) {

                        if (bookingType.equals("Local") && bookingSubType.equals("Book Now")) {

                            var intent = Intent()
                            intent.putExtra("bookingId", addAddressList[0].data[0].bookingID)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        } else if (bookingType.equals("Rental") && bookingSubType.equals("Book Now")) {
                            val intentBroadcast = Intent(Push.action)
                            intentBroadcast.putExtra("from", "rental")
                            intentBroadcast.putExtra("bookingId", addAddressList[0].data[0].bookingID)
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intentBroadcast)
                            finish()
                        } else {
                            val i = Intent(this, SuccessTripActivity::class.java)
                            i.putExtra("bookingId", "" + addAddressList[0].data[0].bookingNo)
                            startActivityForResult(i, 5)
                        }

                    } else {

                        MyUtils.showSnackbar(
                            this,
                            addAddressList[0].message,
                            ll_paymentmain_activity
                        )

                    }

                } else {

                    MyUtils.dismissProgressDialog()
                    try {
                        if (!MyUtils.isInternetAvailable(this)) {
                            MyUtils.showSnackbar(
                                this,
                                resources.getString(R.string.error_common_network),
                                ll_paymentmain_activity
                            )


                        } else {

                            MyUtils.showSnackbar(
                                this,
                                resources.getString(R.string.error_crash_error_message),
                                ll_paymentmain_activity
                            )

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            })


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {

            MyUtils.finishActivity(this, true)

        } else if (requestCode == 5) {
            val intent = Intent()
            setResult(Activity.RESULT_OK, intent)
            finish()
        } else if (requestCode == 401) {
            if (data != null) {
                if (data.hasExtra("from")) {
                    from = data.getStringExtra("from")
                }
                if (data.hasExtra("transactionID")) {
                    transactionID = data.getStringExtra("transactionID")
                }
                if (data.hasExtra("transactionPAYMENTMODE")) {
                    bookingPaymentMode = data.getStringExtra("transactionPAYMENTMODE")
                }

                if (from.equals("successPayment", true)) {
                    createTrip(transactionID, bookingPaymentMode)
                } else if (from.equals("failPayment", true)) {

                    MyUtils.showMessageOK(this@PaymentActivity, intent.getStringExtra("failMessage"),
                        DialogInterface.OnClickListener { dialog, which -> })

                }
            }
        }

    }

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.ll_paymentWallet_activity -> {
                /*payment_wallet_checkbox_activty.isChecked = true
                createTrip("0", "COD")*/
            }
            R.id.payment_wallet_checkbox_activty -> {
                /* ll_paymentWallet_activity.performClick()*/

            }
            R.id.btn_processPay -> {
                if (radioButton_creditCard_activity.isChecked) {
                    onlinePayment()
                    getPaymentApi("" + netAmoutResult, "Credit card")

                } else if (radioButton_debitCard_activity.isChecked) {
                    onlinePayment()
                    getPaymentApi("" + netAmoutResult, "Debit card")

                } else if (radioButton_paytm_activity.isChecked) {
                    onlinePayment()
                    getPaymentApi("" + netAmoutResult, "Paytm wallet")

                } else if (radioButton_cash_activity.isChecked) {

                    payment_wallet_checkbox_activty.isChecked = true
                    if (bookingWallet.toDouble() < 0) {
                        netAmoutResult = ((amoutResult.toDouble()).plus(bookingWallet.toDouble())).toString()
                        actualBookingWallet = "0.00"
                    } else {
                        netAmoutResult = ((amoutResult.toDouble()).minus(bookingWallet.toDouble())).toString()
                        actualBookingWallet = bookingWallet
                    }

                    createTrip("0", "Cash")
                }


            }
        }

    }

    fun onlinePayment() {

        if (bookingWallet.toDouble() < 0) {
            netAmoutResult = ((amoutResult.toDouble()).plus(Math.abs(bookingWallet.toDouble()))).toString()
            actualBookingWallet = bookingWallet
        } else {
            netAmoutResult = ((amoutResult.toDouble()).minus(Math.abs(bookingWallet.toDouble()))).toString()
            actualBookingWallet = bookingWallet
        }
    }

    private fun getPaymentApi(transactionTXNAMOUNT: String, transactionPAYMENTMODE: String) {
        MyUtils.showProgressDialog(this@PaymentActivity, "Please Wait..")

        val paymentModel = ViewModelProviders.of(this@PaymentActivity).get(PaymentModel::class.java)
        paymentModel.getPayment(
            this@PaymentActivity,
            transactionTXNAMOUNT,
            transactionPAYMENTMODE,
            sessionManager.get_Authenticate_User().userID
        ).observe(this, Observer<List<PaymentPojo>> { paymentPojo ->
            if (paymentPojo != null && paymentPojo.isNotEmpty()) {
                if (paymentPojo[0].status.equals("true", true)) {
                    MyUtils.dismissProgressDialog()
                    var intent = Intent(this@PaymentActivity, PaymentWebViewActivity::class.java)
                    intent.putExtra("transactionID", paymentPojo[0].data[0].transactionID)
                    intent.putExtra("url", paymentPojo[0].data[0].url)
                    intent.putExtra("transactionPAYMENTMODE", transactionPAYMENTMODE)
                    startActivityForResult(intent, 401)
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

                } else {
                    MyUtils.dismissProgressDialog()
                    MyUtils.showSnackbar(
                        this,
                        paymentPojo[0].message,
                        ll_paymentmain_activity
                    )

                }

            } else {
                MyUtils.dismissProgressDialog()

                try {
                    if (!MyUtils.isInternetAvailable(this)) {
                        MyUtils.showSnackbar(
                            this,
                            resources.getString(R.string.error_common_network),
                            ll_paymentmain_activity
                        )


                    } else {

                        MyUtils.showSnackbar(
                            this,
                            resources.getString(R.string.error_crash_error_message),
                            ll_paymentmain_activity
                        )

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


}
