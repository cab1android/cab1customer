package com.cab1

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.api.RestClient
import com.cab1.model.OtpVerifyModel
import com.cab1.model.ResendOtpModel
import com.cab1.notification.Push
import com.cab1.pojo.CommonPojo
import com.cab1.pojo.LoginData
import com.cab1.pojo.LoginPojo
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_otp_verification.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class OtpVerificationActivity : AppCompatActivity(), View.OnKeyListener {

    lateinit var sessionManager: SessionManager
    var push: Push? = null
    lateinit var loginData: LoginData
    var uid = ""
    var mobile = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)

        sessionManager = SessionManager(this@OtpVerificationActivity)
//        loginData = sessionManager.get_Authenticate_User()
        if (intent != null) {
            if (intent.hasExtra("push")) {
                push = intent.getSerializableExtra("push") as Push

            }

            if (intent.hasExtra("uid")) {
                uid = intent.getStringExtra("uid")
                mobile = intent.getStringExtra("mobile")
            }

        }
        setSupportActionBar(toolbar)

        toolbar.title = resources.getString(R.string.verification)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        customTextViewResendOTP(tvOtpresend)
        edittext_pin_1.setOnKeyListener(this)

        edittext_pin_1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edittext_pin_1.text.toString().length == 1)
                //size as per your requirement
                {
                    edittext_pin_2.requestFocus()
                }
            }

        })

        edittext_pin_2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edittext_pin_2.text.toString().length == 1)
                //size as per your requirement
                {
                    edittext_pin_3.requestFocus()
                } else if (edittext_pin_2.text.toString().length == 0) {
                    edittext_pin_1.requestFocus()
                }
            }

        })
        edittext_pin_3.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edittext_pin_3.getText().toString().length == 1)
                //size as per your requirement
                {
                    edittext_pin_4.requestFocus()
                } else if (edittext_pin_3.getText().toString().length == 0) {
                    edittext_pin_2.requestFocus()
                }
            }

        })
        edittext_pin_4.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edittext_pin_4.getText().toString().length == 1)
                //size as per your requirement
                {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(
                        edittext_pin_4.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN
                    )
                } else if (edittext_pin_4.getText().toString().length == 0) {
                    edittext_pin_3.requestFocus()
                }
            }

        })

        btnSubmit.setOnClickListener {

            if (edittext_pin_1.text.toString().trim().length == 0 || edittext_pin_2.text.toString().trim().length == 0 || edittext_pin_3.text.toString().trim().length == 0 || edittext_pin_4.text.toString().trim().length == 0) {
                showSnackBar("Enter valid otp")
            } else {


                FirebaseInstanceId.getInstance().instanceId
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            getOtpVerify(it.result!!.token)
                        } else {
                            errorMethod()
                        }
                    }


            }


        }

        edittext_pin_4.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    MyUtils.hideKeyboard1(this@OtpVerificationActivity)

                    if (edittext_pin_1.text.toString().trim().length == 0 || edittext_pin_2.text.toString().trim().length == 0 || edittext_pin_3.text.toString().trim().length == 0 || edittext_pin_4.text.toString().trim().length == 0) {
                        showSnackBar("Enter valid otp")
                    } else {


                        FirebaseInstanceId.getInstance().instanceId
                            .addOnCompleteListener {
                                if (it.isSuccessful) {
                                    getOtpVerify(it.result!!.token)
                                } else {
                                    errorMethod()
                                }
                            }


                    }
                    return true

                }
                return false
            }

        })


    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun getOtpVerify(fcmToken: String) {

        btnSubmit.startAnimation()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", uid)
            jsonObject.put(
                "userOTP",
                edittext_pin_1.text.toString() + edittext_pin_2.text.toString() + edittext_pin_3.text.toString() + edittext_pin_4.text.toString()
            )
            jsonObject.put("userDeviceID", fcmToken)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val loginModel = ViewModelProviders.of(this@OtpVerificationActivity).get(OtpVerifyModel::class.java)
        loginModel.otpVerify(this@OtpVerificationActivity, false, jsonArray.toString())
            .observe(this@OtpVerificationActivity,
                Observer<List<LoginPojo>> { loginPojo ->

                    btnSubmit.revertAnimation()
                    if (loginPojo != null && loginPojo.isNotEmpty()) {
                        if (loginPojo[0].status.equals("true", true)) {


                            StoreSessionManager(loginPojo[0].data)
                            if (loginPojo[0].data[0].pendingdriverratting.isEmpty()) {
                                var intent = Intent(this@OtpVerificationActivity, MainActivity::class.java)
                                if (push != null)
                                    intent.putExtra("push", push)
                                startActivity(intent)
                                finish()
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

                            } else {
                                var intent = Intent(this@OtpVerificationActivity, DriverRatingActivity::class.java)
                                intent.putExtra("driverName", loginPojo[0].data[0].pendingdriverratting[0].driverName)
                                intent.putExtra("driverID", loginPojo[0].data[0].pendingdriverratting[0].driverID)
                                intent.putExtra(
                                    "driverProfilePic",
                                    loginPojo[0].data[0].pendingdriverratting[0].driverProfilePic
                                )
                                intent.putExtra("bookingID", loginPojo[0].data[0].pendingdriverratting[0].bookingID)
                                startActivity(intent)
                                finish()
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)


                            }


                        } else {

                            showSnackBar(loginPojo[0].message)
                        }


                    } else {
                        btnSubmit.revertAnimation()
                        errorMethod()
                    }
                })
    }

    fun showSnackBar(message: String) {

        Snackbar.make(otpVerificationMainLinearLayout, message, Snackbar.LENGTH_LONG).show()

    }

    fun errorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(this@OtpVerificationActivity)) {
                showSnackBar(resources.getString(R.string.error_common_network))
            } else {
                showSnackBar(resources.getString(R.string.error_crash_error_message))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun StoreSessionManager(driverdata: List<LoginData>) {

        val gson = Gson()

        val json = gson.toJson(driverdata[0])

        sessionManager.create_login_session(
            json,
            driverdata[0].userMobile,
            "",
            true, true,
            sessionManager.isEmailLogin()
        )

    }


    private fun customTextViewResendOTP(tvOtpresend: TextView?) {

        val spanTxt: SpannableStringBuilder
        spanTxt = SpannableStringBuilder("Didn't Receive OTP? ")
        spanTxt.append(getString(R.string.ResendOTP))
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                getResendOtp()
            }
        }, spanTxt.length - getString(R.string.ResendOTP).length, spanTxt.length, 0)
        spanTxt.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.text_primary)),
            spanTxt.length - getString(R.string.ResendOTP).length,
            spanTxt.length,
            0
        )

        tvOtpresend?.setMovementMethod(LinkMovementMethod.getInstance())
        tvOtpresend?.setText(spanTxt, TextView.BufferType.SPANNABLE)

    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (event?.action == KeyEvent.ACTION_DOWN) {
            val id = v?.getId()
            when (id) {
                R.id.edittext_pin_4 -> if (keyCode == KeyEvent.KEYCODE_DEL) {

                    edittext_pin_3.requestFocus()
                    edittext_pin_4.setText("")

                    return true
                }

                R.id.edittext_pin_3 -> if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //
                    edittext_pin_2.requestFocus()
                    edittext_pin_3.setText("")

                    return true
                }

                R.id.edittext_pin_2 -> if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //
                    edittext_pin_1.requestFocus()
                    edittext_pin_2.setText("")

                    return true
                }

                R.id.edittext_pin_1 -> if (keyCode == KeyEvent.KEYCODE_DEL) {

                    edittext_pin_1.clearFocus()
                    edittext_pin_1.setText("")

                    return true
                }


                else -> return false
            }
        }

        return false
    }


    private fun getResendOtp() {
        MyUtils.showProgressDialog(this@OtpVerificationActivity, "Please Wait..")

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", uid)
            jsonObject.put("userMobile", mobile)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val resendOtpModel = ViewModelProviders.of(this@OtpVerificationActivity).get(ResendOtpModel::class.java)
        resendOtpModel.getResendOtp(this@OtpVerificationActivity, jsonArray.toString())
            .observe(this@OtpVerificationActivity,
                Observer<List<CommonPojo>?> { commonPojo ->
                    if (commonPojo != null && commonPojo.isNotEmpty()) {
                        if (commonPojo[0].status.equals("true", true)) {
                            MyUtils.dismissProgressDialog()
                            showSnackBar(commonPojo[0].message)
                        } else {
                            MyUtils.dismissProgressDialog()
                            showSnackBar(commonPojo[0].message)
                        }


                    } else {
                        MyUtils.dismissProgressDialog()

                        errorMethod()
                    }
                })


    }

    override fun onBackPressed() {
        MyUtils.showMessageYesNo(this@OtpVerificationActivity, "Are you sure want to exit ?", "Verification Code",
            DialogInterface.OnClickListener { dialogInterface, i ->

                //sessionManager.clear_login_session()
                MyUtils.startActivity(
                    this@OtpVerificationActivity, SplashActivity::class.java,
                    true
                )
            })
    }


}
