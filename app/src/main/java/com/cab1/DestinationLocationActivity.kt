package com.cab1

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.cab1.adapter.FavouritesAdapter
import com.cab1.adapter.GooglePlaceAutocompleteAdapter
import com.cab1.api.RestClient
import com.cab1.model.DeleteAddressModel
import com.cab1.pojo.FavouriteLocation
import com.cab1.pojo.Useraddres
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_destination_location.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class DestinationLocationActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener,
    GoogleApiClient.ConnectionCallbacks {
    private lateinit var favouritesAdapter: FavouritesAdapter
    lateinit var mGoogleApiClient: GoogleApiClient
    internal var mAdapter: GooglePlaceAutocompleteAdapter? = null
    private var mLocationRequest: LocationRequest? = null
    private val INTERVAL = (1000 * 20).toLong()
    private val FASTEST_INTERVAL = (1000 * 5).toLong()
    private var mCurrentLocation: Location? = null
    private var locationAreaList = ArrayList<String>()
    private var locationPostalCode = ArrayList<String>()
    private var locationCityId = ArrayList<String>()
    private var locationAreaId = ArrayList<String>()
    private lateinit var BOUNDS_INDIA: LatLngBounds
    private var alert: AlertDialog? = null
    lateinit var sessionManager: SessionManager
    lateinit var userAddressList: ArrayList<Useraddres>
    lateinit var from: String
    override fun onConnected(p0: Bundle?) {


        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_LOW_POWER
        mLocationRequest!!.interval = 10000 // Update location every second


        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            getLocationPermission()
            return
        }

        val builder = LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest!!)
        builder.setAlwaysShow(true)
        var locationServices = LocationServices.getFusedLocationProviderClient(this)
        locationServices.lastLocation
            .addOnSuccessListener(this) { location ->
                if (location != null) {
                    mCurrentLocation = location
//                    getCompleteAddressString(location.latitude, location.longitude, false)
                } else {
                    val builder = AlertDialog.Builder(this@DestinationLocationActivity)
                    builder.setMessage(
                        "You need to activate location service to use this feature. Please turn on network or GPS mode in location settings"
                    )
                        .setTitle("Enable GPS")
                        .setCancelable(true)
                        .setPositiveButton("Settings") { dialog, which ->
                            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                            startActivity(intent)
                            alert!!.dismiss()
                        }
                        .setNegativeButton("Cancel") { dialog, which -> alert!!.dismiss() }
                    alert = builder.create()
                    alert!!.show()
                }
            }
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    public override fun onStart() {
        mGoogleApiClient.connect()
        super.onStart()
    }

    public override fun onStop() {
        mGoogleApiClient.disconnect()
        super.onStop()
    }


    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destination_location)


        sessionManager = SessionManager(this)

        userAddressList = sessionManager.get_Authenticate_User().useraddress

        if (intent.hasExtra("lats")) {

            var lats = intent.getStringExtra("lats")
            var longs = intent.getStringExtra("longs")

            from = intent.extras.getString("from")

            if (from.equals("Destination")) {
                tvVerifcationTitle.text = resources.getString(R.string.headerdestinationviewtitle)
            } else {

                tvVerifcationTitle.text = resources.getString(R.string.headerpickuptitle)
            }

            BOUNDS_INDIA =
                LatLngBounds(
                    LatLng(lats.toDouble() - 0.05, longs.toDouble() - 0.05),
                    LatLng(lats.toDouble() + 0.05, longs.toDouble() + 0.05)
                )

        }

        if (sessionManager.get_Authenticate_User().useraddress.size > 0) {

            favouritesAdapter = FavouritesAdapter(
                this@DestinationLocationActivity,
                userAddressList,
                object : FavouritesAdapter.OnItemClick {
                    override fun onDelete(pos: Int) {

                        MyUtils.showProgressDialog(this@DestinationLocationActivity, "Please Wait")
                        deleteAddressFavourite(pos)

                    }

                    override fun onClicklisneter(pos: Int, carsName: String) {


                        var lats =
                            "" + sessionManager.get_Authenticate_User().useraddress[pos].addressLatitude.toDouble()
                        var longs =
                            "" + sessionManager.get_Authenticate_User().useraddress[pos].addressLongitude.toDouble()

                        var intent = Intent()
                        intent.putExtra("lattitude", lats)
                        intent.putExtra("longitude", longs)
                        intent.putExtra(
                            "primaryAddress",
                            sessionManager.get_Authenticate_User().useraddress[pos].addressAddressLine1
                        )
                        intent.putExtra(
                            "secondaryAddress",
                            sessionManager.get_Authenticate_User().useraddress[pos].addressAddressLine2
                        )
                        setResult(Activity.RESULT_OK, intent)
                        MyUtils.finishActivity(this@DestinationLocationActivity, true)


                    }
                })

            favouritesRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

            favouritesRecyclerView.adapter = favouritesAdapter
            favouritesRecyclerView.setHasFixedSize(true)
            list_search.visibility = View.GONE
            favouritesRecyclerView.visibility = View.VISIBLE
            tvFavourites.visibility = View.VISIBLE

        } else {
            tvFavourites.visibility = View.GONE
            list_search.visibility = View.VISIBLE
            favouritesRecyclerView.visibility = View.GONE
        }

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, 0 /* clientId */, this)
            .addApi(Places.GEO_DATA_API)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        getLocationPermission()
        try {
            initViews()
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }
//

//        favouritesRecyclerView.layoutManager = LinearLayoutManager(this)
//        favouritesRecyclerView.hasFixedSize()
        var testData = createTestData()

        toolbar.setNavigationOnClickListener {

            onBackPressed()

        }

//        favouritesRecyclerView.adapter = FavouriteDestinationLocationAdapter(
//            this@DestinationLocationActivity,
//            testData,
//            object : FavouriteDestinationLocationAdapter.OnItemClickListener {
//                override fun onItemClick(position: Int, typeofOperation: String) {
//
//                    Toast.makeText(
//                        this@DestinationLocationActivity,
//                        "Clicked: ${testData[position]}" + ": ${typeofOperation}",
//                        Toast.LENGTH_LONG
//                    ).show()
//                }
//
//            });

    }


    private fun deleteAddressFavourite(pos: Int) {


        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("addressID", sessionManager.get_Authenticate_User().useraddress[pos].addressID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val deleteAddressCall = ViewModelProviders.of(this).get(DeleteAddressModel::class.java)


        deleteAddressCall.deleteAddress(this, false, jsonArray.toString()).observe(this, androidx.lifecycle.Observer {


            MyUtils.dismissProgressDialog()

            if (it != null && it.size > 0) {

                if (it[0].status.equals("true")) {

                    val updateSessionManager = sessionManager.get_Authenticate_User()
                    updateSessionManager.useraddress.removeAt(pos)
                    sessionManager.clear_login_session()
                    val gson = Gson()
                    val json = gson.toJson(updateSessionManager)


                    sessionManager.create_login_session(
                        json,
                        updateSessionManager.userMobile,
                        "",
                        true, true,
                        sessionManager.isEmailLogin()
                    )

                    userAddressList.clear()
                    userAddressList.addAll(sessionManager.get_Authenticate_User().useraddress)

                    if (sessionManager.get_Authenticate_User().useraddress.size == 0) {
                        tvFavourites.visibility = View.GONE
                    }

                    favouritesAdapter.notifyDataSetChanged()

                } else {
                    MyUtils.showSnackbar(
                        this,
                        it[0].message,
                        mainLayoutDestinationLocation
                    )
                }

            } else {
                MyUtils.dismissProgressDialog()
                try {
                    if (!MyUtils.isInternetAvailable(this)) {
                        MyUtils.showSnackbar(
                            this,
                            resources.getString(R.string.error_crash_error_message),
                            mainLayoutDestinationLocation
                        )

                    } else {

                        MyUtils.showSnackbar(
                            this,
                            resources.getString(R.string.error_common_network),
                            mainLayoutDestinationLocation
                        )
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })

    }


    private fun getCompleteAddressString(
        latitude: LatLng, primaryAddress: String, secondaryAddress: String
    ) {
        kotlin.run {
            //            var geocoder = Geocoder(this@DestinationLocationActivity, Locale.getDefault())
//            var list = geocoder.getFromLocation(latitude.latitude, latitude.longitude, 1)

//            if (list != null && list.size > 0) {


            var lats = "" + latitude.latitude
            var longs = "" + latitude.longitude

            if (lats != null && lats.length > 0 && longs != null && longs.length > 0) {
                var intent = Intent()
                intent.putExtra("lattitude", lats)
                intent.putExtra("longitude", longs)
                intent.putExtra("primaryAddress", primaryAddress)
                intent.putExtra("secondaryAddress", secondaryAddress)
                setResult(Activity.RESULT_OK, intent)
                MyUtils.finishActivity(this@DestinationLocationActivity, true)
            }

        }
    }

    private fun initViews() {
//        val llm = LinearLayoutManager(this)
//        list_search.layoutManager = llm

        var typeFilter = AutocompleteFilter.Builder()
            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
            .setCountry("IN")
            .build()
        var mGeoDataClient = Places.getGeoDataClient(this)
        mAdapter = GooglePlaceAutocompleteAdapter(
            this,
            mGeoDataClient, BOUNDS_INDIA, typeFilter
        )
        list_search.adapter = mAdapter
        list_search.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->
            run {
                mAdapter?.notifyDataSetChanged()

                MyUtils.hideKeyboard1(this@DestinationLocationActivity)

                try {
                    val placeId = (mAdapter!!.getItem(i).placeId)
                    val primaryAddress =
                        (mAdapter!!.getItem(i).getPrimaryText(null).toString().toLowerCase().capitalize())
                    val secondaryAddress =
                        (mAdapter!!.getItem(i).getSecondaryText(null).toString().toLowerCase().capitalize())
                    val placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId.toString())
                    placeResult.setResultCallback { places ->
                        getCompleteAddressString(places.get(0).latLng, primaryAddress, secondaryAddress)

                    }
                } catch (e: Exception) {
                    Log.e("System out", e.toString())
                }

            }
        }


        search_locality_edit_text.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                //
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (count > 0) {
//                    searchImageViewLocationclear.visibility = View.VISIBLE
                    tvFavourites.visibility = View.GONE
                    favouritesRecyclerView.visibility = View.GONE
                    if (mAdapter != null) {
                        list_search.visibility = View.VISIBLE
//                        text_no_data.visibility = View.GONE
                        list_search.adapter = mAdapter
                        mAdapter?.notifyDataSetChanged()
                    } else {
                        list_search.visibility = View.GONE
//                        text_no_data.visibility = View.VISIBLE
                    }
                } else {
                    tvFavourites.visibility = View.VISIBLE
                    list_search.visibility = View.GONE
                    favouritesRecyclerView.visibility = View.VISIBLE

                }
                if (s.toString() != "" && mGoogleApiClient.isConnected) {
                    mAdapter!!.filter.filter(s.toString())
                } else if (!mGoogleApiClient.isConnected) {
                    //                    Toast.makeText(getApplicationContext(), Constants.API_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
                    Log.e("", "NOT CONNECTED")
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.interval = INTERVAL
        mLocationRequest!!.fastestInterval = FASTEST_INTERVAL
        mLocationRequest!!.priority = LocationRequest.PRIORITY_LOW_POWER
    }

//    private fun getCompleteAddressString(latitude: LatLng) {
//        kotlin.run {
//            var geocoder = Geocoder(this@DestinationLocationActivity, Locale.getDefault())
//            try {
//                var list = geocoder.getFromLocation(latitude.latitude, latitude.longitude, 1)
//                if (list != null && list!!.size > 0) {
//                    var address = list!![0]
//                    Log.d("System out", "address $address")
//                    var flag = false
//                    for (i in 0 until locationPostalCode.size) {
//                        if (address.postalCode.equals(locationPostalCode[i], true)) {
//                            LongFunction.e("select city", locationAreaList[i])
////                            sessionManager!!.searchCity = locationAreaList[i]
////                            sessionManager!!.locationAreaID = locationAreaId[i]
////                            sessionManager!!.locationCityID = locationCityId[i]
////                            sessionManager!!.locationareapincode = locationPostalCode[i]
//                            flag = true
//                            break
//                        }
//                    }
//                    if (!flag) {
////                        ActivitySignIn.startActivity(this)
//                        var sBundle = Bundle()
//                        sBundle.putString("come_from", "google_activity")
////                        ActivityLocation.startActivity(this@LocationGoogleActivity, sBundle)
//                        finish()
//                    } else {
////                        HomeActivity.startActivity(this)
//                        finish()
//                    }
//
//                }
//            } catch (e: IOException) {
//                CustomLog.e(TAG, e.message!!)
//            }
//        }
//    }


    private fun getLocationPermission() {
        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            createLocationRequest()
        } else {
            ActivityCompat.requestPermissions(
                this@DestinationLocationActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1002
            )
        }
    }

    private fun createTestData(): List<FavouriteLocation> {
        var partList = ArrayList<FavouriteLocation>()
        partList.add(FavouriteLocation("Home", " Plot no: 1025/1, sec:3/D, Gandinagar."))
        partList.add(FavouriteLocation("Office", " Plot no: 1025/1, sec:3/D, Gandinagar."))
        partList.add(FavouriteLocation("Shop", " Plot no: 1025/1, sec:3/D, Gandinagar."))
        return partList
    }


    private fun partItemClicked(favItem: FavouriteLocation) {
        Toast.makeText(this, "Clicked: ${favItem.Title}", Toast.LENGTH_LONG).show()
    }

    override fun onBackPressed() {
//        super.onBackPressed()

        MyUtils.finishActivity(this, true)

    }
}
