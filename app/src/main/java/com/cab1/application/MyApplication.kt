package com.cab1.application

import android.app.Application

import androidx.appcompat.app.AppCompatDelegate
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.FirebaseApp


class MyApplication : Application() {

    companion object {
        lateinit var instance: MyApplication
            private set

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Fresco.initialize(this)
        FirebaseApp.initializeApp(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

}