package com.cab1

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.activity_success_trip.*

class SuccessTripActivity : Activity() {
    var bookingId:String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success_trip)
        if(intent!=null)
        {
            if (intent.hasExtra("bookingId"))
            {
              bookingId=intent.getStringExtra("bookingId")
                bookingIdTextView.text="Booking id: $bookingId"
            }
        }
        closeActivityImageView.setOnClickListener {
           onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        var intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        MyUtils.finishActivity(this@SuccessTripActivity, true)

    }
}
