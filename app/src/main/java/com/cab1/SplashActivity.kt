package com.cab1


import android.Manifest.permission
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import android.view.animation.TranslateAnimation
import android.view.inputmethod.EditorInfo
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.api.RestClient
import com.cab1.model.CountrylistModel
import com.cab1.model.LoginModel
import com.cab1.notification.Push
import com.cab1.pojo.Countrylist
import com.cab1.pojo.Data
import com.cab1.pojo.LoginData
import com.cab1.pojo.LoginPojo
import com.cab1.util.LocationProvider
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.login_customer.*
import kotlinx.android.synthetic.main.no_data_internet.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class SplashActivity : AppCompatActivity() {

    private val REQUEST_CODE_Location_PERMISSIONS = 6
    lateinit var from: String
    var userLat: String? = null
    lateinit var sessionManager: SessionManager
    var userLong: String? = null
    private var countrylist = ArrayList<Data>()
    var countryname: String = ""
    var countryID: String = ""
    var countryCode: String = ""
    var countryFlagImage: String = ""
    private lateinit var locationProvider: LocationProvider
    var push: Push? = null
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        FirebaseApp.initializeApp(this)
        sessionManager = SessionManager(this@SplashActivity)

        if (intent != null) {
            if (intent.hasExtra("Push")) {
                push = intent.getSerializableExtra("Push") as Push
            }
        }


        val currentapiVersion = Build.VERSION.SDK_INT
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            permissionLocation()
        } else {
            getCountryList()
        }


        edit_enterphonenumber.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                processLogin()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        btnRetry.setOnClickListener {
            // runSplash()
            ll_nointernet_data_layout.visibility = View.GONE
            getCountryList()

        }

        img_countrymap.setOnClickListener {

            if (countrylist != null && countrylist.size > 1) {
                var i = Intent(this@SplashActivity, CountryListActivity::class.java)
                if (countrylist != null) {
                    i.putExtra("countrylist", countrylist)
                }
                startActivityForResult(i, 1)
                overridePendingTransition(
                    R.anim.slide_in_bottom,
                    R.anim.stay
                )

            }

        }

    }


    private fun connectLocation() {
        locationProvider = LocationProvider(
            this,
            LocationProvider.HIGH_ACCURACY,
            object : LocationProvider.CurrentLocationCallback {
                override fun handleNewLocation(location: Location) {
                    userLat = location.latitude.toString()
                    userLong = location.longitude.toString()
                }

            })

        locationProvider.connect()
    }

    private fun runSplash() {

        connectLocation()

        if (MyUtils.isInternetAvailable(this@SplashActivity)) {
            if (sessionManager.isLoggedIn()) {

                getCustomerDetails()


            } else {
                if (countrylist.size == 0) {
                    getCountryList()
                }
                Handler().postDelayed({

                    cardview_login.visibility = View.VISIBLE

                    val animate = TranslateAnimation(
                        0f, // fromXDelta
                        0f, // toXDelta
                        400f, // fromYDelta
                        0f// toYDelta
                    )
                    animate.duration = 2000
                    animate.fillAfter = false
                    cardview_login.startAnimation(animate)
                    //  MyUtils.startActivity(this,MainActivity::class.java,true)
                }, 2000)

                reDirectVerficationScreen()
            }
        } else {
            ll_nointernet_data_layout.visibility = View.VISIBLE
        }
    }

    private fun reDirectVerficationScreen() {
        img_login.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {

                processLogin()

            }

        })
    }

    fun processLogin() {

        MyUtils.hideKeyboard1(this)

        if (TextUtils.isEmpty(edit_enterphonenumber.text)) {
            Snackbar.make(
                constraintlayoutMain,
                resources.getString(R.string.please_enter_phone_no),
                Snackbar.LENGTH_LONG
            ).show()
        } else if (!TextUtils.isEmpty(edit_enterphonenumber.text) && edit_enterphonenumber.text!!.length < 10) {
            Snackbar.make(
                constraintlayoutMain,
                resources.getString(R.string.enter_phoneno_min10_digits),
                Snackbar.LENGTH_LONG
            ).show()

        } else {

            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener {
                    if (it.isSuccessful) {

                        if (userLat != null && userLat!!.isNotEmpty()) {

                            getCustomerLogin(it.result!!.token)
                        } else {
                            // connectLocation()
                        }

                    } else {
                        errorMethod()
                    }
                }


        }
    }

    private fun StoreSessionManager(driverdata: List<LoginData>) {

        val gson = Gson()

        val json = gson.toJson(driverdata[0])

        sessionManager.create_login_session(
            json,
            driverdata[0].userMobile,
            "",
            true,
            true,
            sessionManager.isEmailLogin()
        )

    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun permissionLocation() {

        if (!addPermission(permission.ACCESS_FINE_LOCATION)) {
            val message = getString(R.string.grant_access_location)

            MyUtils.showMessageOKCancel(this@SplashActivity, message, "Use location service?",
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                    requestPermissions(
                        arrayOf(permission.ACCESS_FINE_LOCATION),
                        REQUEST_CODE_Location_PERMISSIONS
                    )
                })

        } else {
            getCountryList()

        }

    }


    private fun getCountryList() {
        if (sessionManager.isLoggedIn()) {
            openMainActivity()
        } else {
            ll_nointernet_data_layout.visibility = View.VISIBLE
            tNodataTitle.text = "Please wait...."
            btnRetry.visibility = View.GONE


            val countrylistModel = ViewModelProviders.of(this@SplashActivity).get(CountrylistModel::class.java)
            countrylistModel.getCountry(this@SplashActivity, false).observe(this@SplashActivity,
                Observer<List<Countrylist>> { countrylistPojo ->
                    if (countrylistPojo != null && countrylistPojo.size > 0) {
                        if (countrylistPojo[0].status.equals("true")) {
                            ll_nointernet_data_layout.visibility = View.GONE
                            countrylist.clear()
                            countrylist.addAll(countrylistPojo[0].data)

                            if (countrylist.size > 1) {
                                arrowDown.visibility = View.VISIBLE
                            }

                            for (i in 0 until countrylistPojo[0].data.size) {
                                if (countryname.equals(countrylistPojo[0].data[i].countryName, true)) {

                                    countrycode.text = countrylistPojo[0].data[i].countryDialCode
                                    img_countrymap.setImageURI(Uri.parse(RestClient.image_countryflag_url + countrylistPojo[0].data[i].countryFlagImage))
                                    countryID = countrylistPojo[0].data[i].countryID

                                    countryname = countrylistPojo[0].data[i].countryName

                                    countryCode = countrylistPojo[0].data[i].countryDialCode

                                } else {
                                    countrycode.text = countrylistPojo[0].data[0].countryDialCode
                                    img_countrymap.setImageURI(Uri.parse(RestClient.image_countryflag_url + countrylistPojo[0].data[0].countryFlagImage))
                                    countryID = countrylistPojo[0].data[0].countryID

                                    countryname = countrylistPojo[0].data[0].countryName

                                    countryCode = countrylistPojo[0].data[0].countryDialCode
                                }
                            }
                            runSplash()


                        } else {
                            ll_nointernet_data_layout.visibility = View.VISIBLE
                            tNodataTitle.text = "No Data Found"
                            btnRetry.visibility = View.GONE
                            MyUtils.showSnackbar(this@SplashActivity, countrylistPojo[0].message, llfrom)

                        }

                    } else {
                        ll_nointernet_data_layout.visibility = View.VISIBLE

                        try {
                            if (!MyUtils.isInternetAvailable(this@SplashActivity)) {
                                tNodataTitle.text = "No Internet"
                                btnRetry.visibility = View.VISIBLE
                            } else {
                                tNodataTitle.text = "Something went wrong"
                                btnRetry.visibility = View.VISIBLE
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                })
        }
    }


    private fun addPermission(permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) !== PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }

        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_Location_PERMISSIONS -> {
                if (grantResults.size > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        getCountryList()
                    } else {
                        MyUtils.showSnackbar(
                            this@SplashActivity,
                            getString(R.string.read_location_permissions_senied),
                            constraintlayoutMain
                        )

                    }
                }
            }

            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!sessionManager.isLoggedIn()) {
            if (locationProvider != null) {
                locationProvider.disconnect()
            }
        }

    }

    private fun getCustomerLogin(fcmToken: String) {


        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("userMobile", edit_enterphonenumber.text.toString().trim())
            jsonObject.put("userCountryCode", countryCode)
            jsonObject.put("userDeviceType", RestClient.apiType)
            jsonObject.put("userDeviceID", fcmToken)
            jsonObject.put("userLat", userLat)
            jsonObject.put("userLongi", userLong)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        getResponse(jsonArray.toString(), true)

    }

    fun getResponse(json: String, isLogin: Boolean) {
        if (isLogin) {
            img_login.startAnimation()
        }

        val loginModel = ViewModelProviders.of(this@SplashActivity).get(LoginModel::class.java)
        loginModel.getLogin(this@SplashActivity, isLogin, json).observe(this@SplashActivity,
            Observer<List<LoginPojo>> { loginPojo ->
                if (loginPojo != null && loginPojo.isNotEmpty()) {
                    if (loginPojo[0].status.equals("true", true) && loginPojo[0].data.isNotEmpty()) {

//                        StoreSessionManager(loginPojo[0].data)
                        if (isLogin) {
                            var intent = Intent(this@SplashActivity, OtpVerificationActivity::class.java)
                            if (push != null) {
                                intent.putExtra("push", push)
                            }
                            intent.putExtra("uid", loginPojo[0].data[0].userID)
                            intent.putExtra("mobile", loginPojo[0].data[0].userMobile)
                            startActivity(intent)
                            finish()
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

                        } else {
                            if (loginPojo[0].data[0].pendingdriverratting.isNotEmpty()) {
                                var intent = Intent(this@SplashActivity, DriverRatingActivity::class.java)
                                intent.putExtra("driverName", loginPojo[0].data[0].pendingdriverratting[0].driverName)
                                intent.putExtra("driverID", loginPojo[0].data[0].pendingdriverratting[0].driverID)
                                intent.putExtra(
                                    "driverProfilePic",
                                    loginPojo[0].data[0].pendingdriverratting[0].driverProfilePic
                                )
                                intent.putExtra("bookingID", loginPojo[0].data[0].pendingdriverratting[0].bookingID)
                                startActivity(intent)
                                finish()
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

                            } else {

                                var intent = Intent(this@SplashActivity, MainActivity::class.java)
                                if (push != null)
                                    intent.putExtra("push", push)
                                startActivity(intent)
                                finish()
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                            }
                        }


                    } else {
                        if (isLogin) {

                            img_login.revertAnimation()
                        }
                        showSnackBar(loginPojo[0].message)
                    }


                } else {
                    if (isLogin && img_login != null)
                        img_login.revertAnimation()
                    errorMethod(json)
                }
            })
    }


    fun showSnackBar(message: String, json: String = "") {
        if (json.isEmpty())
            Snackbar.make(constraintlayoutMain, message, Snackbar.LENGTH_LONG).show()
        else
            Snackbar.make(constraintlayoutMain, message, Snackbar.LENGTH_LONG).setAction("Retry") {
                if (sessionManager.isLoggedIn())
                    getResponse(json, true)
                else
                    getResponse(json, false)


            }.show()

    }


    fun errorMethod(json: String = "") {
        try {
            if (!MyUtils.isInternetAvailable(this@SplashActivity)) {

                showSnackBar(resources.getString(R.string.error_common_network), json)
            } else {

                showSnackBar(resources.getString(R.string.error_crash_error_message), json)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {

            LocationProvider.CONNECTION_FAILURE_RESOLUTION_REQUEST -> {
                connectLocation()
            }

            1 -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        if (data!!.hasExtra("countryID")) {
                            countryID = data.getStringExtra("countryID")

                            countryID = data.getStringExtra("countryID")
                            if (countryID != null && countryID.length > 0) {

                            } else {
                                countryID = countrylist.get(0).countryID
                            }


                        }
                        if (data!!.hasExtra("countryname")) {
                            countryname = data.getStringExtra("countryname")
                            if (countryname != null && countryname.length > 0) {

                            } else {
                                countryname = countrylist.get(0).countryName
                            }


                        }
                        if (data!!.hasExtra("countryCode")) {
                            countryCode = data.getStringExtra("countryCode")

                            if (countryCode != null && countryCode.length > 0) {
                                countrycode.text = countryCode
                            } else {
                                countryCode = countrylist.get(0).countryDialCode
                            }

                            countrycode.text = countryCode
                        }
                        if (data!!.hasExtra("countryFlagImage")) {
                            countryFlagImage = data.getStringExtra("countryFlagImage")
                            img_countrymap.setImageURI(Uri.parse(RestClient.image_countryflag_url + countryFlagImage))

                        }
                    }
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }

    }

    private fun getCustomerDetails() {


        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("languageID", "1")

            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        getResponse(jsonArray.toString(), false)
    }

    fun openMainActivity() {
        Handler().postDelayed(

            {


                var intent = Intent(this@SplashActivity, MainActivity::class.java)
                if (push != null)
                    intent.putExtra("push", push)
                startActivity(intent)
                finish()
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }, 2000
        )


    }


}
