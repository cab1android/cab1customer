package com.cab1


import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.api.RestClient
import com.cab1.driver.model.CancelTripMasterModel
import com.cab1.fragment.CancelBottomSheetFragment
import com.cab1.pojo.CancelTrip
import com.cab1.pojo.CancelTripData
import com.cab1.pojo.GetMyBookingData
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_my_booking_details.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable


class MyBookingDetailsActivity : AppCompatActivity(), CancelBottomSheetFragment.CancelTripListener {

    private var v: View? = null
    private var tabposition: Int = -1
    var getMyBookingData: GetMyBookingData? = null
    var phoneNumber = ""
    private val MY_PERMISSIONS_REQUEST_CALL_PHONE = 501
    lateinit var sessionManager: SessionManager
    private var cancelTripMaster = ArrayList<CancelTripData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_my_booking_details)
        sessionManager = SessionManager(this@MyBookingDetailsActivity)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        tvVerifcationTitle.text = "Trip Detail"
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }


        if (intent != null) {
            tabposition = intent!!.getIntExtra("position", -1)
            getMyBookingData = intent!!.getSerializableExtra("bookingData") as GetMyBookingData?
        }


        when (tabposition) {
            0 -> {
                ll_bookingtrip_timeDate.visibility = View.GONE
                billDetailslayout.visibility = View.GONE
                ll_estimated_actual.visibility = View.GONE
                ll_cancel_trip.isEnabled = true
                rellayer_cancel.visibility = View.GONE
                if (getMyBookingData!!.bookingType.equals("rental", true)) {
                    ll_to_address.visibility = View.GONE
                    image_dotted.visibility = View.GONE
                    image_dottedLine.visibility = View.GONE

                } else {
                    ll_to_address.visibility = View.VISIBLE
                    image_dotted.visibility = View.VISIBLE
                    image_dottedLine.visibility = View.VISIBLE


                }
                if (getMyBookingData!!.statusName.equals(
                        "Accepted",
                        true
                    ) || getMyBookingData!!.statusName.equals(
                        "Started",
                        true
                    ) || getMyBookingData!!.statusName.equals("Reached", true)
                ) {
                    ll_track_trip.isEnabled = true
                    imgTrackTrip.isEnabled = true
                    tvTrackTrip.isEnabled = true
                    rellayer_tarack.visibility = View.GONE
                } else {
                    ll_track_trip.isEnabled = false
                    imgTrackTrip.isEnabled = false
                    tvTrackTrip.isEnabled = false
                    rellayer_tarack.visibility = View.VISIBLE

                }


            }
            1 -> {
                ll_bookingtrip_timeDate.visibility = View.VISIBLE
                btnCallDriver.visibility = View.GONE
                ll_track_trip.isEnabled = false
                ll_cancel_trip.isEnabled = false
                imgTrackTrip.isEnabled = false
                tvTrackTrip.isEnabled = false
                rellayer_tarack.visibility = View.VISIBLE
                rellayer_cancel.visibility = View.VISIBLE
                ll_to_address.visibility = View.VISIBLE
                image_dotted.visibility = View.VISIBLE
                image_dottedLine.visibility = View.VISIBLE
            }
            2 -> {
                billDetailslayout.visibility = View.GONE
                driverInformationLayout.visibility = View.GONE
                ll_track_trip.isEnabled = false
                ll_cancel_trip.isEnabled = false
                imgTrackTrip.isEnabled = false
                tvTrackTrip.isEnabled = false
                rellayer_tarack.visibility = View.VISIBLE
                rellayer_cancel.visibility = View.VISIBLE

                ll_to_address.visibility = View.VISIBLE
                image_dotted.visibility = View.VISIBLE
                image_dottedLine.visibility = View.VISIBLE                             
            }
        }

        ll_cancel_trip.setOnClickListener {
            cancelTripReasonMaster(getMyBookingData!!.bookingID)
        }


        ll_tripdetails_support.setOnClickListener {
            var intent = Intent()
            intent.putExtra("from", "supportTrip")
            setResult(Activity.RESULT_OK, intent)
            MyUtils.finishActivity(this@MyBookingDetailsActivity, true)
        }
        if (getMyBookingData!!.statusName.equals("Accepted", true) || getMyBookingData!!.statusName.equals(
                "Started",
                true
            ) || getMyBookingData!!.statusName.equals("Reached", true)
        ) {
            ll_track_trip.isEnabled = true
        }

        if (getMyBookingData!!.driverID.isEmpty() || getMyBookingData!!.driverID == "0") {
            noDriverTv.visibility = View.VISIBLE
        } else {
            noDriverTv.visibility = View.GONE
        }
        tvBookingId.text = "Booking Id: " + getMyBookingData!!.bookingNo
        tvBookingDate.text = MyUtils.formatDate(
            getMyBookingData!!.bookingDate,
            "yyyy-MM-dd HH:mm:ss",
            "EEE, MMM d, hh:mm aaa"
        )
        tvStartDate.text = MyUtils.formatDate(
            getMyBookingData!!.bookingTripStartTime,
            "yyyy-MM-dd HH:mm:ss",
            "EEE, d MMM"
        )
        tvStartTime.text = MyUtils.formatDate(
            getMyBookingData!!.bookingTripStartTime,
            "yyyy-MM-dd HH:mm:ss",
            "hh:mm aaa"
        )
        tvEndDate.text = MyUtils.formatDate(
            getMyBookingData!!.bookingTripEndTime,
            "yyyy-MM-dd HH:mm:ss",
            "EEE,d MMM"
        )
        tvEndTime.text = MyUtils.formatDate(
            getMyBookingData!!.bookingTripEndTime,
            "yyyy-MM-dd HH:mm:ss",
            "hh:mm aaa"
        )
        tvTypeOfMode.text = getMyBookingData!!.bookingType
        if (getMyBookingData!!.bookingSubType.contains("street", true))
            tvTypeOfMode.text = resources.getString(R.string.easy_ride)

        imgCallDriverIcon.setImageURI(RestClient.image_driver_url + getMyBookingData!!.driverProfilePic)
        if (getMyBookingData!!.driverRatting != null) {
            ratingBar.rating = getMyBookingData!!.driverRatting.toFloat()

        } else {
            ratingBar.rating = 0f
        }
        if (getMyBookingData!!.bookingRatting != null) {
            ratingBarYourRating.rating = getMyBookingData!!.bookingRatting.toFloat()
        } else {
            ratingBarYourRating.rating = 0f
        }
        phoneNumber = getMyBookingData!!.driverMobile
        tvDriverName.text = getMyBookingData!!.driverName
        tvTripType.text = getMyBookingData!!.bookingType
        imgTypeOfCar.setImageURI(RestClient.image_category_url + getMyBookingData!!.categoryImage)
        imgTypeOfCar.background = resources.getDrawable(R.drawable.ic_car_background_unselected_24dp)

        tvCarName.text = getMyBookingData!!.drvcarModel
        tvDriverValue.text = getMyBookingData!!.bookingDistanceEst + " kms"
        tvFromLocationAddress.text = getMyBookingData!!.bookingPickupAddress
        tvdestinationLocationAddress.text = getMyBookingData!!.bookingDropAddress
        tvPaymentOptionValue.text = getMyBookingData!!.bookingPaymentMode

        tvestimatedamountValue.text = "Rs. " + MyUtils.priceFormat(getMyBookingData!!.bookingEstimatedAmount)

        tvActualAmountValue.text = "Rs. " + MyUtils.priceFormat(getMyBookingData!!.bookingActualAmount)
        if(tabposition==0)
        {
            if (getMyBookingData!!.bookingPaymentMode.equals("Cash", true)) {
                tvwalletAmountText.visibility = View.GONE
                tvCreaditAmountText.text = "Due"
                tvCreaditWalletAmountValue.text = "Rs. " + MyUtils.priceFormat(getMyBookingData!!.bookingEstimatedAmount)

            }else{
                tvwalletAmountText.visibility = View.GONE
                tvCreaditAmountText.text = "Due"
                tvCreaditWalletAmountValue.text = "Rs. " + "0.00"

            }

        }else{
            if (getMyBookingData!!.bookingPaymentMode.equals("Cash", true)) {
                tvwalletAmountText.visibility = View.GONE
                tvCreaditAmountText.text = "Due"
                tvCreaditWalletAmountValue.text = "Rs. " + MyUtils.priceFormat(getMyBookingData!!.bookingActualAmount)

            } else {

                var duePament =
                    (getMyBookingData!!.bookingEstimatedAmount.toDouble()).minus(getMyBookingData!!.bookingActualAmount.toDouble())
                if (duePament >= 0) {
                    tvwalletAmountText.visibility = View.VISIBLE
                    tvCreaditAmountText.text = "Credit"
                    tvCreaditWalletAmountValue.text = "Rs. " + MyUtils.priceFormat(duePament)
                } else {
                    tvwalletAmountText.visibility = View.VISIBLE
                    tvCreaditAmountText.text = "Debit"
                    tvCreaditWalletAmountValue.text = "Rs. " + MyUtils.priceFormat(Math.abs(duePament))
                }
            }
        }

        tvBaseFareValue.text = "Rs. " + MyUtils.priceFormat(getMyBookingData!!.optionBasefare)
        tvTimeValue.text = getMyBookingData!!.bookingDurationActual + " mins"
        tvSubtotalValue.text = "Rs. " + MyUtils.priceFormat(getMyBookingData!!.bookingActualAmount)
        tvTotalValue.text = "Rs. " + MyUtils.priceFormat(getMyBookingData!!.bookingActualAmount)


        btnCallDriver.setOnClickListener {
            call(phoneNumber)
        }

        ll_track_trip.setOnClickListener {
            var intent = Intent()
            intent.putExtra("from", "trackTrip")
            intent.putExtra("bookingId", getMyBookingData!!.bookingID)
            intent.putExtra("statusName", getMyBookingData!!.statusName)
            intent.putExtra("bookingData", getMyBookingData as Serializable)
            setResult(Activity.RESULT_OK, intent)
            MyUtils.finishActivity(this@MyBookingDetailsActivity, true)

        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun call(phoneNumber: String) {

        try {
            if (ActivityCompat.checkSelfPermission(
                    this@MyBookingDetailsActivity,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                (this@MyBookingDetailsActivity).requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    MY_PERMISSIONS_REQUEST_CALL_PHONE
                )

            } else if (phoneNumber.isNotEmpty()) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + phoneNumber)

                startActivity(callIntent)

            }

        } catch (e: ActivityNotFoundException) {
            Log.e("call", "Call failed", e)
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_CALL_PHONE && grantResults.isNotEmpty()) {
            call(phoneNumber)
        }
    }

    private fun cancelTripReasonMaster(bookingId: String) {
        MyUtils.showProgressDialog(this@MyBookingDetailsActivity, "Please wait...")
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("reasonFor", "Customer")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val cancelTripModel =
            ViewModelProviders.of(this@MyBookingDetailsActivity).get(CancelTripMasterModel::class.java)
        cancelTripModel.getCancelTripMaster(this@MyBookingDetailsActivity, false, jsonArray.toString())
            .observe(this@MyBookingDetailsActivity,
                Observer<List<CancelTrip>> { canceltripPojo ->
                    if (canceltripPojo != null && canceltripPojo.size > 0) {
                        if (canceltripPojo[0].status.equals("true")) {

                            MyUtils.dismissProgressDialog()

                            cancelTripMaster.clear()
                            cancelTripMaster.addAll(canceltripPojo[0].data)

                            var canceltripBottomSheetFragment = CancelBottomSheetFragment()
                            var bundle = Bundle()
                            bundle.putSerializable("CancelTripData", canceltripPojo[0].data as Serializable)
                            bundle.putString("bookingId", bookingId)
                            canceltripBottomSheetFragment.arguments = bundle
                            canceltripBottomSheetFragment.show(supportFragmentManager!!, "CancelTrip")

                        } else {
                            MyUtils.dismissProgressDialog()
                            showSnackBar(canceltripPojo[0].message)
                        }

                    } else {
                        MyUtils.dismissProgressDialog()
                        errorMethod()
                    }
                })

    }

    override fun onBackPressed() {
        MyUtils.finishActivity(this@MyBookingDetailsActivity, true)
    }


    override fun onSucessfullyCancelTrip() {
        var intent = Intent()
        intent.putExtra("from", "cancelTrip")
        intent.putExtra("bookingId", getMyBookingData!!.bookingID)
        intent.putExtra("statusName", getMyBookingData!!.statusName)
        intent.putExtra("bookingData", getMyBookingData as Serializable)
        setResult(Activity.RESULT_OK, intent)
        MyUtils.finishActivity(this@MyBookingDetailsActivity, true)


    }

    fun showSnackBar(message: String) {
        if ((rv_my_bookingDetails != null) and !isFinishing)
            Snackbar.make(this.rv_my_bookingDetails!!, message, Snackbar.LENGTH_LONG).show()

    }


    fun errorMethod(json: String = "") {
        try {
            if (!MyUtils.isInternetAvailable(this@MyBookingDetailsActivity)) {

                showSnackBar(resources.getString(R.string.error_common_network))
            } else {

                showSnackBar(resources.getString(R.string.error_crash_error_message))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
