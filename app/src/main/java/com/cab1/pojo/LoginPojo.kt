package com.cab1.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class LoginPojo(
    @SerializedName("data")
    var `data`: List<LoginData>,
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: String
) : Serializable

data class LoginData(
    @SerializedName("userCountryCode")
    var userCountryCode: String,
    @SerializedName("userDeviceID")
    var userDeviceID: String,
    @SerializedName("userDeviceType")
    var userDeviceType: String,
    @SerializedName("userEmail")
    var userEmail: String,
    @SerializedName("userEmailNotification")
    var userEmailNotification: String,
    @SerializedName("userFullName")
    var userFullName: String,
    @SerializedName("userID")
    var userID: String,
    @SerializedName("userMobile")
    var userMobile: String,
    @SerializedName("userProfilePicture")
    var userProfilePicture: String,
    @SerializedName("userPushNotification")
    var userPushNotification: String,
    @SerializedName("userVerified")
    var userVerified: String,
    @SerializedName("userLatitude")
    var userLatitude: String,
    @SerializedName("userWallet")
    var userWallet: String="0.0",
    @SerializedName("userLongitude")
    var userLongitude: String,
    @SerializedName("useraddress")
    var useraddress: ArrayList<Useraddres>,
    @SerializedName("pendingdriverratting")
    var pendingdriverratting: ArrayList<PendingRatingList>,
    @SerializedName("userDefaultCash")
    var userDefaultCash:String="",
    @SerializedName("setting")
    var setting: ArrayList<Setting>

) : Serializable

data class Useraddres(
    @SerializedName("addressAddressLine1")
    var addressAddressLine1: String,
    @SerializedName("addressAddressLine2")
    var addressAddressLine2: String,
    @SerializedName("addressID")
    var addressID: String,
    @SerializedName("addressLatitude")
    var addressLatitude: String,
    @SerializedName("addressLongitude")
    var addressLongitude: String,
    @SerializedName("addressPincode")
    var addressPincode: String,
    @SerializedName("addressTitle")
    var addressTitle: String,
    @SerializedName("cityID")
    var cityID: String,
    @SerializedName("cityName")
    var cityName: String,
    @SerializedName("countryID")
    var countryID: String,
    @SerializedName("countryName")
    var countryName: String,
    @SerializedName("stateID")
    var stateID: String,
    @SerializedName("stateName")
    var stateName: String
) : Serializable

data class PendingRatingList(
    @SerializedName("driverID")
    val driverID: String = "",
    @SerializedName("driverMobile")
    val driverMobile: String = "",
    @SerializedName("driverName")
    val driverName: String = "",
    @SerializedName("driverProfilePic")
    val driverProfilePic: String = "",
    @SerializedName("bookingID")
    val bookingID: String = ""
) : Serializable

