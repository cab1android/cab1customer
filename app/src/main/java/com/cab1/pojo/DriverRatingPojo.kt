package com.cab1.pojo
import com.google.gson.annotations.SerializedName

data class DriverRatingPojo(
    @SerializedName("data")
    val `data`: List<CreateTripData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)
