package com.cab1.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class CouponofferPojo(
    @SerializedName("data")
    val data: List<CouponofferData>,
    @SerializedName("message")
    val message: String = "",
    @SerializedName("status")
    val status: String = ""
):Serializable

data class  CouponofferData(
    @SerializedName("couponDescription")
    val couponDescription: String = "",
    @SerializedName("couponName")
    val couponName: String = "" ,
    @SerializedName("couponCode")
    val couponCode: String = ""
):Serializable
