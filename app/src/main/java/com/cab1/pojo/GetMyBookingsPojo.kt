package com.cab1.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class GetMyBookingsPojo(
    @SerializedName("data")
    var `data`: List<GetMyBookingData>,
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: String
):Serializable

data class GetMyBookingData(
    @SerializedName("bookingActualAmount")
    var bookingActualAmount: String="",
    @SerializedName("bookingDate")
    var bookingDate:  String="",
    @SerializedName("bookingDistanceActual")
    var bookingDistanceActual:  String="",
    @SerializedName("bookingDistanceEst")
    var bookingDistanceEst:  String="",
    @SerializedName("bookingDropAddress")
    var bookingDropAddress:  String="",
    @SerializedName("bookingDroplatlong")
    var bookingDroplatlong:  String="",
    @SerializedName("bookingDurationActual")
    var bookingDurationActual:  String="",
    @SerializedName("bookingDurationEst")
    var bookingDurationEst:  String="",
    @SerializedName("bookingEstimatedAmount")
    var bookingEstimatedAmount:  String="",
    @SerializedName("bookingID")
    var bookingID:  String="",
    @SerializedName("bookingOTP")
    var bookingOTP:  String="",
    @SerializedName("bookingPaymentMode")
    var bookingPaymentMode:  String="",
    @SerializedName("bookingPickupAddress")
    var bookingPickupAddress:  String="",
    @SerializedName("bookingPickupTime")
    var bookingPickupTime:  String="",
    @SerializedName("bookingPickuplatlong")
    var bookingPickuplatlong:  String="",
    @SerializedName("bookingRatting")
    var bookingRatting:  String="0.0",
    @SerializedName("bookingTravelRoute")
    var bookingTravelRoute: List<BookingTravelRoute>,
    @SerializedName("bookingTraveledRoute")
    var bookingTraveledRoute:  String="",
    @SerializedName("bookingTripEndTime")
    var bookingTripEndTime:   String="",
    @SerializedName("bookingTripEndlatlong")
    var bookingTripEndlatlong:  String="",
    @SerializedName("bookingTripStartTime")
    var bookingTripStartTime:  String="",
    @SerializedName("bookingTripStartlatlong")
    var bookingTripStartlatlong: String="",
    @SerializedName("bookingType")
    var bookingType: String="",
    @SerializedName("bookingWaitCharge")
    var bookingWaitCharge: String="",
    @SerializedName("bookingWaitMinute")
    var bookingWaitMinute: String="",
    @SerializedName("brandID")
    var brandID: String="",
    @SerializedName("brandImage")
    var brandImage: String="",
    @SerializedName("brandName")
    var brandName: String="",
    @SerializedName("categoryID")
    var categoryID: String="",
    @SerializedName("categoryImage")
    var categoryImage: String="",
    @SerializedName("categoryName")
    var categoryName: String="",
    @SerializedName("driverCountryCode")
    var driverCountryCode: String="",
    @SerializedName("driverID")
    var driverID: String="",
    @SerializedName("driverMobile")
    var driverMobile: String="",
    @SerializedName("driverName")
    var driverName: String="",
    @SerializedName("driverProfilePic")
    var driverProfilePic: String="",
    @SerializedName("driverRatting")
    var driverRatting: String="0.0",
    @SerializedName("drvcarBackImage")
    var drvcarBackImage: String="",
    @SerializedName("drvcarColor")
    var drvcarColor: String="",
    @SerializedName("drvcarFrontImage")
    var drvcarFrontImage: String="",
    @SerializedName("drvcarID")
    var drvcarID: String="",
    @SerializedName("drvcarInteriorImage")
    var drvcarInteriorImage: String="",
    @SerializedName("drvcarLeftImage")
    var drvcarLeftImage: String="",
    @SerializedName("drvcarModel")
    var drvcarModel: String="",
    @SerializedName("drvcarNo")
    var drvcarNo: String="",
    @SerializedName("drvcarRightImage")
    var drvcarRightImage: String="",
    @SerializedName("drvcarYear")
    var drvcarYear: String="",
    @SerializedName("optionAdditionalKmCharge")
    var optionAdditionalKmCharge: String="",
    @SerializedName("optionBaseKM")
    var optionBaseKM: String="",
    @SerializedName("optionBaseMinute")
    var optionBaseMinute: String="",
    @SerializedName("optionBasefare")
    var optionBasefare: String="",
    @SerializedName("optionPerMinCharge")
    var optionPerMinCharge: String="",
    @SerializedName("statusID")
    var statusID: String="",
    @SerializedName("statusUpdateDate")
    var statusUpdateDate: String="",
    @SerializedName("userEmail")
    var userEmail: String="",
    @SerializedName("userFullName")
    var userFullName: String="",
    @SerializedName("userID")
    var userID: String="",
    @SerializedName("userMobile")
    var userMobile: String="",
    @SerializedName("userProfilePicture")
    var userProfilePicture: String="",
    @SerializedName("statusName")
    var statusName: String="",
    @SerializedName("bookingSubType")
    var bookingSubType: String="",
    @SerializedName("bookingNo")
    var bookingNo: String=""
):Serializable

data class BookingTravelRoute(
    @SerializedName("bounds")
    var bounds: Bounds,
    @SerializedName("copyrights")
    var copyrights: String,
    @SerializedName("legs")
    var legs: List<Leg>,
    @SerializedName("overview_polyline")
    var overviewPolyline: OverviewPolyline,
    @SerializedName("summary")
    var summary: String,
    @SerializedName("warnings")
    var warnings: List<Any>,
    @SerializedName("waypoint_order")
    var waypointOrder: List<Any>
):Serializable

