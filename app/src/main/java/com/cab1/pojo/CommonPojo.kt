package com.cab1.pojo

import com.google.gson.annotations.SerializedName


data class CommonPojo(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)