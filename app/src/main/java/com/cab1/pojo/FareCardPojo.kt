package com.cab1.pojo
import com.google.gson.annotations.SerializedName


data class FareCardPojo(
    @SerializedName("data")
    val `data`: List<FareCardData> = listOf(),
    @SerializedName("message")
    val message: String = "",
    @SerializedName("status")
    val status: String = ""
)

data class  FareCardData(
    @SerializedName("farecardCancellationFee")
    val farecardCancellationFee: String = "",
    @SerializedName("farecardID")
    val farecardID: String = "",
    @SerializedName("farecardTotalFare")
    val farecardTotalFare: String = ""
)