package com.cab1.pojo

import com.google.gson.annotations.SerializedName


data class CreateTripPojo(
    @SerializedName("data")
    val `data`: List<CreateTripData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)

data class CreateTripData(
    @SerializedName("bookingActualAmount")
    val bookingActualAmount: String,
    @SerializedName("bookingDate")
    val bookingDate: String,
    @SerializedName("bookingDistanceActual")
    val bookingDistanceActual: Any,
    @SerializedName("bookingDistanceEst")
    val bookingDistanceEst: String,
    @SerializedName("bookingDropAddress")
    val bookingDropAddress: String,
    @SerializedName("bookingDroplatlong")
    val bookingDroplatlong: String,
    @SerializedName("bookingDurationActual")
    val bookingDurationActual: Any,
    @SerializedName("bookingDurationEst")
    val bookingDurationEst: String,
    @SerializedName("bookingEstimatedAmount")
    val bookingEstimatedAmount: String,
    @SerializedName("bookingID")
    val bookingID: String,
    @SerializedName("bookingOTP")
    val bookingOTP: String,
    @SerializedName("bookingPaymentMode")
    val bookingPaymentMode: String,
    @SerializedName("bookingPickupAddress")
    val bookingPickupAddress: String,
    @SerializedName("bookingPickupTime")
    val bookingPickupTime: String,
    @SerializedName("bookingPickuplatlong")
    val bookingPickuplatlong: String,
    @SerializedName("bookingTravelRoute")
    val bookingTravelRoute: List<Route>,
    @SerializedName("bookingTraveledRoute")
    val bookingTraveledRoute: String = "",
    @SerializedName("bookingTripEndTime")
    val bookingTripEndTime: String,
    @SerializedName("bookingTripStartTime")
    val bookingTripStartTime: String,
    @SerializedName("bookingType")
    val bookingType: String,
    @SerializedName("bookingWaitCharge")
    val bookingWaitCharge: Any,
    @SerializedName("bookingWaitMinute")
    val bookingWaitMinute: Any,
    @SerializedName("brandID")
    val brandID: Any,
    @SerializedName("brandImage")
    val brandImage: Any,
    @SerializedName("brandName")
    val brandName: Any,
    @SerializedName("categoryID")
    val categoryID: String,
    @SerializedName("categoryImage")
    val categoryImage: String,
    @SerializedName("categoryName")
    val categoryName: String,
    @SerializedName("driverCountryCode")
    val driverCountryCode: Any,
    @SerializedName("driverID")
    val driverID: String = "",
    @SerializedName("driverMobile")
    val driverMobile: String = "",
    @SerializedName("driverName")
    val driverName: String = "",
    @SerializedName("driverProfilePic")
    val driverProfilePic: String = "",
    @SerializedName("driverRatting")
    val driverRatting: String = "",
    @SerializedName("drvcarBackImage")
    val drvcarBackImage: Any,
    @SerializedName("drvcarColor")
    val drvcarColor: Any,
    @SerializedName("drvcarFrontImage")
    val drvcarFrontImage: Any,
    @SerializedName("drvcarID")
    val drvcarID: Any,
    @SerializedName("drvcarInteriorImage")
    val drvcarInteriorImage: Any,
    @SerializedName("drvcarLeftImage")
    val drvcarLeftImage: Any,
    @SerializedName("drvcarModel")
    val drvcarModel: Any,
    @SerializedName("drvcarNo")
    val drvcarNo: Any,
    @SerializedName("drvcarRightImage")
    val drvcarRightImage: Any,
    @SerializedName("drvcarYear")
    val drvcarYear: Any,
    @SerializedName("statusID")
    val statusID: String,
    @SerializedName("statusUpdateDate")
    val statusUpdateDate: String,
    @SerializedName("userEmail")
    val userEmail: String,
    @SerializedName("userFullName")
    val userFullName: String,
    @SerializedName("userID")
    val userID: String,
    @SerializedName("userMobile")
    val userMobile: String,
    @SerializedName("userProfilePicture")
    val userProfilePicture: String,
    @SerializedName("bookingNo")
    var bookingNo: String = ""
)



