package com.cab1.pojo
import com.google.gson.annotations.SerializedName


data class AddAddressPojo(
    @SerializedName("data")
    val `data`: List<Useraddres>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)

data class AddAddressData(
    @SerializedName("addressAddressLine1")
    val addressAddressLine1: String,
    @SerializedName("addressAddressLine2")
    val addressAddressLine2: String,
    @SerializedName("addressID")
    val addressID: String,
    @SerializedName("addressLatitude")
    val addressLatitude: String,
    @SerializedName("addressLongitude")
    val addressLongitude: String,
    @SerializedName("addressPincode")
    val addressPincode: String,
    @SerializedName("addressTitle")
    val addressTitle: String,
    @SerializedName("cityID")
    val cityID: String,
    @SerializedName("cityName")
    val cityName: String,
    @SerializedName("countryID")
    val countryID: String,
    @SerializedName("countryName")
    val countryName: String,
    @SerializedName("stateID")
    val stateID: String,
    @SerializedName("stateName")
    val stateName: String
)