package com.cab1.pojo

data class DaysMonthsPojo(
    var day: Int = 0,
    var month: Int = 0,
    var monthName: String = "",
    var dayName: String = "",
    var year: Int = 0,
    var date: String = "",
    var isSelect:Boolean=false,
    var isEnable:Boolean=false


)



