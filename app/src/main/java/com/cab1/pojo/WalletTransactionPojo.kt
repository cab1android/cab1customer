package com.cab1.pojo
import com.google.gson.annotations.SerializedName
import java.util.*


data class WalletTransactionPojo(
    @SerializedName("data")
    var `data`: ArrayList<WalletTransactionData?> = ArrayList(),
    @SerializedName("message")
    var message: String? = "",
    @SerializedName("status")
    var status: String? = "",
    @SerializedName("walletamount")
    var walletamount: String? = "",

    var transactiondate: String = ""
)

data class WalletTransactionData(
    @SerializedName("wallettransactonAmount")
    var wallettransactonAmount: String? = "",
    @SerializedName("wallettransactonDate")
    var wallettransactonDate: String? = "",
    @SerializedName("wallettransactonDesc")
    var wallettransactonDesc: String? = "",
    @SerializedName("wallettransactonID")
    var wallettransactonID: String? = "",
    @SerializedName("wallettransactonMode")
    var wallettransactonMode: String? = "",
    @SerializedName("wallettransactonReferenceID")
    var wallettransactonReferenceID: String? = "",
    @SerializedName("wallettransactonStatus")
    var wallettransactonStatus: String? = "",
    @SerializedName("wallettransactonType")
    var wallettransactonType: String? = ""
)