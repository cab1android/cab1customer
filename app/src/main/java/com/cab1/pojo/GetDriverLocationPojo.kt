package com.cab1.pojo

import com.google.gson.annotations.SerializedName


data class GetDriverLocation(
    @SerializedName("data")
    var `data`: List<DriverLocationData>,
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: String
)

data class DriverLocationData(
    @SerializedName("driverLat")
    var driverLat: String,
    @SerializedName("driverLongi")
    var driverLongi: String,
    @SerializedName("driverSerializeLocation")
    val driverSerializeLocation: DriverSerializeLocation = DriverSerializeLocation()
)

data class DriverSerializeLocation(
    @SerializedName("mAccuracy")
    val mAccuracy: Double = 0.0,
    @SerializedName("mAltitude")
    val mAltitude: Double = 0.0,
    @SerializedName("mBearing")
    val mBearing: Float = 0f,
    @SerializedName("mDistance")
    val mDistance: Double = 0.0,
    @SerializedName("mElapsedRealtimeNanos")
    val mElapsedRealtimeNanos: Long = 0,
    @SerializedName("mExtras")
    val mExtras: MExtras = MExtras(),
    @SerializedName("mHasAccuracy")
    val mHasAccuracy: Boolean = false,
    @SerializedName("mHasAltitude")
    val mHasAltitude: Boolean = false,
    @SerializedName("mHasBearing")
    val mHasBearing: Boolean = false,
    @SerializedName("mHasSpeed")
    val mHasSpeed: Boolean = false,
    @SerializedName("mInitialBearing")
    val mInitialBearing: Double = 0.0,
    @SerializedName("mIsFromMockProvider")
    val mIsFromMockProvider: Boolean = false,
    @SerializedName("mLat1")
    val mLat1: Double = 0.0,
    @SerializedName("mLat2")
    val mLat2: Double = 0.0,
    @SerializedName("mLatitude")
    val mLatitude: Double = 0.0,
    @SerializedName("mLon1")
    val mLon1: Double = 0.0,
    @SerializedName("mLon2")
    val mLon2: Double = 0.0,
    @SerializedName("mLongitude")
    val mLongitude: Double = 0.0,
    @SerializedName("mProvider")
    val mProvider: String = "",
    @SerializedName("mResults")
    val mResults: List<Int> = listOf(),
    @SerializedName("mSpeed")
    val mSpeed: Double = 0.0,
    @SerializedName("mTime")
    val mTime: Long = 0
)

data class MExtras(
    @SerializedName("mAllowFds")
    val mAllowFds: Boolean = false,
    @SerializedName("mFdsKnown")
    val mFdsKnown: Boolean = false,
    @SerializedName("mHasFds")
    val mHasFds: Boolean = false,
    @SerializedName("mParcelledData")
    val mParcelledData: MParcelledData = MParcelledData()
)

data class MParcelledData(
    @SerializedName("mNativePtr")
    val mNativePtr: Long = 0,
    @SerializedName("mOwnsNativeParcelObject")
    val mOwnsNativeParcelObject: Boolean = false
)