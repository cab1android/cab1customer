package com.cab1.pojo

data class AddressListPojo(
    val `data`: List<Data>,
    val message: String,
    val status: String
)

data class AddressData(
    val addressAddressLine1: String,
    val addressAddressLine2: String,
    val addressID: String,
    val addressLatitude: String,
    val addressLongitude: String,
    val addressPincode: String,
    val addressTitle: String,
    val cityID: String,
    val cityName: String,
    val countryID: String,
    val countryName: String,
    val stateID: String,
    val stateName: String
)