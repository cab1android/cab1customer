package com.cab1.pojo
import com.google.gson.annotations.SerializedName


data class CitylistPojo(
    @SerializedName("data")
    val `data`: List<CitylistData> = listOf(),
    @SerializedName("message")
    val message: String = "",
    @SerializedName("status")
    val status: String = ""
)

data class CitylistData(
    @SerializedName("cityID")
    val cityID: String = "",
    @SerializedName("cityName")
    val cityName: String = "",
    @SerializedName("countryID")
    val countryID: String = "",
    @SerializedName("stateID")
    val stateID: String = ""
)