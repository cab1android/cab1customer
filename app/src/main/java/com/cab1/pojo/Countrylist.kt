package com.cab1.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Countrylist(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
):Serializable

  data class Data(
    @SerializedName("countryCurrencyCode")
    val countryCurrencyCode: String,
    @SerializedName("countryDialCode")
    val countryDialCode: String,
    @SerializedName("countryFlagImage")
    val countryFlagImage: String,
    @SerializedName("countryID")
    val countryID: String,
    @SerializedName("countryISO2Code")
    val countryISO2Code: String,
    @SerializedName("countryISO3Code")
    val countryISO3Code: String,
    @SerializedName("countryName")
    val countryName: String
):Serializable