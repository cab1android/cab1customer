package com.cab1.pojo
import com.google.gson.annotations.SerializedName


 class CouponCodeAppliedPojo(
    @SerializedName("date")
    val date: List<CouponCodeDate> = listOf(),
    @SerializedName("message")
    val message: String = "",
    @SerializedName("status")
    val status: String = ""
)

data class CouponCodeDate(
    @SerializedName("couponDiscount")
    val couponDiscount: String = "",
    @SerializedName("couponDiscountType")
    val couponDiscountType: String = ""
)