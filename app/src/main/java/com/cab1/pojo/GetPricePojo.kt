package com.cab1.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*


data class GetPricePojo(
    @SerializedName("data")
    val `data`: List<Data1>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) : Serializable

data class Data1(
    @SerializedName("localPrice")
    val localPrice: ArrayList<LocalPrice>,
    @SerializedName("outstationPrice")
    val outstationPrice: ArrayList<OutStationPrice>,
    @SerializedName("rentalPrice")
    val rentalPrice: ArrayList<RentalPrice>,
    @SerializedName("setting")
    val setting: List<Setting>
) : Serializable


data class OutStationPrice(
    @SerializedName("categoryCarModel")
    var categoryCarModel: String,
    @SerializedName("categoryID")
    var categoryID: String,
    @SerializedName("categoryImage")
    var categoryImage: String,
    @SerializedName("categoryName")
    var categoryName: String,
    @SerializedName("categoryNoOfBags")
    var categoryNoOfBags: String,
    @SerializedName("categorySeats")
    var categorySeats: String,
    @SerializedName("outstationMinPrice")
    var outstationMinPrice: String,
    @SerializedName("categoryType")
    var categoryType: String,
    @SerializedName("franchiseID")
    var franchiseID: String,
    @SerializedName("fromCityID")
    var fromCityID: String,
    @SerializedName("outstationAdditionalKM")
    var outstationAdditionalKM: String,
    @SerializedName("outstationAdditionalMinute")
    var outstationAdditionalMinute: String,
    @SerializedName("outstationCreatedDate")
    var outstationCreatedDate: String,
    @SerializedName("outstationDriverAdditionalKM")
    var outstationDriverAdditionalKM: String,
    @SerializedName("outstationDriverAdditionalMinute")
    var outstationDriverAdditionalMinute: String,
    @SerializedName("outstationExcludes")
    var outstationExcludes: String,
    @SerializedName("outstationHours")
    var outstationHours: String,
    @SerializedName("outstationID")
    var outstationID: String,
    @SerializedName("outstationKM")
    var outstationKM: String,
    @SerializedName("outstationNightCharges")
    var outstationNightCharges: String,
    @SerializedName("outstationNightTimeStart")
    var outstationNightTimeStart: String,
    @SerializedName("outstationPrice")
    var outstationPrice: String,
    @SerializedName("outstationStatus")
    var outstationStaus: String,
    @SerializedName("outstationTnC")
    var outstationTnC: String,
    @SerializedName("categoryGST")
    var categoryGST: String,
    @SerializedName("outstationType")
    var outstationType: String,
    @SerializedName("outstationFreeWaitingMinute")
    var outstationFreeWaitingMinute: String,
    @SerializedName("outstationWaitingPerMinuteCharge")
    var outstationWaitingPerMinuteCharge: String,
    @SerializedName("toCityID")
    var toCityID: String
) : Serializable


data class RentalPrice(
    @SerializedName("rentalHours")
    var rentalHours: String,
    @SerializedName("rentalKM")
    var rentalKM: String,
    @SerializedName("rentalpackages")
    var rentalpackages: ArrayList<Rentalpackage>,
    var selected: Boolean = false
) : Serializable

public data class Rentalpackage(
    @SerializedName("categoryCarModel")
    var categoryCarModel: String,
    @SerializedName("categoryID")
    var categoryID: String,
    @SerializedName("categoryImage")
    var categoryImage: String,
    @SerializedName("categoryName")
    var categoryName: String,
    @SerializedName("categoryNoOfBags")
    var categoryNoOfBags: String,
    @SerializedName("categorySeats")
    var categorySeats: String,
    @SerializedName("categoryType")
    var categoryType: String,
    @SerializedName("rentalAdditionalKM")
    var rentalAdditionalKM: String,
    @SerializedName("rentalAdditionalMinute")
    var rentalAdditionalMinute: String,
    @SerializedName("rentalDriverAdditionalKM")
    var rentalDriverAdditionalKM: String,
    @SerializedName("rentalDriverAdditionalMinute")
    var rentalDriverAdditionalMinute: String,
    @SerializedName("rentalExcludes")
    var rentalExcludes: String,
    @SerializedName("rentalHours")
    var rentalHours: String,
    @SerializedName("rentalID")
    var rentalID: String,
    @SerializedName("rentalKM")
    var rentalKM: String,
    @SerializedName("rentalName")
    var rentalName: String,
    @SerializedName("rentalMinPrice")
    var rentalMinPrice: String,
    @SerializedName("rentalNightCharges")
    var rentalNightCharges: String,
    @SerializedName("rentalNightTimeStart")
    var rentalNightTimeStart: String,
    @SerializedName("rentalPrice")
    var rentalPrice: String,
    @SerializedName("rentalTnC")
    var rentalTnC: String,
    @SerializedName("categoryGST")
    var categoryGST: String,
    @SerializedName("rentalFreeWaitingMinute")
    var rentalFreeWaitingMinute: String,
    @SerializedName("rentalWaitingPerMinuteCharge")
    var rentalWaitingPerMinuteCharge: String,
    var selected: Boolean
) : Serializable

//data class RentalPrice(
//    @SerializedName("categoryCarModel")
//    var categoryCarModel: String,
//    @SerializedName("categoryID")
//    var categoryID: String,
//    @SerializedName("categoryImage")
//    var categoryImage: String,
//    @SerializedName("categoryName")
//    var categoryName: String,
//    @SerializedName("categoryNoOfBags")
//    var categoryNoOfBags: String,
//    @SerializedName("categorySeats")
//    var categorySeats: String,
//    @SerializedName("categoryType")
//    var categoryType: String,
//    @SerializedName("rentalpackages")
//    var rentalpackages: List<Rentalpackage>,
//    var selected: Boolean = false
//) : Serializable
//
//data class Rentalpackage(
//    @SerializedName("rentalAdditionalKM")
//    var rentalAdditionalKM: String,
//    @SerializedName("rentalAdditionalMinute")
//    var rentalAdditionalMinute: String,
//    @SerializedName("rentalDriverAdditionalKM")
//    var rentalDriverAdditionalKM: String,
//    @SerializedName("rentalDriverAdditionalMinute")
//    var rentalDriverAdditionalMinute: String,
//    @SerializedName("rentalExcludes")
//    var rentalExcludes: String,
//    @SerializedName("rentalHours")
//    var rentalHours: String,
//    @SerializedName("rentalID")
//    var rentalID: String,
//    @SerializedName("rentalKM")
//    var rentalKM: String,
//    @SerializedName("rentalName")
//    var rentalName: String,
//    @SerializedName("rentalNightCharges")
//    var rentalNightCharges: String,
//    @SerializedName("rentalNightTimeStart")
//    var rentalNightTimeStart: String,
//    @SerializedName("rentalPrice")
//    var rentalPrice: String,
//    @SerializedName("rentalTnC")
//    var rentalTnC: String,
//    var selected: Boolean = false
//) : Serializable

data class Setting(
    @SerializedName("settingsReachDistanceMetre")
    val settings012912024056678977918comcab1DOkHttpReachDistanceMetre: String,
    @SerializedName("settingsArrivingDistanceMetre")
    val settingsArrivingDistanceMetre: String,
    @SerializedName("settingsDisplayBookingRequestMinute")
    val settingsDisplayBookingRequestMinute: String,
    @SerializedName("settingsFreeWaitingMinute")
    val settingsFreeWaitingMinute: String,
    @SerializedName("settingsWaitingPerMinuteCharge")
    val settingsWaitingPerMinuteCharge: String,
    @SerializedName("settingsSupportEmail")
    val settingsSupportEmail: String = "",
    @SerializedName("settingsSupportPhone")
    val settingsSupportPhone: String = ""
) : Serializable

data class Drivers(
    @SerializedName("brandImage")
    val brandImage: String,
    @SerializedName("brandName")
    val brandName: String,
    @SerializedName("categoryImage")
    val categoryImage: String,
    @SerializedName("categoryName")
    val categoryName: String,
    @SerializedName("distance_km")
    val distanceKm: String,
    @SerializedName("driverCountryCode")
    val driverCountryCode: String,
    @SerializedName("driverEmail")
    val driverEmail: String,
    @SerializedName("driverGender")
    val driverGender: String,
    @SerializedName("driverID")
    val driverID: String,
    @SerializedName("driverLat")
    val driverLat: String,
    @SerializedName("driverLongi")
    val driverLongi: String,
    @SerializedName("driverMobile")
    val driverMobile: String,
    @SerializedName("driverName")
    val driverName: String,
    @SerializedName("driverRides")
    val driverRides: String,
    @SerializedName("drvcarColor")
    val drvcarColor: String,
    @SerializedName("drvcarModel")
    val drvcarModel: String,
    @SerializedName("drvcarNo")
    val drvcarNo: String,
    @SerializedName("drvcarYear")
    val drvcarYear: String,
    @SerializedName("eta_min")
    val etaMin: String
) : Serializable

data class LocalPrice(
    @SerializedName("Drivers")
    val drivers: List<Drivers>,
    @SerializedName("specialpriceoptions")
    val specialpriceoptions: List<Specialpriceoptions>,
    @SerializedName("categoryID")
    val categoryID: String,
    @SerializedName("categoryImage")
    val categoryImage: String,
    @SerializedName("categoryName")
    val categoryName: String,
    @SerializedName("categorySeats")
    val categorySeats: String,
    @SerializedName("cityID")
    val cityID: String,
    @SerializedName("franchiseID")
    val franchiseID: String,
    @SerializedName("optionAdditionalKmCharge")
    val optionAdditionalKmCharge: String,
    @SerializedName("optionBaseKM")
    val optionBaseKM: String,
    @SerializedName("optionBaseMinute")
    val optionBaseMinute: String,
    @SerializedName("optionBasefare")
    val optionBasefare: String,
    @SerializedName("optionCreatedDate")
    val optionCreatedDate: String,
    @SerializedName("optionDriverAdditionalKmCharge")
    val optionDriverAdditionalKmCharge: String,
    @SerializedName("optionDriverBaseFare")
    val optionDriverBaseFare: String,
    @SerializedName("optionDriverPerMinCharge")
    val optionDriverPerMinCharge: String,
    @SerializedName("optionID")
    val optionID: String,
    @SerializedName("optionPerMinCharge")
    val optionPerMinCharge: String,
    @SerializedName("optionStatus")
    val optionStatus: String,
    @SerializedName("priceCreatedDate")
    val priceCreatedDate: String,
    @SerializedName("priceEndDate")
    val priceEndDate: String,
    @SerializedName("priceExcludes")
    val priceExcludes: String,
    @SerializedName("priceFestivalAdditional")
    val priceFestivalAdditional: String,
    @SerializedName("priceGST")
    val priceGST: String,
    @SerializedName("priceHolidayAdditional")
    val priceHolidayAdditional: String,
    @SerializedName("priceID")
    val priceID: String,
    @SerializedName("priceStartDate")
    val priceStartDate: String,
    @SerializedName("priceStatus")
    val priceStatus: String,
    @SerializedName("priceTerms")
    val priceTerms: String,
    @SerializedName("optionMinBaseFare")
    val optionMinBaseFare: String,
    @SerializedName("priceWeekendAdditional")
    val priceWeekendAdditional: String,
    @SerializedName("timingEnd")
    val timingEnd: String,
    @SerializedName("timingStart")
    val timingStart: String,
    @SerializedName("categoryGST")
    val categoryGST: String,
    @SerializedName("priceFreeWaitingMinute")
    val priceFreeWaitingMinute: String,
    @SerializedName("priceWaitingPerMinuteCharge")
    val priceWaitingPerMinuteCharge: String,
    @SerializedName("timingsID")
    val timingsID: String,
    @SerializedName("surgeFlage")
    val surgeFlage: String,
    @SerializedName("localprice")
    val localprice: String,
    @SerializedName("priceType")
    val priceType: String,
    var selected: Boolean = false
) : Serializable

data class Specialpriceoptions(
    @SerializedName("optionAdditionalKmCharge")
    var optionAdditionalKmCharge: String,
    @SerializedName("optionBaseKM")
    var optionBaseKM: String,
    @SerializedName("optionBaseMinute")
    var optionBaseMinute: String,
    @SerializedName("optionBasefare")
    var optionBasefare: String,
    @SerializedName("optionDriverAdditionalKmCharge")
    var optionDriverAdditionalKmCharge: String,
    @SerializedName("optionDriverBaseFare")
    var optionDriverBaseFare: String,
    @SerializedName("optionDriverPerMinCharge")
    var optionDriverPerMinCharge: String,
    @SerializedName("optionID")
    var optionID: String,
    @SerializedName("optionMinBaseFare")
    var optionMinBaseFare: String,
    @SerializedName("optionPerMinCharge")
    var optionPerMinCharge: String,
    @SerializedName("priceID")
    var priceID: String,
    @SerializedName("slotEndKM")
    var slotEndKM: String,
    @SerializedName("slotID")
    var slotID: String,
    @SerializedName("slotName")
    var slotName: String,
    @SerializedName("slotStartKM")
    var slotStartKM: String
) : Serializable