package com.cab1.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data  class CmsPagePojo(
    @SerializedName("data")
    val `data`: List<CmsPageData> = listOf(),
    @SerializedName("message")
    val message: String = "",
    @SerializedName("status")
    val status: String = ""
):Serializable
data class  CmsPageData(
    @SerializedName("cmspageContents")
    val cmspageContents: String = "",
    @SerializedName("cmspageImage")
    val cmspageImage: String = "",
    @SerializedName("cmspageName")
    val cmspageName: String = "",
    @SerializedName("faq")
    val `faq`: List<Faq> = listOf()

):Serializable


data class Faq(
    @SerializedName("faqAnswer")
    val faqAnswer: String = "",
    @SerializedName("faqID")
    val faqID: String = "",
    @SerializedName("faqTitle")
    val faqTitle: String = ""
):Serializable




