package com.cab1.pojo
import com.google.gson.annotations.SerializedName


data class CarsCategoryPojo(
    @SerializedName("data")
    val `data`: List<CarsCategoryData> = listOf(),
    @SerializedName("message")
    val message: String = "",
    @SerializedName("status")
    val status: String = ""
)

data class  CarsCategoryData(
    @SerializedName("categoryID")
    val categoryID: String = "",
    @SerializedName("categoryName")
    val categoryName: String = "",
    @SerializedName("categorySeats")
    val categorySeats: String = "",
    @SerializedName("categoryType")
    val categoryType: String = ""
)