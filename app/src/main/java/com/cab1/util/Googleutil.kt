package com.cab1.cab1.driver.util

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.BounceInterpolator
import android.view.animation.LinearInterpolator
import com.cab1.R
import com.cab1.pojo.GoogleDirection
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter
import java.util.*
import kotlin.collections.ArrayList


class Googleutil {

    companion object {
        var runnable: Runnable? = null
        var carRunnable: Runnable? = null
        fun getMyLocationAddress(activity: Context, lat: Double, long: Double): String {

            var address = ""
            val geocoder = Geocoder(activity, Locale.getDefault())
            try {
                val addresses = geocoder.getFromLocation(lat, long, 1)
                if (addresses != null) {
                    val strAddress = addresses.get(0)
                    val strbuilderAddress = StringBuilder("")

                    for (i in 0..strAddress.maxAddressLineIndex) {
                        strbuilderAddress.append(strAddress.getAddressLine(i)).append(", ")
                    }
                    address = strbuilderAddress.toString()
                    Log.e("address", address)
                } else {
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


            return address
        }

        fun getMyLocationAddressList(activity: Context, lat: Double, long: Double): List<Address> {

            var address = ""
            val geocoder = Geocoder(activity, Locale.getDefault())

            var addresses: List<Address>? = null

            try {
                addresses = geocoder.getFromLocation(lat, long, 5)
                if (addresses != null) {
                    val strAddress = addresses.get(0)
                    val strbuilderAddress = StringBuilder("")

                    for (i in 0..strAddress.maxAddressLineIndex) {
                        strbuilderAddress.append(strAddress.getAddressLine(i)).append(", ")
                    }
                    address = strbuilderAddress.toString()
                    Log.e("address", address)
                } else {
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


            return addresses!!
        }


        fun decodePoly(encoded: String): ArrayList<LatLng> {


            val poly = ArrayList<LatLng>()
            var index = 0
            val len = encoded.length
            var lat = 0
            var lng = 0

            while (index < len) {
                var b: Int
                var shift = 0
                var result = 0
                do {
                    b = encoded[index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lat += dlat

                shift = 0
                result = 0
                do {
                    b = encoded[index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lng += dlng

                val p = LatLng(
                    lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5
                )
                poly.add(p)
            }

            return poly

        }

        /** Encodes a sequence of LatLngs into an encoded path string.  */


        fun bounceMarker(mMap: GoogleMap, marker: Marker) {

            val handler = Handler()

            val startTime = SystemClock.uptimeMillis()
            val duration: Long = 1500

            val proj = mMap.projection
            val markerLatLng = marker.position
            val startPoint = proj.toScreenLocation(markerLatLng)
            startPoint.offset(0, -100)
            val startLatLng = proj.fromScreenLocation(startPoint)

            val interpolator = BounceInterpolator()
            val elapsed = SystemClock.uptimeMillis() - startTime

            val t = interpolator.getInterpolation(elapsed.toFloat() / duration.toFloat())
            var lng = t * markerLatLng.longitude + (1 - t) * startLatLng.longitude

            val lat = t * markerLatLng.latitude + (1 - t) * startLatLng.latitude

            marker.position = LatLng(lat, lng)
            handler.post(object : Runnable {
                override fun run() {
                    val elapsed = SystemClock.uptimeMillis() - startTime
                    val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                    val lng = t.toDouble()
                    markerLatLng.longitude + (1 - t)
                    startLatLng.longitude

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    }
                }
            })

        }

        fun getDemoRoute(context: Context): GoogleDirection {
            val inputStream = context.resources.openRawResource(R.raw.route)
            val writer = StringWriter()
            try {
                val reader = BufferedReader(InputStreamReader(inputStream, "UTF-8"))
                var line: String? = reader.readLine()
                while (line != null) {
                    writer.write(line)
                    line = reader.readLine()
                }
            } catch (e: Exception) {
                Log.e("JsonRead", "Unhandled exception while using JSONResourceReader", e)
            } finally {
                try {
                    inputStream.close()
                } catch (e: Exception) {
                    Log.e("JsonRead", "Unhandled exception while using JSONResourceReader", e)
                }

            }


            val jsonProductsString = writer.toString()
            val gson = Gson()

            return gson.fromJson(jsonProductsString, GoogleDirection::class.java)
        }


        fun animateCar(destination: LatLng, driverMarker: Marker) {
            val startPosition = driverMarker.position
            val endPosition = LatLng(destination.latitude, destination.longitude)
            var latLngInterpolator = LatLngInterpolator.LinearFixed()
            val valueAnimator = ValueAnimator.ofFloat(0.0F, 1.0F)
            valueAnimator.duration = 2000 // duration 5 seconds
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition)
                    driverMarker.position = newPosition
                } catch (ex: Exception) {
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
            })
            valueAnimator.start()
        }


        fun bearingBetweenLocations(latLng1: LatLng, latLng2: LatLng): Double {

            val PI = 3.14159
            val lat1 = latLng1.latitude * PI / 180
            val long1 = latLng1.longitude * PI / 180
            val lat2 = latLng2.latitude * PI / 180
            val long2 = latLng2.longitude * PI / 180

            val dLon = long2 - long1

            val y = Math.sin(dLon) * Math.cos(lat2)
            val x = Math.cos(lat1) * Math.sin(lat2) - (Math.sin(lat1)
                    * Math.cos(lat2) * Math.cos(dLon))

            var brng = Math.atan2(y, x)

            brng = Math.toDegrees(brng)
            brng = (brng + 360) % 360
            return brng
        }

        private interface LatLngInterpolator {
            fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng
            class LinearFixed : LatLngInterpolator {
                override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                    val lat = (b.latitude - a.latitude) * fraction + a.latitude
                    var lngDelta = b.longitude - a.longitude
                    if (Math.abs(lngDelta) > 180) {
                        lngDelta -= Math.signum(lngDelta) * 360
                    }
                    val lng = lngDelta * fraction + a.longitude
                    return LatLng(lat, lng)
                }
            }
        }

        fun getPolyLineOption(roadColor: Int, points: ArrayList<LatLng>, width: Float = 8f): PolylineOptions {
            var blackPolylineOptions = PolylineOptions()
            blackPolylineOptions.width(width)
            blackPolylineOptions.color(roadColor)
            blackPolylineOptions.startCap(SquareCap())
            blackPolylineOptions.endCap(SquareCap())
            blackPolylineOptions.jointType(JointType.ROUND)
            blackPolylineOptions.addAll(points)
            return blackPolylineOptions
        }

        fun polyLineAnimation(firstPolyline: Polyline, secondPolyLine: Polyline) {
            var polylineAnimator = ValueAnimator.ofFloat(0F, 100.0F)
            polylineAnimator.duration = 10000
            polylineAnimator.interpolator = LinearInterpolator()
            // polylineAnimator.repeatCount =20
            polylineAnimator.addUpdateListener {
                var points = firstPolyline?.points
                var percentValue = it.animatedValue
                var size = points?.size
                var newPoints = size!! * (percentValue as Float / 100.0f)
                var p = points?.subList(0, newPoints.toInt())
                secondPolyLine?.points = p


            }



            polylineAnimator.addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {

                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }

            })
            polylineAnimator.start()


        }


        fun moveVechile(myMarker: Marker, finalPosition: LatLng) {

            var startPosition = myMarker.position

            var handler = Handler()
            var start = SystemClock.uptimeMillis()
            var interpolator = AccelerateDecelerateInterpolator()
            var durationInMs = 3000
            var hideMarker = false

            carRunnable = Runnable {
                // Calculate progress using interpolator
                var elapsed = SystemClock.uptimeMillis() - start
                var t = elapsed / durationInMs
                var v = interpolator.getInterpolation(t.toFloat())

                var currentPosition = LatLng(
                    startPosition.latitude * (1 - t) + (finalPosition.latitude) * t,
                    startPosition.longitude * (1 - t) + (finalPosition.longitude) * t
                )
                myMarker.position = currentPosition
                // myMarker.setRotation(finalPosition.getBearing())


                // Repeat till progress is completeelse
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed({
                        carRunnable
                    }, 16)
                    // handler.postDelayed(this, 100);
                } else {
                    myMarker.isVisible = !hideMarker
                }
            }

            handler.post {

                carRunnable

            }


        }


        fun rotateMarker(marker: Marker, toRotation: Float) {
            var handler = Handler()
            var start = SystemClock.uptimeMillis()
            var startRotation = marker.rotation
            var duration = 500


            var interpolator = LinearInterpolator()

            runnable = Runnable {
                var elapsed = SystemClock.uptimeMillis() - start
                var t = interpolator.getInterpolation(elapsed.toFloat() / duration)

                var rot = t * toRotation + (1 - t) * startRotation


                marker.rotation = if (-rot > 180) {
                    rot / 2
                } else {
                    rot
                }
                startRotation = if (-rot > 180) rot / 2 else rot
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(runnable, 16)
                }
            }


            handler.post(
                runnable
            )


        }

    }


}