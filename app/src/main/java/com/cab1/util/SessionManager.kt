package com.example.admin.myapplication

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.cab1.pojo.LoginData
import com.google.gson.Gson

public class SessionManager {
    companion object {
        val KEY_Currentlocation: String = "Location"
        val PREF_NAME: String = "LoginSession"
        val KEY_IS_LOGGEDIN: String = "isLoggedIn"
        val KEY_USER_OBJ: String = "AuthenticateUser"
        val KEY_USER_SETTINGS: String = "DefaultSettings"
        val KEY_USER_EMAIL: String = "UserEmail"
        val KEY_USER_PASSWORD: String = "Location"
        val KEY_SYNCED: String = "Synced"
        val KEY_USER_SOCIAL: String = "isSocial"
        val KEY_IsVerify: String = "isVerify"
        val Key_Email: String = "isEmailLogin"
        val Key_PosterId: String = "posterId"
        val Key_DeviceToken: String = "deviceToken"//fireBase
        val Key_PushCounter: String = "pushCounter"//fireBase
        val TAG = SessionManager::class.java.simpleName
        val KEY_IsOnline: String = "isOnline"



    }

    var pref: SharedPreferences
    var editor: SharedPreferences.Editor
    var _context: Context
    var PRIVATE_MODE = 0

    public constructor(context: Context) {
        this._context = context
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    public fun create_login_session(
        AuthenticateUser: String,
        email: String,
        password: String,
        isLoggedIn:Boolean,
        isVerify: Boolean,
        isEmail: Boolean,
        isUpdate:Boolean=false
    ) {
        if(!isUpdate) {
            clear_login_session()
        }
            editor.putBoolean(KEY_IS_LOGGEDIN, true)
            editor.putString(KEY_USER_OBJ, AuthenticateUser)
            editor.putString(Key_Email, email)
            editor.putString(KEY_USER_PASSWORD, password)
            editor.putBoolean(KEY_IsVerify, isVerify)
            editor.putBoolean(Key_Email, isEmail)

        editor.commit()
        Log.d(TAG, "User login session modified!")


    }

    public fun syncContacts(Synced: String) {
        editor.putString(KEY_SYNCED, Synced)
        editor.commit()
    }

    public fun Current_city(Synced: String) {
        editor.putString(KEY_Currentlocation, Synced)
        editor.commit()
    }

    public fun getPreferences(Key: String): String {
        return pref.getString(Key, "")
    }

    public fun isLoggedIn(): Boolean {

        if ((!getIsVerify()!!)!!)
            clear_login_session()
        return pref.getBoolean(KEY_IS_LOGGEDIN, false)
    }

    public fun isEmailLogin(): Boolean {

        return pref.getBoolean(Key_Email, false)
    }

    fun get_Authenticate_User(): LoginData {

        val gson = Gson()
        val json = pref.getString(KEY_USER_OBJ, "")
        return gson.fromJson(json, LoginData::class.java)
    }

    /*fun get_UserSettings(): UsersettingPojo.Datum {

        val gson = Gson()
        val json = pref.getString(KEY_USER_SETTINGS, "")
        return gson.fromJson<Any>(json, UsersettingPojo.Datum::class.java)
    }*/

    public fun set_UserSettings(defaultSettings: String) {

        editor.putString(KEY_USER_SETTINGS, defaultSettings)
        editor.commit()
        Log.d(TAG, "User settings modified!")
    }

    public fun get_Email(): String {
        return pref.getString(KEY_USER_EMAIL, "")
    }

    public fun get_Password(): String {
        return pref.getString(KEY_USER_PASSWORD, "")
    }

    public fun getIsVerify(): Boolean? {
        return pref.getBoolean(KEY_IsVerify, false)
    }

    public fun setIsVerify(isVerfiy: Boolean?) {
        editor.putBoolean(KEY_IsVerify, isVerfiy!!)
        // commit changes
        editor.commit()
        Log.d(TAG, "User login session modified!")
    }

    public fun getPosterId(): String {
        return pref.getString(Key_PosterId, "")
    }

    public fun setPosterId(posterId: String) {
        editor.putString(Key_PosterId, posterId)
        // commit changes
        editor.commit()

    }

    public fun getDeviceToken(): String {
        return pref.getString(Key_DeviceToken, "")
    }

    public fun setDeviceToken(posterId: String) {
        editor.putString(Key_DeviceToken, posterId)
        editor.commit()

    }

//    public fun sessionExpire(okListener: DialogInterface.OnClickListener): Dialog {
//        val builder = android.app.AlertDialog.Builder(_context)
//        builder.setMessage(R.string.session_expire)
//        builder.setCancelable(false)
//        builder.setPositiveButton("Ok", okListener)
//
//        val alert = builder.create()
//        alert.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        alert.show()
//
//        return alert
//    }


    public fun set_password(password: String) {
        editor.putString(KEY_USER_PASSWORD, password)
        // commit changes
        editor.commit()
        Log.d(TAG, "User login session modified!")
    }

    public fun clear_login_session() {
        editor.clear()
        editor.commit()

    }


    var NotificationRead: Boolean
        get() = pref.getBoolean("NotificationRead", true)
        set(NotificationRead) {
            editor.putBoolean("NotificationRead", NotificationRead)
            editor.commit()
        }
}