package com.cab1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.api.RestClient
import com.cab1.model.DriverRatingModel
import com.cab1.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_driver_rating.*
import org.json.JSONArray
import org.json.JSONObject

class DriverRatingActivity : AppCompatActivity() {

    lateinit var sessionManager: SessionManager
    var driverID: String = ""
    var bookingId: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_rating)

        sessionManager = SessionManager(this)

        if (intent != null) {
            tv_driver_name.text = intent.getStringExtra("driverName")
            driverID = intent.getStringExtra("driverID")

            bookingId = intent.getStringExtra("bookingID")
            driver_Profile.setImageURI(RestClient.image_driver_url + intent.getStringExtra("driverProfilePic"))

        }

        bookingIdTextView.text = "Booking Id: " + bookingId

        closeImageView.setOnClickListener {
            onBackPressed()
        }

        btnSubmit.setOnClickListener {

            if (driver_rating.rating > 0) {
                driverRating(driver_rating.rating.toString(), driverID, bookingId)
            } else {
                MyUtils.showSnackbar(this@DriverRatingActivity, "Please give ratings", ll_rating)
            }
        }
    }


    private fun driverRating(rating: String, driverId: String, bookingId: String) {
        btnSubmit.startAnimation()


        val jsonObject = JSONObject()
        val jsonArray = JSONArray()

        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().userID)
            jsonObject.put("bookingID", bookingId)
            jsonObject.put("driverID", driverId)
            jsonObject.put("bookingRatting", rating)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: Exception) {
        }
        jsonArray.put(jsonObject)

        val driverRatingModel = ViewModelProviders.of(this).get(DriverRatingModel::class.java)
        driverRatingModel.driverRating(this, jsonArray.toString()).observe(this, Observer { driverRatingPojo ->
            if (driverRatingPojo != null) {
                if (driverRatingPojo[0].status.equals("true", true)) {
                    btnSubmit.revertAnimation()

                    MyUtils.startActivity(this@DriverRatingActivity, MainActivity::class.java, true)

                } else {
                    btnSubmit.revertAnimation()

                    showSnackBar(
                        driverRatingPojo[0].message
                    )
                }
            } else {
                btnSubmit.revertAnimation()

                MyUtils.dismissProgressDialog()
                try {
                    if (!MyUtils.isInternetAvailable(this)) {
                        showSnackBar("resources.getString(R.string.error_crash_error_message")

                    } else {

                        showSnackBar(resources.getString(R.string.error_common_network))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }


        })


    }

    override fun onBackPressed() {
        MyUtils.startActivity(this@DriverRatingActivity, MainActivity::class.java, true)
    }

    fun showSnackBar(message: String) {
        if ((ll_rating != null) and !isFinishing)
            Snackbar.make(ll_rating!!, message, Snackbar.LENGTH_LONG).show()

    }


}
