package com.cab1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.CarsCategoryData
import kotlinx.android.synthetic.main.item_cancel_trip_layout.view.*

class CarscategoryListAdapter(val context: Context, val list:ArrayList<CarsCategoryData?>?= ArrayList(), var onItemClickListener: OnItemClickListener? = null) :
    RecyclerView.Adapter<CarscategoryListAdapter.ViewHolder>() {


    var mSelection = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cancel_trip_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.reasonTextView_price.visibility=View.GONE
        holder.reason.text= list!![position]!!.categoryName

        if (mSelection == position)
        {
            holder.radioButtonSelection.isChecked=true

        } else {
            holder.radioButtonSelection.isChecked=false

        }


        holder.itemView.setOnClickListener {
            if (mSelection == holder.adapterPosition) {
                mSelection = -1
            } else {
                mSelection = holder.adapterPosition
                if (onItemClickListener != null)
                    onItemClickListener!!.itemClick(holder.adapterPosition,list[holder.adapterPosition]!!.categoryName,list[holder.adapterPosition]!!.categoryID)

            }
            notifyDataSetChanged()
        }
        holder.radioButtonSelection.setOnClickListener{
            holder.itemView.performClick()
        }

    }
    override fun getItemCount(): Int {
        return list!!.size

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val reason = view.reasonTextView
        val radioButtonSelection = view.radioButtonSelection
        val reasonTextView_price = view.reasonTextView_price


    }

    interface OnItemClickListener {
        fun itemClick(pos: Int, categoryName: String,  categoryId: String)
    }

}
