package com.cab1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.WalletTransactionData
import com.cab1.pojo.WalletTransactionPojo
import com.cab1.util.DividerItemDecorator
import com.cab1.util.MyUtils
import com.cab1.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_wallet_transactions.view.*


class WalletTransactionsAdapter(var context: Context,var walletTransactionPojo: ArrayList<WalletTransactionPojo?>? = ArrayList()) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if(viewType==MyUtils.Loder_TYPE)
        {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)
        }else {

            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_wallet_transactions, parent, false)
            return ViewHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return walletTransactionPojo!!.size
    }
    override fun getItemViewType(position: Int): Int {
        return if (walletTransactionPojo!![position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE

    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is LoaderViewHolder) run {
            val loaderViewHolder = holder as LoaderViewHolder
            loaderViewHolder.mProgressBar?.visibility=View.VISIBLE
            return

        } else if (holder is ViewHolder) {
            val holder1 = holder as ViewHolder
            holder1.transactionDate.text=walletTransactionPojo!![position]!!.transactiondate
            holder1.subTransactionsAdapter.setData(walletTransactionPojo!![position]!!.data)
        }

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val subRecyclerView = itemView.transactionList_RecycleView
        var transactionDate = itemView.transactionMonth_TextView
        lateinit var linearLayoutManager: LinearLayoutManager

        var itemList: ArrayList<WalletTransactionData?> = ArrayList()

        var subTransactionsAdapter = WalletSubTransactionsAdapter(itemView.context, itemList)

        init {

             linearLayoutManager = LinearLayoutManager(itemView.context)

            subRecyclerView.layoutManager = linearLayoutManager
            subRecyclerView.addItemDecoration(
                DividerItemDecorator(
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.divider_recycleview_item
                    )!!
                )
            )
            subRecyclerView.isNestedScrollingEnabled=false
            subRecyclerView.adapter = subTransactionsAdapter
        }


    }
}