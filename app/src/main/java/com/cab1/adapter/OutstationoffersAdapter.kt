package com.cab1.adapter

import android.app.Activity
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.CouponofferData
import kotlinx.android.synthetic.main.item_outstationoffers.view.*

class OutstationoffersAdapter(var context: Activity,var couponofferData:ArrayList<CouponofferData>,var onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<OutstationoffersAdapter.CarsTripViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OutstationoffersAdapter.CarsTripViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_outstationoffers, parent, false)
        return CarsTripViewHolder(v)
    }


    override fun onBindViewHolder(holder: OutstationoffersAdapter.CarsTripViewHolder, position: Int) {

        holder.bind("",position,onItemClickListener,couponofferData[position])

    }

    override fun getItemCount(): Int {
        return couponofferData.size
    }


    class CarsTripViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(
            item: String,
            position: Int,
            onItemClickListener: OnItemClickListener,
            couponofferData: CouponofferData
        ) {
            with(itemView)
            {
                  tv_code.text=couponofferData.couponCode
                  tv_description.text=Html.fromHtml(couponofferData.couponDescription)
                offersViewMoreTv.setOnClickListener {
                    if (onItemClickListener != null)
                        onItemClickListener!!.itemClick(position, item)
                   /* var bottomSheetFragment = SelectCouponCodeBottomSheetFragment()
                    bottomSheetFragment.show((context as MainActivity).supportFragmentManager, bottomSheetFragment.tag)*/
                }
            }
        }
    }
    interface OnItemClickListener {
        fun itemClick(pos: Int, code: String)
    }

}

