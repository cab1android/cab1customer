package com.cab1.adapter

import android.app.Activity
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.CouponofferData
import kotlinx.android.synthetic.main.coupon_list_layout.view.*

class CouponCodeDataAdapter(val context: Activity, val list: ArrayList<CouponofferData>, var onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<CouponCodeDataAdapter.ViewHolder>() {
    var mSelection = -1


    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.coupon_list_layout, parent, false)


        return ViewHolder(view)
    }

    override fun onBindViewHolder(@NonNull holder: ViewHolder, position: Int) {
        if (mSelection == position) {
            holder.couponDataAdapterSelectionImageView.isChecked = true

        } else {
            holder.couponDataAdapterSelectionImageView.isChecked = false

        }
        holder.couponCodeDataAdapterTextView.text=list[position].couponCode
        holder.couponCodeDataAdapterDescTextView.text= Html.fromHtml(list[position].couponDescription)


        holder.itemView.setOnClickListener {
            if (mSelection == holder.adapterPosition) {
                mSelection = -1
            } else {
                mSelection = holder.adapterPosition
                if (onItemClickListener != null)
                    onItemClickListener!!.itemClick(holder.adapterPosition, list[holder.adapterPosition].couponCode)

            }
            notifyDataSetChanged()
        }
        holder.couponDataAdapterSelectionImageView.setOnClickListener {
            holder.itemView.performClick()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        val couponDataAdapterSelectionImageView = itemView.couponDataAdapterSelectionRadioButton
        val couponCodeDataAdapterTextView = itemView.couponCodeDataAdapterTextView
        val couponCodeDataAdapterDescTextView = itemView.couponCodeDataAdapterDescTextView


    }

    interface OnItemClickListener {
        fun itemClick(pos: Int, code: String)
    }
}
