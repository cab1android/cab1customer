package com.cab1.adapter

import android.app.Activity
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.Faq
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.item_faq.view.*

class FaqAdapter(val context: Activity, var faqlist: List<Faq>, val onItemClick: OnItemClick) :
    RecyclerView.Adapter<FaqAdapter.CounteryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CounteryViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_faq, parent, false)
        return CounteryViewHolder(v)
    }


    override fun onBindViewHolder(holder: CounteryViewHolder, position: Int) {
        holder.bind(faqlist[position], position, onItemClick)

    }

    override fun getItemCount(): Int {
        return faqlist.size
    }


    class CounteryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(faq: Faq, position: Int, onitemClick: OnItemClick) = with(itemView) {
            tv_faq_que.text = faq.faqTitle
            tv_faq_ans.text = Html.fromHtml(faq.faqAnswer)

            ll_sub_faq. setOnClickListener {
                if (tv_faq_ans.visibility == View.VISIBLE) {
                    MyUtils.collapse(tv_faq_ans)
                    tv_faq_que.setTextColor(resources.getColor(R.color.black))
                    img_arrow.setImageResource(R.drawable.ic_right_arrow)
                } else {
                    MyUtils.expand(tv_faq_ans)
                    tv_faq_que.setTextColor(resources.getColor(R.color.black))
                    img_arrow.setImageResource(R.drawable.ic_up_arrow)
                }
            }
        }
    }

    interface OnItemClick {
        fun onClicklisneter(pos: Int, faq: Faq)

    }

}