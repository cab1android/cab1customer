package com.cab1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.WalletTransactionData
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.item_sub_transactions.view.*


class WalletSubTransactionsAdapter(var context: Context,var itemList: java.util.ArrayList<WalletTransactionData?>): RecyclerView.Adapter<WalletSubTransactionsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
          return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sub_transactions,parent,false))
    }

    fun setData(itemList1:java.util.ArrayList<WalletTransactionData?>)
    {
        itemList=ArrayList()
        itemList.addAll(itemList1)
       notifyDataSetChanged()

    }
    override fun getItemCount(): Int {
     return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.transactionTitleTextView.text=itemList[position]?.wallettransactonType
        holder.transactionRSTextView.text=itemList[position]?.wallettransactonMode!!+"Rs. "+MyUtils.priceFormat(itemList[position]?.wallettransactonAmount!!)
        holder.transactionDateTitleTextView.text= MyUtils.formatDate(itemList[position]?.wallettransactonDate!!, "yyyy-MM-dd HH:mm:ss", "EEE, MMM d") +" | Paid By "+itemList[position]?.wallettransactonMode!!

        if(itemList[position]?.wallettransactonMode!!.equals("-",true))
        {
            holder.transactionRSTextView.setTextColor(context.resources.getColor(R.color.debit_wallet))

        }else if(itemList[position]?.wallettransactonMode!!.equals("+",true))
        {
            holder.transactionRSTextView.setTextColor(context.resources.getColor(R.color.credit_wallet))
        }

    }


    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
    {
        var transactionRSTextView= itemView.transactionRSTextView!!
        var transactionTitleTextView= itemView.transactionTitleTextView!!
        var transactionDateTitleTextView= itemView.transactionDateTitleTextView!!
        var transactionImage= itemView.transactionImage!!
    }

}