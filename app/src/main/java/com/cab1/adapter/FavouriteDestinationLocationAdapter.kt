package com.cab1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.FavouriteLocation
import kotlinx.android.synthetic.main.item_destination_location_layout_inflater.view.*

class FavouriteDestinationLocationAdapter (val context: Context, val favouriteLocationItemList: List<FavouriteLocation>,var onRecyclerItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<FavouriteDestinationLocationAdapter.ViewHolder>() {

    var mClickListener: OnItemClickListener? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener=onRecyclerItemClickListener
        holder?.tv_FavouritesLocationName.text = favouriteLocationItemList[position].Title;
        holder?.tv_FavouritesLocationAddress.text = favouriteLocationItemList[position].Address;
        holder?.imgFavouriteDelete.setOnClickListener {
            if(mClickListener != null){
                mClickListener!!.onItemClick(position,"Delet")
            }

        }
        holder?.itemView.setOnClickListener {
            if(mClickListener != null){
                mClickListener!!.onItemClick(position,"SelectionView")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_destination_location_layout_inflater, parent, false))
    }

//    companion object {
//        var mClickListener: OnItemClickListener? = null
//    }

    override fun getItemCount() = favouriteLocationItemList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tv_FavouritesLocationName = itemView.tvFavouritesLocationName
        val tv_FavouritesLocationAddress= itemView.tvFavouritesLocationAddress
        val imgFavouriteDelete= itemView.img_FavouriteDelete

    }

    interface OnItemClickListener {
        fun onItemClick(position: Int,typeofOperation: String)
    }
}