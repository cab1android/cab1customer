package com.cab1.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.DaysMonthsPojo
import kotlinx.android.synthetic.main.days_adapter_layout.view.*
import java.text.SimpleDateFormat
import java.util.*

class DaysAdapter(var context: Context, val list: ArrayList<DaysMonthsPojo>, var onItemClickListener: onClickedListener) :
    RecyclerView.Adapter<DaysAdapter.ViewHolder>() {

    var selectedDatePostion = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.days_adapter_layout, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (list[position].isSelect) {
            selectedDatePostion = position
            holder.selectionBg.visibility = View.VISIBLE
        } else {
            holder.selectionBg.visibility = View.INVISIBLE
        }
        if (list[position].isEnable) {
            holder.daysNumber.setTextColor(context.resources.getColor(R.color.text_primary))
            holder.daysNames.setTextColor(context.resources.getColor(R.color.text_primary))
        } else {
            holder.daysNumber.setTextColor(context.resources.getColor(R.color.text_secondary))
            holder.daysNames.setTextColor(context.resources.getColor(R.color.text_secondary))
        }

        holder.daysNumber.text = "" + list.get(position).day

        holder.daysNames.text = list.get(position).dayName

        holder.itemView.setOnClickListener {

            if (list[holder.adapterPosition].isEnable) {
                if (selectedDatePostion > -1 && holder.adapterPosition != selectedDatePostion) {
                    list[selectedDatePostion].isSelect = false
                }
                selectedDatePostion = holder.adapterPosition
                list[selectedDatePostion].isSelect = true
                onItemClickListener.clicked(holder.adapterPosition)
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val daysNames = view.daysName
        val daysNumber = view.daysNumber
        val selectionBg = view.selectionBgImageView

    }


    private fun getDay(date_in_string_format: String): String? {
        val df = SimpleDateFormat("dd-M-yyyy")
        val date: Date
        try {
            date = df.parse(date_in_string_format)
        } catch (e: Exception) {
            Log.d("Error:", "Exception $e")
            return null
        }

        return SimpleDateFormat("EE").format(date)
    }
    interface onClickedListener {
        fun clicked(pos: Int)
    }

}
