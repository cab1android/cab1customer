package com.cab1.adapter

import android.app.Activity
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.api.RestClient
import com.cab1.pojo.GoogleDirection
import com.cab1.pojo.LocalPrice
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.item_cars.view.*
import java.util.*


class CarsLocalTripAdapter(
    val context: Activity, var directionRouteResponse: GoogleDirection?,
    public val localPriceData: ArrayList<LocalPrice> = ArrayList<LocalPrice>(),
    val onItemClick: OnItemClick
) : RecyclerView.Adapter<CarsLocalTripAdapter.CarsTripViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarsLocalTripAdapter.CarsTripViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cars, parent, false)
        val width = parent.measuredWidth / 3
        v.minimumWidth = width

        return CarsTripViewHolder(v)
    }

    override fun onBindViewHolder(holder: CarsLocalTripAdapter.CarsTripViewHolder, position: Int) {
        holder.bind(localPriceData, position, directionRouteResponse, onItemClick)

        holder.itemView.setOnClickListener {
            onItemClick.onClicklisneter(position, "")

            android.os.Handler().postDelayed({
                holder.itemView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.abc_grow_fade_in_from_bottom
                    )
                )
            }, 50)

        }
    }

    override fun getItemCount(): Int {
        return localPriceData.size
    }

    class CarsTripViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            localPriceData: ArrayList<LocalPrice>,
            position: Int,
            directionRouteResponse: GoogleDirection?,
            onitemClick: OnItemClick
        ) = with(itemView) {

            var totalAmount: Double = 0.0
            var extra: Double = 0.0

            tvdriver_eta_time.visibility = View.VISIBLE

            tv_carsname.text = localPriceData[position].categoryName.capitalize()

            tvridersize.text = "1 - " + localPriceData[position].categorySeats


            if (directionRouteResponse != null) {

                fareBreakupIcon.visibility = View.VISIBLE

                fareBreakupIcon.setOnClickListener {
                    onitemClick.onFareBreakupClick(position)
                }

                val distance = directionRouteResponse!!.routes!![0]!!.legs!![0]!!.distance!!.value!!.toDouble() / 1000
                val duration = directionRouteResponse!!.routes!![0]!!.legs!![0]!!.duration!!.value!!.toDouble() / 60

                var baseCharge = 0.00
                var baseKms = 0.00
                var baseMin = 0.00
                var diffDistance: Double = 0.0
                var diffDuration: Double = 0.0


                if (localPriceData[position].priceType.equals("specialprice") && !localPriceData[position].specialpriceoptions.isNullOrEmpty()) {

                    var mDistnaceKm = distance
                    var subTotal = 0.0

                    for (i in 0 until localPriceData[position].specialpriceoptions.size) {

                        if (localPriceData[position].specialpriceoptions[i].slotEndKM.toInt() <= mDistnaceKm) {

                            baseCharge = localPriceData[position].specialpriceoptions[i].optionBasefare.toDouble()
                            baseKms = localPriceData[position].specialpriceoptions[i].optionBaseKM.toDouble()

                            if (localPriceData[position].specialpriceoptions[i].slotEndKM.toInt() > baseKms) {
                                diffDistance =
                                    (localPriceData[position].specialpriceoptions[i].slotEndKM.toInt() - baseKms) * localPriceData[position].specialpriceoptions[i].optionAdditionalKmCharge.toDouble()
                            }

                            mDistnaceKm -= localPriceData[position].specialpriceoptions[i].slotEndKM.toInt()
                            subTotal += baseCharge + diffDistance

                        }

                        if (mDistnaceKm <= 0)
                            break
                    }


                    if (mDistnaceKm > 0 && !localPriceData[position].specialpriceoptions.isNullOrEmpty()) {
                        baseCharge =
                            localPriceData[position].specialpriceoptions[0].optionBasefare.toDouble()
                        baseKms =
                            localPriceData[position].specialpriceoptions[0].optionBaseKM.toDouble()

                        if (baseKms >= mDistnaceKm) {
                            subTotal += baseCharge
                        } else {
                            diffDistance =
                                (mDistnaceKm - baseKms) * localPriceData[position].specialpriceoptions[0].optionAdditionalKmCharge.toDouble()

                            subTotal += baseCharge + diffDistance
                        }

                    }
                    // duration
                    if (!localPriceData[position].specialpriceoptions.isNullOrEmpty()) {
                        baseMin =
                            duration.minus(localPriceData[position].specialpriceoptions[0].optionBaseMinute.toInt())
                        if (baseMin > 0) {
                            subTotal += baseMin * localPriceData[position].specialpriceoptions[0].optionPerMinCharge.toDouble()
                        }
                    }

                    totalAmount = subTotal

                } else {

//                if (localPriceData[position].optionBasefare.toDouble() > localPriceData[position].optionMinBaseFare.toDouble()) {
                    baseCharge = localPriceData[position].optionBasefare.toDouble()
//                } else {
//                    baseCharge = localPriceData[position].optionMinBaseFare.toDouble()
//                }

                    baseKms = localPriceData[position].optionBaseKM.toDouble()
                    baseMin = localPriceData[position].optionBaseMinute.toDouble()
                    if (distance > baseKms) {
                        diffDistance =
                            (distance - baseKms) * localPriceData[position].optionAdditionalKmCharge.toDouble()
                    }

                    if (duration > baseMin) {
                        diffDuration = (duration - baseMin) * localPriceData[position].optionPerMinCharge.toDouble()
                    }

                    totalAmount = baseCharge + diffDistance + diffDuration
                    Log.d("System out", "without GST : " + totalAmount)

                    if (localPriceData[position].optionMinBaseFare.toDouble() > totalAmount) {
                        totalAmount = localPriceData[position].optionMinBaseFare.toDouble()
                    }

                }

                totalAmount = totalAmount + totalAmount * localPriceData[position].categoryGST.toDouble() / 100
                pricePerCarTv.text = "Rs. " + MyUtils.priceFormat(totalAmount)
                pricePerCarTv.visibility = View.VISIBLE
            } else {
                fareBreakupIcon.visibility = View.GONE
            }


            val imageUri =
                Uri.parse(RestClient.image_category_url + localPriceData[position].categoryImage)

            img_cars.setImageURI(imageUri)

            if (localPriceData[position].selected) {
                img_cars.background = context.getDrawable(R.drawable.ic_car_background_selected_24dp)
            } else {
                img_cars.background = context.getDrawable(R.drawable.ic_car_background_unselected_24dp)
            }

            if (localPriceData[position].drivers.size > 0) {
                tvdriver_eta_time.visibility = View.VISIBLE
                tvdriver_eta_time.text = localPriceData[position].drivers[0].etaMin + " min"
            } else {
                tvdriver_eta_time.visibility = View.INVISIBLE
            }
        }
    }

    fun fillDirectionResponse(directionRouteResponse: GoogleDirection?) {
        this.directionRouteResponse = directionRouteResponse
        notifyDataSetChanged()
    }

    interface OnItemClick {
        fun onClicklisneter(pos: Int, carsName: String)
        fun onFareBreakupClick(pos: Int)
    }
}