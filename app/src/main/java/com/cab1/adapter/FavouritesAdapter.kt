package com.cab1.adapter

import android.app.Activity
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.pojo.Useraddres
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.favourite_list_layout.view.*
import java.util.*

class FavouritesAdapter(
    val mContext: Activity,
    val data: ArrayList<Useraddres> = ArrayList<Useraddres>(),
    val onItemClick: OnItemClick
) :
    RecyclerView.Adapter<FavouritesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.favourite_list_layout, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position], position, onItemClick)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(address1: Useraddres, position: Int, onitemClick: OnItemClick) =
            with(itemView) {

                //                favaddress.text = address1.
                favaddress.text = address1.addressTitle

//                favaddress_des.text = address1.cityName + ", " + address1.stateName + ", " + address1.countryName
                favaddress_des.text = address1.addressAddressLine1

                if (address1.addressTitle.equals("Home")) {
                    addressTitleIv.setImageResource(R.drawable.favourite_home_icon)
                } else if (address1.addressTitle.equals("Work")) {
                    addressTitleIv.setImageResource(R.drawable.favourite_work_icon)
                } else {
                    addressTitleIv.setImageResource(R.drawable.location_icon_grey)
                }

                itemView.setOnClickListener {
                    onitemClick.onClicklisneter(position, "")
                }

                deleteAddressIv.setOnClickListener {
                    MyUtils.showMessageYesNo(context, "Are you sure want remove your favourite location?", "Delete favourite?",
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            onitemClick.onDelete(position)

                        })

                }

            }
    }


    interface OnItemClick {
        fun onClicklisneter(pos: Int, carsName: String)
        fun onDelete(pos: Int)

    }
}
