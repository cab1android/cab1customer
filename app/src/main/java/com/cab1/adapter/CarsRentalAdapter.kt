package com.cab1.adapter

import android.app.Activity
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.api.RestClient
import com.cab1.pojo.RentalPrice
import com.cab1.pojo.Rentalpackage
import kotlinx.android.synthetic.main.item_cars.view.*

class CarsRentalAdapter(
    val context: Activity,
    val rentalData: ArrayList<RentalPrice> = ArrayList<RentalPrice>(),
    val onItemClick: OnItemClick
) :
    RecyclerView.Adapter<CarsRentalAdapter.CarsTripViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarsRentalAdapter.CarsTripViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cars, parent, false)
        val width = parent.measuredWidth / 3
        v.minimumWidth = width



        return CarsTripViewHolder(v)
    }


    override fun onBindViewHolder(holder: CarsRentalAdapter.CarsTripViewHolder, position: Int) {
        holder.bind(rentalData[0].rentalpackages[position], position, onItemClick)

        holder.itemView.setOnClickListener {

            onItemClick.onClicklisneter(position, "")
            android.os.Handler().postDelayed({
                holder.itemView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.abc_grow_fade_in_from_bottom
                    )
                )
            }, 50)
        }

    }

    override fun getItemCount(): Int {

        if (rentalData != null && rentalData.size > 0) {

            return rentalData[0].rentalpackages.size
        } else {
            return 0
        }

    }


    class CarsTripViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(rentalData: Rentalpackage, position: Int, onitemClick: OnItemClick) =
            with(itemView) {

                tvdriver_eta_time.visibility = View.INVISIBLE
                tv_carsname.text = rentalData.categoryName.toString().capitalize()
                tvridersize.text = "1 - " + rentalData.categorySeats

                if (rentalData.selected) {

                    img_cars.background = context.getDrawable(R.drawable.ic_car_background_selected_24dp)
                } else {

                    img_cars.background = context.getDrawable(R.drawable.ic_car_background_unselected_24dp)
                }

                val imageUri =
                    Uri.parse(RestClient.image_category_url + rentalData.categoryImage)

                img_cars.setImageURI(imageUri)

            }
    }

    interface OnItemClick {
        fun onClicklisneter(pos: Int, carsName: String)

    }
}