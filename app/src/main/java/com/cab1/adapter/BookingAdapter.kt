package com.cab1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.api.RestClient
import com.cab1.pojo.GetMyBookingData
import com.cab1.util.MyUtils
import com.cab1.viewholder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_mybooking_layout_inflater.view.*

class BookingAdapter(
    val context: Context, val tabposition: Int, var bookingdata:ArrayList<GetMyBookingData?>?= ArrayList(),
    var onRecyclerItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mClickListener: OnItemClickListener? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder , position: Int) {
        if (holder is LoaderViewHolder) run {
            val loaderViewHolder = holder as LoaderViewHolder
            loaderViewHolder.mProgressBar?.visibility=View.VISIBLE
            return

        } else if (holder is ViewHolder) {
            val holder1 = holder as ViewHolder
            holder1!!.bind(bookingdata!![position]!!, position, tabposition, onRecyclerItemClickListener)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder  {
        if(viewType==MyUtils.Loder_TYPE)
        {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)
        }else {

            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_mybooking_layout_inflater, parent, false)
            return ViewHolder(v)
        }
    }

    override fun getItemCount(): Int {
        return bookingdata!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (bookingdata!![position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            bookingdata1:GetMyBookingData,
            position: Int,
            tabposition: Int,
            onitemClick: OnItemClickListener
        ) = with(itemView) {

            tvCarName.text = bookingdata1.drvcarModel
            tvTripType.text = bookingdata1.bookingType
            if (bookingdata1!!.bookingSubType.contains("street", true))
                tvTripType.text = resources.getString(R.string.easy_ride)

            tvTypeOfPayment.text = bookingdata1.bookingPaymentMode
            tvRidePrice.text = "Rs. " + MyUtils.priceFormat(bookingdata1.bookingEstimatedAmount)
            tvFromLocationAddress.text = bookingdata1.bookingPickupAddress
            tvdestinationLocationAddress.text = bookingdata1.bookingDropAddress
            tvDateAndTime.text = MyUtils.formatDate(
                bookingdata1.bookingDate,
                "yyyy-MM-dd HH:mm:ss",
                "EEE, MMM d, hh:mm aaa"
            )
            val userimageUri =RestClient.image_driver_url + bookingdata1.driverProfilePic
            val carimageUri =RestClient.image_category_url + bookingdata1.categoryImage

            imgDriverPhoto.setImageURI(userimageUri)
            imgTypeOfCar.setImageURI(carimageUri)
            imgTypeOfCar.background = resources.getDrawable(R.drawable.ic_car_background_unselected_24dp)



           when (tabposition) {
                0 -> {
                    if (bookingdata1.bookingType.equals("rental", true)) {
                        ll_to_address.visibility = View.GONE
                        image_dotted.visibility = View.GONE
                        image_dottedLine.visibility = View.GONE
                    }else
                    {
                        ll_to_address.visibility = View.VISIBLE
                        image_dotted.visibility = View.VISIBLE
                        image_dottedLine.visibility = View.VISIBLE
                    }
                }
                1 -> {
                    ll_to_address.visibility = View.VISIBLE
                    image_dotted.visibility = View.VISIBLE
                    image_dottedLine.visibility = View.VISIBLE
                }
                2 -> {
                    ll_to_address.visibility = View.VISIBLE
                    image_dotted.visibility = View.VISIBLE
                    image_dottedLine.visibility = View.VISIBLE

                }

            }


            setOnClickListener {
                if (onitemClick != null) {
                    onitemClick!!.onItemClick(position, "SelectionView")
                }
            }
        }


    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, typeofOperation: String)
    }
}