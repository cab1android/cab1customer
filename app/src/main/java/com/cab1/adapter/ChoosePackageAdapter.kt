package com.cab1.adapter

import android.app.Activity
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.cab1.R
import com.cab1.api.RestClient
import com.cab1.pojo.OutStationPrice
import com.cab1.pojo.Rentalpackage
import com.cab1.util.MyUtils
import kotlinx.android.synthetic.main.choose_package_layout.view.*

class ChoosePackageAdapter(
    val context: Activity,
    var pricePos: Int,
    var bookingType: String,
    var outstationType: String,
    var data: List<Rentalpackage>,
    var outStationPrice: List<OutStationPrice>,
    val type: String,

    val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ChoosePackageAdapter.ViewHolder>() {

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.choose_package_layout, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(@NonNull holder: ViewHolder, position: Int) {
        if (bookingType.equals("Rental", true)) {
            holder.bind(pricePos, position, bookingType, outstationType, data, onItemClickListener, context)
        } else {
            holder.bindOutstation(
                pricePos,
                position,
                bookingType,
                outstationType,
                outStationPrice[position],
                onItemClickListener,
                context
            )
        }


        holder.itemView.setOnClickListener {

            onItemClickListener.onClickCell(position)


            notifyDataSetChanged()

        }

    }

    override fun getItemCount(): Int {

        if (bookingType.equals("Rental", true))
            return data.size
        else
            return outStationPrice.size

    }

    fun changePackage(position: Int) {
        this.pricePos = position
        notifyDataSetChanged()
    }

    fun updateData() {
        notifyDataSetChanged()
    }

    class ViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            pricePosition: Int,
            position: Int,
            bookingType: String,
            outstationType: String,
            data: List<Rentalpackage>,
            onItemClickListener: OnItemClickListener,
            context: Activity
        ) {
            with(itemView) {


                val imageUri =
                    Uri.parse(RestClient.image_category_url + data[position].categoryImage)
                choosePackageCarImageView.setImageURI(imageUri)

                if (data[position].selected) {
                    choosePackageSelectionImageView.setImageResource(R.drawable.radio_btn_selected_payment)
                } else {
                    choosePackageSelectionImageView.setImageResource(R.drawable.radio_btn_unselected_payment)
                }

                var GstIncludedPrice = 0.00

                if (data[position].rentalPrice.toDouble() > data[position].rentalMinPrice.toDouble()) {
                    GstIncludedPrice = data[position].rentalPrice.toDouble()
                } else {
                    GstIncludedPrice = data[position].rentalMinPrice.toDouble()
                }

                val gstval = data[position].categoryGST.toDouble()

                GstIncludedPrice = GstIncludedPrice + GstIncludedPrice * gstval / 100

//                if (data[position].selected && pricePosition <= data.size) {

                packagePriceTextView.text =
                    "Rs. " + MyUtils.formatingPriceDisplay("" + GstIncludedPrice)
//                } else {
//                    packagePriceTextView.text =
////                        "Rs. " + MyUtils.formatingPriceDisplay(GstIncludedPrice)
//                }

                categorySeats.text = data[position].categorySeats + " Seats"
                categoryNoOfBags.text =
                    data[position].categoryNoOfBags + " Luggage Bag"

                carTypeTextView.text = data[position].categoryName
                carNamesTextView.text = data[position].categoryCarModel



                setOnClickListener {
                    if (onItemClickListener != null) {
                        onItemClickListener!!.onItemClick(position, "SelectionView")
                    }
                }
            }
        }

        fun bindOutstation(
            pricePosition: Int,
            position: Int,
            bookingType: String,
            outstationType: String,
            outstation: OutStationPrice,
            onItemClickListener: OnItemClickListener,
            context: Activity
        ) {
            with(itemView) {
                if (bookingType.equals("Outstation")) {

                    if (outstation.outstationType.equals(outstationType)) {

                        choosePackageSelectionImageView.visibility = View.GONE

                        val imageUri =
                            Uri.parse(RestClient.image_category_url + outstation.categoryImage)
                        choosePackageCarImageView.setImageURI(imageUri)

                        categorySeats.text = outstation.categorySeats + " Seats"
                        categoryNoOfBags.text = outstation.categoryNoOfBags + " Luggage Bag"

                        carTypeTextView.text = outstation.categoryName
                        carNamesTextView.text = outstation.categoryCarModel

                        var GstIncludedPrice = 0.00

                        if (outstation.outstationPrice.toDouble() > outstation.outstationMinPrice.toDouble()) {
                            GstIncludedPrice = outstation.outstationPrice.toDouble()
                        } else {
                            GstIncludedPrice = outstation.outstationMinPrice.toDouble()
                        }

                        val gstval = outstation.categoryGST.toDouble()

                        GstIncludedPrice = GstIncludedPrice + GstIncludedPrice * gstval / 100

                        packagePriceTextView.text =
                            "Rs. " + MyUtils.formatingPriceDisplay("" + GstIncludedPrice)

                    }


                }
                setOnClickListener {
                    if (onItemClickListener != null) {
                        onItemClickListener!!.onItemClick(position, "SelectionView")
                    }
                }

            }
        }


    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, typeofOperation: String)
        fun onClickCell(position: Int)
    }

}
